const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/vte.js',
  output: {
    path: path.resolve(__dirname, 'dist-pack'),
    filename: 'vte.js',
    libraryTarget: 'umd',
    library: 'volcanoApi'
  },
};