"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.EXCEL_FORMAT = void 0;
var _errors = _interopRequireDefault(require("../model/errors"));
var _xlsx = _interopRequireDefault(require("xlsx"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _isFunction2 = _interopRequireDefault(require("lodash/isFunction"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var EXCEL_FORMAT = exports.EXCEL_FORMAT = "excel";

/**
 * Class to manage the data exportation to several formats and from
 * several sources.
 *
 * @author Cayetano H. Osma <chernandez@volcanoteide.com>
 * @version Oct 2020
 *
 * @param {array} header
 * @param {string} type
 * @param {string} filename
 *
 */
var DataExport = /*#__PURE__*/function () {
  /**
   * constructor
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @param {object[]} header     Fields definition.
   * @param {string} type         Output format.
   * @param {string} filename     Filename to be exported.
   */
  function DataExport(header, type, filename) {
    _classCallCheck(this, DataExport);
    /**
     * array containing the allowed types.
     *
     * @type {string[]}
     */
    this.allowedTypes = [EXCEL_FORMAT];
    if (!header.length) {
      throw new _errors["default"].ObjectIsEmpty("header");
    }

    /**
     * Array of object with fields definition.
     *
     * key:       Mandatory field name into data from database.
     * name:      Mandatory column name into the new results.
     * callback:  Optionally a callback to return the complex data or data in a deep level (> 1).
     *
     * @type {object[]}
     */
    this.fields = header;
    if (!type.length && !this.allowedTypes.includes(type)) {
      throw new _errors["default"].ParameterMissing("type");
    }

    /**
     * Which kind of exportation we should do.
     *
     * @type {string}
     * @see Constants at the begin of the file
     */
    this.type = type;
    if (!filename.length) {
      throw new _errors["default"].ParameterMissing("filename");
    }

    /**
     * filename where the data should be persisted.
     *
     * @type {string}
     */
    this.filename = filename;

    /**
     * Resulting object where the data will be stored.
     *
     * @type {object[]}
     */
    this.result = [];
    this.convertedData = [];
    this.workBook = undefined;
  }

  /**
   * Return the filename
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   * 
   * @returns {string}
   */
  return _createClass(DataExport, [{
    key: "getFilename",
    value: function getFilename() {
      return this.filename;
    }

    /**
     * Return the type
     *
     * @author Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version Oct 2020
     *
     * @returns {string}
     */
  }, {
    key: "getType",
    value: function getType() {
      return this.type;
    }

    /**
     * Return the result
     *
     * @author Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version Oct 2020
     *
     * @returns {object}
     */
  }, {
    key: "getResult",
    value: function getResult() {
      return this.result;
    }

    /**
     * Returns an array of objects with the items ready to export.
     *
     * @author Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version Oct 2020
     *
     * @param {object[]} data   Data to be added
     */
  }, {
    key: "addData",
    value: function addData(data) {
      var _this = this;
      data.forEach(function (item) {
        return _this.result.push(_this.fields.reduce(function (rowData, field) {
          return _objectSpread(_objectSpread({}, rowData), {}, _defineProperty({}, field.name, (0, _isFunction2["default"])(field.callback) ? field.callback(item) : (0, _get2["default"])(item, field.key, "")));
        }, {}));
      });
    }

    /**
     * Format the data as parameter type indicate.
     *
     * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
     * @version Oct 2020
     *
     * @params {string} format  Format to be converted.
     */
  }, {
    key: "convertTo",
    value: function convertTo(format) {
      this.format = format || this.getType();
      switch (this.type) {
        case EXCEL_FORMAT:
          this.doExcelExport();
          break;
        default:
          this.doExcelExport();
      }
    }

    /**
     * Do the export to excel format (CSV)
     *
     * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
     * @version Oct 2020
     */
  }, {
    key: "doExcelExport",
    value: function doExcelExport() {
      this.convertToExcel();
      /* create a new blank workbook */
      this.workBook = _xlsx["default"].utils.book_new();
      var workSheet = _xlsx["default"].utils.aoa_to_sheet(this.convertedData, {
        dateNF: "dd/MM/yyyy h:mm"
      });
      _xlsx["default"].utils.sheet_to_csv(workSheet);
      _xlsx["default"].utils.book_append_sheet(this.workBook, workSheet, "Export Data");
    }

    /**
     * Transform the result array to the specific format to be converted.
     * The result will be stored in convertedData array
     *
     * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
     * @version Oct 2020
     *
     */
  }, {
    key: "convertToExcel",
    value: function convertToExcel() {
      var _this2 = this;
      this.convertedData.push(this.fields.filter(function (field) {
        return !field.hasOwnProperty("found") || field.found !== false;
      }).map(function (field) {
        return field.name;
      }));

      // this.convertedData.push(this.fields.map((field) => {
      //   if (!field.hasOwnProperty('found') || field.found !== false) {
      //     return field.name;
      //   }
      // }));
      this.result.forEach(function (data) {
        _this2.convertedData.push(Object.values(data));
      });
    }

    /**
     * Download the file to the browser.
     *
     * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
     * @version Oct 2020
     */
  }, {
    key: "download",
    value: function download() {
      var destFile = this.filename + "_" + Date.now();
      switch (this.getType()) {
        case EXCEL_FORMAT:
          destFile = destFile + ".xlsx";
          break;
        default:
          destFile = destFile + ".csv";
      }
      return _xlsx["default"].writeFile(this.workBook, destFile);
    }
  }]);
}();
var _default = exports["default"] = {
  EXCEL_FORMAT: EXCEL_FORMAT,
  DataExport: DataExport
};