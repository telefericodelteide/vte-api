"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));
var _entity = _interopRequireDefault(require("./entity/entity"));
var _constants = _interopRequireDefault(require("../constants"));
var _lodash = require("lodash");
var _product = _interopRequireDefault(require("./entity/catalog/product"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_SIGNUP_INTERMEDIARY = 'intermediaries/signup';
var ENDPOINT_GET_INTERMEDIARIES = 'crm/intermediaries';
var ENDPOINT_GET_INTERMEDIARY = ENDPOINT_GET_INTERMEDIARIES + '/:id';
var ENDPOINT_GET_INTERMEDIARY_PDF = ENDPOINT_GET_INTERMEDIARY + '/pdf';
var ENDPOINT_GET_INTERMEDIARY_PRODUCTS = ENDPOINT_GET_INTERMEDIARY + '/products';
var ENDPOINT_GET_INTERMEDIARY_PRODUCT = ENDPOINT_GET_INTERMEDIARY_PRODUCTS + '/:product_id';
var ENDPOINT_GET_OFFICES = 'crm/offices';
var ENDPOINT_GET_OFFICE = ENDPOINT_GET_OFFICES + '/:id';
var ENDPOINT_GET_INTERMEDIARY_OFFICES = ENDPOINT_GET_INTERMEDIARY + '/offices';
var ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK = ENDPOINT_GET_INTERMEDIARY + '/payments/one-click';
var ENDPOINT_GET_ENTITY_ACTION_LOGS = ENDPOINT_GET_INTERMEDIARY + '/entity-action-logs';
var _default = exports["default"] = IntermediaryClient;
/**
 * Container which has the intermediary and offices methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function IntermediaryClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of intermediaries that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getIntermediaries = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_INTERMEDIARIES, params);
  };

  /**
   * Returns the intermediary that matches the id passed.
   * 
   * @param {string} intermediaryId 
   * @returns {Entity}
   */
  this.getIntermediary = function (intermediaryId) {
    var endpoint = 'crm/intermediaries/:id';
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_INTERMEDIARY, intermediaryId);
  };
  this.signup = function (data) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_SIGNUP_INTERMEDIARY, {
      method: "POST",
      isAdmin: false,
      data: data
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return new _entity["default"](res);
    });
  };

  /**
   * Changes the state of the intermediary that matches the id passed.
   * 
   * @param {string} intermediaryId 
   * @param {string} state 
   * @returns 
   */
  this.setIntermediaryState = function (intermediaryId, state) {
    if ((0, _isEmpty["default"])(intermediaryId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_INTERMEDIARY + '/change-state', intermediaryId, _constants["default"].HTTP_POST, {
      state: state
    }, {
      path: {
        id: intermediaryId
      },
      isAdmin: true
    });
  };

  /**
   * Creates a new intermediary.
   * 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.createIntermediary = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_INTERMEDIARIES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null, 'intermediary');
  };

  /**
   * Updates the intermediary that matches the id passed.
   * 
   * @param {string} intermediaryId 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.editIntermediary = function (intermediaryId, data) {
    if ((0, _isEmpty["default"])(intermediaryId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_INTERMEDIARY, intermediaryId, _constants["default"].HTTP_PATCH, data, {
      path: {
        id: intermediaryId
      },
      isAdmin: true
    });
  };

  /** Intermediary offices management */

  /**
   * Returns the collection of offices that matches the query parameters passed.
   * 
   * @param {string|null} intermediaryId 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getIntermediaryOffices = function (intermediaryId, params) {
    if (!intermediaryId) {
      return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_OFFICES, params);
    } else {
      return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_INTERMEDIARY_OFFICES, params, {
        parameters: {
          id: intermediaryId
        }
      });
    }
  };

  /**
   * Returns the office that matches the id passed.
   * 
   * @param {string} officeId 
   * @returns {Entity}
   */
  this.getIntermediaryOffice = function (officeId) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_OFFICE, officeId);
  };

  /**
   * Creates a new intermediary office.
   * 
   * @param {string} intermediaryId 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.createIntermediaryOffice = function (intermediaryId, data) {
    if ((0, _isEmpty["default"])(intermediaryId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_INTERMEDIARY_OFFICES, intermediaryId, _constants["default"].HTTP_POST, data, {
      path: {
        id: intermediaryId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Updates the office that matches the id passed.
   * 
   * @param {string} officeId 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.editIntermediaryOffice = function (officeId, data) {
    if ((0, _isEmpty["default"])(officeId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_OFFICE, officeId, _constants["default"].HTTP_PATCH, data, {
      path: {
        id: officeId
      },
      isAdmin: true
    });
  };

  /**
   * Enables or disables the office that matches the id passed.
   * 
   * @param {string} officeId 
   * @param {boolean} isActive 
   * @returns 
   */
  this.setIntermediaryOfficeState = function (officeId, isActive) {
    if ((0, _isEmpty["default"])(officeId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_OFFICE + '/set-active', officeId, _constants["default"].HTTP_POST, {
      active: isActive
    }, {
      path: {
        id: officeId
      },
      isAdmin: true
    });
  };

  /**
   * Requests a payment token for the intermediary that matches the id passed.
   *  
   * @param {string} intermediaryId 
   * @returns {object} payment data
   */
  this.requestPaymentToken = function (intermediaryId) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK, _constants["default"].HTTP_POST, null, {
      isAdmin: true,
      path: {
        id: intermediaryId
      }
    }, null);
  };

  /**
   * Deletes the payment token for the intermediary that matches the id passed. 
   * @param {string} intermediaryId 
   * @returns 
   */
  this.deletePaymentToken = function (intermediaryId) {
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK, intermediaryId, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: intermediaryId
      },
      isAdmin: true
    });
  };

  /** 
   * Returns a collection with all products of an intermediary with pagination.
   * 
   * @param {string} id intermediary identifier
   * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
   * if you want to move to specific page, you must add 'page: 3' to the params object.
   * 
   * @returns {Collection} 
   */
  this.getIntermediaryProducts = function (id, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_INTERMEDIARY_PRODUCTS, params, {
      parameters: {
        id: id
      }
    }, _product["default"]);
  };

  /** 
   * Add and/or delete a collection of products from an intermediary.
   * 
   * @param {string} id intermediary identifier
   * @param {array} data 
   * 
   * @returns {Collection} 
   */
  this.editIntermediaryProducts = function (id, data) {
    return _this.volcanoClient.addEntityRequest(ENDPOINT_GET_INTERMEDIARY_PRODUCTS, data, true, {
      path: {
        id: id
      }
    });
  };

  /** 
   * Edit the configuration of a product in an intermediary.
   * 
   * @param {string} id intermediary identifier
   * @param {int} productId product identifier
   * @param {array} data 
   * 
   * @returns {Entity} 
   */
  this.editIntermediaryProduct = function (id, productId, data) {
    return _this.volcanoClient.editEntityRequest(ENDPOINT_GET_INTERMEDIARY_PRODUCT, id, data, true, {
      path: {
        id: id,
        product_id: productId
      }
    });
  };

  /**
   * Deletes a product from an intermediary.
   * @param {*} id intermediary identifier
   * @param {*} productId product identifier
   * @returns 
   */
  this.deleteIntermediaryProduct = function (id, productId) {
    return _this.volcanoClient.deleteResourceRequest(ENDPOINT_GET_INTERMEDIARY_PRODUCT, id, true, {
      path: {
        product_id: productId
      }
    });
  };
  this.exportIntermediaries = function (fields, params, progressHandler) {
    return _this.volcanoClient.makeCollectionExportRequest(ENDPOINT_GET_INTERMEDIARIES, params, null, fields, 'intermediaries', progressHandler);
  };

  /**
   * Returns the collection of entity action logs for this intermediary
   * 
   * @param {string|null} intermediaryId 
   * @returns {Collection} 
   */
  this.getEntityActionLogs = function (intermediaryId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ENTITY_ACTION_LOGS, params, {
      parameters: {
        id: intermediaryId
      }
    });
  };
}