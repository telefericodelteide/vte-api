"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _lodash = require("lodash");
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
//import Rol from "./entity/rol";
var _default = exports["default"] = RoleClient;
var ROL_GET_ROLES = 'auth/roles';
var ROL_GET_ROL_BY_ID = 'auth/roles/:id';
var ROL_GET_ROL_PERMISSIONS = 'auth/roles/:id/permissions';
var ROL_ADD = 'auth/roles/add';
var ROL_EDIT = 'auth/roles/:id/edit';
var prepareRoleData = function prepareRoleData(data) {
  console.log("6");
  console.log(data);
  console.log("7");
  console.log(Boolean(data.is_system));
  console.log("8vv");
  if (data.is_system !== undefined) {
    data = _objectSpread(_objectSpread({}, data), {}, {
      is_system: Boolean(data.is_system)
    });
  }
  if (data.corporate_account_available !== undefined) {
    data = _objectSpread(_objectSpread({}, data), {}, {
      corporate_account_available: Boolean(data.corporate_account_available)
    });
  }
  console.log(data);
  console.log("9");
  return data;
};

/**
 * Uses to access Volcano API role endpoints.
 * 
 * @constructor RoleClient
 * @param {VolcanoClient} volcanoClient 
 */
function RoleClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.getPermissions = function (roleId) {
    return _this.volcanoClient.makeCollectionRequest(ROL_GET_ROL_PERMISSIONS, roleId);
  };
  this.getRoles = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ROL_GET_ROLES, params);
  };
  this.getRole = function (roleId) {
    return _this.volcanoClient.getEntityRequest(ROL_GET_ROL_BY_ID, roleId);
  };

  /**
       * Create rol
       *
       * @param { object } data   This should have the following format.
       *                          {
       *                             "name": "rol_name",
       *                             "slug": "slug",
       *                             "is_system": true,
       *                             "corporate_account_available": false,
       *                             "permissions": [
       *                                 {"id": 1}
       *                             ],
       *                         }
       *
       * @return {Entity}
       */
  this.createRol = function (data) {
    data = prepareRoleData(data);
    return _this.volcanoClient.makeResourceRequest(ROL_ADD, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null, 'role');
  };

  /**
   * Edit rol
   *
   * @param {string}  rolId  Role identifier
   * @param { object } data   This should have the following format.
   *                          {
   *                             "name": "rol_name",
   *                             "slug": "slug",
   *                             "is_system": true,
   *                             "corporate_account_available": true,
   *                             "permissions": [
   *                                 {"id": 1}
   *                             ],
   *                        }
   * @return {Entity}
   */
  this.editRol = function (rolId, data) {
    if ((0, _lodash.isEmpty)(rolId)) {
      return null;
    }
    data = prepareRoleData(data);
    console.log("a!");
    console.log(data);
    console.log("b");
    return _this.volcanoClient.makeEntityRequest(ROL_EDIT, rolId, _constants["default"].HTTP_POST, data, {
      path: {
        id: rolId
      },
      isAdmin: true
    }, null, 'role');
  };
}