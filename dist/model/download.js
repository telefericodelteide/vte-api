"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _vte = _interopRequireDefault(require("../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_DOWNLOADS = "dl";
var ENDPOINT_DOWNLOADS_CREATE = ENDPOINT_DOWNLOADS + "/create";

/**
 * Download resources client.
 *
 * @param {VolcanoApi} volcanoClient
 */
function DownloadClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;
  this.createLink = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_DOWNLOADS_CREATE, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };
}
var _default = exports["default"] = DownloadClient;