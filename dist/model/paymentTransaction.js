"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../vte"));
var _errors = require("./errors");
var _get2 = _interopRequireDefault(require("lodash/get"));
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports["default"] = PaymentTransactionClient; // Endpoints
var TRANSACTION_RESULT = "payment-transactions/:id/client-result";
var TRANSACTION_GET_TRANSACTIONS = 'payment-transactions';
var TRANSACTION_GET_TRANSACTION_BY_ID = TRANSACTION_GET_TRANSACTIONS + '/:id';

/**
 * Container which has the payment transactions methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function PaymentTransactionClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of payment transactions that matches the query parameters.
   *
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection}
   */
  this.getTransactions = function (params) {
    params = _objectSpread(_objectSpread({}, params), {}, {
      isAdmin: true
    });
    return _this.volcanoClient.makeCollectionRequest(TRANSACTION_GET_TRANSACTIONS, params);
  };

  /**
     * Return an Entity containing a transaction which match by Id `transactionId`.
     *
     * @param {integer} [transactionId]  transaction ID.
     *
     * @returns {Entity}
     */
  this.getTransaction = function (transactionId, params) {
    return _this.volcanoClient.getEntityRequest(TRANSACTION_GET_TRANSACTION_BY_ID, transactionId, true, params);
  };
  this.processTransactionResult = function (params, sessionId) {
    //todo: check payment gateway

    sessionId = sessionId || _this.volcanoClient.cart.getSessionId();
    if (!sessionId) {
      throw new _errors.InvalidSessionIdException();
    }
    var transactionId = (0, _get2["default"])(params, "vte_transaction_id", null);
    if (!transactionId) {
      throw new _errors.InvalidTransactionIdException();
    }
    delete params.vte_transaction_id;
    return _this.getBookingsFromTransaction(transactionId, sessionId, params).then(function (bookings) {
      _this.volcanoClient.cart.resetLocalCart();
      return bookings;
    });
  };
  this.getBookingsFromTransaction = function (transactionId, sessionId, params) {
    var request = _this.volcanoClient.prepareRequest(TRANSACTION_RESULT, {
      method: "GET",
      isAdmin: false,
      parameters: {
        id: transactionId
      },
      query: _objectSpread({
        sid: sessionId || _this.volcanoClient.cart.getSessionId()
      }, params)
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.bookings;
    });
  };
}