"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ORDER_CHANGE_CUSTOMER = void 0;
var _vte = _interopRequireDefault(require("../vte"));
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = OrderClient;
var ENDPOINT_GET_ORDERS = 'orders';
var ENDPOINT_GET_ORDER = ENDPOINT_GET_ORDERS + '/:id';
var ENDPOINT_ORDER_CHANGE_CUSTOMER = exports.ORDER_CHANGE_CUSTOMER = ENDPOINT_GET_ORDER + '/customers/:customer_id';
var ENDPOINT_ORDER_CHANGE_INTERMEDIARY = ENDPOINT_GET_ORDER + '/collaborator-change';
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function OrderClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Let to change the customer in the booking record.
   *
   * @param {string | number} orderId   order ID
   * @param {string | number} customerId   customer ID
   * @param {object} data Customer Data to be updated.
   *                      The data should be in the following format
   *
   *                      {
   *                          "comments": "",
   *                          "email": "trash@volcanoteide.com",
   *                          "language_code3": "fra",
   *                          "name": "Juan",
   *                          "phone": "666666666"
   *                          "surename": "Perez"
   *                      }
   *
   *                      And it should contains at leastone of the below items.
   *
   *                      comments:           Customer Comment
   *                      email:              Customer email address
   *                      language_code3":    Customer language
   *                      name:               Customer Name
   *                      phone:              Customer phone number
   *                      surname:            Customer surname
   *
   * @returns {Entity}
   */
  this.changeOrderCustomer = function (orderId, customerId, data) {
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_ORDER_CHANGE_CUSTOMER, orderId, _constants["default"].HTTP_POST, data, {
      path: {
        id: orderId,
        customer_id: customerId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Updates the intermediary of the order.
   * 
   * @param {number} orderId order id
   * @param {object} data update data
   * @returns 
   */
  this.setIntermediary = function (orderId, data) {
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_ORDER_CHANGE_INTERMEDIARY, orderId, _constants["default"].HTTP_POST, data, {
      path: {
        id: orderId
      },
      isAdmin: true
    }, null);
  };
}