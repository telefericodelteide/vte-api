"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_BILLING_INFORMATIONS = "billing-informations";
var ENDPOINT_GET_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id";
var ENDPOINT_CREATE_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS;
var ENDPOINT_CREATE_BILLING_INFORMATION_INVOICE = ENDPOINT_GET_BILLING_INFORMATION + '/invoices';
var ENDPOINT_EDIT_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id";
var ENDPOINT_DELETE_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id";
var _default = exports["default"] = BillingInformationClient;
/**
 * Uses to access Volcano API billing information endpoints.
 *
 * @constructor BillingInformationClient
 * @param {VolcanoClient} volcanoClient
 */
function BillingInformationClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of billing information hat matches the query parameters passed.
   *
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection}
   */
  this.getBillingInformations = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_BILLING_INFORMATIONS, params);
  };
  this.getBillingInformation = function (billingInformationId) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_BILLING_INFORMATION, billingInformationId);
  };
  this.exportBillingInformations = function (fields, params, progressHandler) {
    return _this.volcanoClient.makeCollectionExportRequest(ENDPOINT_GET_BILLING_INFORMATIONS, params, null, fields, "billing-informations", progressHandler);
  };

  /**
   * Make a request to create a customer or collaborator billing information
   *
   * @param { object } data   This should have the following format.
   *
   * @return {Entity}
   */
  this.createBillingInformation = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_CREATE_BILLING_INFORMATION, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };

  /**
   * Make a request to update an billing information
   *
   * @param {string}  billingInformationId id of the billing information to be updated
   * @param { object } data   This should have the following format.
   *
   * @return {Entity}
   */
  this.editBillingInformation = function (billingInformationId, data) {
    if (!billingInformationId) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_EDIT_BILLING_INFORMATION, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: billingInformationId
      }
    }, null);
  };

  /**
  * Delete billing information
  *
  * @param {string}  billingInformationId  Billing information identifier
  * 
  * @return {Entity}
  */
  this.deleteBillingInformation = function (billingInformationId) {
    if (!billingInformationId) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_DELETE_BILLING_INFORMATION, billingInformationId, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true
    }, null);
  };
}