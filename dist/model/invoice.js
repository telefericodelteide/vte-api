"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _lodash = require("lodash");
var _entity = _interopRequireDefault(require("./entity/entity"));
var _dateFns = require("date-fns");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
// Endpoints
var ENDPOINT_GET_INVOICES = 'invoices';
var ENDPOINT_GET_INVOICE = ENDPOINT_GET_INVOICES + '/:id';
var ENDPOINT_GET_INVOICE_PDF = ENDPOINT_GET_INVOICE + '/pdf';
var ENDPOINT_CREATE_INVOICE = ENDPOINT_GET_INVOICES;
var ENDPOINT_AMEND_INVOICE = ENDPOINT_GET_INVOICE + '/amend';
var ENDPOINT_GET_ACCOUNTANCY_CONFIGS = "accountancy-configs";
var _default = exports["default"] = InvoiceClient;
/**
 * Uses to access Volcano API invoice endpoints.
 *
 * @constructor InvoiceClient
 * @param {VolcanoClient} volcanoClient
 */
function InvoiceClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of invoices that matches the query parameters passed.
   *
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection}
   */
  this.getInvoices = function (params) {
    var endpoint = ENDPOINT_GET_INVOICES;
    if ((0, _dateFns.isDate)(params.date_from)) {
      params.date_from = (0, _dateFns.format)(params.date_from, "yyyy-MM-dd");
    }
    return _this.volcanoClient.makeCollectionRequest(endpoint, params);
  };
  this.getInvoice = function (invoiceId) {
    var endpoint = ENDPOINT_GET_INVOICE;
    return _this.volcanoClient.getEntityRequest(endpoint, invoiceId);
  };
  this.exportInvoices = function (fields, params, progressHandler) {
    var endpoint = ENDPOINT_GET_INVOICES;
    return _this.volcanoClient.makeCollectionExportRequest(endpoint, params, null, fields, "invoices", progressHandler);
  };

  /**
   * Return an application/PDF octet
   *
   * @author Roberto Muñoz <rmunglez@gmail.com>
   * @version Nov 2020
   *
   * @param invoicegId
   *
   * @returns {null|Object|Entity}
   */
  this.getInvoicePdf = function (invoiceId, params) {
    var id = parseInt(invoiceId);
    if (isNaN(invoiceId)) {
      return null;
    }
    params = (0, _lodash.isObject)(params) ? params : {};
    return _this.volcanoClient.makeFileRequest(ENDPOINT_GET_INVOICE_PDF, id, true, params);
  };

  /**
   * Make a request to create a customer or collaborator invoice
   *
   * @param {numeric | string}  orderId     Id of the booking which want to be cancelled.
   *
   * @return {Entity}
   */
  this.createInvoice = function (orderId, data) {
    var id = parseInt(orderId);
    if (isNaN(orderId)) {
      return null;
    }
    data = _objectSpread(_objectSpread({}, data), {}, {
      allow_repeated: true
    });
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_CREATE_INVOICE, _constants["default"].HTTP_POST, data, {
      isAdmin: true,
      query: {
        order_id: id
      }
    }, null);
  };

  /**
  * Make a request to create a customer or collaborator invoice
  *
  * @param {array}  bookings Bookings to generate invoice
  *
  * @return {Entity}
  */
  this.createBookingsInvoice = function (bookings, data) {
    data = _objectSpread(_objectSpread({}, data), {}, {
      bookings: bookings
    });
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_CREATE_INVOICE, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };

  /**
   * Make a request to update an invoice
   *
   * @param {numeric}  invoiceId id of the invoice to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editInvoice = function (invoiceId, data) {
    var id = parseInt(invoiceId);
    if (isNaN(invoiceId)) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_INVOICE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Make a request to update an invoice
   *
   * @param {numeric}  invoiceId     id of the invoice to be updated
   * @param {string}   amendmentType amendment type (full, partial)
   *
   * @return {Entity}
   */
  this.amendInvoice = function (invoiceId, amendmentType) {
    var id = parseInt(invoiceId);
    if (isNaN(invoiceId)) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_AMEND_INVOICE, _constants["default"].HTTP_POST, {
      amendment_type: amendmentType
    }, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Return a collection with all accountancy configs with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getAccountancyConfigs = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ACCOUNTANCY_CONFIGS, params);
  };
}