"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _dateFns = require("date-fns");
var _isObject = _interopRequireDefault(require("lodash/isObject"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
/**
 * Volcano API experience client
 *
 * The ExperienceClient class wraps the experience content resource and booking process endpoints of the Volcano REST API.
 *
 * @file   This files defines the ExperienceClient class.
 * @author Roberto Muñoz <rmunglez@gmail.com>
 * @author Cayetano H. Osma <chernandez@volcanoteide.com>
 * @version Ago 2020
 *
 * @since  0.1
 */
var _default = exports["default"] = ExperienceClient; // Constants for the endpoints
var GET_EXPERIENCES = 'experiences';
var GET_PRODUCTS = 'experiences/:id/products';
var GET_PRODUCT_AVAILABILITY = 'products/:id/availability/:date';
var GET_PRODUCT_RATES = 'products/:id/rates/:date';

/**
 * Uses to access Volcano API experience content endpoints.
 *
 * @constructor ExperienceClient
 * @param {VolcanoClient} volcanoClient
 */
function ExperienceClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Retrieve all available experiences from API
   *
   * @param {Object} params
   * @returns {Collection}
   */
  this.getExperiences = function (params) {
    params = params || {};
    return _this.volcanoClient.makeCollectionRequest(GET_EXPERIENCES, params);
  };

  /**
   * Retrieve all available products of an experiences from API
   * 
   * @param {number} experienceId
   *
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProducts = function (experienceId) {
    var request = _this.volcanoClient.prepareRequest(GET_PRODUCTS, {
      method: 'GET',
      isAdmin: false,
      parameters: {
        id: experienceId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };

  /**
   * Return the experience for ID given.
   *
   * @param {number} experienceId
   *
   * @returns {PromiseLike<Entity> | Promise<Entity>}
   */
  this.getExperience = function (experienceId) {
    return _this.volcanoClient.content.getExperience(experienceId);
  };

  /**
   * Returns the availability for a product.
   *
   * @param {number} productId
   * @param {Date} date
   * @param {boolean} fullMonth true if the availability for the entire month of the date should be retrieved
   * @param {params} params
   *
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductAvailability = function (productId, date, fullMonth, params) {
    if (!date) {
      date = new Date();
    }
    params = (0, _isObject["default"])(params) ? params : {};
    var request = _this.volcanoClient.prepareRequest(GET_PRODUCT_AVAILABILITY, {
      method: 'GET',
      isAdmin: false,
      parameters: {
        id: productId,
        date: fullMonth ? (0, _dateFns.format)(date, 'yyyy-MM') : (0, _dateFns.format)(date, 'yyyy-MM-dd')
      },
      query: _objectSpread({}, params)
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };

  /**
   * Returns the availability for a product.
   *
   * @param {number} productId
   * @param {Date} date
   * @param {number} months number of months to query
   * @param {params} params
   **
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductAvailabilityRange = function (productId, date, months, params) {
    if (!date) {
      date = new Date();
    }
    if (!months) {
      months = 1;
    }
    params = (0, _isObject["default"])(params) ? params : {};
    var requests = new Array(months);
    for (var i = 0; i < months; i++) {
      var request = _this.volcanoClient.prepareRequest(GET_PRODUCT_AVAILABILITY, {
        method: 'GET',
        isAdmin: false,
        parameters: {
          id: productId,
          date: (0, _dateFns.format)(date, 'yyyy-MM')
        },
        query: _objectSpread({}, params)
      });
      date = (0, _dateFns.addMonths)(date, 1);
      requests[i] = _this.volcanoClient.makeRequest(request).then(function (res) {
        return res[Object.keys(res)[0]];
      });
    }
    return Promise.all(requests).then(function (values) {
      return values.flat();
    });
  };

  /**
   * Returns the rates for a product in a specific date
   * 
   * @param {number} productId
   * @param {Date} date
   *
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductRates = function (productId, date) {
    var collaboratorId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var params = collaboratorId == null ? {} : {
      collaborator_id: collaboratorId
    };
    if (!date) {
      date = new Date();
    }
    var request = _this.volcanoClient.prepareRequest(GET_PRODUCT_RATES, {
      method: 'GET',
      isAdmin: false,
      parameters: {
        id: productId,
        date: (0, _dateFns.format)(date, 'yyyy-MM-dd')
      },
      query: params
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };
}