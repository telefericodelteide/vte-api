"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.USER_TYPE_SITE = exports.USER_TYPE_APPLICATION = exports.USER_TYPE_API = exports.USER_SET_ACTIVE = exports.USER_GET_USER_PERMISSIONS = exports.USER_GET_USER_BY_ID = exports.USER_GET_USERS = exports.USER_GET_ME = exports.USER_EDIT = exports.USER_DELETE = exports.USER_ADD = void 0;
var _lodash = require("lodash");
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
var _user = _interopRequireDefault(require("./entity/user"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _slicedToArray(r, e) { return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray(r, e) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _iterableToArrayLimit(r, l) { var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (null != t) { var e, n, i, u, a = [], f = !0, o = !1; try { if (i = (t = t.call(r)).next, 0 === l) { if (Object(t) !== t) return; f = !1; } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0); } catch (r) { o = !0, n = r; } finally { try { if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return; } finally { if (o) throw n; } } return a; } }
function _arrayWithHoles(r) { if (Array.isArray(r)) return r; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports["default"] = UserClient;
var API_ADMIN_USERS = "users";

// TYPES
var USER_TYPE_APPLICATION = exports.USER_TYPE_APPLICATION = 'application';
var USER_TYPE_SITE = exports.USER_TYPE_SITE = 'site';
var USER_TYPE_API = exports.USER_TYPE_API = 'api';
//

// Endpoints
var USER_GET_ME = exports.USER_GET_ME = 'auth/me';
var USER_GET_USERS = exports.USER_GET_USERS = 'auth/:type-users';
var USER_GET_USER_BY_ID = exports.USER_GET_USER_BY_ID = 'auth/:type-users/:id';
var USER_GET_USER_PERMISSIONS = exports.USER_GET_USER_PERMISSIONS = 'auth/:type-users/:id/permissions';
var USER_ADD = exports.USER_ADD = 'auth/:type-users';
var USER_EDIT = exports.USER_EDIT = 'auth/:type-users/:id/edit';
var USER_SET_ACTIVE = exports.USER_SET_ACTIVE = 'auth/:type-users/:id/set-active';
var USER_DELETE = exports.USER_DELETE = 'auth/:type-users/:id';
var prepareAppUserDataToSave = function prepareAppUserDataToSave(data) {
  data = _objectSpread(_objectSpread({}, data), {}, {
    name: data.first_name,
    surname: data.last_name
  });
  if (data.active !== undefined) {
    data = _objectSpread(_objectSpread({}, data), {}, {
      active: JSON.parse(data.active)
    });
  }
  delete data.first_name;
  delete data.last_name;
  return data;
};

/**
 * Uses to access Volcano API user endpoints.
 *
 * @constructor UserClient
 * @param {VolcanoClient} volcanoClient
 */
function UserClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.getProfile = function (userId) {
    var params = userId ? {} : {
      enterprise_id: null
    };
    if (!userId) {
      var payload = _this.volcanoClient.getPayload();
      userId = payload.sub;
    }

    // load the user and their permissions
    return Promise.all([_this.getApplicationUser(userId, params), _this.getUserPermissions(userId, params)]).then(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
        user = _ref2[0],
        permissions = _ref2[1];
      user.permissions = permissions;
      return user;
    });
  };
  this.getUserPermissions = function (userId, params) {
    var request = _this.volcanoClient.prepareRequest(USER_GET_USER_PERMISSIONS, {
      method: "GET",
      isAdmin: true,
      parameters: {
        id: userId,
        type: USER_TYPE_APPLICATION
      },
      query: params
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };
  this.getApplicationUsers = function (params) {
    return _this.volcanoClient.makeCollectionRequest(USER_GET_USERS, params, {
      parameters: {
        type: USER_TYPE_APPLICATION
      }
    });
  };
  this.getUser = function (endpoint, userId, params) {
    return _this.volcanoClient.getEntityRequest(endpoint, userId, true, params).then(function (res) {
      return new _user["default"](res);
    });
  };
  this.me = function () {
    var endpoint = USER_GET_ME;
    return _this.getUser(endpoint, null);
  };
  this.getApplicationUser = function (userId, params) {
    var endpoint = (0, _lodash.replace)(USER_GET_USER_BY_ID, ":type", USER_TYPE_APPLICATION);
    return _this.getUser(endpoint, userId, params);
  };
  this.getApiUser = function (userId) {
    var endpoint = (0, _lodash.replace)(USER_GET_USER_BY_ID, ':type', USER_TYPE_API);
    return _this.getUser(endpoint, userId);
  };
  this.getSiteUser = function (userId) {
    if (!userId) {
      // load all sites and get first
      var _endpoint = (0, _lodash.replace)(USER_GET_USERS, ':type', USER_TYPE_SITE);
      return _this.get;
    }
    var endpoint = (0, _lodash.replace)(USER_GET_USER_BY_ID, ':type', USER_TYPE_SITE);
    return _this.getUser(endpoint, userId);
  };
  this.exportApplicationUsers = function (fields, params, progressHandler) {
    var endpoint = (0, _lodash.replace)(USER_GET_USERS, ':type', USER_TYPE_APPLICATION);
    var user_application_type = "" + params['is_admin'];
    params = _objectSpread(_objectSpread({}, params), {}, {
      application_type: user_application_type === '' ? 'all' : user_application_type === "0" ? 'external' : 'internal'
    });
    return _this.volcanoClient.makeCollectionExportRequest(endpoint, params, null, fields, "users", progressHandler);
  };

  /**
     * Create user
     *
     * @param { object } data   This should have the following format.
     *                          {
     *                             "username": "username",
     *                             "password": "test",
     *                             "password_repeat": "test",
     *                             "name": "Name",
     *                             "surname": "",
     *                             "email": "test@domain.com",
     *                             "phone": "922333333",
     *                             "active": 1,
     *                             "collaborator_id": "393637",
     *                             "salesman": {
     *                                 "office_id": "",
     *                                 "id_card": "00000000T"
     *                             },
     *                             "roles": [
     *                                 {"id": 1}
     *                             ],
     *                             "collaborator_register": false
     *                         }
     * @param {string}  userType  User type (application, site or api)
     *
     * @return {Entity}
     */
  this.createUser = function (data, userType) {
    userType = userType == undefined ? USER_TYPE_APPLICATION : userType;
    if (userType == USER_TYPE_APPLICATION) {
      data = prepareAppUserDataToSave(data);
    }
    return _this.processUserRequest(function () {
      return _this.volcanoClient.makeResourceRequest(USER_ADD, _constants["default"].HTTP_POST, data, {
        path: {
          type: userType
        },
        isAdmin: true
      }, null, 'user');
    });
  };

  /**
     * Edit user
     *
     * @param {string}  userId  User identifier
     * @param { object } data   This should have the following format.
     *                          {
     *                            "username": "Mamen",
     *                            "name": "Carmen",
     *                            "surname": "test",
     *                            "email": "reservas@gaea-travel.com",
     *                            "phone": "",
     *                            "active": 1,
     *                            "corporate_account": {
     *                                "id": "b2fa82d7-28e0-11eb-80ba-0cc47ac3a6b8"
     *                            },
     *                            "collaborator_id": "393637",
     *                            "salesman": {
     *                                "office_id": "",
     *                                "id_card": "00000000T"
     *                            },
     *                            "roles": [
     *                                {"id": 1}
     *                            ],
     *                            "languages": [
     *                                {"id": "3"},
     *                                {"id": "4"}
     *                            ]
     *                        }
     * @param {string}  userType  User type (application, site or api)
     *
     * @return {Entity}
     */
  this.editUser = function (userId, data, userType) {
    if ((0, _lodash.isEmpty)(userId)) {
      return null;
    }
    if (userType == USER_TYPE_APPLICATION) {
      data = prepareAppUserDataToSave(data);
    }
    userType = userType == undefined ? USER_TYPE_APPLICATION : userType;
    return _this.processUserRequest(function () {
      return _this.volcanoClient.makeEntityRequest(USER_EDIT, userId, _constants["default"].HTTP_POST, data, {
        path: {
          id: userId,
          type: userType
        },
        isAdmin: true
      }, null, 'user');
    });
  };

  /**
     * Set user active / unactive
     *
     * @param {string}  userId  User identifier
     * @param {boolean} active  True to activate user, false otherwise
     * @param {string}  userType  User type (application, site or api)
     *
     * @return {Entity}
     */
  this.setUserActive = function (userId, active, userType) {
    if ((0, _lodash.isEmpty)(userId)) {
      return null;
    }
    userType = userType == undefined ? USER_TYPE_APPLICATION : userType;
    var data = {
      active: active ? 1 : 0
    };
    return _this.volcanoClient.makeEntityRequest(USER_SET_ACTIVE, userId, _constants["default"].HTTP_POST, data, {
      path: {
        id: userId,
        type: userType
      },
      isAdmin: true
    }, null, 'user');
  };

  /**
     * Delete user
     *
     * @param {string}  userId  User identifier
     * @param {string}  userType  User type (application, site or api)
     *
     * @return {Entity}
     */
  this.deleteUser = function (userId, userType) {
    if ((0, _lodash.isEmpty)(userId)) {
      return null;
    }
    userType = userType == undefined ? USER_TYPE_APPLICATION : userType;
    return _this.volcanoClient.makeEntityRequest(USER_DELETE, userId, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: userId,
        type: userType
      },
      isAdmin: true
    }, null, 'user');
  };

  /**
     * Change application user password
     *
     * @param {string}  userId  User identifier
     * @param { object } data   This should have the following format.
     *                          {
     *                            "password": "test",
     *                            "password_repeat": "test"
     *                          }
     *
     * @return {Entity}
     */
  this.appUserChangePassword = function (userId, data) {
    if ((0, _lodash.isEmpty)(userId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(USER_EDIT, userId, _constants["default"].HTTP_POST, data, {
      path: {
        id: userId,
        type: USER_TYPE_APPLICATION
      },
      isAdmin: true
    }, null, 'user');
  };
  this.processUserRequest = function (request) {
    return request().then(function (res) {
      return res;
    })["catch"](function (error) {
      var _error$data;
      if ((_error$data = error.data) !== null && _error$data !== void 0 && _error$data.application_user) {
        var _errorData$username, _errorData$email;
        var errorData = error.data.application_user;
        if (errorData !== null && errorData !== void 0 && (_errorData$username = errorData.username) !== null && _errorData$username !== void 0 && _errorData$username.unique) {
          error.message = "Username already in use";
        } else if (errorData !== null && errorData !== void 0 && (_errorData$email = errorData.email) !== null && _errorData$email !== void 0 && _errorData$email.unique) {
          error.message = "Email already in use";
        }
      }
      throw error;
    });
  };
}