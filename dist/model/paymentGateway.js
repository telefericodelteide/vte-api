"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = exports["default"] = PaymentGatewayClient;
/**
 * Uses to access Volcano API payment gateways endpoints.
 * 
 * @constructor PaymentGatewayClient
 * @param {VolcanoClient} volcanoClient 
 */
function PaymentGatewayClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.getPaymentGateways = function (params) {
    var endpoint = 'payment-gateways';
    return _this.volcanoClient.makeCollectionRequest(endpoint, params);
  };
  this.getPaymentGateway = function (paymentGatewayId) {
    var endpoint = 'payment-gateways/:id';
    return _this.volcanoClient.getEntityRequest(endpoint, paymentGatewayId);
  };
}