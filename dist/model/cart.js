"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _uuid = require("uuid");
var _functions = require("../functions");
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
/**
 * Volcano API cart client
 *
 * The CartClient class has all the required functionalities to manage a shopping cart with the Volcano REST API.
 *
 * @file   This files defines the CartClient class.
 * @author Roberto Muñoz (rmunglez@gmail.com)
 * @since  0.1
 */
var _default = exports["default"] = CartClient;
/**
 * Uses to access Volcano API Cart endpoints.
 * 
 * @constructor CartClient
 * @param {VolcanoClient} volcanoClient 
 */
function CartClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.baseEndpoint = 'carts';
  this.makeParams = function (extra) {
    extra = extra || {};
    var cartId = _this.getCartId();
    var params = {
      path: _objectSpread({}, extra),
      query: {
        sid: _this.getSessionId()
      }
    };
    if (cartId) {
      params.path.id = cartId;
    }
    return params;
  };

  /**
   * Sets the cart uuid in local storage
   * @param {*} cartId uuid of the cart
   */
  this.setCartId = function (cartId) {
    (0, _functions.setCookie)("vle_cart_id", cartId, 30);
  };

  /**
   * Returns the cart uuid from local storage
   * @returns {*} cart identifier
   */
  this.getCartId = function () {
    return (0, _functions.getCookie)("vle_cart_id");
  };

  /**
   * Sets the client session uuid in local storate.
   * @param {*} sessionId  uuid of the local session
   */
  this.setSessionId = function (sessionId) {
    (0, _functions.setCookie)("vle_cart_session_id", sessionId, 30);
  };

  /**
   * Returns the local session uuid from local storage.
   * @returns {*} session identifier 
   */
  this.getSessionId = function () {
    return (0, _functions.getCookie)("vle_cart_session_id");
  };
  this.storeLocalCart = function (config) {
    _this.setCartId(config.cart_id);
    _this.setSessionId(config.session_id);
  };
  this.removeLocalCart = function () {
    (0, _functions.deleteCookie)("vle_cart_id");
    (0, _functions.deleteCookie)("vle_cart_session_id");
  };
  this.resetLocalCart = function () {
    _this.removeLocalCart();

    // create a local session identifier
    var sessionId = (0, _uuid.v4)();
    _this.setSessionId(sessionId);
  };

  /**
   * Creates a shopping cart with the information of a booking.
   * @param {*} data 
   */
  this.createCart = function (data) {
    var endpoint = _this.baseEndpoint;
    _this.resetLocalCart();
    return _this.makeCartRequest(endpoint, "POST", _this.makeParams(), data).then(function (cart) {
      _this.setCartId(cart.id);
      return cart;
    });
  };
  this.getCart = function () {
    var endpoint = _this.baseEndpoint + '/:id';
    return _this.makeCartRequest(endpoint, "GET", _this.makeParams())["catch"](function (err) {
      if (err instanceof Error && err.type == "NOT_FOUND") {
        _this.removeLocalCart();
      }
      throw err;
    });
  };
  this.getSummary = function () {
    var endpoint = _this.baseEndpoint + '/:id/summary';
    return _this.makeCartRequest(endpoint, "GET", _this.makeParams())["catch"](function (err) {
      if (err instanceof Error && err.type == "NOT_FOUND") {
        _this.removeLocalCart();
      }
      throw err;
    });
  };
  this.addBooking = function (data) {
    if (_this.getCartId()) {
      // if cart exists then add the booking to the current cart
      return _this.addLineItem(data);
    } else {
      // if the cart does not exist create a new one
      return _this.createCart(data);
    }
  };
  this.addDiscount = function (discountId) {
    return _this.addLineItem({
      type: "discount",
      discount_id: discountId
    });
  };
  this.addLineItem = function (data) {
    var endpoint = _this.baseEndpoint + '/:id/line-items';
    return _this.makeCartRequest(endpoint, "POST", _this.makeParams(), data);
  };
  this.removeLineItem = function (lineItemId) {
    var endpoint = _this.baseEndpoint + '/:id/line-items/:line_item_id';
    return _this.makeCartRequest(endpoint, "DELETE", _this.makeParams({
      line_item_id: lineItemId
    }));
  };
  this.confirmCart = function (data) {
    var endpoint = _this.baseEndpoint + '/:id/confirm';
    return _this.makeCartRequest(endpoint, "POST", _this.makeParams(), data);
  };
  this.getPaymentGateways = function () {
    var endpoint = _this.baseEndpoint + '/:id/payment-gateways';
    return _this.makeCartRequest(endpoint, "GET", _this.makeParams(), null, function (res) {
      return res.payment_gateways;
    });
  };
  this.makeCartRequest = function (endpoint, method, params, data, callback) {
    var request = _this.volcanoClient.prepareRequest(endpoint, {
      method: method,
      isAdmin: false,
      parameters: params.path,
      query: params.query
    });
    if (data) {
      request.data = data;
    }
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      if (callback) {
        return callback(res);
      } else {
        return new _entity["default"](res[Object.keys(res)[0]]);
      }
    });
  };
}