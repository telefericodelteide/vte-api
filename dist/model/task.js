"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _vte = _interopRequireDefault(require("../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_TASKS = 'tasks';
var ENDPOINT_GET_TASK = ENDPOINT_GET_TASKS + '/:id';
var _default = exports["default"] = TaskClient;
/**
 * Container which has the task methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function TaskClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;
  this.add = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TASKS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Create a task to perform an operation on a set of entities.
   * 
   * @param {*} operation operation to perform
   * @param {*} entityType type of the entities to perform the operation
   * @param {*} entities array of entities ids
   * @param {*} payload operation payload (optional) 
   * @returns 
   */
  this.createEntitiesTask = function (operation, entityType, entities) {
    var payload = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var data = {
      operation: operation,
      entity_type: entityType,
      entities: entities,
      payload: payload
    };
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TASKS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Return a collection with all tasks with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getTasks = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_TASKS, params);
  };

  /**
   * Return an Entity containing a task which match by Id `taskId`.
   *
   * @param {BigInteger} [id]  task ID.
   *
   * @returns {Entity}
   */
  this.getTask = function (id, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_TASK, id);
  };
  this.cancelTask = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TASK + '/cancel', _constants["default"].HTTP_POST, null, {
      path: {
        id: id
      },
      isAdmin: true
    });
  };
}