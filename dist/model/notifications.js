"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _lodash = require("lodash");
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports["default"] = NotificationClient;
var ENDPOINT_GET_NOTIFICATIONS_SCHEMA = 'notification-templates/schema';
var NT_GET_TEMPLATES = 'notification-templates';
var NT_GET_TEMPLATE_BY_ID = 'notification-templates/:id';
var NT_EDIT_TEMPLATE = 'notification-templates/:id/edit';
var NT_COPY_TEMPLATE = 'notification-templates/:id/copy';
var NT_GET_PRODUCTS_TEMPLATE_BY_ID = 'notification-templates/:id/products';
var NT_DELETE_PRODUCT_TEMPLATE = 'notification-templates/:id/delete-product';
var NT_ADD_PRODUCTS_TEMPLATE = 'notification-templates/:id/add-products';
var NT_EDIT_RECIPIENT_TEMPLATE = 'notification-templates/:id/edit-recipient';
var N_GET_NOTIFICATIONS = 'notifications';
var N_GET_NOTIFICATION_BY_ID = N_GET_NOTIFICATIONS + '/:id';
var N_EDIT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/edit';
var N_ADD_PRODUCTS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/add-products';
var N_DELETE_PRODUCT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/delete-product';
var N_EDIT_RECIPIENT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/edit-recipient';
var N_UPDATE_PRODUCTS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/update-products';
var N_PREPARE_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/prepare';
var N_SET_SEND_RECIPIENTS_NOTIFICATION = 'notifications/:id/set-send-recipients';
var N_SET_EDITION_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/set-edition';
var N_SET_DATE_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/set-date';
var N_SEND_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/send';
var N_CANCEL_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/cancel';
var N_PREVIEW_NOTIFICATION = N_GET_NOTIFICATIONS + '/:id/preview/:nr';
var N_DETAILS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/detail';
var N_GET_RECIPIENTS = N_GET_NOTIFICATION_BY_ID + '/notification-recipients';

/**
 * Uses to access Volcano API notification templates endpoints.
 * 
 * @constructor NotificationTemplatesClient
 * @param {VolcanoClient} volcanoClient 
 */
function NotificationClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.getNotificationTemplates = function (params) {
    return _this.volcanoClient.makeCollectionQueryRequest(NT_GET_TEMPLATES, params);
  };
  this.getNotificationTemplate = function (templateId) {
    return _this.volcanoClient.getEntityRequest(NT_GET_TEMPLATE_BY_ID, templateId, false);
  };

  /**
       * Create notification template
       *
       * @param { object } data   This should have the following format.
       *                          {
       *                             "name": "template",
       *                             "system_template": true,
       *                             "enterpriseId": 1,
       *                         }
       *
       * @return {Entity}
       */
  this.createNotificationTemplate = function (data) {
    return _this.volcanoClient.makeResourceRequest(NT_GET_TEMPLATES, _constants["default"].HTTP_POST, data, {
      isAdmin: false
    }, null, 'template');
  };

  /**
   * Edit template
   *
   * @param {string}  templateId  Template identifier
   * @param { object } data   This should have the following format.
   *                          {
   *                             "name": "template",
   *                             "system_template": true,
   *                             "enterpriseId": 1,
   *                         }
   * @return {Entity}
   */
  this.editNotificationTemplate = function (templateId, data) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(NT_EDIT_TEMPLATE, templateId, _constants["default"].HTTP_POST, data, {
      path: {
        id: templateId
      },
      isAdmin: false
    }, null, 'template');
  };
  this.deleteNotificationTemplate = function (templateId) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(NT_GET_TEMPLATE_BY_ID, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: templateId
      },
      isAdmin: false
    });
  };

  /**
   * Copy template
   *
   * @param {string}  templateId  Template identifier
   * @return {Entity}
   */
  this.copyNotificationTemplate = function (templateId) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(NT_COPY_TEMPLATE, templateId, _constants["default"].HTTP_POST, [], {
      path: {
        id: templateId
      },
      isAdmin: false
    }, null, 'template');
  };

  /**
   * Returns the products realted with the notification template
   * 
   * @param {int} templateId 
   * @param {array} params 
   * @returns 
   */
  this.getNotificationTemplateProducts = function (templateId, params) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(NT_GET_PRODUCTS_TEMPLATE_BY_ID, {
      method: 'GET',
      isAdmin: false,
      query: _objectSpread({}, params),
      parameters: {
        id: templateId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.notification_template_products;
    });
  };

  /**
   * Deletes the relation between the notification template and the product
   * 
   * @param {int} templateId 
   * @param {int} productId 
   * @returns 
   */
  this.deleteProductFromNotificationTemplate = function (templateId, productId) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(NT_DELETE_PRODUCT_TEMPLATE, {
      method: _constants["default"].HTTP_DELETE,
      isAdmin: false,
      parameters: {
        id: templateId
      },
      data: {
        productId: productId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };

  /**
   * Add the relation among the notification template and the products in data
   * 
   * @param {int} templateId 
   * @param {array} data 
   * @returns 
   */
  this.addProductToNotificationTemplate = function (templateId, data) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(NT_ADD_PRODUCTS_TEMPLATE, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: templateId
      },
      data: {
        products: data
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.modifyRecipientToNotificationTemplate = function (templateId, recipientType, data) {
    if ((0, _lodash.isEmpty)(templateId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(NT_EDIT_RECIPIENT_TEMPLATE, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: templateId
      },
      data: {
        recipient_type: recipientType,
        template: data
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.getLegend = function () {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, {
      method: _constants["default"].HTTP_GET,
      isAdmin: false
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res['legend_send_method']['options'];
    });
  };

  /**
   * Returns the notifications
   * 
   * @param {array} params 
   * @returns 
   */
  this.getNotifications = function (params) {
    return _this.volcanoClient.makeCollectionQueryRequest(N_GET_NOTIFICATIONS, params);
  };

  /**
   * Get the notification
   * 
   * @param {int} notificationId 
   * @returns 
   */
  this.getNotification = function (notificationId) {
    return _this.volcanoClient.getEntityRequest(N_GET_NOTIFICATION_BY_ID, notificationId, false);
  };

  /**
  * Edit notification
  *
  * @param {string}  notificationId  Template identifier
  * @param { object } data   This should have the following format.
  *                          {
  *                             "name": "notification",
  *                             "enterpriseId": 1,
  *                         }
  * @return {Entity}
  */
  this.editNotification = function (notificationId, data) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(N_EDIT_NOTIFICATION, notificationId, _constants["default"].HTTP_POST, data, {
      path: {
        id: notificationId
      },
      isAdmin: false
    }, null, 'notification');
  };

  /**
   * 
   * @param { string } notificationId 
   * @returns 
   */
  this.deleteNotification = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(N_GET_NOTIFICATION_BY_ID, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: notificationId
      },
      isAdmin: false
    });
  };

  /**
   * Deletes the relation between the notification and the product
   * 
   * @param {int} notificationId 
   * @param {int} productId 
   * @returns 
   */
  this.deleteProductFromNotification = function (notificationId, productId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_DELETE_PRODUCT_NOTIFICATION, {
      method: _constants["default"].HTTP_DELETE,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: {
        productId: productId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };

  /**
   * Add the relation among the notification and the products in data
   * 
   * @param {int} notificationId 
   * @param {array} data 
   * @returns 
   */
  this.addProductToNotification = function (notificationId, data) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_ADD_PRODUCTS_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: {
        products: data
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.updateProductsNotification = function (notificationId, data) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_UPDATE_PRODUCTS_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: {
        products: data.products
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.notification;
    });
  };
  this.createNotification = function (data) {
    return _this.volcanoClient.makeResourceRequest(N_GET_NOTIFICATIONS, _constants["default"].HTTP_POST, data, {
      isAdmin: false
    }, null, 'notification');
  };
  this.createNotificationFromTemplate = function (templateId) {
    var data = {
      template_id: templateId
    };
    var request = _this.volcanoClient.prepareRequest(N_GET_NOTIFICATIONS, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      data: data
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.notification;
    });
  };
  this.modifyRecipientToNotification = function (notificationId, recipientType, data) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_EDIT_RECIPIENT_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: {
        recipient_type: recipientType,
        template: data
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.prepareNotification = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_PREPARE_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.markSendRecipients = function (notificationId, elements) {
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    if ((0, _lodash.isEmpty)(notificationId) || elements == null) {
      return null;
    }
    var recipients = elements.map(function (element) {
      return element.id;
    });
    var request = _this.volcanoClient.prepareRequest(N_SET_SEND_RECIPIENTS_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: {
        type: type,
        recipients: recipients
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.notification;
    });
  };
  this.setEditNotification = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_SET_EDITION_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.setDateNotification = function (notificationId, data) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_SET_DATE_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      },
      data: data
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.sendNotification = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_SEND_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.cancelNotification = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_CANCEL_NOTIFICATION, {
      method: _constants["default"].HTTP_POST,
      isAdmin: false,
      parameters: {
        id: notificationId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.getPreview = function (notificationId, notificationRecipientId) {
    if ((0, _lodash.isEmpty)(notificationId) || (0, _lodash.isEmpty)(notificationRecipientId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_PREVIEW_NOTIFICATION, {
      method: _constants["default"].HTTP_GET,
      isAdmin: false,
      parameters: {
        id: notificationId,
        nr: notificationRecipientId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res;
    });
  };
  this.getDetails = function (notificationId) {
    if ((0, _lodash.isEmpty)(notificationId)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(N_DETAILS_NOTIFICATION, {
      method: _constants["default"].HTTP_GET,
      isAdmin: false,
      parameters: {
        id: notificationId
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      if (res.notification && res.notification.details) {
        return res.notification.details;
      } else {
        return;
      }
    });
  };
  this.getRecipients = function (notificationId, params) {
    if (params == null) {
      return null;
    }
    var result = _this.volcanoClient.makeCollectionQueryRequest(N_GET_RECIPIENTS, params, {
      parameters: {
        id: notificationId
      }
    });
    return result;
  };
}