"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _vte = _interopRequireDefault(require("../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_BASE_ACCESS_CONTROL = 'access-control';
var ENDPOINT_GET_VENUES = ENDPOINT_BASE_ACCESS_CONTROL + '/venues';
var ENDPOINT_GET_VENUE = ENDPOINT_GET_VENUES + '/:venue_id';
var ENDPOINT_GET_POINTS = ENDPOINT_GET_VENUE + '/points';
var ENDPOINT_GET_POINT = ENDPOINT_GET_POINTS + '/:point_id';
var _default = exports["default"] = AccessControlClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function AccessControlClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Access control venues methods */

  this.addVenue = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Return a collection with all venues with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getVenues = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_VENUES, params);
  };

  /**
   * Return an Entity containing a venue which match by Id `venueId`.
   *
   * @param {string} [id]  venue ID.
   *
   * @returns {Entity}
   */
  this.getVenue = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, {
      parameters: {
        venue_id: id
      }
    });
  };
  this.deleteVenue = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUE, _constants["default"].HTTP_DELETE, null, {
      path: {
        venue_id: venueId
      },
      isAdmin: true
    });
  };

  /** Access control points methods */

  this.addPoint = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_POINTS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Return a collection with all points for a venue with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getPoints = function (venueId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_VENUES, params, {
      isAdmin: true,
      parameters: {
        venue_id: venueId
      }
    });
  };

  /**
   * Return an Entity containing a point within a venue which match by Id `pointId`.
   *
   * @param {string} [venueId]  venue ID.
   * @param {string} [id]  point ID.
   *
   * @returns {Entity}
   */
  this.getPoint = function (venueId, id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, {
      parameters: {
        venue_id: venueId,
        point_id: id
      }
    });
  };
  this.deletePoint = function (venueId, id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUE, _constants["default"].HTTP_DELETE, null, {
      path: {
        venue_id: venueId,
        point_id: id
      },
      isAdmin: true
    });
  };
}