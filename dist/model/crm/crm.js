"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _salesman = _interopRequireDefault(require("./salesman"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = CrmClient;
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CrmClient(volcanoClient) {
  this.salesman = new _salesman["default"](volcanoClient);
}