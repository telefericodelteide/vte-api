"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
// This file contains the client model for the CRM module
var ENDPOINT_GET_BILLING_CLIENT = 'crm/clients';
var _default = exports["default"] = BillingClient;
/**
 * Container which has the salesmen methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function BillingClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of salesmen that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getSalesmen = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_BILLING_CLIENT, params, {});
  };
}