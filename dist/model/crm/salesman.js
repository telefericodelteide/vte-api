"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_SALESMEN = 'crm/salesmen';
var ENDPOINT_GET_SALESMAN = ENDPOINT_GET_SALESMEN + '/:id';
var _default = exports["default"] = SalesmanClient;
/**
 * Container which has the salesmen methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SalesmanClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of salesmen that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getSalesmen = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_SALESMEN, params);
  };

  /**
   * Returns the salesman that matches the id passed.
   * 
   * @param {string} salesmanId 
   * @returns {Entity}
   */
  this.getSalesman = function (salesmanId) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_SALESMAN, salesmanId);
  };

  /**
   * Creates a new salesman.
   * 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.createSalesman = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_SALESMEN, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null, 'salesmen');
  };

  /**
   * Updates the salesman that matches the id passed.
   * 
   * @param {string} salesmanId 
   * @param {Object} data 
   * @returns {Entity}
   */
  this.editSalesman = function (salesmanId, data) {
    if ((0, _isEmpty["default"])(salesmanId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_GET_SALESMAN, salesmanId, _constants["default"].HTTP_PATCH, data, {
      path: {
        id: salesmanId
      },
      isAdmin: true
    });
  };
}