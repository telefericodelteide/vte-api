"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _locale = require("date-fns/locale");
var _entity = _interopRequireDefault(require("../entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
function _possibleConstructorReturn(t, e) { if (e && ("object" == _typeof(e) || "function" == typeof e)) return e; if (void 0 !== e) throw new TypeError("Derived constructors may only return object or undefined"); return _assertThisInitialized(t); }
function _assertThisInitialized(e) { if (void 0 === e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); return e; }
function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
function _getPrototypeOf(t) { return _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function (t) { return t.__proto__ || Object.getPrototypeOf(t); }, _getPrototypeOf(t); }
function _inherits(t, e) { if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function"); t.prototype = Object.create(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } }), Object.defineProperty(t, "prototype", { writable: !1 }), e && _setPrototypeOf(t, e); }
function _setPrototypeOf(t, e) { return _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function (t, e) { return t.__proto__ = e, t; }, _setPrototypeOf(t, e); }
var Product = /*#__PURE__*/function (_Entity) {
  /**
   * Initializes the Product object with the entity passed.
   * 
   * @param {*} props 
   */
  function Product(props) {
    _classCallCheck(this, Product);
    return _callSuper(this, Product, [props]);
  }

  /**
   * Returns the qty restrictions of the product.
   *
   * @param {object} defaultValues Default values to return if the product has no qty restrictions
   * @returns {object} Valid qty restrictions of the product
   */
  _inherits(Product, _Entity);
  return _createClass(Product, [{
    key: "getQtyRestrictions",
    value: function getQtyRestrictions(defaultValues) {
      var _this$qty_restriction,
        _this$qty_restriction2,
        _this$container_confi,
        _this$container_confi4,
        _this = this;
      var result = {
        min: (this === null || this === void 0 || (_this$qty_restriction = this.qty_restrictions) === null || _this$qty_restriction === void 0 ? void 0 : _this$qty_restriction.min) || (defaultValues === null || defaultValues === void 0 ? void 0 : defaultValues.min) || -1,
        max: (this === null || this === void 0 || (_this$qty_restriction2 = this.qty_restrictions) === null || _this$qty_restriction2 === void 0 ? void 0 : _this$qty_restriction2.max) || (defaultValues === null || defaultValues === void 0 ? void 0 : defaultValues.max) || -1
      };
      if ((this === null || this === void 0 || (_this$container_confi = this.container_configuration) === null || _this$container_confi === void 0 ? void 0 : _this$container_confi.qty_restrictions) !== undefined) {
        var _this$container_confi2, _this$container_confi3;
        result = _objectSpread(_objectSpread({}, result), {}, {
          min: (this === null || this === void 0 || (_this$container_confi2 = this.container_configuration) === null || _this$container_confi2 === void 0 || (_this$container_confi2 = _this$container_confi2.qty_restrictions) === null || _this$container_confi2 === void 0 ? void 0 : _this$container_confi2.min) || result.min,
          max: (this === null || this === void 0 || (_this$container_confi3 = this.container_configuration) === null || _this$container_confi3 === void 0 || (_this$container_confi3 = _this$container_confi3.qty_restrictions) === null || _this$container_confi3 === void 0 ? void 0 : _this$container_confi3.max) || result.max
        });
      }

      // process customer types restrictions
      if ((this === null || this === void 0 || (_this$container_confi4 = this.container_configuration) === null || _this$container_confi4 === void 0 ? void 0 : _this$container_confi4.customer_types_restrictions) !== undefined && this.rates) {
        var _this$container_confi5;
        result.customer_types_restrictions = this === null || this === void 0 || (_this$container_confi5 = this.container_configuration) === null || _this$container_confi5 === void 0 ? void 0 : _this$container_confi5.customer_types_restrictions.map(function (customerTypeId) {
          var rate = _this.rates.find(function (rate) {
            return rate.customer_types.customer_types.some(function (customerType) {
              return customerType.id === customerTypeId;
            });
          });
          return {
            id: customerTypeId,
            name: rate.customer_types.customer_types.find(function (customerType) {
              return customerType.id === customerTypeId;
            }).name
          };
        });
      }
      return result;
    }
  }]);
}(_entity["default"]);
var _default = exports["default"] = Product;