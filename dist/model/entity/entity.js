"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var Entity = /*#__PURE__*/function () {
  /**
   * 
   * @param {*} props 
   */
  function Entity(props) {
    var _this = this;
    _classCallCheck(this, Entity);
    Object.assign(this, props);
    this.actions = [];
    if (this._links) {
      Object.entries(this._links).forEach(function (link) {
        var action = link[0].indexOf(':') < 0 ? link[0] : link[0].split(':')[1];
        if (link[1].data) {
          _this.actions[action] = {
            href: link[1].href,
            data: link[1].data
          };
        } else {
          _this.actions[action] = link[1].href;
        }
      });
    }
  }

  /**
   * Returns the actions of the entity.
   * 
   * @returns {array}
   */
  return _createClass(Entity, [{
    key: "getActions",
    value: function getActions() {
      return this.actions;
    }

    /**
     * Checks if the entity has the action passed.
     * 
     * @param {string} action 
     * @param {string} operation Action's operation (optional)
     * 
     * @returns {boolean} true if the entity has the action, false otherwise.
     */
  }, {
    key: "hasAction",
    value: function hasAction(action) {
      var allowedOperation = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      if (this.actions[action] === undefined) {
        return false;
      }
      if (allowedOperation === null) {
        return true;
      }
      if (this.actions[action].data && this.actions[action].data.allowed_operations) {
        return this.actions[action].data.allowed_operations.includes(allowedOperation);
      }
      return false;
    }
  }]);
}();
var _default = exports["default"] = Entity;