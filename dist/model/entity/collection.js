"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _entity = _interopRequireDefault(require("./entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var PAGINATOR_LINKS = ['first', 'last', 'next', 'prev', 'self'];
var _default = exports["default"] = Collection;
function Collection(volcanoClient, data, entityClass) {
  var _this = this;
  entityClass = entityClass || _entity["default"];
  this.volcanoClient = volcanoClient;
  this.total = data.total;
  this.pageCount = data.page_count;
  this.count = data.count;
  this.currentPage = data.page;
  this.items = Array.isArray(Object.values(data._embedded)[0]) ? Object.values(data._embedded)[0].map(function (entity) {
    return new entityClass(entity);
  }) : [new entityClass(Object.values(data._embedded)[0])];
  this.aggregateFields = Object.values(data._aggregate)[0];
  this.resoureType = Object.keys(data._embedded)[0];
  this.paginator = [];
  this.actions = [];
  if (data._links) {
    Object.entries(data._links).forEach(function (link) {
      if (PAGINATOR_LINKS.includes(link[0])) {
        // paginator link
        _this.paginator[link[0]] = link[1].href;
      } else if (link[1].hasOwnProperty(_this.resoureType)) {
        var _link$1$_this$resoure;
        if (link[0] === "add") {
          // add link
          link[1][_this.resoureType].scope = "add";
        }
        if (((_link$1$_this$resoure = link[1][_this.resoureType]) === null || _link$1$_this$resoure === void 0 ? void 0 : _link$1$_this$resoure.scope) !== undefined) {
          var _link$1$_this$resoure2;
          // add action
          _this.actions[link[0]] = {
            scope: link[1][_this.resoureType].scope,
            href: link[1][_this.resoureType].href,
            data: (_link$1$_this$resoure2 = link[1][_this.resoureType]) === null || _link$1$_this$resoure2 === void 0 ? void 0 : _link$1$_this$resoure2.data
          };
        }
      }
    });
  }

  /**
   * @return {number} total results in the collection
   */
  this.getTotal = function () {
    return _this.total;
  };

  /**
   * @return {number} total number of pages in the collection
   */
  this.getPageCount = function () {
    return _this.pageCount;
  };

  /**
   * @return {number} total number of results in the current page
   */
  this.getCount = function () {
    return _this.count;
  };

  /**
   * @return {number} current page index, starting in 1
   */
  this.getCurrentPage = function () {
    return _this.currentPage;
  };

  /**
   * @return {Array} the entities in the current page
   */
  this.getItems = function () {
    return _this.items;
  };

  /**
   * @return {object} the paginator for the current page
   */
  this.getPaginator = function () {
    return _this.paginator;
  };

  /**
   * @return {object} the aggregate fields for the current page
   */
  this.getAggregateFields = function () {
    return _this.aggregateFields;
  };

  /**
   * @return {string} the type of the entities of the collection
   */
  this.getResourceType = function () {
    return _this.resoureType;
  };
  this.paginate = function (page) {
    var endpoint = _this.paginator.self.replace(/page=\d+/, 'page=' + page);
    var request = _this.volcanoClient.prepareRequest(endpoint, {
      method: 'GET',
      isFull: true
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return new Collection(_this.volcanoClient, res);
    });
  };

  /**
   * Returns the actions of the collection.
   * 
   * @returns {array}
   */
  this.getActions = function () {
    return _this.actions;
  };

  /**
   * Checks if the collection has the action passed.
   * 
   * @param {string} action 
   * @param {string} operation Action's operation (optional)
   * 
   * @returns {boolean} true if the entity has the action, false otherwise.
   */
  this.hasAction = function (action) {
    var allowedOperation = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    if (_this.actions[action] === undefined) {
      return false;
    }
    if (allowedOperation === null) {
      return true;
    }
    if (_this.actions[action].data && _this.actions[action].data.allowed_operations) {
      return _this.actions[action].data.allowed_operations.includes(allowedOperation);
    }
    return false;
  };
}