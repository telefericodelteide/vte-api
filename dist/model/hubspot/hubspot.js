"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = exports["default"] = HubspotClient;
var ENDPOINT_GET_POSTS = 'hubspot/posts';
var ENDPOINT_GET_DEALS = 'hubspot/deals';

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function HubspotClient(volcanoClient) {
  var _this = this;
  // This is under admin namespace.
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of posts that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getPosts = function (params) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_POSTS, {
      method: 'GET',
      isAdmin: false,
      query: params
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.data;
    });
  };

  /**
   * Returns the collection of posts that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getDeals = function (params) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_DEALS, {
      method: 'GET',
      isAdmin: false,
      query: params
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.data;
    });
  };
}