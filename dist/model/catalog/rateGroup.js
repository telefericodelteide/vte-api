"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _lodash = require("lodash");
var _vte = _interopRequireDefault(require("../../vte"));
var _entity = _interopRequireDefault(require("../entity/entity"));
var _product = _interopRequireDefault(require("../entity/catalog/product"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_RATE_GROUPS = "catalog/rate-groups";
var ENDPOINT_RATE_GROUP = ENDPOINT_RATE_GROUPS + "/:id";
var ENDPOINT_RATE_GROUP_PDF = ENDPOINT_RATE_GROUP + '/pdf';
var ENDPOINT_RATE_GROUP_PRODUCTS = ENDPOINT_RATE_GROUP + "/products";
var ENDPOINT_RATE_GROUP_PRODUCT = ENDPOINT_RATE_GROUP_PRODUCTS + "/:product_id";
var ENDPOINT_RATE_GROUP_INTERMEDIARIES = ENDPOINT_RATE_GROUP + "/intermediaries";
var ENDPOINT_RATE_GROUP_EXPERIENCES = ENDPOINT_RATE_GROUP + "/experiences";
var ENDPOINT_RATE_GROUP_COPY = ENDPOINT_RATE_GROUP + "/copy";
var _default = exports["default"] = RateGroupClient;
/**
 * Container which has the rate groups methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function RateGroupClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns a collection with all rate groups with pagination.
   *
   * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
   * if you want to move to specific page, you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getRateGroups = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_RATE_GROUPS, params);
  };

  /**
   * Returns an entity containing a rate group which match by Id 
   *
   * @param {string} [id] rate group identifier.
   *
   * @returns {Entity}
   */
  this.getRateGroup = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_RATE_GROUP, id, true, {
      parameters: {
        id: id
      }
    });
  };

  /**
   * Creates a rate group
   *
   * @param {Object} data data to be updated
   *
   * @returns {Entity}
   */
  this.addRateGroup = function (data) {
    return _this.volcanoClient.addEntityRequest(ENDPOINT_RATE_GROUPS, _this.processRateGroupRequestData(data), true);
  };

  /**
   * Updates a rate group
   *
   * @param {string} id id of the rate group to be updated
   * @param {Object} data data to be updated
   *
   * @return {Entity}
   */
  this.editRateGroup = function (id, data) {
    return _this.volcanoClient.editEntityRequest(ENDPOINT_RATE_GROUP, id, _this.processRateGroupRequestData(data), true);
  };

  /**
   * Copies a rate group
   *
   * @param {string} id id of the rate group to be copied
   * @param {Object} data data to be updated
   *
   * @return {Entity}
   */
  this.copyRateGroup = function (id, data) {
    return _this.volcanoClient.addEntityRequest(ENDPOINT_RATE_GROUP_COPY, _this.processRateGroupRequestData(data), true, {
      path: {
        id: id
      }
    });
  };

  /**
   * Deletes a rate group
   * 
   * @param {string} id 
   * 
   * @returns {boolean} true if the rate group was deleted
   */
  this.deleteRateGroup = function (id) {
    return _this.volcanoClient.deleteResourceRequest(ENDPOINT_RATE_GROUP, id, true);
  };

  /**
   * Return an application/PDF octet
   *
   * @param id
   *
   * @returns {Object|Entity}
   */
  this.getRateGroupPdf = function (id, params) {
    params = (0, _lodash.isObject)(params) ? params : {};
    return _this.volcanoClient.makeFileRequest(ENDPOINT_RATE_GROUP_PDF, id, true, params);
  };

  /** 
   * Returns a collection with all products of a rate group with pagination.
   * 
   * @param {string} id rate group identifier
   * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
   * if you want to move to specific page, you must add 'page: 3' to the params object.
   * 
   * @returns {Collection} 
   */
  this.getRateGroupProducts = function (id, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_RATE_GROUP_PRODUCTS, params, {
      parameters: {
        id: id
      }
    }, _product["default"]);
  };

  /** 
   * Add and/or delete a collection of products from a rate group.
   * 
   * @param {string} id rate group identifier
   * @param {array} data 
   * 
   * @returns {Collection} 
   */
  this.editRateGroupProducts = function (id, data) {
    return _this.volcanoClient.addEntityRequest(ENDPOINT_RATE_GROUP_PRODUCTS, data, true, {
      path: {
        id: id
      }
    });
  };

  /** 
   * Edit the configurarion of a product in a rate group.
   * 
   * @param {string} id rate group identifier
   * @param {int} productId product identifier
   * @param {array} data 
   * 
   * @returns {Entity} 
   */
  this.editRateGroupProduct = function (id, productId, data) {
    return _this.volcanoClient.editEntityRequest(ENDPOINT_RATE_GROUP_PRODUCT, id, data, true, {
      path: {
        id: id,
        product_id: productId
      }
    });
  };

  /**
   * Deletes a product from a rate group 
   * @param {*} id rate group identifier
   * @param {*} productId product identifier
   * @returns 
   */
  this.deleteRateGroupProduct = function (id, productId) {
    return _this.volcanoClient.deleteResourceRequest(ENDPOINT_RATE_GROUP_PRODUCT, id, true, {
      path: {
        product_id: productId
      }
    });
  };

  /**
   * Returns the list of experiences of a rate group
   *  
   * @param {string} id rate group identifier  
   * @returns {Array}  
   */
  this.getRateGroupExperiences = function (id) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_RATE_GROUP_EXPERIENCES, {
      method: 'GET',
      isAdmin: true,
      parameters: {
        id: id
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };

  /**
   * Updates the order of the experiences of a rate group.
   *  
   * @param {*} id rate group identifier
   * @param {*} data sorted experiences
   * @returns {Array}
   */
  this.editRateGroupExperiences = function (id, data) {
    return _this.volcanoClient.editEntityRequest(ENDPOINT_RATE_GROUP_EXPERIENCES, id, data);
  };

  /** 
   * Returns a collection with all intermediaries of a rate group with pagination.
   * 
   * @param {string} id rate group identifier
   * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
   * if you want to move to specific page, you must add 'page: 3' to the params object.
   * 
   * @returns {Collection} 
   */
  this.getRateGroupIntermediaries = function (id, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_RATE_GROUP_INTERMEDIARIES, params, {
      parameters: {
        id: id
      }
    });
  };
  this.processRateGroupRequestData = function (data) {
    if (data.config) {
      if (typeof data.config["protected"] === 'string') {
        data.config["protected"] = data.config["protected"] === 'true' || data.config["protected"] === '1';
      }
      data.config["protected"] = !!data.config["protected"];
      if (data.config.default_commission) {
        data.config.default_commission = parseFloat(data.config.default_commission);
      }
    }
    return data;
  };
}