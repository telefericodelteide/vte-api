"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _subcategory = _interopRequireDefault(require("./subcategory"));
var _experience = _interopRequireDefault(require("./experience"));
var _product = _interopRequireDefault(require("./product"));
var _supplier = _interopRequireDefault(require("./supplier"));
var _discountCode = _interopRequireDefault(require("./discountCode"));
var _language = _interopRequireDefault(require("./language"));
var _transport = _interopRequireDefault(require("./transport"));
var _remoteProductProvider = _interopRequireDefault(require("./remoteProductProvider"));
var _rateGroup = _interopRequireDefault(require("./rateGroup"));
var _productRatesPublication = _interopRequireDefault(require("./productRatesPublication"));
var _customerTypeGroup = _interopRequireDefault(require("./customerTypeGroup"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = CatalogClient;
/**
 * Container which has the catalog clients and methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CatalogClient(volcanoClient) {
  this.customerTypeGroup = new _customerTypeGroup["default"](volcanoClient);
  this.subcategory = new _subcategory["default"](volcanoClient);
  this.experience = new _experience["default"](volcanoClient);
  this.product = new _product["default"](volcanoClient);
  this.supplier = new _supplier["default"](volcanoClient);
  this.discountCode = new _discountCode["default"](volcanoClient);
  this.language = new _language["default"](volcanoClient);
  this.transport = new _transport["default"](volcanoClient);
  this.remoteProductProvider = new _remoteProductProvider["default"](volcanoClient);
  this.rateGroup = new _rateGroup["default"](volcanoClient);
  this.productRatesPublication = new _productRatesPublication["default"](volcanoClient);
}