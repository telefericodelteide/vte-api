"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_CUSTOMER_TYPE_GROUPS = "catalog/customer-type-groups";
var _default = exports["default"] = CustomerTypeGroupClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CustomerTypeGroupClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Categories methods */

  /**
   * Return a collection with all Customer type groups with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getCustomerTypeGroups = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_CUSTOMER_TYPE_GROUPS, params);
  };
}