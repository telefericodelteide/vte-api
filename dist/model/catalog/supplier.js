"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_SUPPLIERS = "catalog/suppliers";
var ENDPOINT_GET_SUPPLIER = ENDPOINT_GET_SUPPLIERS + "/:id";
var _default = exports["default"] = SupplierClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SupplierClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Suppliers methods */

  /**
   * Return a collection with all suppliers with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getSuppliers = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_SUPPLIERS, params);
  };

  /**
   * Return an Entity containing a supplier which match by Id .
   *
   * @param {string} [id]  supplier ID.
   *
   * @returns {Entity}
   */
  this.getSupplier = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_SUPPLIER, id, true, {
      parameters: {
        id: id
      }
    });
  };
}