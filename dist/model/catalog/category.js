"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_CATEGORIES = "catalog/categories";
var ENDPOINT_GET_CATEGORY = ENDPOINT_GET_CATEGORIES + "/:id";
var _default = exports["default"] = CategoryClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CategoryClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Categories methods */

  /**
   * Return a collection with all Categories with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getCategories = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_CATEGORIES, params);
  };

  /**
   * Return an Entity containing a Category which match by Id .
   *
   * @param {number} [id]  CATEGORY ID.
   *
   * @returns {Entity}
   */
  this.getCategory = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_CATEGORY, id, true, {
      parameters: {
        id: id
      }
    });
  };
}