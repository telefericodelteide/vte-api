"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _entity = _interopRequireDefault(require("../entity/entity"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = ExperienceClient; // Constants for the endpoints
var ENDPOINT_GET_EXPERIENCES = 'catalog/experiences';
var ENDPOINT_GET_EXPERIENCE = ENDPOINT_GET_EXPERIENCES + '/:id';
var SET_EXPERIENCE_PRODUCTS = ENDPOINT_GET_EXPERIENCE + "/config/experience-products";

/**
 * Container which has the product methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ExperienceClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Return a collection with all Experiences with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getExperiences = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_EXPERIENCES, params);
  };

  /**
   * Return an Entity containing an experience which match by Id `experienceId`.
   *
   * @param {BigInteger} [experienceId] experience ID.
   *
   * @returns {Entity}
   */
  this.getExperience = function (experienceId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_EXPERIENCE, experienceId, true, params);
  };

  /**
   * Return a XLS file containing a collection of Experiences.
   */
  this.exportExperiences = function (fields, params, progressHandler) {
    var endpoint = ENDPOINT_GET_EXPERIENCES;
    return _this.volcanoClient.makeCollectionExportRequest(endpoint, params, null, fields, "experiences", progressHandler);
  };

  /**
   * Set products from experience
   *
   * @param {string} id experience Id
   * @param {array} products
   *
   * @return {Entity}
   */
  this.setExperienceProducts = function (id, products) {
    return _this.volcanoClient.makeEntityRequest(SET_EXPERIENCE_PRODUCTS, id, _constants["default"].HTTP_POST, {
      experience_products: products
    }, {
      isAdmin: true
    }, null);
  };
}