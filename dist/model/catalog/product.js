"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _dateFns = require("date-fns");
var _isObject = _interopRequireDefault(require("lodash/isObject"));
var _vte = _interopRequireDefault(require("../../vte"));
var _entity = _interopRequireDefault(require("../entity/entity"));
var _constants = _interopRequireDefault(require("../../constants"));
var _product = _interopRequireDefault(require("../entity/catalog/product"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var _default = exports["default"] = ProductClient; // Constants for the endpoints
var ENDPOINT_GET_CORE_PRODUCTS = "products";
var ENDPOINT_GET_CORE_PRODUCT = ENDPOINT_GET_CORE_PRODUCTS + "/:id";
var ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY = ENDPOINT_GET_CORE_PRODUCT + "/availability/:date";
var ENDPOINT_GET_CORE_PRODUCT_RATES = ENDPOINT_GET_CORE_PRODUCT + "/rates/:date";
var ENDPOINT_GET_PRODUCTS = "catalog/products";
var ENDPOINT_GET_PRODUCT = ENDPOINT_GET_PRODUCTS + "/:id";
var ENDPOINT_GET_PRODUCT_CONTAINERS = ENDPOINT_GET_PRODUCT + "/containers";
var ENDPOINT_GET_PRODUCT_CONTAINER = ENDPOINT_GET_PRODUCT_CONTAINERS + "/:container_id";
var ENDPOINT_GET_COMPOUND_PRODUCTS = ENDPOINT_GET_PRODUCT + "/compound-products";
var ENDPOINT_GET_PRODUCT_EXPERIENCES = ENDPOINT_GET_PRODUCT + "/experiences";
var ENDPOINT_GET_IACPOS_PRODUCT_RATES = "products/:id/iacpos_rates";

/**
 * Container which has the product methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ProductClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Return a collection with all Products with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getProducts = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_PRODUCTS, params, {}, _product["default"]);
  };

  /**
   * Return a collection with all featured products.
   * 
   * @param {array} [params] Optional object which contains the parameters to build the request.
   */
  this.getFeaturedProducts = function (params) {
    return _this.getProducts(_objectSpread(_objectSpread({}, params), {}, {
      featured: true
    }));
  };

  /**
   * Return an Entity containing a product which match by Id `productId`.
   *
   * @param {BigInteger} [id]  booking ID.
   * @param {boolean} ratesPublished true to get the rates published of the product
   * @param {array} [params] Optional object which contains the parameters to build the request.
   *
   * @returns {Entity}
   */
  this.getProduct = function (id, ratesPublished, params) {
    params = (0, _isObject["default"])(params) ? params : {};
    params.rates_published = ratesPublished ? 1 : 0;
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_PRODUCT, id, true, params, "product");
  };

  /**
   * Add a product
   *
   * @param {Object}   data     
   *
   * @returns {Entity}
   */
  this.addProduct = function (data) {
    return _this.volcanoClient.addEntityRequest(ENDPOINT_GET_PRODUCTS, data);
  };

  /**
   * Edit a product which match by Id 
   *
   * @param {numeric}  id        id of the product to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editProduct = function (id, data, ratesPublished) {
    if (isNaN(parseInt(id))) {
      return null;
    }
    var params = {
      query: {
        rates_published: ratesPublished ? 1 : 0
      }
    };
    return _this.volcanoClient.editEntityRequest(ENDPOINT_GET_PRODUCT, id, data, true, params);
  };

  /** 
   * Edit the configuration of a product at enterprise level.
   * 
   * @param {string} id product id
   * @param {array} data 
   * 
   * @returns {Entity} 
   */
  this.editContainerProduct = function (id, containerId, data) {
    return _this.volcanoClient.editEntityRequest(ENDPOINT_GET_PRODUCT_CONTAINER, id, data, true, {
      path: {
        id: id,
        container_id: containerId
      }
    });
  };

  /**
   * Returns the availability for a product.
   *
   * @param {number} productId
   * @param {Date} date
   * @param {boolean} fullMonth true if the availability for the entire month of the date should be retrieved
   * @param {params} params
   *
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductAvailability = function (productId, date, fullMonth, params) {
    if (!date) {
      date = new Date();
    }
    params = (0, _isObject["default"])(params) ? params : {};
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY, {
      method: "GET",
      isAdmin: false,
      parameters: {
        id: productId,
        date: fullMonth ? (0, _dateFns.format)(date, "yyyy-MM") : (0, _dateFns.format)(date, "yyyy-MM-dd")
      },
      query: _objectSpread({}, params)
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };

  /**
   * Returns the availability for a product.
   *
   * @param {number} productId
   * @param {Date} date
   * @param {number} months number of months to query
   * @param {params} params
   **
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductAvailabilityRange = function (productId, date, months, params) {
    if (!date) {
      date = new Date();
    }
    if (!months) {
      months = 1;
    }
    params = (0, _isObject["default"])(params) ? params : {};
    var requests = new Array(months);
    for (var i = 0; i < months; i++) {
      var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY, {
        method: "GET",
        isAdmin: false,
        parameters: {
          id: productId,
          date: (0, _dateFns.format)(date, "yyyy-MM")
        },
        query: _objectSpread({}, params)
      });
      date = (0, _dateFns.addMonths)(date, 1);
      requests[i] = _this.volcanoClient.makeRequest(request).then(function (res) {
        return res[Object.keys(res)[0]];
      });
    }
    return Promise.all(requests).then(function (values) {
      return values.flat();
    });
  };

  /**
   * Return a collection with all rates of a given product id with pagination.
   *
   * @param {number} productId
   * 
   * @returns {Collection}
   */
  this.getProductRates = function (productId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_PRODUCT_RATES, params, {
      is_admin: true,
      parameters: {
        id: productId
      }
    });
  };

  /**
   * Return a collection with all compound products of a given product id with pagination.
   *
   * @param {number} productId
   * 
   * @returns {Collection}
   */
  this.getCompoundProducts = function (productId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_COMPOUND_PRODUCTS, params, {
      is_admin: true,
      parameters: {
        id: productId
      }
    });
  };

  /**
   * Edit the base products of a given compound product id
   *
   * @param {numeric}  productId  id of compound products base product
   * @param {Object}   data       data to be updated
   *
   * @return {Entity}
   */
  this.editCompoundProducts = function (productId, data) {
    if (isNaN(parseInt(productId))) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_COMPOUND_PRODUCTS, _constants["default"].HTTP_PATCH, data.result, {
      isAdmin: true,
      path: {
        id: productId
      }
    }, null);
  };

  /**
   * Return a collection with all iacpos product rates of a given product id with pagination.
   *
   * @param {number} productId
   * 
   * @returns {Collection}
   */
  this.getProductIacposRates = function (productId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_IACPOS_PRODUCT_RATES, params, {
      is_admin: true,
      parameters: {
        id: productId
      }
    });
  };

  /**
   * Returns the rates for a product in a specific date
   * 
   * @param {number} productId
   * @param {Date} date
   *
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getProductRatesForDate = function (productId, queryDate, dateFrom) {
    var crmIntermediaryId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var currency = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
    if (!queryDate) {
      queryDate = new Date();
    }
    if (!dateFrom) {
      dateFrom = new Date();
    }
    var params = {
      query_date: (0, _dateFns.format)(queryDate, "yyyy-MM-dd"),
      crm_intermediary_id: crmIntermediaryId == null ? null : crmIntermediaryId
    };
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_CORE_PRODUCT_RATES, {
      method: "GET",
      isAdmin: false,
      currency: currency,
      parameters: {
        id: productId,
        date: (0, _dateFns.format)(dateFrom, "yyyy-MM-dd")
      },
      query: params
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res[Object.keys(res)[0]];
    });
  };

  /**
   * Return a collection with all Shared Experiences of a given product id with pagination.
   *
   * @param {number} productId
   * 
   * @returns {Collection}
   */
  this.getProductSharedExperiences = function (productId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_PRODUCT_EXPERIENCES, params, {
      is_admin: true,
      parameters: {
        id: productId
      }
    });
  };

  /**
   * Return a XLS file containing a collection of Products.
   */
  this.exportProducts = function (fields, params, progressHandler) {
    var endpoint = ENDPOINT_GET_PRODUCTS;
    return _this.volcanoClient.makeCollectionExportRequest(endpoint, params, null, fields, "products", progressHandler);
  };
}