"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_DISCOUNT_CODES = "catalog/discount-codes";
var ENDPOINT_GET_DISCOUNT_CODE = ENDPOINT_GET_DISCOUNT_CODES + "/:id";
var ENDPOINT_DISCOUNT_CODE_PRODUCTS = ENDPOINT_GET_DISCOUNT_CODE + "/products";
var ENDPOINT_DISCOUNT_CODE_DELETE_PRODUCT = ENDPOINT_DISCOUNT_CODE_PRODUCTS + "/:product_id";
var _default = exports["default"] = DiscountCodeClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function DiscountCodeClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Discount Code methods */

  /**
   * Return a collection with all discount codes with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getDiscountCodes = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_DISCOUNT_CODES, params);
  };

  /**
   * Return an Entity containing a discount code which match by Id 
   *
   * @param {string} [id]  discount code ID.
   *
   * @returns {Entity}
   */
  this.getDiscountCode = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_DISCOUNT_CODE, id, true, {
      parameters: {
        id: id
      }
    });
  };

  /**
   * Add a discount code
   *
   * @param {Object}   data      data to be updated
   *
   * @returns {Entity}
   */
  this.addDiscountCode = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_DISCOUNT_CODES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Edit a discount code which match by Id 
   *
   * @param {numeric}  id        id of the discount code to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editDiscountCode = function (id, data) {
    if (isNaN(parseInt(id))) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_DISCOUNT_CODE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Delete a discount code which match by Id 
   * 
   * @param {number} id 
   * 
   * @returns {Entity}
   */
  this.deleteDiscountCode = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_DISCOUNT_CODE, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: id
      },
      isAdmin: true
    });
  };

  /**
   * Add a product to a discount code id
   *
   * @param {numeric}  id        id of the discount code to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.addProduct = function (id, data) {
    if (isNaN(parseInt(id))) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_DISCOUNT_CODE_PRODUCTS, _constants["default"].HTTP_POST, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Delete a product from a discount code id
   * 
   * @param {number} id
   * @param {number} product_id
   * 
   * @returns {Entity}
   */
  this.deleteProduct = function (id, product_id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_DISCOUNT_CODE_DELETE_PRODUCT, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: id,
        product_id: product_id
      },
      isAdmin: true
    });
  };
}