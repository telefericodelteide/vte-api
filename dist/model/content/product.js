"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var GET_PRODUCTS_ROOT = "content/products";
var GET_PRODUCT = GET_PRODUCTS_ROOT + "/:id";
var _default = exports["default"] = ProductClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ProductClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
  * Retrieve a collection of Products
  *
  * @param params
  *
  * @returns {Collection}
  */
  this.getProducts = function (params) {
    return _this.volcanoClient.makeCollectionRequest(GET_PRODUCTS_ROOT, params, {
      isAdmin: false
    });
  };

  /**
      * Return the product for ID given.
      *
      * @param productId
      * @param params
      *
      * @returns {Entity}
      */
  this.getProduct = function (productId, params) {
    return _this.volcanoClient.makeEntityRequest(GET_PRODUCT, productId, _constants["default"].HTTP_GET, null, {
      path: {
        id: productId
      },
      isAdmin: false,
      query: params
    }, null);
  };
}