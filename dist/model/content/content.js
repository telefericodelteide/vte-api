"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _site = _interopRequireDefault(require("./site"));
var _experience = _interopRequireDefault(require("./experience"));
var _product = _interopRequireDefault(require("./product"));
var _subcategory = _interopRequireDefault(require("./subcategory"));
var _section = _interopRequireDefault(require("./section"));
var _article = _interopRequireDefault(require("./article"));
var _tag = _interopRequireDefault(require("./tag"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = ContentClient;
/**
 * Container which has the content clients and methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ContentClient(volcanoClient) {
  var _this = this;
  this.site = new _site["default"](volcanoClient);
  this.subcategory = new _subcategory["default"](volcanoClient);
  this.section = new _section["default"](volcanoClient);
  this.article = new _article["default"](volcanoClient);
  this.experience = new _experience["default"](volcanoClient);
  this.product = new _product["default"](volcanoClient);
  this.tag = new _tag["default"](volcanoClient);

  // Deprecate methods
  this.getSites = function (params) {
    return _this.site.getSites(params);
  };
  this.getSite = function (siteId) {
    return _this.site.getSite(siteId);
  };
  this.getExperiences = function (params) {
    return _this.experience.getExperiences(params);
  };
  this.getExperience = function (experienceId) {
    return _this.experience.getExperience(experienceId);
  };
  this.getProducts = function (params) {
    return _this.product.getProducts(params);
  };
  this.getProduct = function (productId) {
    return _this.product.getProduct(productId);
  };
  this.getArticle = function (siteId, sectionId, articleId) {
    return _this.article.getArticle(siteId, sectionId, articleId);
  };
  this.getSubcategories = function (siteId, viewMode) {
    return _this.subcategory.getSubcategories(siteId, viewMode);
  };
  this.getSubcategory = function (siteId, viewMode) {
    return _this.subcategory.getSubcategory(siteId, viewMode);
  };
  this.getFeaturedProducts = function (siteId) {
    return _this.site.getFeaturedProducts(siteId);
  };
  //
}