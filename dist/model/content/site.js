"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../../constants"));
var _vte = _interopRequireDefault(require("../../vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
// Endpoints
var GET_SITES_ROOT = "content/sites";
var GET_SITE = GET_SITES_ROOT + "/:id";
var SITE_CACHE_INVALIDATIONS = "content/sites/:id/cache-invalidations";
var SITE_PUBLISH = "content/sites/:id/publications";
var GET_FEATURED_PRODUCTS = "content/sites/:id/featured-products";
var SET_FEATURED_PRODUCTS = "content/sites/:id/config/featured-products";
var SET_FEATURED_EXPERIENCES = "content/sites/:id/config/featured-experiences";
var GET_MENU = "content/sites/:id/menus/:menu_id";
var ENDPOINT_GET_ENTITY_ACTION_LOGS = 'entity-logs/entity-action-logs';
var _default = exports["default"] = SiteClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SiteClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Site methods */

  /**
  * Return a collection with all sites with pagination.
  *
  * @param {array} [params]  Optional object which contains the parameters to build the
  *                          request, for instance, if you want to move to specific page,
  *                          you must add 'page: 3' to the params object.
  *
  * @returns {Collection}
  */
  this.getSites = function (params) {
    return _this.volcanoClient.makeCollectionRequest(GET_SITES_ROOT, params, {
      isAdmin: false
    });
  };

  /**
  * Return an Entity containing a site which match by Id
  *
  * @param {string} [id]  site ID.
  *
  * @returns {Entity}
  */
  this.getSite = function (id) {
    return _this.volcanoClient.getEntityRequest(GET_SITE, id, false, {
      parameters: {
        id: id
      }
    });
  };

  /**
  * Add a new site
  *
  * @param {Object}   data      data to be updated
  *
  * @returns {Entity}
  */
  this.addSite = function (data) {
    return _this.volcanoClient.makeResourceRequest(GET_SITE, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
  * Make a request to update a site
  *
  * @param {numeric}  id        id of the site to be updated
  * @param {Object}   data      data to be updated
  *
  * @return {Entity}
  */
  this.editSite = function (id, data) {
    if (isNaN(parseInt(id))) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(GET_SITE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
  * (Soft) delete a site
  *
  * @param {string}  id   site Id
  *
  * @return {Entity}
  */
  this.deleteSite = function (id) {
    return _this.volcanoClient.makeResourceRequest(GET_SITE, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };

  /**
  * Clean site cache
  *
  * @param {string}  id   site Id
  *
  * @return {Entity}
  */
  this.invalidateCache = function (id) {
    return _this.volcanoClient.makeResourceRequest(SITE_CACHE_INVALIDATIONS, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };

  /**
  * Publish site
  *
  * @param {string}  id   site Id
  *
  * @return {Entity}
  */
  this.publish = function (id) {
    return _this.volcanoClient.makeResourceRequest(SITE_PUBLISH, _constants["default"].HTTP_POST, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };

  /**
  * Return the featured products for site ID given.
  * 
  * @param {number} siteId 
  * @param {string} viewMode 
  * @returns {Collection}
  */
  this.getFeaturedProducts = function (siteId) {
    var viewMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'compact';
    return _this.volcanoClient.makeCollectionRequest(GET_FEATURED_PRODUCTS, {
      view_mode: viewMode
    }, {
      isAdmin: false,
      parameters: {
        id: siteId
      }
    });
  };

  /**
  * Set featured products from site
  *
  * @param {string} id site Id
  * @param {array} products
  *
  * @return {Entity}
  */
  this.setFeaturedProducts = function (id, products) {
    return _this.volcanoClient.makeEntityRequest(SET_FEATURED_PRODUCTS, id, _constants["default"].HTTP_POST, {
      featured_products: products
    }, {
      isAdmin: true
    }, null);
  };

  /**
  * Set featured experiences from site
  *
  * @param {string} id site Id
  * @param {array} experiences
  *
  * @return {Entity}
  */
  this.setFeaturedExperiences = function (id, experiences) {
    return _this.volcanoClient.makeEntityRequest(SET_FEATURED_EXPERIENCES, id, _constants["default"].HTTP_POST, {
      featured_experiences: experiences
    }, {
      isAdmin: true
    }, null);
  };

  /**
   * Returns the collection of entity action logs for this site
   * 
   * @param {string|null} intermediaryId 
   * @returns {Collection} 
   */
  this.getEntityActionLogs = function (siteId, params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ENTITY_ACTION_LOGS, _objectSpread(_objectSpread({}, params), {}, {
      entity_id: siteId,
      entity_model: 'Sites'
    }), {
      isAdmin: true
    });
  };
}