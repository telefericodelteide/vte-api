"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var GET_SECTION = "content/sites/:siteId/sections/:id";
var _default = exports["default"] = SectionClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SectionClient(volcanoClient) {
  var _this = this;
  /**
       * @var {VolcanoApi}
       */
  this.volcanoClient = volcanoClient;

  /**
      * Return the section for ID given.
      *
      * @param sectionId
      *
      * @returns {Entity}
      */
  this.getSection = function (siteId, sectionId) {
    return _this.volcanoClient.makeEntityRequest(GET_SECTION, sectionId, _constants["default"].HTTP_GET, null, {
      path: {
        id: sectionId,
        siteId: siteId
      },
      isAdmin: false
    }, null);
  };
}