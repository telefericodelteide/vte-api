"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../../constants"));
var _vte = _interopRequireDefault(require("../../vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_TAGS = "content/tags";
var ENDPOINT_GET_TAG = ENDPOINT_GET_TAGS + "/:id";
var ENDPOINT_GET_TAG_GROUPS = ENDPOINT_GET_TAGS + "/groups";
var _default = exports["default"] = TagClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function TagClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Tag methods */

  /**
  * Return a collection with all tags with pagination.
  *
  * @param {array} [params]  Optional object which contains the parameters to build the
  *                          request, for instance, if you want to move to specific page,
  *                          you must add 'page: 3' to the params object.
  *
  * @returns {Collection}
  */
  this.getTags = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_TAGS, params, {
      isAdmin: false
    });
  };

  /**
  * Return an Entity containing a tag which match by Id
  *
  * @param {string} [id]  tag ID.
  *
  * @returns {Entity}
  */
  this.getTag = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_TAG, id, false, {
      parameters: {
        id: id
      }
    });
  };

  /**
  * Return a collection with all tag groups with pagination.
  *
  * @param {array} [params] 
  * 
  * @return {Collection}
  */
  this.getTagGroups = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_TAG_GROUPS, params, {
      isAdmin: false
    });
  };

  /**
  * Add a new tag
  *
  * @param {Object}   data      data to be updated
  *
  * @returns {Entity}
  */
  this.addTag = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TAGS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
  * Make a request to update a tag
  *
  * @param {numeric}  id        id of the tag to be updated
  * @param {Object}   data      data to be updated
  *
  * @return {Entity}
  */
  this.editTag = function (id, data) {
    if (isNaN(parseInt(id))) {
      return null;
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TAG, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
  * (Soft) delete a tag
  *
  * @param {string}  id   tag Id
  *
  * @return {Entity}
  */
  this.deleteTag = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_TAG, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}