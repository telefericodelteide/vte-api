"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var GET_EXPERIENCES_ROOT = "content/experiences";
var GET_EXPERIENCE = GET_EXPERIENCES_ROOT + "/:id";
var _default = exports["default"] = ExperienceClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ExperienceClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
  * Retrieve a collection of Experiences
  *
  * @param params
  *
  * @returns {Collection}
  */
  this.getExperiences = function (params) {
    return _this.volcanoClient.makeCollectionRequest(GET_EXPERIENCES_ROOT, params, {
      isAdmin: false
    });
  };

  /**
  * Return the experience for ID given.
  *
  * @param experienceId
  *
  * @returns {Entity}
  */
  this.getExperience = function (experienceId) {
    return _this.volcanoClient.getEntityRequest(GET_EXPERIENCE, experienceId, false);
  };
}