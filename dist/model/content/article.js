"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var GET_ARTICLE = "content/sites/:siteId/sections/:sectionId/articles/:id";
var _default = exports["default"] = ArticleClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ArticleClient(volcanoClient) {
  var _this = this;
  /**
       * @var {VolcanoApi}
       */
  this.volcanoClient = volcanoClient;

  /**
      * Return the article for ID given.
      *
      * @param articleId
      *
      * @returns {Entity}
      */
  this.getArticle = function (siteId, sectionId, articleId) {
    return _this.volcanoClient.makeEntityRequest(GET_ARTICLE, articleId, _constants["default"].HTTP_GET, null, {
      path: {
        id: articleId,
        sectionId: sectionId,
        siteId: siteId
      },
      isAdmin: false
    }, null);
  };
}