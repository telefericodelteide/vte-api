"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _entity = _interopRequireDefault(require("./entity/entity"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_COLLABORATORS = 'colaborators';
var ENDPOINT_GET_COLLABORATOR = 'colaborators/:id';
var _default = exports["default"] = CollaboratorClient;
/**
 * Container which has the collaborator methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CollaboratorClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of collaborators that matches the query parameters passed.
   * 
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection} 
   */
  this.getCollaborators = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_COLLABORATORS, params, {
      isAdmin: false
    });
  };

  /**
   * Returns the collaborator that matches the id passed.
   * 
   * @param {string} collaboratorId 
   * @returns {Entity}
   */
  this.getCollaborator = function (collaboratorId) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_COLLABORATOR, collaboratorId, false);
  };
}