"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../vte"));
var _constants = _interopRequireDefault(require("../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_CONTACTS_SCHEMA = 'crm/contacts/schema';
var ENDPOINT_GET_REFUNDS_SCHEMA = 'refunds/schema';
var ENDPOINT_GET_CRM_SCHEMA = 'crm/schema';
var ENDPOINT_GET_NOTIFICATIONS_SCHEMA = 'notification-templates/schema';
var ENDPOINT_GET_PRODUCTS_SCHEMA = 'catalog/products/schema';
var _default = exports["default"] = SchemaClient;
/**
 * Container which has the schemas to get form config to create/edit an entity
 *
 * @param {VolcanoApi} volcanoClient
 */
function SchemaClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;
  this.getContactsSchema = function () {
    return _this.getSchema(ENDPOINT_GET_CONTACTS_SCHEMA, true);
  };
  this.getRefundsSchema = function () {
    return _this.getSchema(ENDPOINT_GET_REFUNDS_SCHEMA, true);
  };
  this.getCrmIntermediariesSchema = function () {
    return _this.getSchema(ENDPOINT_GET_CRM_SCHEMA, true);
  };
  this.getNotificationsSchema = function () {
    return _this.getSchema(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, false);
  };
  this.getEntitySchema = function (entity) {
    switch (entity) {
      case 'contact_schema':
        return _this.getSchema(ENDPOINT_GET_CONTACTS_SCHEMA, true);
      case 'crm_intermediaries_schema':
        return _this.getSchema(ENDPOINT_GET_CRM_SCHEMA, true);
      case 'notifications_schema':
        return _this.getSchema(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, false);
      case 'products_schema':
        return _this.getSchema(ENDPOINT_GET_PRODUCTS_SCHEMA, true);
      default:
        return _this.getSchema(ENDPOINT_GET_REFUNDS_SCHEMA, true);
    }
  };
  this.getSchema = function (endpoint, isAdmin) {
    var request = _this.volcanoClient.prepareRequest(endpoint, {
      method: _constants["default"].HTTP_GET,
      isAdmin: isAdmin
    });
    return _this.volcanoClient.makeRequest(request);
  };
}