"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _entity = _interopRequireDefault(require("./entity/entity"));
var _lodash = require("lodash");
var _dateFnsTz = require("date-fns-tz");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function _slicedToArray(r, e) { return _arrayWithHoles(r) || _iterableToArrayLimit(r, e) || _unsupportedIterableToArray(r, e) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(r, a) { if (r) { if ("string" == typeof r) return _arrayLikeToArray(r, a); var t = {}.toString.call(r).slice(8, -1); return "Object" === t && r.constructor && (t = r.constructor.name), "Map" === t || "Set" === t ? Array.from(r) : "Arguments" === t || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(t) ? _arrayLikeToArray(r, a) : void 0; } }
function _arrayLikeToArray(r, a) { (null == a || a > r.length) && (a = r.length); for (var e = 0, n = Array(a); e < a; e++) n[e] = r[e]; return n; }
function _iterableToArrayLimit(r, l) { var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"]; if (null != t) { var e, n, i, u, a = [], f = !0, o = !1; try { if (i = (t = t.call(r)).next, 0 === l) { if (Object(t) !== t) return; f = !1; } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0); } catch (r) { o = !0, n = r; } finally { try { if (!f && null != t["return"] && (u = t["return"](), Object(u) !== u)) return; } finally { if (o) throw n; } } return a; } }
function _arrayWithHoles(r) { if (Array.isArray(r)) return r; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
/** Endpoints */

// Bookings
var ENDPOINT_GET_BOOKINGS = 'bookings';
var ENDPOINT_GET_PRODUCTS_FOR_CHANGE = ENDPOINT_GET_BOOKINGS + '/available-products-change';

// Booking
var ENDPOINT_GET_BOOKING = ENDPOINT_GET_BOOKINGS + '/:id';
var BOOKING_GET_RELATED_BOOKINGS = ENDPOINT_GET_BOOKING + '/bookings';
var BOOKING_GET_HISTORY = ENDPOINT_GET_BOOKING + '/booking-history';
var BOOKING_ADD_COMMENT = ENDPOINT_GET_BOOKING + '/add-comment';
var BOOKING_SET_VOUCHER = ENDPOINT_GET_BOOKING + '/set-voucher';
var BOOKING_SET_NOTES = ENDPOINT_GET_BOOKING + '/set-notes';
var BOOKING_SET_GIFT = ENDPOINT_GET_BOOKING + '/set-gift';
var BOOKING_ASSIGN_PAX_MANAGER_COLLABORATOR = ENDPOINT_GET_BOOKING + '/assign-pax-manager-collaborator';
var BOOKING_CHANGE_BOOKING_DATE = ENDPOINT_GET_BOOKING + '/change-booking-date';
var BOOKING_CHANGE_PRODUCT_REQUEST = ENDPOINT_GET_BOOKING + '/product-change-request';
var BOOKING_CHANGE_RATES_REQUEST = ENDPOINT_GET_BOOKING + '/rates-change-request';
var BOOKING_CHANGE_PARTICIPANTS = ENDPOINT_GET_BOOKING + '/participants';
var BOOKING_CHANGE_PICKUP_POINT = ENDPOINT_GET_BOOKING + '/change-pickup-point';
var BOOKING_CANCEL_REQUEST = ENDPOINT_GET_BOOKING + '/cancellation-request';
var BOOKING_SET_NO_SHOW = ENDPOINT_GET_BOOKING + '/no-show';
var BOOKING_REVERT_NO_SHOW = ENDPOINT_GET_BOOKING + '/revert-no-show';
var BOOKING_SET_VALIDATION_DATE = ENDPOINT_GET_BOOKING + '/exchange-date';
var BOOKING_SEND_EMAIL = ENDPOINT_GET_BOOKING + '/send-email';
var BOOKING_PDF = ENDPOINT_GET_BOOKING + '/pdf';
var BOOKING_QR_CODES = ENDPOINT_GET_BOOKING + '/qr-codes';
var BOOKING_GET_BOOKING_CHECK_ACTION = ENDPOINT_GET_BOOKING + '/check-action/:action';
var BOOKING_CHANGE_CONFIRM = ENDPOINT_GET_BOOKING + '/confirm';
var BOOKING_CONFIRM_PAYMENT = ENDPOINT_GET_BOOKING + '/confirm-payment';
var BOOKING_LOCK = ENDPOINT_GET_BOOKING + '/lock';
var BOOKING_UNLOCK = ENDPOINT_GET_BOOKING + '/unlock';
var BOOKING_UNLOCK_ALL = ENDPOINT_GET_BOOKINGS + '/unlock-all';
var BOOKING_CHANGE_MANAGED_STATE = ENDPOINT_GET_BOOKING + '/managed';

// Booking validations
var ENDPOINT_ADD_VALIDATION_BY_LOCATOR = ENDPOINT_GET_BOOKINGS + '/validations';
var ENDPOINT_BOOKING_GET_VALIDATIONS = ENDPOINT_GET_BOOKING + '/validations';
var ENDPOINT_BOOKING_GET_VALIDATION = ENDPOINT_BOOKING_GET_VALIDATIONS + '/:validation_id';

// Payment transaction
var BOOKING_GET_TRANSACTION_BOOKINGS = 'payment-transactions/:id/client-result';

// Booking constants
var BOOKING_DATE_NULL = "1980-01-01 00:00:00";
var _default = exports["default"] = BookingClient;
/**
 * Container which has the booking methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function BookingClient(volcanoClient) {
  var _this = this;
  // This is under admin namespace.
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Return a collection with all Bookings with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getBookings = function (params) {
    _this.extendEntity(_this);
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_BOOKINGS, params);
  };

  /**
   * Return a collection with all Bookings with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getRelatedBookings = function (bookingId, params) {
    return _this.volcanoClient.makeCollectionRequest(BOOKING_GET_RELATED_BOOKINGS, params, {
      isAdmin: true,
      parameters: {
        id: bookingId
      }
    });
  };
  this.exportBookings = function (fields, params, progressHandler) {
    return _this.volcanoClient.makeCollectionExportRequest(ENDPOINT_GET_BOOKINGS, params, null, fields, 'bookings', progressHandler);
  };

  /**
   * Return a booking from transaction
   *
   * @param transactionId
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getBookingsFromTransaction = function (transactionId) {
    var request = _this.volcanoClient.prepareRequest(BOOKING_GET_TRANSACTION_BOOKINGS, {
      method: 'GET',
      isAdmin: true,
      parameters: {
        id: transactionId
      },
      query: {
        sid: _this.volcanoClient.cart.getSessionId()
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.bookings;
    });
  };

  /**
   * Return an Entity containing a booking which match by Id `bookingId`.
   *
   * @param {BigInteger} [bookingId]  booking ID.
   *
   * @returns {Entity}
   */
  this.getBooking = function (bookingId, params) {
    _this.extendEntity();
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_BOOKING, bookingId, true, params);
  };

  /**
   * Return a booking from transaction
   *
   * @param bookingId
   * @param action
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.checkBookingAction = function (bookingId, action) {
    if (!(bookingId && action)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(BOOKING_GET_BOOKING_CHECK_ACTION, {
      method: 'GET',
      isAdmin: true,
      parameters: {
        id: bookingId,
        action: action
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return {
        allowed: res.allowed
      };
    });
  };

  /**
   * Return a history list from booking
   *
   * @param {number} bookingId  booking ID.
   * @param {object} params Query parameters
   * 
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getBookingHistory = function (bookingId, params) {
    params = params || {};
    return _this.volcanoClient.makeCollectionRequest(BOOKING_GET_HISTORY, params, {
      isAdmin: true,
      parameters: {
        id: bookingId
      }
    });
  };

  /**
   * Return a the validations of a booking
   *
   * @param {number} bookingId booking ID.
   * @param {object} params Query parameters
   * 
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.getValidations = function (bookingId, params) {
    params = params || {};
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_BOOKING_GET_VALIDATIONS, params, {
      isAdmin: true,
      parameters: {
        id: bookingId
      }
    });
  };

  /**
   * Delete a validation or all validations from a booking. 
   * 
   * @param {number} bookingId 
   * @param {number|null} validationId 
   * 
   * @returns {PromiseLike<*> | Promise<*>}
   */
  this.deleteValidation = function (bookingId, validationId) {
    return _this.volcanoClient.makeResourceRequest(validationId ? ENDPOINT_BOOKING_GET_VALIDATION : ENDPOINT_BOOKING_GET_VALIDATIONS, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: bookingId,
        validation_id: validationId
      },
      isAdmin: true
    });
  };

  /**
   * Requests a validation for a booking or booking ticket by locator 
   * @param {*} params validation request data
   *  {
   *     locator: string,                 // booking or ticket locator
   *     date: string,                    // validation date
   *     access_control_point_id: string, // access control point id
   *     activity_id: string,             // activity id
   *     coordinates: {                   // coordinates of the validation
   *        lat: float,
   *        lon: float,
   *        status: string                // status of the coordinates (ignored, available, unavailable, denied)
   *     }
   *  }
   * 
   * @returns 
   */
  this.addValidationFromLocator = function (params) {
    params = params || {};
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_ADD_VALIDATION_BY_LOCATOR, null, _constants["default"].HTTP_POST, params, {
      isAdmin: true
    }, null);
  };

  /**
   * Returns the collection of products available for change for a collection of bookings.
   * 
   * @param {number} experienceId
   * @param {array} bookings bookings data required for the endpoint
   * 
   * @returns {Collection} 
   */
  this.getProductsForChange = function (experienceId, bookings) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_PRODUCTS_FOR_CHANGE, {
      method: 'POST',
      isAdmin: true,
      query: {
        experience_id: experienceId,
        limit: 1000
      },
      data: {
        bookings: (bookings !== null && bookings !== void 0 ? bookings : []).map(function (booking) {
          return {
            created: (0, _lodash.get)(booking, "order.created"),
            enterprise_id: (0, _lodash.get)(booking, "order.enterprise.id"),
            site_id: (0, _lodash.get)(booking, "order.site.id"),
            collaborator_id: (0, _lodash.get)(booking, "order.collaborator.id"),
            crm_intermediary_id: (0, _lodash.get)(booking, "order.collaborator.crm_intermediary_id"),
            product_id: (0, _lodash.get)(booking, "product.id"),
            booking_date: (0, _lodash.get)(booking, "booking_date"),
            billing_type: (0, _lodash.get)(booking, "order.billing_type"),
            currency_code: (0, _lodash.get)(booking, "order.currency"),
            booking_rates: booking.product_rates.map(function (bookingRate) {
              return {
                customer_type_ids: bookingRate.customer_type_ids,
                qty: bookingRate.qty,
                total_amount: bookingRate.total_amount
              };
            })
          };
        })
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.products;
    });
  };

  /**
   * Add comment to booking
   *
   * @param {string | number} bookingId Booking ID
   * @param {string} comment Comment text
   *
   * @returns {Entity}
   */
  this.addComment = function (bookingId, comment) {
    return _this.volcanoClient.makeEntityRequest(BOOKING_ADD_COMMENT, bookingId, _constants["default"].HTTP_POST, {
      comment: comment
    }, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Set voucher for a given booking
   *
   * @param {string | number} bookingId Booking ID
   * @param {string} voucher voucher id
   *
   * @returns {Entity}
   */
  this.setVoucher = function (bookingId, voucher) {
    return _this.volcanoClient.makeEntityRequest(BOOKING_SET_VOUCHER, bookingId, _constants["default"].HTTP_POST, {
      voucher_id: voucher
    }, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Set note for a given booking
   *
   * @param {string | number} bookingId Booking ID
   * @param {string} notes Note
   *
   * @returns {Entity}
   */
  this.setNotes = function (bookingId, notes) {
    return _this.volcanoClient.makeEntityRequest(BOOKING_SET_NOTES, bookingId, _constants["default"].HTTP_POST, {
      notes: notes
    }, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Set gift info for a given booking
   *
   * @param {string | number} bookingId Booking ID
   * @param {string} gift Gift info
   *
   * @returns {Entity}
   */
  this.setGift = function (bookingId, gift) {
    return _this.volcanoClient.makeEntityRequest(BOOKING_SET_GIFT, bookingId, _constants["default"].HTTP_POST, {
      gift: gift
    }, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Change the date of a booking given.
   *
   * @param { number | string } bookingId  id of the booking.
   * @param {date} date  The data containing the new date to be set for that booking.
   *                      {
   *                        "reservation_date" : "2020-10-11 14:00:00"
   *                      }
   *
   * @returns {Entity}
   */
  this.changeBookingDate = function (bookingId, date) {
    var bookingDate = date ? (0, _dateFnsTz.format)(date, 'yyyy-MM-dd HH:mm:ss', {
      timeZone: 'Atlantic/Canary'
    }) : BOOKING_DATE_NULL;
    var dateJsonObject = {
      booking_date: bookingDate
    };
    var result = _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_BOOKING_DATE, bookingId, _constants["default"].HTTP_POST, dateJsonObject, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
    return result;
  };

  /**
   * Delete booking date from a given booking.
   *
   * @param {string | number} bookingId Booking ID
   *
   * @returns {Entity}
   */
  this.deleteBookingDate = function (bookingId) {
    return _this.changeBookingDate(bookingId);
  };

  /**
   * Do a product change and return an Entity with changes.
   *
   * @param { number | string } bookingId
   * @param { object } data This object must be in the following form:
   *                        {
   *                          "product_id": Product Id to be changed,
   *                          "booking_date": New booking date (optional)
   *                        }
   * @return { Entity }
   */
  this.changeProduct = function (bookingId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_PRODUCT_REQUEST, bookingId, _constants["default"].HTTP_POST, data, {
      isAdmin: true,
      path: {
        id: bookingId
      }
    }, null);
  };

  /**
   * Do a change rate and return the Entity
   *
   * @param {number | string } bookingId
   * @param { object } data   This should have the following format.
   *                          {
   *                            "product_id": 1785,
   *                            "product_rates": [
   *                              {
   *                                "id": 2273918, (if rate exists in booking)
   *                                "rate_id": 15810, (if it's a new rate in booking)
   *                                "qty": 4,
   *                                "unit_price": 28.5, (optional)
   *                                "participants": [
   *                                  {
   *                                      "id":89880, (if participant exists in booking rate)
   *                                      "first_name":"00000000A",
   *                                      "last_name":"11111111A",
   *                                      "id_card":"00000000A"
   *                                  },
   *                                  {
   *                                      "id":89881,
   *                                      "deleted":true
   *                                  },
   *                                  ...
   *                                ]
   *                              },
   *                              ...
   *                            ]
   *                          }
   *
   * @retuen { Entity }
   */
  this.changeRates = function (bookingId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_RATES_REQUEST, bookingId, _constants["default"].HTTP_POST, data, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Do a participants change and return the Entity
   *
   * @param {number | string } bookingId
   * @param { object } data   This should have the following format.
   *                          {
   *                            "participants": [
   *                              {
   *                                  "id":89880,
   *                                  "first_name":"00000000A",
   *                                   "last_name":"11111111A",
   *                                   "id_card":"00000000A"
   *                               },
   *                              ...
   *                            ]
   *                          }
   *
   * @retuen { Entity }
   */
  this.changeParticipants = function (bookingId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_PARTICIPANTS, bookingId, _constants["default"].HTTP_POST, data, {
      path: {
        id: bookingId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Let to change the pickup in the booking record.
   *
   * @param {string | number} bookingId   booking ID
   * @param {object} data Pickup data to be updated.
   *                      The data should be in the following format
   *
   *                      {
   *                          "pickup_point_id": "eea4f932-8c3b-41a4-ba27-f89f2499ed1d",
                              "lodgin_id": "f0e22021-66fa-4f27-b06c-348a976c7c7b"
   *                      }
   *
   * @returns {Entity}
   */
  this.changePickupPoint = function (bookingId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_PICKUP_POINT, bookingId, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };

  /**
   * Assign some booking rates pax to some collaborator.
   *
   * @param {string | number} bookingId   booking ID
   * @param {object} data Data to perform action.
   *                      The data should be in the following format
   *
   *                      {
   *                          "manager_collaborator_id": 35733,
   *                          "booking_rates": [
   *                              {
   *                                  "id": 2087453,
   *                                  "qty": 1
   *                              }
   *                          ]
   *                      }
   *
   * @returns {Entity}
   */
  this.assignPaxManagerCollaborator = function (bookingId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_ASSIGN_PAX_MANAGER_COLLABORATOR, bookingId, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };

  /**
   * Do a booking Cancellation or make a request to cancel
   * depends on if a refund is needed.
   *
   * @param {numeric} bookingId Id of the booking which want to be cancelled.
   * @param {object}  data      Data to perform action.
   *
   * @return {Entity}
   */
  this.cancelBooking = function (bookingId, data) {
    return _this.executeAction(bookingId, data, BOOKING_CANCEL_REQUEST);
  };

  /**
   * Sets booking no show.
   *
   * @param {number} bookingId id of the booking
   * @param {object}  data      data to executes the action
   *
   * @return {Entity}
   */
  this.setNoShow = function (bookingId, data) {
    return _this.executeAction(bookingId, data, BOOKING_SET_NO_SHOW);
  };

  /**
   * Revert booking no show.
   *
   * @param {number} bookingId id of the booking
   *
   * @return {Entity}
   */
  this.revertNoShow = function (bookingId) {
    return _this.executeAction(bookingId, null, BOOKING_REVERT_NO_SHOW);
  };

  /**
   * Sets booking validation date.
   *
   * @param {number} bookingId id of the booking
   * @param {object}  data      data to executes the action
   *
   * @return {Entity}
   */
  this.setValidationDate = function (bookingId, data) {
    return _this.executeAction(bookingId, data, BOOKING_SET_VALIDATION_DATE);
  };

  /**
   * Update booking requires_confirmed to 2 (state confirmed).
   *
   * @param bookingId
   *
   * @returns {null|Object|Entity}
   */
  this.confirm = function (bookingId) {
    return _this.executeAction(bookingId, null, BOOKING_CHANGE_CONFIRM);
  };

  /**
   * Confirms payment.
   *
   * @param {number} bookingId id of the booking
   *
   * @return {Entity}
   */
  this.confirmPayment = function (bookingId) {
    return _this.executeAction(bookingId, null, BOOKING_CONFIRM_PAYMENT);
  };

  /**
   * 
   * @param {number} bookingId id of the booking
   * @param {object}  data      data to executes the action
   * @param {string}  endpoint  action endpoint
   * 
   * @returns {Entity}
   */
  this.executeAction = function (bookingId, data, endpoint) {
    var id = parseInt(bookingId);
    if (isNaN(bookingId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(endpoint, id, _constants["default"].HTTP_POST, data, {
      path: {
        id: id
      },
      isAdmin: true
    }, null);
  };

  /**
   * Send the emails regarding with the given booking.
   *
   * @param bookingId
   * @param emails
   *
   * @returns {null|Object|Entity}
   */
  this.sendBookingEmail = function (bookingId, emails) {
    var emailAddresses = emails || {};
    var id = parseInt(bookingId);
    if (isNaN(bookingId)) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(BOOKING_SEND_EMAIL, id, _constants["default"].HTTP_POST, emailAddresses, {
      path: {
        id: id
      },
      isAdmin: true
    }, null);
  };

  /**
   * Return an application/PDF octet
   *
   * @param bookingId
   *
   * @returns {null|Object|Entity}
   */
  this.getBookingPdf = function (bookingId, sid, viewMode) {
    var params = _objectSpread(_objectSpread({}, sid && {
      sid: sid
    }), viewMode && {
      view_mode: viewMode
    });
    var id = parseInt(bookingId);
    if (isNaN(id)) {
      return null;
    }
    return _this.volcanoClient.makeFileRequest(BOOKING_PDF, id, true, params);
  };

  /**
   * Return the booking QR codes
   *
   * @param bookingId
   * @param sid
   *
   * @returns {null|Object|Entity}
   */
  this.getBookingQrCodes = function (bookingId, sid) {
    var id = parseInt(bookingId);
    if (isNaN(id)) {
      return null;
    }
    var request = _this.volcanoClient.prepareRequest(BOOKING_QR_CODES, {
      method: 'GET',
      isAdmin: false,
      parameters: {
        id: id
      },
      query: {
        sid: sid
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.qr_codes;
    });
  };
  this.exportBookings = function (fields, params, progressHandler) {
    var endpoint = 'bookings';
    return _this.volcanoClient.makeCollectionExportRequest(endpoint, params, 100, fields, 'bookings', progressHandler);
  };
  this.extendEntity = function () {
    _entity["default"].prototype.getTotalQuantity = function () {
      return (0, _lodash.get)(this, 'product_rates').reduce(function (acc, rate) {
        return acc += rate.qty;
      }, 0);
    };
    _entity["default"].prototype.getRelatedEntities = /*#__PURE__*/_asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
      var _this2 = this;
      var promises;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            promises = [volcanoClient.refund.getRefunds({
              booking_id: (0, _lodash.get)(this, 'id'),
              order_id: (0, _lodash.get)(this, 'order.id'),
              sort_by_created: 'desc'
            }), volcanoClient.invoice.getInvoices({
              order_id: (0, _lodash.get)(this, 'order.id')
            })];
            return _context.abrupt("return", Promise.all(promises).then(function (_ref2) {
              var _ref3 = _slicedToArray(_ref2, 2),
                refunds = _ref3[0],
                invoices = _ref3[1];
              (0, _lodash.set)(_this2, "refunds", refunds.getCount() > 0 ? refunds.getItems() : []);
              (0, _lodash.set)(_this2, "invoices", invoices.getCount() > 0 ? invoices.getItems() : []);
              return _this2;
            })["catch"](function (error) {
              (0, _lodash.set)(this, "refunds", null);
              (0, _lodash.set)(this, "invoices", null);
              return this;
            }));
          case 2:
          case "end":
            return _context.stop();
        }
      }, _callee, this);
    }));
    _entity["default"].prototype.getPendingRefund = /*#__PURE__*/_asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
      var pendingRefund;
      return _regeneratorRuntime().wrap(function _callee2$(_context2) {
        while (1) switch (_context2.prev = _context2.next) {
          case 0:
            pendingRefund = null;
            if ((0, _lodash.get)(this, 'state') === 'refund_requested') {
              pendingRefund = (0, _lodash.isEmpty)((0, _lodash.get)(this, 'refunds')) ? null : (0, _lodash.get)(this, 'refunds')[0];
            }
            return _context2.abrupt("return", pendingRefund);
          case 3:
          case "end":
            return _context2.stop();
        }
      }, _callee2, this);
    }));
    _entity["default"].prototype.getAvailableActions = /*#__PURE__*/_asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
      var actions, promises;
      return _regeneratorRuntime().wrap(function _callee3$(_context3) {
        while (1) switch (_context3.prev = _context3.next) {
          case 0:
            actions = this.getActions();
            promises = [this.getPendingRefund()];
            return _context3.abrupt("return", Promise.all(promises).then(function (_ref6) {
              var _ref7 = _slicedToArray(_ref6, 1),
                pendingRefund = _ref7[0];
              return {
                change_date: !(0, _lodash.isEmpty)(actions['booking_date_change']),
                cancel: !(0, _lodash.isEmpty)(actions['cancellation_request']),
                cancel_refund: !(0, _lodash.isEmpty)(pendingRefund) && !(0, _lodash.isEmpty)(pendingRefund.getActions()['cancel_refund']),
                request_invoice: !(0, _lodash.isEmpty)(actions['create_customer_invoice'])
              };
            })["catch"](function (error) {
              return {
                change_date: !(0, _lodash.isEmpty)(actions['booking_date_change']),
                cancel: !(0, _lodash.isEmpty)(actions['cancellation_request']),
                cancel_refund: false,
                request_invoice: !(0, _lodash.isEmpty)(actions['create_customer_invoice'])
              };
            }));
          case 3:
          case "end":
            return _context3.stop();
        }
      }, _callee3, this);
    }));
  };

  /**
   * Lock booking for managed in routes
   *
   * @param bookingId
   *
   * @returns {Object|Entity|null}
   */
  this.lockBooking = function (bookingId) {
    if (isNaN(bookingId)) {
      return null;
    }
    var id = parseInt(bookingId);
    return _this.volcanoClient.makeEntityRequest(BOOKING_LOCK, id, _constants["default"].HTTP_POST, null, {
      path: {
        id: id
      },
      isAdmin: true
    }, null);
  };

  /**
   * Unlock booking for managed in routes
   *
   */
  this.unlockBooking = function (bookingId) {
    if (isNaN(bookingId)) {
      return null;
    }
    var id = parseInt(bookingId);
    return _this.volcanoClient.makeEntityRequest(BOOKING_UNLOCK, id, _constants["default"].HTTP_DELETE, null, {
      path: {
        id: id
      },
      isAdmin: true
    }, null);
  };

  /**
   * Unlock all bookings
   *
   * @param params
   * @returns {Object|Entity|null}
   */
  this.unlockAllBookings = function (params) {
    params = params || {};
    return _this.volcanoClient.makeEntityRequest(BOOKING_UNLOCK_ALL, null, _constants["default"].HTTP_DELETE, params, {
      isAdmin: true
    }, null);
  };

  /**
   * Change booking managed state
   *
   * @param bookingId
   * @param data
   * @returns {Object|Entity|null}
   */
  this.changeManagedState = function (bookingId, data) {
    if (isNaN(bookingId) || !data) {
      return null;
    }
    var id = parseInt(bookingId);
    return _this.volcanoClient.makeEntityRequest(BOOKING_CHANGE_MANAGED_STATE, id, _constants["default"].HTTP_PATCH, data, {
      path: {
        id: id
      },
      isAdmin: true
    }, null);
  };
}