"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isEmpty = _interopRequireDefault(require("lodash/isEmpty"));
var _constants = _interopRequireDefault(require("../constants"));
var _vte = _interopRequireDefault(require("../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_SUGGESTIONS = "suggestions";
var ENDPOINT_GET_SUGGESTION = ENDPOINT_GET_SUGGESTIONS + "/:id";
var ENDPOINT_SUGGESTION_SET_MANAGED = ENDPOINT_GET_SUGGESTION + "/set-managed";
var _default = exports["default"] = SuggestionClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SuggestionClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** Suggestion methods */

  this.addSuggestion = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_SUGGESTIONS, _constants["default"].HTTP_POST, data, {
      isAdmin: false
    });
  };

  /**
   * Return a collection with all suggestions with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getSuggestions = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_SUGGESTIONS, params);
  };

  /**
   * Return an Entity containing a suggestion which match by Id `suggestionId`.
   *
   * @param {string} [id]  suggestion ID.
   *
   * @returns {Entity}
   */
  this.getSuggestion = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_SUGGESTION, id, true, {
      parameters: {
        id: id
      }
    });
  };

  /**
  * Set as 'managed' a suggestion that matches the id passed.
  * 
  * @param {string} id
  * @param {boolean} managed 
  * @returns 
  */
  this.setSuggestionManaged = function (id, managed) {
    if ((0, _isEmpty["default"])(id)) {
      return null;
    }
    ;
    return _this.volcanoClient.makeEntityRequest(ENDPOINT_SUGGESTION_SET_MANAGED, id, _constants["default"].HTTP_POST, {
      managed: managed
    }, {
      path: {
        id: id
      },
      isAdmin: true
    });
  };
}