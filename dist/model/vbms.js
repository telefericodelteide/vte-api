"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_CONFIG = 'vbms/config';
var ENDPOINT_GET_USER_SETTINGS = 'vbms/user-settings/:id';
var _default = exports["default"] = VbmsClient;
/**
 * Uses to access Volcano API VBMS endpoints.
 * 
 * @constructor VbmsClient
 * @param {VolcanoClient} volcanoClient 
 */
function VbmsClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.getConfig = function (params) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_CONFIG, {
      method: _constants["default"].HTTP_GET,
      isAdmin: false
    });
    return _this.volcanoClient.makeRequest(request);
  };
  this.getUserSettings = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_USER_SETTINGS, id, false);
  };
}