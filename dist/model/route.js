"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _dateFns = require("date-fns");
/**
 * Volcano API route client
 *
 * The RouteClient class has all the required functionalities to query and manage routes with the Volcano REST API.
 *
 * @file   This files defines the RouteClient class.
 * @author Roberto Muñoz (rmunglez@gmail.com)
 * @since  0.1
 */
var _default = exports["default"] = RouteClient;
/**
 * Uses to access Volcano API Route endpoints.
 * 
 * @constructor RouteClient
 * @param {VolcanoClient} volcanoClient 
 */
function RouteClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;
  this.baseEndpoint = 'routes';
  this.getPickupData = function (productId, date) {
    var endpoint = 'routes/product/:product_id/:date';
    var request = _this.volcanoClient.prepareRequest(endpoint, {
      method: "GET",
      isAdmin: false,
      parameters: {
        product_id: productId,
        date: (0, _dateFns.format)(date, "yyyy-MM-dd")
      }
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return res.route;
    })["catch"](function (err) {
      if (err instanceof Error && err.type == "NOT FOUND") {
        return null;
      }
      throw err;
    });
  };
}