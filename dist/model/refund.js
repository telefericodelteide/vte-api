"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.REFUND_GET_REFUNDS = exports.REFUND_GET_REFUND = void 0;
var _vte = _interopRequireDefault(require("../vte"));
var _entity = _interopRequireDefault(require("./entity/entity"));
var _constants = _interopRequireDefault(require("../constants"));
var _lodash = require("lodash");
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = RefundClient; // Endpoints
var REFUND_GET_REFUNDS = exports.REFUND_GET_REFUNDS = "refunds";
var REFUND_GET_REFUND = exports.REFUND_GET_REFUND = REFUND_GET_REFUNDS + '/:id';
var REFUND_ADD_COMMENT = REFUND_GET_REFUND + '/add-comment';
var REFUND_ASSIGN = REFUND_GET_REFUND + '/assign';
var REFUND_ACCEPT = REFUND_GET_REFUND + '/accept';
var REFUND_REJECT = REFUND_GET_REFUND + '/reject';
var REFUND_CANCEL_REFUND = REFUND_GET_REFUND + "/cancel";
var REFUND_COST_REFUND = REFUND_GET_REFUND + "/refund-cost";
/**
 * Container which has the booking refunds methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function RefundClient(volcanoClient) {
  var _this = this;
  // This is under admin namespace.
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Return a collection with all Refunds with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getRefunds = function (params) {
    return _this.volcanoClient.makeCollectionRequest(REFUND_GET_REFUNDS, params);
  };

  /**
   * Return an Entity containing a refund which match by Id `refundId`.
   *
   * @param {string} [refundId]  refund ID.
   *
   * @returns {Entity}
   */
  this.getRefund = function (refundId, params) {
    return _this.volcanoClient.getEntityRequest(REFUND_GET_REFUND, refundId, true, params);
  };
  this.exportRefunds = function (fields, params, progressHandler) {
    return _this.volcanoClient.makeCollectionExportRequest(REFUND_GET_REFUNDS, params, null, fields, 'refunds', progressHandler);
  };

  /**
   * Add comment to refund
   *
   * @param {string} refundId Refund ID
   * @param {string} comment Comment text
   *
   * @returns {Entity}
   */
  this.addComment = function (refundId, comment) {
    return _this.volcanoClient.makeEntityRequest(REFUND_ADD_COMMENT, refundId, _constants["default"].HTTP_POST, {
      comment: comment
    }, {
      path: {
        id: refundId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Assign refund to user and return the Entity
   *
   * @param { string } refundId
   * @param { string } userId User to assign the refund (optional)
   *
   * @return { Entity }
   */
  this.assign = function (refundId) {
    var userId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var data = userId === null ? {} : {
      user_id: userId
    };
    return _this.volcanoClient.makeEntityRequest(REFUND_ASSIGN, refundId, _constants["default"].HTTP_POST, data, {
      path: {
        id: refundId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Accept refund and return the Entity
   *
   * @param { string } refundId
   * @param { object } data   This should have the following format.
   *                          {
   *                            "cost": 5.05,
   *                            "manual_refund": true,
   *                            "comment": "Comment text..."
   *                          }
   *
   *
   * @return { Entity }
   */
  this.accept = function (refundId, data) {
    if (data === null) {
      return null;
    }
    return _this.volcanoClient.makeEntityRequest(REFUND_ACCEPT, refundId, _constants["default"].HTTP_POST, data, {
      path: {
        id: refundId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Reject refund and return the Entity
   *
   * @param { string } refundId
   * @param { boolean } notifyClient True to notify client, false otherwise
   *
   * @return { Entity }
   */
  this.reject = function (refundId) {
    var notifyClient = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    return _this.volcanoClient.makeEntityRequest(REFUND_REJECT, refundId, _constants["default"].HTTP_POST, {
      notify_client: notifyClient
    }, {
      path: {
        id: refundId
      },
      isAdmin: true
    }, null);
  };

  /**
   * Do a refund refund cancellation.
   *
   * @param {string}  refundId     Id of the refund which want to be cancelled.
   *
   * @return {Entity}
   */
  this.cancelRefund = function (refundId, params) {
    if ((0, _lodash.isEmpty)(refundId)) {
      return null;
    }
    params = (0, _lodash.isObject)(params) ? params : {};
    return _this.volcanoClient.makeEntityRequest(REFUND_CANCEL_REFUND, refundId, _constants["default"].HTTP_POST, null, {
      path: {
        id: refundId
      },
      isAdmin: true,
      query: params
    }, null);
  };

  /**
   * Creates new refund for the refund cost of the entity given. Returns the new Entity
   *
   * @param { string } refundId
   *
   * @return { Entity }
   */
  this.refundCost = function (refundId) {
    if ((0, _lodash.isEmpty)(refundId)) {
      return null;
    }
    var data = {};
    return _this.volcanoClient.makeEntityRequest(REFUND_COST_REFUND, refundId, _constants["default"].HTTP_POST, data, {
      path: {
        id: refundId
      },
      isAdmin: true
    }, null);
  };
}