"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../../../dist/constants"));
var _vte = _interopRequireDefault(require("../../../dist/vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
var _facilityBookLine = _interopRequireDefault(require("./facilityBookLine"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_VENUES = "venues";
var ENDPOINT_GET_VENUE = ENDPOINT_GET_VENUES + "/:id";
var _default = exports["default"] = VenueClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function VenueClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;
  this.facilityBookLine = new _facilityBookLine["default"](volcanoClient);

  /** Venue methods */

  /**
  * Return a collection with all venues with pagination.
  *
  * @param {array} [params]  Optional object which contains the parameters to build the
  *                          request, for instance, if you want to move to specific page,
  *                          you must add 'page: 3' to the params object.
  *
  * @returns {Collection}
  */
  this.getVenues = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_VENUES, params, {
      isAdmin: true
    });
  };

  /**
  * Return an Entity containing a venue which match by Id
  *
  * @param {string} [id]  venue ID
  *
  * @returns {Entity}
  */
  this.getVenue = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, {
      parameters: {
        id: id
      }
    });
  };

  /**
  * Add a new venue
  *
  * @param {Object}   data      data to be updated
  *
  * @returns {Entity}
  */
  this.addVenue = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
  * Make a request to update a venue
  *
  * @param {string}}  id        id of the venue to be updated
  * @param {Object}   data      data to be updated
  *
  * @return {Entity}
  */
  this.editVenue = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
  * (Soft) delete a venue
  *
  * @param {string}  id   venue Id
  *
  * @return {Entity}
  */
  this.deleteVenue = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_VENUE, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}