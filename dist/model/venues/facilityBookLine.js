"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../../../dist/constants"));
var _vte = _interopRequireDefault(require("../../../dist/vte"));
var _collection = _interopRequireDefault(require("../entity/collection"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_FACILITYBOOKLINES_SCHEMA = 'facility-book-lines/schema';
var ENDPOINT_GET_VENUE_FACILITYBOOKLINES = "venues/:venue_id/facility-book-lines";
var ENDPOINT_GET_FACILITYBOOKLINES = "facility-book-lines";
var ENDPOINT_GET_FACILITYBOOKLINE = "facility-book-lines/:id";
var _default = exports["default"] = FacilityBookLineClient;
/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function FacilityBookLineClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /** FacilityBookLine methods */

  /**
  * Return a collection with all venues with pagination.
  *
  * @param {int} [venueId]
  * @param {array} [params]       Optional object which contains the parameters to build the
  *                               request, for instance, if you want to move to specific page,
  *                               you must add 'page: 3' to the params object.
  *
  * @returns {Collection}
  */
  this.getFacilityBookLines = function (venueId, params) {
    var endpoint = ENDPOINT_GET_FACILITYBOOKLINES;
    var queryParams = {};
    if (venueId != null) {
      endpoint = ENDPOINT_GET_VENUE_FACILITYBOOKLINES;
      queryParams = {
        venue_id: venueId
      };
    }
    return _this.volcanoClient.makeCollectionRequest(endpoint, params, {
      isAdmin: true,
      parameters: queryParams
    });
  };

  /**
  * Return an Entity containing a venue which match by Id
  *
  * @param {int} [venueId]
  * @param {string} [id]  facilityBookLine Id
  *
  * @returns {Entity}
  */
  this.getFacilityBookLine = function (id) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_FACILITYBOOKLINE, id, true, {
      parameters: {
        id: id
      }
    });
  };

  /**
  * Make a request to update a venue
  *
  * @param {int} venueId
  * @param {string}}  id        id of the facilityBookLine to be updated
  * @param {Object}   data      data to be updated
  *
  * @return {Entity}
  */
  this.editFacilityBookLines = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_FACILITYBOOKLINE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };
  this.getFacilityStates = function () {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_FACILITYBOOKLINES_SCHEMA, {
      method: _constants["default"].HTTP_GET,
      isAdmin: true
    });
    return _this.volcanoClient.makeRequest(request).then(function (res) {
      return Object.entries(res['facility_state_id']['options']);
    });
  };
}