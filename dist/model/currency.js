"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.ENDPOINT_GET_CURRENCIES = void 0;
var _vte = _interopRequireDefault(require("../vte"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = CurrencyClient; // Endpoints
var ENDPOINT_GET_CURRENCIES = exports.ENDPOINT_GET_CURRENCIES = "currencies";
/**
 * Container which has the currencies methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CurrencyClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Return a collection with all Currencies with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getCurrencies = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_CURRENCIES, params);
  };
}