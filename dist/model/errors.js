"use strict";

var AuthenticationException = function AuthenticationException() {
  return new Error("User could not be authenticated");
};
var InvalidTokenException = function InvalidTokenException() {
  return new Error("Invalid token");
};
var InvalidSessionIdException = function InvalidSessionIdException() {
  return new Error("Invalid session identifier");
};
var InvalidTransactionIdException = function InvalidTransactionIdException() {
  return new Error("Invalid payment transaction identifier");
};
var ObjectIsEmpty = function ObjectIsEmpty(objectName) {
  return new Error("The object " + objectName + " is empty");
};
var ParameterMissing = function ParameterMissing(parameter) {
  return new Error('Missing parameter ' + parameter);
};
var MissingProperty = function MissingProperty(property) {
  return new Error("The " + property + " is missing into the object");
};
AuthenticationException.prototype = Object.create(Error.prototype);
InvalidTokenException.prototype = Object.create(Error.prototype);
InvalidSessionIdException.prototype = Object.create(Error.prototype);
InvalidTransactionIdException.prototype = Object.create(Error.prototype);
module.exports = {
  AuthenticationException: AuthenticationException,
  InvalidTokenException: InvalidTokenException,
  InvalidSessionIdException: InvalidSessionIdException,
  InvalidTransactionIdException: InvalidTransactionIdException,
  ObjectIsEmpty: ObjectIsEmpty,
  MissingProperty: MissingProperty,
  ParameterMissing: ParameterMissing
};