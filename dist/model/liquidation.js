"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _constants = _interopRequireDefault(require("../constants"));
var _vte = _interopRequireDefault(require("../vte"));
var _entity = _interopRequireDefault(require("./entity/entity"));
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_LIQUIDATIONS = 'liquidations';
var ENDPOINT_GENERATE_LIQUIDATIONS = ENDPOINT_GET_LIQUIDATIONS + '/generate';
var ENDPOINT_GET_LIQUIDATION = ENDPOINT_GET_LIQUIDATIONS + '/:id';
var ENDPOINT_GET_LIQUIDATION_PDF = ENDPOINT_GET_LIQUIDATION + '/pdf';
var _default = exports["default"] = LiquidationClient;
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function LiquidationClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;
  this.generate = function () {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GENERATE_LIQUIDATIONS, _constants["default"].HTTP_POST, null, {
      isAdmin: true
    });
  };

  /**
   * Return a collection with all liquidations with pagination.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   *
   * @returns {Collection}
   */
  this.getLiquidations = function (params) {
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_LIQUIDATIONS, params);
  };

  /**
   * Return an Entity containing a liquidation which match by Id `liquidationId`.
   *
   * @param {BigInteger} [liquidationId]  liquidation ID.
   *
   * @returns {Entity}
   */
  this.getLiquidation = function (liquidationId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_LIQUIDATION, liquidationId, true, params);
  };

  /**
   * Return an application/PDF octet
   *
   * @param liquidationId
   *
   * @returns {null|Object|Entity}
   */
  this.getLiquidationPdf = function (liquidationId, params) {
    var id = parseInt(liquidationId);
    if (isNaN(liquidationId)) {
      return null;
    }
    params = (0, _isObject2["default"])(params) ? params : {};
    return _this.volcanoClient.makeFileRequest(ENDPOINT_GET_LIQUIDATION_PDF, id, true, params);
  };
}