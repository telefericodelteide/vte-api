"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_ACTIVITY_CONFIGS = 'activity-manager/activity-configs';
var ENDPOINT_GET_ACTIVITY_CONFIG = ENDPOINT_GET_ACTIVITY_CONFIGS + '/:id';
var ENDPOINT_UPDATE_ACTIVITY_CONFIG = ENDPOINT_GET_ACTIVITY_CONFIG;
var ENDPOINT_UPDATE_ACTIVITY_CONFIG_PRODUCTS = ENDPOINT_UPDATE_ACTIVITY_CONFIG + '/update-products';
var ENDPOINT_UPDATE_ACTIVITY_CONFIG_CONFIGURATION = ENDPOINT_UPDATE_ACTIVITY_CONFIG + '/update-configurations';
var _default = exports["default"] = ActivityConfigClient;
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityConfigClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns a collection with activities.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activity config.
   * 
   * @returns {Collection}
   */
  this.getActivityConfigs = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ACTIVITY_CONFIGS, params, {});
  };

  /**
   * Return an Entity containing an activity which match by Id `activityId`.
   *
   * @param {string} [activityConfigId]  activity ID.
   * @param {array} [params]  Optional object which contains the parameters to build the
   *
   * @returns {Entity}
   */
  this.getActivityConfig = function (activityConfigId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_ACTIVITY_CONFIG, activityConfigId, true, params, null);
  };

  /**
   * Add a new activity config.
   * @param data
   * @returns {Object|Entity}
   */
  this.addActivityConfig = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ACTIVITY_CONFIGS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    }, null);
  };

  /**
   * Edit an activity config.
   * @param {string} id
   * @param {Object} data
   */
  this.editActivityConfig = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_ACTIVITY_CONFIG, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Update the configuration of an activity config.
   * @param id
   * @param data
   * @returns {Object|Entity}
   */
  this.updateConfiguration = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_ACTIVITY_CONFIG_CONFIGURATION, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Update the products of an activity config.
   * @param id
   * @param data
   * @returns {Object|Entity}
   */
  this.updateProducts = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_ACTIVITY_CONFIG_PRODUCTS, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };

  /**
   * delete an activity config.
   * @param {string} id
   */
  this.deleteActivityConfig = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_ACTIVITY_CONFIG, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}