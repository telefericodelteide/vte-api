"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_LODGINGS = 'activity-manager/lodgings';
var ENDPOINT_GET_LODGING = ENDPOINT_GET_LODGINGS + '/:id';
var ENDPOINT_UPDATE_PICKUP_POINTS = ENDPOINT_GET_LODGINGS + '/:id/pickup-points-update';
var ENDPOINT_GET_PICKUP_POINTS = ENDPOINT_GET_LODGINGS + '/:id/pickup-points';
var _default = exports["default"] = LodgingClient;
function LodgingClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of lodgings
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getLodgings = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_LODGINGS, params);
  };

  /**
   * Get a lodging
   *
   * @param id uuid id of the lodging
   * @param params
   * @returns {Entity}
   */
  this.getLodging = function (id, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_LODGING, id, true, params);
  };

  /**
   * Add a new lodging
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addLodging = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_LODGINGS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Make a request to update a lodging
   *
   * @param {string}  id        uuid id of the lodging to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editLodging = function (id, data) {
    // validate id
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_LODGING, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Get a collection of pickup points
   *
   * @param id
   * @param params
   * @returns {any}
   */
  this.getPickupPoints = function (id, params) {
    var endpoint = ENDPOINT_GET_PICKUP_POINTS.replace(':id', id);
    return _this.volcanoClient.makeCollectionRequest(endpoint, params, {});
  };

  /**
   * Make a request to update a pickup points in lodging
   *
   * @param {string}  id        uuid id of the lodging to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editPickupPoints = function (id, data) {
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_PICKUP_POINTS, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * (Soft) delete a lodging
   *
   * @param {string}  id   lodging Id
   *
   * @return {Entity}
   */
  this.deleteLodging = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_LODGING, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}