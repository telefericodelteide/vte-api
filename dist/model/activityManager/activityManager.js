"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _activity = _interopRequireDefault(require("./activity"));
var _activityConfig = _interopRequireDefault(require("./activityConfig"));
var _zone = _interopRequireDefault(require("./zone"));
var _area = _interopRequireDefault(require("./area"));
var _pickupPoint = _interopRequireDefault(require("./pickupPoint"));
var _lodging = _interopRequireDefault(require("./lodging"));
var _pickupConfig = _interopRequireDefault(require("./pickupConfig"));
var _route = _interopRequireDefault(require("./route"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var _default = exports["default"] = ActivityManagerClient;
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityManagerClient(volcanoClient) {
  this.activity = new _activity["default"](volcanoClient);
  this.activityConfig = new _activityConfig["default"](volcanoClient);
  this.zone = new _zone["default"](volcanoClient);
  this.area = new _area["default"](volcanoClient);
  this.pickupPoint = new _pickupPoint["default"](volcanoClient);
  this.lodging = new _lodging["default"](volcanoClient);
  this.pickupConfig = new _pickupConfig["default"](volcanoClient);
  this.route = new _route["default"](volcanoClient);
}