"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _vte = _interopRequireDefault(require("../../vte"));
var _entity = _interopRequireDefault(require("../entity/entity"));
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
// Endpoints
var ENDPOINT_GET_ACTIVITIES = 'activity-manager/activities';
var ENDPOINT_GET_ACTIVITY = ENDPOINT_GET_ACTIVITIES + '/:id';
var ENDPOINT_UPDATE_ACTIVITY = ENDPOINT_GET_ACTIVITY;
var ENDPOINT_GET_ACTIVITIES_STATES = ENDPOINT_GET_ACTIVITIES + '/states';
var _default = exports["default"] = ActivityClient;
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityClient(volcanoClient) {
  var _this = this;
  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns a collection with activities.
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   *                          request, for instance, if you want to move to specific page,
   *                          you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   * 
   * @returns {Collection}
   */
  this.getActivities = function (params) {
    var viewMode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'compact';
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ACTIVITIES, params);
  };

  /**
   * Return an Entity containing an activity which match by Id `activityId`.
   *
   * @param {string} [activityId]  activity ID.
   *
   * @returns {Entity}
   */
  this.getActivity = function (activityId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_ACTIVITY, activityId, true, params);
  };

  /**
   * Add a new activity
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addActivity = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ACTIVITIES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Get Activity states filter options
   */
  this.getActivityStates = function (isAdmin) {
    var request = _this.volcanoClient.prepareRequest(ENDPOINT_GET_ACTIVITIES_STATES, {
      method: _constants["default"].HTTP_GET,
      isAdmin: isAdmin
    });
    return _this.volcanoClient.makeRequest(request);
  };

  /**
   * delete an activity
   */
  this.deleteActivity = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_ACTIVITY, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}