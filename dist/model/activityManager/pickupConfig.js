"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_PICKUP_CONFIGS = 'activity-manager/pickup-configs';
var ENDPOINT_GET_PICKUP_CONFIG = ENDPOINT_GET_PICKUP_CONFIGS + '/:id';
var ENDPOINT_GET_PICKUP_CONFIG_PICKUP_POINTS = ENDPOINT_GET_PICKUP_CONFIG + '/pickup-points';
var ENDPOINT_UPDATE_PICKUP_POINTS = ENDPOINT_GET_PICKUP_CONFIGS + '/:id/pickup-points-update';
var _default = exports["default"] = PickupConfigClient;
function PickupConfigClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of pickup configs
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getPickupConfigs = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_PICKUP_CONFIGS, params);
  };

  /**
   * Get a pickup config
   *
   * @param pickupPointId
   * @param params
   * @returns {Entity}
   */
  this.getPickupConfig = function (pickupPointId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_PICKUP_CONFIG, pickupPointId, true, params);
  };

  /**
   * Add a new pickup config
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addPickupConfig = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_CONFIGS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Make a request to update a pickup config
   *
   * @param {string}  id        uuid id of the pickup config to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editPickupConfig = function (id, data) {
    // validate id
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_CONFIG, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   *
   * @param id
   * @param params
   * @returns {any}
   */
  this.getPickupConfigPickupPoints = function (id, params) {
    var endpoint = ENDPOINT_GET_PICKUP_CONFIG_PICKUP_POINTS.replace(':id', id);
    return _this.volcanoClient.makeCollectionRequest(endpoint, params, {});
  };

  /**
   * Update a pickup points for a pickup config
   */
  this.editPickupConfigPickupPoints = function (id, data) {
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_PICKUP_POINTS, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Delete a pickup config
   *
   * @param {string}  id   pickup config Id
   * @return {Entity}
   */
  this.deletePickupConfig = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_CONFIG, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}