"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_AREAS = 'activity-manager/areas';
var _default = exports["default"] = AreaClient;
function AreaClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of zones
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getAreas = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_AREAS, params);
  };
}