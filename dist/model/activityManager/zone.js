"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_ZONES = 'activity-manager/zones';
var ENDPOINT_GET_ZONE = ENDPOINT_GET_ZONES + '/:id';
var _default = exports["default"] = ZoneClient;
function ZoneClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of zones
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getZones = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ZONES, params);
  };

  /**
   * Get a zone
   *
   * @param zoneId
   * @param params
   * @returns {Entity}
   */
  this.getZone = function (zoneId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_ZONE, zoneId, true, params);
  };

  /**
   * Add a new zone
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addZone = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ZONES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Make a request to update a zone
   *
   * @param {string}  id        uuid id of the zone to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editZone = function (id, data) {
    // validate id
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ZONE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * (Soft) delete a zone
   *
   * @param {string}  id   zone Id
   *
   * @return {Entity}
   */
  this.deleteZone = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ZONE, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}