"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_PICKUP_POINTS = 'activity-manager/pickup-points';
var ENDPOINT_GET_PICKUP_POINT = ENDPOINT_GET_PICKUP_POINTS + '/:id';
var ENDPOINT_GET_PICKUP_POINT_LODGINGS = ENDPOINT_GET_PICKUP_POINT + '/lodgings';
var _default = exports["default"] = PickupPointClient;
function PickupPointClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of pickup points
   *
   * @param {object} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getPickupPoints = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_PICKUP_POINTS, params);
  };

  /**
   * Get a pickup point
   *
   * @param pickupPointId
   * @param params
   * @returns {Entity}
   */
  this.getPickupPoint = function (pickupPointId, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_PICKUP_POINT, pickupPointId, true, params);
  };

  /**
   * Add a new pickup point
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addPickupPoint = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_POINTS, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Make a request to update a pickup point
   *
   * @param {string}  id        uuid id of the pickup point to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editPickupPoint = function (id, data) {
    // validate id
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_POINT, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Get a collection of lodgings for a pickup point
   *
   * @param id
   * @param params
   * @returns {Collection}
   */
  this.getLodgings = function (id, params) {
    var endpoint = ENDPOINT_GET_PICKUP_POINT_LODGINGS.replace(':id', id);
    return _this.volcanoClient.makeCollectionRequest(endpoint, params, {});
  };

  /**
   * Delete a pickup point
   *
   * @param {string}  id   pickup point Id
   * @return {Entity}
   */
  this.deletePickupPoint = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_PICKUP_POINT, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}