"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _constants = _interopRequireDefault(require("../../constants"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
var ENDPOINT_GET_ROUTES = 'activity-manager/routes';
var ENDPOINT_GET_ROUTE = ENDPOINT_GET_ROUTES + '/:id';
var ENDPOINT_UPDATE_CONFIGURATION = ENDPOINT_GET_ROUTES + '/:id/update-configuration';
var ENDPOINT_UPDATE_PRODUCTS = ENDPOINT_GET_ROUTES + '/:id/update-update-products';
var ENDPOINT_UPDATE_PICKUP_CONFIGS = ENDPOINT_GET_ROUTES + '/:id/update-pickup-configs';
var _default = exports["default"] = RouteClient;
function RouteClient(volcanoClient) {
  var _this = this;
  this.volcanoClient = volcanoClient;

  /**
   * Get a collection of routes
   *
   * @param {array} [params]  Optional object which contains the parameters to build the
   * request, for instance, if you want to move to specific page,
   * you must add 'page: 3' to the params object.
   * @param {string} [viewMode]  Optional string with the view mode of the activities.
   *
   * @returns {Collection}
   *
   */
  this.getRoutes = function (params, viewMode) {
    params = (0, _isObject2["default"])(params) ? params : {};
    params.view_mode = viewMode;
    return _this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ROUTES, params);
  };

  /**
   * Get a route
   *
   * @param id uuid id of the route
   * @param params
   * @returns {Entity}
   */
  this.getRoute = function (id, params) {
    return _this.volcanoClient.getEntityRequest(ENDPOINT_GET_ROUTE, id, true, params);
  };

  /**
   * Add a new route
   *
   * @param {Object}   data      data to be created
   *
   * @returns {Entity}
   */
  this.addRoute = function (data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ROUTES, _constants["default"].HTTP_POST, data, {
      isAdmin: true
    });
  };

  /**
   * Make a request to update a route
   *
   * @param {string}  id        uuid id of the route to be updated
   * @param {Object}   data      data to be updated
   *
   * @return {Entity}
   */
  this.editRoute = function (id, data) {
    // validate id
    if (!id) {
      return Promise.reject(new Error('id is required'));
    }
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ROUTE, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Update configuration of a route
   * @param {string} id
   * @param {Object} data
   *
   * @returns {Entity}
   */
  this.updateConfiguration = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_CONFIGURATION, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Update products of a route
   * @param {string} id
   * @param {Object} data
   *
   * @returns {Entity}
   */
  this.updateProducts = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_PRODUCTS, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * Update pickup configs of a route
   * @param {string} id
   * @param {Object} data
   *
   * @returns {Entity}
   */
  this.updatePickupConfigs = function (id, data) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_UPDATE_PICKUP_CONFIGS, _constants["default"].HTTP_PATCH, data, {
      isAdmin: true,
      path: {
        id: id
      }
    }, null);
  };

  /**
   * (Soft) delete a route
   *
   * @param {string}  id   route Id
   *
   * @return {Entity}
   */
  this.deleteRoute = function (id) {
    return _this.volcanoClient.makeResourceRequest(ENDPOINT_GET_ROUTE, _constants["default"].HTTP_DELETE, null, {
      isAdmin: true,
      path: {
        id: id
      }
    });
  };
}