"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _url = _interopRequireDefault(require("url"));
var _axios = _interopRequireDefault(require("axios"));
var _regeneratorRuntime2 = _interopRequireDefault(require("regenerator-runtime"));
var _jwtDecode = _interopRequireDefault(require("jwt-decode"));
var _constants = _interopRequireDefault(require("./constants"));
var _errors = _interopRequireDefault(require("./model/errors"));
var _collection = _interopRequireDefault(require("./model/entity/collection"));
var _entity = _interopRequireDefault(require("./model/entity/entity"));
var _role = _interopRequireDefault(require("./model/role"));
var _permission = _interopRequireDefault(require("./model/permission"));
var _content = _interopRequireDefault(require("./model/content/content"));
var _booking = _interopRequireDefault(require("./model/booking"));
var _user = _interopRequireDefault(require("./model/user"));
var _invoice = _interopRequireDefault(require("./model/invoice"));
var _paymentGateway = _interopRequireDefault(require("./model/paymentGateway"));
var _paymentTransaction = _interopRequireDefault(require("./model/paymentTransaction"));
var _cart = _interopRequireDefault(require("./model/cart"));
var _route = _interopRequireDefault(require("./model/route"));
var _catalog = _interopRequireDefault(require("./model/catalog/catalog"));
var _experience = _interopRequireDefault(require("./model/experience"));
var _intermediary = _interopRequireDefault(require("./model/intermediary"));
var _collaborator = _interopRequireDefault(require("./model/collaborator"));
var _refund = _interopRequireDefault(require("./model/refund"));
var _enterprise = _interopRequireDefault(require("./model/enterprise"));
var _order = _interopRequireDefault(require("./model/order"));
var _liquidation = _interopRequireDefault(require("./model/liquidation"));
var _billingInformation = _interopRequireDefault(require("./model/billingInformation"));
var _user2 = _interopRequireDefault(require("./model/entity/user"));
var _task = _interopRequireDefault(require("./model/task"));
var _accessControl = _interopRequireDefault(require("./model/accessControl"));
var _activityManager = _interopRequireDefault(require("./model/activityManager/activityManager"));
var _suggestion = _interopRequireDefault(require("./model/suggestion"));
var _venue = _interopRequireDefault(require("./model/venues/venue"));
var _currency = _interopRequireDefault(require("./model/currency"));
var _vbms = _interopRequireDefault(require("./model/vbms"));
var _schema = _interopRequireDefault(require("./model/schema"));
var _crm = _interopRequireDefault(require("./model/crm/crm"));
var _client = _interopRequireDefault(require("./model/crm/client"));
var _download = _interopRequireDefault(require("./model/download"));
var _DataExport = _interopRequireDefault(require("./lib/DataExport"));
var _isObject2 = _interopRequireDefault(require("lodash/isObject"));
var _get2 = _interopRequireDefault(require("lodash/get"));
var _set2 = _interopRequireDefault(require("lodash/set"));
var _notifications = _interopRequireDefault(require("./model/notifications"));
var _product = _interopRequireDefault(require("./model/entity/catalog/product"));
var _hubspot = _interopRequireDefault(require("./model/hubspot/hubspot"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { "default": e }; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return e; }; var t, e = {}, r = Object.prototype, n = r.hasOwnProperty, o = Object.defineProperty || function (t, e, r) { t[e] = r.value; }, i = "function" == typeof Symbol ? Symbol : {}, a = i.iterator || "@@iterator", c = i.asyncIterator || "@@asyncIterator", u = i.toStringTag || "@@toStringTag"; function define(t, e, r) { return Object.defineProperty(t, e, { value: r, enumerable: !0, configurable: !0, writable: !0 }), t[e]; } try { define({}, ""); } catch (t) { define = function define(t, e, r) { return t[e] = r; }; } function wrap(t, e, r, n) { var i = e && e.prototype instanceof Generator ? e : Generator, a = Object.create(i.prototype), c = new Context(n || []); return o(a, "_invoke", { value: makeInvokeMethod(t, r, c) }), a; } function tryCatch(t, e, r) { try { return { type: "normal", arg: t.call(e, r) }; } catch (t) { return { type: "throw", arg: t }; } } e.wrap = wrap; var h = "suspendedStart", l = "suspendedYield", f = "executing", s = "completed", y = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var p = {}; define(p, a, function () { return this; }); var d = Object.getPrototypeOf, v = d && d(d(values([]))); v && v !== r && n.call(v, a) && (p = v); var g = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(p); function defineIteratorMethods(t) { ["next", "throw", "return"].forEach(function (e) { define(t, e, function (t) { return this._invoke(e, t); }); }); } function AsyncIterator(t, e) { function invoke(r, o, i, a) { var c = tryCatch(t[r], t, o); if ("throw" !== c.type) { var u = c.arg, h = u.value; return h && "object" == _typeof(h) && n.call(h, "__await") ? e.resolve(h.__await).then(function (t) { invoke("next", t, i, a); }, function (t) { invoke("throw", t, i, a); }) : e.resolve(h).then(function (t) { u.value = t, i(u); }, function (t) { return invoke("throw", t, i, a); }); } a(c.arg); } var r; o(this, "_invoke", { value: function value(t, n) { function callInvokeWithMethodAndArg() { return new e(function (e, r) { invoke(t, n, e, r); }); } return r = r ? r.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); } }); } function makeInvokeMethod(e, r, n) { var o = h; return function (i, a) { if (o === f) throw Error("Generator is already running"); if (o === s) { if ("throw" === i) throw a; return { value: t, done: !0 }; } for (n.method = i, n.arg = a;;) { var c = n.delegate; if (c) { var u = maybeInvokeDelegate(c, n); if (u) { if (u === y) continue; return u; } } if ("next" === n.method) n.sent = n._sent = n.arg;else if ("throw" === n.method) { if (o === h) throw o = s, n.arg; n.dispatchException(n.arg); } else "return" === n.method && n.abrupt("return", n.arg); o = f; var p = tryCatch(e, r, n); if ("normal" === p.type) { if (o = n.done ? s : l, p.arg === y) continue; return { value: p.arg, done: n.done }; } "throw" === p.type && (o = s, n.method = "throw", n.arg = p.arg); } }; } function maybeInvokeDelegate(e, r) { var n = r.method, o = e.iterator[n]; if (o === t) return r.delegate = null, "throw" === n && e.iterator["return"] && (r.method = "return", r.arg = t, maybeInvokeDelegate(e, r), "throw" === r.method) || "return" !== n && (r.method = "throw", r.arg = new TypeError("The iterator does not provide a '" + n + "' method")), y; var i = tryCatch(o, e.iterator, r.arg); if ("throw" === i.type) return r.method = "throw", r.arg = i.arg, r.delegate = null, y; var a = i.arg; return a ? a.done ? (r[e.resultName] = a.value, r.next = e.nextLoc, "return" !== r.method && (r.method = "next", r.arg = t), r.delegate = null, y) : a : (r.method = "throw", r.arg = new TypeError("iterator result is not an object"), r.delegate = null, y); } function pushTryEntry(t) { var e = { tryLoc: t[0] }; 1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e); } function resetTryEntry(t) { var e = t.completion || {}; e.type = "normal", delete e.arg, t.completion = e; } function Context(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(pushTryEntry, this), this.reset(!0); } function values(e) { if (e || "" === e) { var r = e[a]; if (r) return r.call(e); if ("function" == typeof e.next) return e; if (!isNaN(e.length)) { var o = -1, i = function next() { for (; ++o < e.length;) if (n.call(e, o)) return next.value = e[o], next.done = !1, next; return next.value = t, next.done = !0, next; }; return i.next = i; } } throw new TypeError(_typeof(e) + " is not iterable"); } return GeneratorFunction.prototype = GeneratorFunctionPrototype, o(g, "constructor", { value: GeneratorFunctionPrototype, configurable: !0 }), o(GeneratorFunctionPrototype, "constructor", { value: GeneratorFunction, configurable: !0 }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, u, "GeneratorFunction"), e.isGeneratorFunction = function (t) { var e = "function" == typeof t && t.constructor; return !!e && (e === GeneratorFunction || "GeneratorFunction" === (e.displayName || e.name)); }, e.mark = function (t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, define(t, u, "GeneratorFunction")), t.prototype = Object.create(g), t; }, e.awrap = function (t) { return { __await: t }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, c, function () { return this; }), e.AsyncIterator = AsyncIterator, e.async = function (t, r, n, o, i) { void 0 === i && (i = Promise); var a = new AsyncIterator(wrap(t, r, n, o), i); return e.isGeneratorFunction(r) ? a : a.next().then(function (t) { return t.done ? t.value : a.next(); }); }, defineIteratorMethods(g), define(g, u, "Generator"), define(g, a, function () { return this; }), define(g, "toString", function () { return "[object Generator]"; }), e.keys = function (t) { var e = Object(t), r = []; for (var n in e) r.push(n); return r.reverse(), function next() { for (; r.length;) { var t = r.pop(); if (t in e) return next.value = t, next.done = !1, next; } return next.done = !0, next; }; }, e.values = values, Context.prototype = { constructor: Context, reset: function reset(e) { if (this.prev = 0, this.next = 0, this.sent = this._sent = t, this.done = !1, this.delegate = null, this.method = "next", this.arg = t, this.tryEntries.forEach(resetTryEntry), !e) for (var r in this) "t" === r.charAt(0) && n.call(this, r) && !isNaN(+r.slice(1)) && (this[r] = t); }, stop: function stop() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval; }, dispatchException: function dispatchException(e) { if (this.done) throw e; var r = this; function handle(n, o) { return a.type = "throw", a.arg = e, r.next = n, o && (r.method = "next", r.arg = t), !!o; } for (var o = this.tryEntries.length - 1; o >= 0; --o) { var i = this.tryEntries[o], a = i.completion; if ("root" === i.tryLoc) return handle("end"); if (i.tryLoc <= this.prev) { var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc"); if (c && u) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } else if (c) { if (this.prev < i.catchLoc) return handle(i.catchLoc, !0); } else { if (!u) throw Error("try statement without catch or finally"); if (this.prev < i.finallyLoc) return handle(i.finallyLoc); } } } }, abrupt: function abrupt(t, e) { for (var r = this.tryEntries.length - 1; r >= 0; --r) { var o = this.tryEntries[r]; if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) { var i = o; break; } } i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null); var a = i ? i.completion : {}; return a.type = t, a.arg = e, i ? (this.method = "next", this.next = i.finallyLoc, y) : this.complete(a); }, complete: function complete(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), y; }, finish: function finish(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), y; } }, "catch": function _catch(t) { for (var e = this.tryEntries.length - 1; e >= 0; --e) { var r = this.tryEntries[e]; if (r.tryLoc === t) { var n = r.completion; if ("throw" === n.type) { var o = n.arg; resetTryEntry(r); } return o; } } throw Error("illegal catch attempt"); }, delegateYield: function delegateYield(e, r, n) { return this.delegate = { iterator: values(e), resultName: r, nextLoc: n }, "next" === this.method && (this.arg = t), y; } }, e; }
function asyncGeneratorStep(n, t, e, r, o, a, c) { try { var i = n[a](c), u = i.value; } catch (n) { return void e(n); } i.done ? t(u) : Promise.resolve(u).then(r, o); }
function _asyncToGenerator(n) { return function () { var t = this, e = arguments; return new Promise(function (r, o) { var a = n.apply(t, e); function _next(n) { asyncGeneratorStep(a, r, o, _next, _throw, "next", n); } function _throw(n) { asyncGeneratorStep(a, r, o, _next, _throw, "throw", n); } _next(void 0); }); }; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _defineProperty(e, r, t) { return (r = _toPropertyKey(r)) in e ? Object.defineProperty(e, r, { value: t, enumerable: !0, configurable: !0, writable: !0 }) : e[r] = t, e; }
function _classCallCheck(a, n) { if (!(a instanceof n)) throw new TypeError("Cannot call a class as a function"); }
function _defineProperties(e, r) { for (var t = 0; t < r.length; t++) { var o = r[t]; o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, _toPropertyKey(o.key), o); } }
function _createClass(e, r, t) { return r && _defineProperties(e.prototype, r), t && _defineProperties(e, t), Object.defineProperty(e, "prototype", { writable: !1 }), e; }
function _toPropertyKey(t) { var i = _toPrimitive(t, "string"); return "symbol" == _typeof(i) ? i : i + ""; }
function _toPrimitive(t, r) { if ("object" != _typeof(t) || !t) return t; var e = t[Symbol.toPrimitive]; if (void 0 !== e) { var i = e.call(t, r || "default"); if ("object" != _typeof(i)) return i; throw new TypeError("@@toPrimitive must return a primitive value."); } return ("string" === r ? String : Number)(t); }
var entityHandlers = {
  'default': _entity["default"],
  'user': _user2["default"],
  'product': _product["default"]
};

/**
 * This class is the Volcano API JS Core.
 *
 * @class     VolcanoApi
 * @extends   None
 */
var VolcanoApi = /*#__PURE__*/function () {
  /**
   * @constructor
   *
   * @param {object} options
   */
  function VolcanoApi(options) {
    _classCallCheck(this, VolcanoApi);
    this.options = options;
    this.protocol = options.protocol || _constants["default"].HTTPS_PROTOCOL;
    this.host = options.host;
    this.port = options.port || null;
    this.base = options.base || '';
    this.timeout = options.timeout || 5000;
    this.locale = options.locale || 'en';
    this.strictSSL = options.hasOwnProperty(_constants["default"].STRICT_SSL) ? options.strictSSL : true;
    this.initialize(this.options);
  }

  /**
   * Initilizes client.
   * 
   * @param {*} options 
   */
  return _createClass(VolcanoApi, [{
    key: "initialize",
    value: function initialize(options) {
      this.token = false;
      this.baseOptions = {
        auth: {},
        default_params: {}
      };
      if (options.api_key && options.api_secret) {
        this.baseOptions.auth = {
          api_key: options.api_key,
          api_secret: options.api_secret
        };
      } else if (options.username && options.password) {
        this.baseOptions.auth = {
          username: options.username,
          password: options.password
        };
      }
      if (options.site_key) {
        this.baseOptions.auth = _objectSpread(_objectSpread({}, this.baseOptions.auth), {
          type: _constants["default"].API_KEY_LABEL,
          token: options.site_key,
          headerName: _constants["default"].X_API_KEY_LABEL
        });
      }
      if (options.bearer) {
        this.setToken(options.bearer);
      }
      if (options.bearer_interceptor && typeof options.bearer_interceptor === 'function') {
        this.interceptor = options.bearer_interceptor;
      }
      if (options.with_default_params) {
        this.baseOptions.with_default_params = options.with_default_params;
      } else {
        options.with_default_params = false;
      }
      this.client = this.getClient();

      // Here are the list of models included and which are
      // accesible from the VolcanoApi instances.

      // You must include here the models
      this.download = new _download["default"](this);
      this.vbms = new _vbms["default"](this);
      this.schema = new _schema["default"](this);
      this.role = new _role["default"](this);
      this.permission = new _permission["default"](this);
      this.user = new _user["default"](this);
      this.content = new _content["default"](this);
      this.paymentGateway = new _paymentGateway["default"](this);
      this.paymentTransaction = new _paymentTransaction["default"](this);
      this.invoice = new _invoice["default"](this);
      this.cart = new _cart["default"](this);
      this.booking = new _booking["default"](this);
      this.catalog = new _catalog["default"](this);
      this.experience = new _experience["default"](this);
      this.crm = new _crm["default"](this);
      this.intermediary = new _intermediary["default"](this);
      this.collaborator = new _collaborator["default"](this);
      this.activityManager = new _activityManager["default"](this);
      this.route = new _route["default"](this);
      this.refund = new _refund["default"](this);
      this.enterprise = new _enterprise["default"](this);
      this.order = new _order["default"](this);
      this.liquidation = new _liquidation["default"](this);
      this.billingInformation = new _billingInformation["default"](this);
      this.task = new _task["default"](this);
      this.accessControl = new _accessControl["default"](this);
      this.suggestion = new _suggestion["default"](this);
      this.venue = new _venue["default"](this);
      this.currency = new _currency["default"](this);
      this.billingClient = new _client["default"](this);
      this.notifications = new _notifications["default"](this);
      this.hubspot = new _hubspot["default"](this);
    }

    /**
     * Get the config of axios instance or the instance itself
     *
     * @returns object | AxiosInstance
     */
  }, {
    key: "getClient",
    value: function getClient() {
      var _this = this;
      var instance = _axios["default"].create();
      instance.interceptors.request.use(function (config) {
        config.headers = _objectSpread(_objectSpread({}, config.headers), {}, {
          Accept: _constants["default"].APPLICATION_JSON,
          'Content-Type': _constants["default"].APPLICATION_JSON,
          'Accept-Language': _this.locale
        });
        if (_this.interceptor) {
          _this.setToken(_this.interceptor());
        }
        if (_this.baseOptions.auth.headerName) {
          config.headers[_this.baseOptions.auth.headerName] = _this.baseOptions.auth.token;
        }
        return config;
      });
      return instance;
    }

    /**
     * Set as token the return value of the callback function.
     *
     * @param {function} callback   Function that returns the JWT token
     */
  }, {
    key: "setTokenInterceptor",
    value: function setTokenInterceptor(callback) {
      this.setToken(callback());
    }

    /**
     * Set token using the parameter.
     *
     * @param {string} token
     */
  }, {
    key: "setToken",
    value: function setToken(token) {
      if (token) {
        this.baseOptions.auth = {
          type: _constants["default"].BEARER_LABEL,
          token: token,
          headerName: _constants["default"].AUTHORIZATION
        };
        this.token = token;
        if (this.baseOptions.with_default_params) {
          var payload = this.getPayload();
          if (payload.enterprise_id) {
            this.baseOptions.default_params = _objectSpread(_objectSpread({}, this.baseOptions.default_params), {}, {
              enterprise_id: payload.enterprise_id
            });
          }
        }
      }
      return this.token;
    }

    /**
     * Set default parameter.
     *
     * @param {string} key
     * @param {object} value
     */
  }, {
    key: "setDefaultParam",
    value: function setDefaultParam(key, value) {
      (0, _set2["default"])(this.baseOptions.default_params, key, value);
    }

    /**
     * Return the token set into the object.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @returns boolean | string
     *
     * @see   Why we don't throw an exception instead returning a boolean.
     *        Even we can return undefined or an empty string.
     *        It would be mode consistenent with the function itself.
     */
  }, {
    key: "getToken",
    value: function getToken() {
      if (!this.token) {
        return false;
      }
      return this.token;
    }

    /**
     * Returns the token or an exception if it is not set.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @returns {string}  the decoded token.
     * @throws  {Error}   if the token is not set.
     */
  }, {
    key: "getPayload",
    value: function getPayload() {
      if (!this.token) {
        throw new _errors["default"].InvalidTokenException();
      }
      return (0, _jwtDecode["default"])(this.token);
    }

    /**
     * Authenticate the user using username and password
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} username
     * @param {string} password
     *
     * @returns {object}
     */
  }, {
    key: "authenticate",
    value: function authenticate(username, password) {
      var _this2 = this;
      var corporateAccount = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var data = {};
      if (username && password) {
        data = {
          username: username,
          password: password
        };
        if (corporateAccount) {
          data = _objectSpread(_objectSpread({}, data), {}, {
            corporate_account: corporateAccount
          });
        }
      } else if (this.baseOptions.auth.username) {
        data = {
          username: this.baseOptions.auth.username,
          password: this.baseOptions.auth.password
        };
      } else if (this.baseOptions.auth.api_key) {
        data = {
          api_key: this.baseOptions.auth.api_key,
          api_secret: this.baseOptions.auth.api_secret
        };
      }
      var request = this.prepareRequest(_constants["default"].TOKEN_LABEL, {
        method: _constants["default"].HTTP_POST,
        data: data,
        ignore_default_params: true
      });
      return this.makeRequest(request).then(function (res) {
        if (res.success) {
          _this2.setToken(res.data.token);
          return _this2.user.getProfile().then(function (user) {
            return {
              token: _this2.token,
              user: user
            };
          });
        }
      });
    }
  }, {
    key: "logout",
    value: function logout() {
      this.initialize(this.options);
    }

    /**
     * Authenticate the user using booking locator and customer email
     *
     * @param {string} bookingLocator
     * @param {string} customerEmail
     *
     * @returns {object}
     */
  }, {
    key: "authenticateCustomer",
    value: function authenticateCustomer(bookingLocator, customerEmail) {
      var _this3 = this;
      var data = {
        type: _constants["default"].SITE_LABEL,
        api_key: this.baseOptions.auth.token,
        booking_locator: bookingLocator,
        customer_email: customerEmail
      };
      var request = this.prepareRequest(_constants["default"].TOKEN_LABEL, {
        method: _constants["default"].HTTP_POST,
        data: data
      });
      return this.makeRequest(request).then(function (res) {
        if (res.success) {
          _this3.setToken(res.data.token);
          return {
            success: true,
            token: _this3.token
          };
        }
      });
    }

    /**
     * Do a reset password request for an application user and sends mail to user email with the reset password link.
     *
     * @param {string}  email               Email
     * @param {string}  corporateAccount    User corporate account
     *
     * @return {Entity}
     */
  }, {
    key: "resetPasswordRequest",
    value: function resetPasswordRequest(email, corporateAccount) {
      var data = {
        email: email,
        corporate_account: corporateAccount
      };
      return this.makeResourceRequest(_constants["default"].RESET_PASSWORD_REQUEST, _constants["default"].HTTP_POST, data, {
        isAdmin: true
      }, null);
    }

    /**
     * Reset an application user password
     *
     * @param { object } data   This should have the following format.
     *                          {
     *                            "reset_hash": "cb24413df603182af3beda902c73ba53",
     *                            "password": "test",
     *                            "password_repeat": "test"
     *                          }
     * 
     * @return {Entity}
     */
  }, {
    key: "resetPassword",
    value: function resetPassword(data) {
      return this.makeResourceRequest(_constants["default"].RESET_PASSWORD, _constants["default"].HTTP_POST, data, {
        isAdmin: true
      }, null);
    }

    /**
     * Using the API endpoint to renew token.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Oct 2020
     */
  }, {
    key: "renewToken",
    value: function renewToken() {
      var _this4 = this;
      var enterpriseId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var token = this.getToken();
      var data = {};
      if (enterpriseId !== null) {
        data = {
          enterprise_id: enterpriseId
        };
      }
      var request = this.prepareRequest(_constants["default"].TOKEN_RENEW, {
        method: _constants["default"].HTTP_POST,
        data: data,
        ignore_default_params: true,
        parameters: {
          token: token
        }
      });
      return this.makeRequest(request).then(function (res) {
        if (res.success) {
          return _this4.setToken(res.data.token, true);
        }
      });
    }

    /**
     * Creates an object with all info needed by a request and using the  based on the default template for one
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} uri
     * @param {prepareRequestOptions} [options] - an object containing request parameters
     *
     * @returns {object}
     */
  }, {
    key: "prepareRequest",
    value: function prepareRequest(path) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      // check parameters
      if (options.parameters) {
        Object.entries(options.parameters).forEach(function (param) {
          path = path.replace(':' + param[0], param[1]);
        });
      }
      if (this.baseOptions.with_default_params && !options.ignore_default_params) {
        options.query = _objectSpread(_objectSpread({}, this.baseOptions.default_params), options.query);
      }
      var result = {
        url: options.isFull ? path : options.isAdmin ? this.makeAdminUri(path) : this.makeUri(path),
        method: options.method || _constants["default"].HTTP_GET,
        timeout: options.timeout || this.timeout,
        data: options.data || null,
        params: options.query
      };
      if (options.currency) {
        result.headers = {
          'x-currency': options.currency || null
        };
      }
      return result;
    }

    /**
     * Create a valid URI with the path given
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} path    String to be added at the end of the URL
     *
     * @returns {string}  a wellformed URI
     */
  }, {
    key: "makeUri",
    value: function makeUri(path) {
      var uri = _url["default"].format({
        protocol: this.protocol,
        hostname: this.host,
        port: this.port,
        pathname: "".concat(this.base, "/").concat(path)
      });
      return decodeURIComponent(uri);
    }

    /**
     * Returns an URI with the admin prefix and with path as sufix
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} path
     *
     * @returns {string}
     */
  }, {
    key: "makeAdminUri",
    value: function makeAdminUri(path) {
      return this.makeUri(_constants["default"].ADMIN_URL_PREFIX + "/".concat(path));
    }

    /**
     * Do a request and returns the response's data or false if the callback is called.
     *
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {AxiosRequestConfig}  requestOptions
     * @param {function}            errorCallback
     *
     * @throws {Error}  An error with the following data:
     *                    code:     HTTP Error code.
     *                    message:  Human readable message from the response.
     *                    type:     Error type
     *                    data:     Additional data of the error.
     * @returns {object | boolean}
     */
  }, {
    key: "makeRequest",
    value: (function () {
      var _makeRequest = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee(requestOptions, errorCallback) {
        var res, err;
        return _regeneratorRuntime().wrap(function _callee$(_context) {
          while (1) switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return this.client.request(requestOptions);
            case 3:
              res = _context.sent;
              if (!(res.data && res.data.error)) {
                _context.next = 12;
                break;
              }
              err = new Error(res.data.message);
              err.type = res.data.type;
              err.data = res.data.data;
              err.code = res.data.code;
              throw err;
            case 12:
              return _context.abrupt("return", res.data);
            case 13:
              _context.next = 22;
              break;
            case 15:
              _context.prev = 15;
              _context.t0 = _context["catch"](0);
              if (!(_context.t0 instanceof Error)) {
                _context.next = 21;
                break;
              }
              throw _context.t0;
            case 21:
              if (errorCallback) {
                errorCallback(_context.t0);
              }
            case 22:
              return _context.abrupt("return", false);
            case 23:
            case "end":
              return _context.stop();
          }
        }, _callee, this, [[0, 15]]);
      }));
      function makeRequest(_x, _x2) {
        return _makeRequest.apply(this, arguments);
      }
      return makeRequest;
    }()
    /**
     * Do a GET request to an 'admin/*' URL and return the data as a collection.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} endpoint
     * @param {object} query
     * @param {object} config
     * @param {*} entityClass
     *
     * @returns {Collection}
     */
    )
  }, {
    key: "makeCollectionRequest",
    value: function makeCollectionRequest(endpoint, query, config) {
      var _this5 = this;
      var entityClass = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      entityClass = entityClass || _entity["default"];
      var baseConfig = {
        method: _constants["default"].HTTP_GET,
        isAdmin: true
      };
      query = query || {};
      query = _objectSpread(_objectSpread({}, query), {}, {
        page: query.page || _constants["default"].DEFAULT_PAGE_NUMBER,
        limit: query.limit || _constants["default"].DEFAULT_ELEMENTS_PER_PAGE
      });
      var request = this.prepareRequest(endpoint, _objectSpread(_objectSpread(_objectSpread({}, baseConfig), config), {}, {
        query: query || null
      }));
      return this.makeRequest(request).then(function (res) {
        return new _collection["default"](_this5, res, entityClass);
      });
    }
  }, {
    key: "makeCollectionQueryRequest",
    value: function makeCollectionQueryRequest(endpoint, query, config) {
      var _this6 = this;
      var entityClass = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
      entityClass = entityClass || _entity["default"];
      var baseConfig = {
        method: _constants["default"].HTTP_GET
      };
      query = query || {};
      if (query.isAdmin === undefined) {
        baseConfig.isAdmin = false;
      }
      query = _objectSpread(_objectSpread({}, query), {}, {
        page: query.page || _constants["default"].DEFAULT_PAGE_NUMBER,
        limit: query.limit || _constants["default"].DEFAULT_ELEMENTS_PER_PAGE
      });
      var request = this.prepareRequest(endpoint, _objectSpread(_objectSpread(_objectSpread({}, baseConfig), config), {}, {
        query: query || null
      }));
      return this.makeRequest(request).then(function (res) {
        return new _collection["default"](_this6, res, entityClass);
      });
    }

    /**
     * Make a request to return an entity or object after do the request using the verb specified with method param.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     *            Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version   Ago 2020
     *
     * @param {string} method         en HTTP verb
     * @param {number|string} id      Id
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     * @param {string|null} entityClass Class to be used to create the entity.
     *
     * @return {object | Entity}
     */
  }, {
    key: "makeEntityRequest",
    value: function makeEntityRequest(endpoint, id, method, data, params, callback, entityClass) {
      // Check params structure.
      params = params || {
        path: {},
        query: {},
        isAdmin: false
      };
      if (params.path === undefined) {
        params.path = {};
      }
      params.path.id = id;
      return this.makeResourceRequest(endpoint, method, data, params, callback, entityClass);
    }

    /**
     * Make a request to return a resource after do the request using the verb specified with method param.
     *
     * @param {string} method         en HTTP verb
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     *
     * @return {object | Entity}
     */
  }, {
    key: "makeResourceRequest",
    value: function makeResourceRequest(endpoint, method, data, params, callback, entityClass) {
      var _this7 = this;
      // Check params structure.
      params = params || {
        path: {},
        query: {},
        isAdmin: false
      };
      if (params.query === undefined) {
        params.query = {};
      }
      if (params.isAdmin === undefined) {
        params.isAdmin = false;
      }
      var request = this.prepareRequest(endpoint, {
        method: method,
        isAdmin: params.isAdmin,
        parameters: params.path,
        query: params.query,
        timeout: params.timeout || this.timeout
      });
      if (data) {
        request.data = data;
      }
      return this.makeRequest(request).then(function (res) {
        if (callback) {
          return callback(res);
        }
        if (res != null) {
          return _this7.getEntityInstance(entityClass, res[Object.keys(res)[0]]);
        } else {
          return null;
        }
      });
    }

    /**
     * Make a request to return an entity or object after do the request using the verb specified with method param.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     *            Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version   Ago 2020
     *
     * @param {string} method         en HTTP verb
     * @param {number|string} id      Id
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     *
     * @return {object | Entity}
     */
  }, {
    key: "makeFileRequest",
    value: function makeFileRequest(endpoint, id, isAdmin, params) {
      params = (0, _isObject2["default"])(params) ? params : {};
      var parameters = (0, _get2["default"])(params, 'parameters', {});
      delete params.parameters;
      var request = _objectSpread(_objectSpread({}, this.prepareRequest(endpoint, {
        method: _constants["default"].HTTP_GET,
        isAdmin: isAdmin,
        parameters: _objectSpread({
          id: id
        }, parameters),
        query: _objectSpread({}, params)
      })), {}, {
        responseType: 'blob'
      });
      return this.makeRequest(request);
    }

    /**
     * Returns an Entity as result of a GET request to the endpoint given
     *
     * @param {string}            endpoint
     * @param {BigInteger|string} id
     * @param {boolean|null}      isAdmin
     * @param {object|null}       params
     * @param {string|null}       entityClass
     *
     * @returns {Entity}
     */
  }, {
    key: "getEntityRequest",
    value: function getEntityRequest(endpoint, id, isAdmin, params, entityClass) {
      var _this8 = this;
      params = (0, _isObject2["default"])(params) ? params : {};
      var routeParams = (0, _get2["default"])(params, 'parameters', {
        id: id
      });
      delete params['parameters'];
      var request = this.prepareRequest(endpoint, {
        method: 'GET',
        isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
        parameters: routeParams,
        query: params
      });
      return this.makeRequest(request).then(function (res) {
        return _this8.getEntityInstance(entityClass, res[Object.keys(res)[0]]);
      });
    }

    /**
     * Creates an Entity instance based on the entityClass param.
     * 
     * @param {string|null} entityClass 
     * @param {*} data 
     * @returns 
     */
  }, {
    key: "getEntityInstance",
    value: function getEntityInstance(entityClass, data) {
      if (entityClass && entityHandlers.hasOwnProperty(entityClass)) {
        return new entityHandlers[entityClass](data);
      }
      return new _entity["default"](data);
    }

    /**
     * Returns an Entity as result of a POST request to the endpoint given
     *  
     * @param {string} endpoint 
     * @param {*} data 
     * @param {boolean} isAdmin 
     * @returns {Entity}
     */
  }, {
    key: "addEntityRequest",
    value: function addEntityRequest(endpoint, data, isAdmin) {
      var params = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
      return this.makeResourceRequest(endpoint, _constants["default"].HTTP_POST, data, _objectSpread(_objectSpread({}, params), {}, {
        isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin
      }));
    }

    /**
     * Returns an Entity as result of a PATCH request to the endpoint given.
     * 
     * @param {string} endpoint 
     * @param {*} id 
     * @param {*} data 
     * @param {boolean} isAdmin 
     * @returns {Entity}
     */
  }, {
    key: "editEntityRequest",
    value: function editEntityRequest(endpoint, id, data, isAdmin) {
      var params = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
      return this.makeResourceRequest(endpoint, _constants["default"].HTTP_PATCH, data, _objectSpread({
        isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
        path: {
          id: id
        }
      }, params), null);
    }

    /**
     * Deletes an entity.
     *  
     * @param {string} endpoint 
     * @param {*} id 
     * @param {boolean} isAdmin 
     * @returns {boolean} true if the entity was deleted
     */
  }, {
    key: "deleteResourceRequest",
    value: function deleteResourceRequest(endpoint, id, isAdmin, params) {
      return this.makeResourceRequest(endpoint, _constants["default"].HTTP_DELETE, null, {
        isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
        path: _objectSpread({
          id: id
        }, params && params.path ? params.path : {})
      });
    }

    /**
     * Export the data based on options object elements.
     *
     * @param {string} endpoint
     * @param {object} query
     * @param {object} config
     * @param {object[]} fields
     * @param { string } filename
     *
     * @returns {object}
     */
  }, {
    key: "makeCollectionExportRequest",
    value: function makeCollectionExportRequest(endpoint, query, limit, fields, filename, progressHandler) {
      limit = limit || 100;
      query = _objectSpread(_objectSpread({}, query), {}, {
        page: 1,
        limit: limit
      });

      // TODO we should create an Request object to manage the request along the code.
      this.makeCollectionRequest(endpoint, query).then(function (collection) {
        var pageLimit = query.pageLimit || collection.getPageCount() - 1;
        var exportDataQuery = /*#__PURE__*/function () {
          var _ref = _asyncToGenerator(/*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
            var total, exportData, tCollection;
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  total = collection.getTotal(); // Create the export-agent object
                  exportData = new _DataExport["default"].DataExport(fields, _DataExport["default"].EXCEL_FORMAT, filename); // Export the first page.
                  exportData.addData(collection.getItems());
                  progressHandler && progressHandler(Math.min(100, Math.round(100 * (query.page * query.limit) / total)));

                  // Iterate over the paginated results
                case 4:
                  if (!(query.page <= pageLimit)) {
                    _context2.next = 13;
                    break;
                  }
                  query.page = query.page + 1;
                  _context2.next = 8;
                  return collection.paginate(query.page);
                case 8:
                  tCollection = _context2.sent;
                  exportData.addData(tCollection.getItems());
                  progressHandler && progressHandler(Math.min(100, Math.round(100 * (query.page * query.limit) / total)));
                  _context2.next = 4;
                  break;
                case 13:
                  return _context2.abrupt("return", new Promise(function (resolve, reject) {
                    return resolve(exportData);
                  }));
                case 14:
                case "end":
                  return _context2.stop();
              }
            }, _callee2);
          }));
          return function exportDataQuery() {
            return _ref.apply(this, arguments);
          };
        }();
        exportDataQuery().then(function (exportData) {
          exportData.convertTo();
          exportData.download();
        });
      });
    }
  }]);
}();
var _default = exports["default"] = VolcanoApi;