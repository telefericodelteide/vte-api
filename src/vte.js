import url from 'url'
import axios from 'axios'
import regenerationRuntime from "regenerator-runtime"
import jwt_decode from 'jwt-decode'
import constants from './constants'
import Errors from './model/errors'
import Collection from './model/entity/collection'
import Entity from './model/entity/entity'
import RoleClient from './model/role'
import PermissionClient from './model/permission'
import ContentClient from './model/content/content'
import BookingClient from './model/booking'
import UserClient from './model/user'
import InvoiceClient from './model/invoice'
import PaymentGatewayClient from './model/paymentGateway'
import PaymentTransactionClient from './model/paymentTransaction'
import CartClient from './model/cart'
import RouteClient from './model/route'
import CatalogClient from './model/catalog/catalog'
import ExperienceClient from './model/experience'
import IntermediaryClient from './model/intermediary'
import CollaboratorClient from './model/collaborator'
import RefundClient from './model/refund'
import EnterpriseClient from './model/enterprise'
import OrderClient from './model/order'
import LiquidationClient from './model/liquidation'
import BillingInformationClient from './model/billingInformation'
import User from './model/entity/user'
import TaskClient from './model/task'
import AccessControlClient from './model/accessControl'
import ActivityManagerClient from './model/activityManager/activityManager'
import SuggestionClient from './model/suggestion'
import VenueClient from './model/venues/venue'
import CurrencyClient from './model/currency'
import VbmsClient from './model/vbms'
import SchemaClient from './model/schema'
import CrmClient from './model/crm/crm'
import BillingClient from "./model/crm/client";
import DownloadClient from './model/download'
import DataExport from './lib/DataExport'
import _isObject from 'lodash/isObject'
import _get from 'lodash/get'
import _set from 'lodash/set'
import NotificationClient from './model/notifications'
import Product from './model/entity/catalog/product'
import HubspotClient from './model/hubspot/hubspot'

const entityHandlers = {
    'default': Entity,
    'user': User,
    'product': Product,
}

/**
 * This class is the Volcano API JS Core.
 *
 * @class     VolcanoApi
 * @extends   None
 */
class VolcanoApi {
    /**
     * @constructor
     *
     * @param {object} options
     */
    constructor(options) {
        this.options = options

        this.protocol = options.protocol || constants.HTTPS_PROTOCOL
        this.host = options.host
        this.port = options.port || null
        this.base = options.base || ''
        this.timeout = options.timeout || 5000
        this.locale = options.locale || 'en'
        this.strictSSL = options.hasOwnProperty(constants.STRICT_SSL)
            ? options.strictSSL
            : true

        this.initialize(this.options)
    }

    /**
     * Initilizes client.
     * 
     * @param {*} options 
     */
    initialize(options) {
        this.token = false

        this.baseOptions = { auth: {}, default_params: {} }

        if (options.api_key && options.api_secret) {
            this.baseOptions.auth = {
                api_key: options.api_key,
                api_secret: options.api_secret,
            }
        } else if (options.username && options.password) {
            this.baseOptions.auth = {
                username: options.username,
                password: options.password,
            }
        }

        if (options.site_key) {
            this.baseOptions.auth = {
                ...this.baseOptions.auth,
                ...{
                    type: constants.API_KEY_LABEL,
                    token: options.site_key,
                    headerName: constants.X_API_KEY_LABEL,
                },
            }
        }

        if (options.bearer) {
            this.setToken(options.bearer)
        }

        if (
            options.bearer_interceptor &&
            typeof options.bearer_interceptor === 'function'
        ) {
            this.interceptor = options.bearer_interceptor
        }

        if (options.with_default_params) {
            this.baseOptions.with_default_params = options.with_default_params
        } else {
            options.with_default_params = false
        }

        this.client = this.getClient()

        // Here are the list of models included and which are
        // accesible from the VolcanoApi instances.

        // You must include here the models
        this.download = new DownloadClient(this)
        this.vbms = new VbmsClient(this)
        this.schema = new SchemaClient(this)
        this.role = new RoleClient(this)
        this.permission = new PermissionClient(this)
        this.user = new UserClient(this)
        this.content = new ContentClient(this)
        this.paymentGateway = new PaymentGatewayClient(this)
        this.paymentTransaction = new PaymentTransactionClient(this)
        this.invoice = new InvoiceClient(this)
        this.cart = new CartClient(this)
        this.booking = new BookingClient(this)
        this.catalog = new CatalogClient(this)
        this.experience = new ExperienceClient(this)
        this.crm = new CrmClient(this)
        this.intermediary = new IntermediaryClient(this)
        this.collaborator = new CollaboratorClient(this)
        this.activityManager = new ActivityManagerClient(this)
        this.route = new RouteClient(this)
        this.refund = new RefundClient(this)
        this.enterprise = new EnterpriseClient(this)
        this.order = new OrderClient(this)
        this.liquidation = new LiquidationClient(this)
        this.billingInformation = new BillingInformationClient(this)
        this.task = new TaskClient(this)
        this.accessControl = new AccessControlClient(this)
        this.suggestion = new SuggestionClient(this)
        this.venue = new VenueClient(this)
        this.currency = new CurrencyClient(this)
        this.billingClient = new BillingClient(this)
        this.notifications = new NotificationClient(this)
        this.hubspot = new HubspotClient(this)
    }

    /**
     * Get the config of axios instance or the instance itself
     *
     * @returns object | AxiosInstance
     */
    getClient() {
        const instance = axios.create()

        instance.interceptors.request.use((config) => {
            config.headers = {
                ...config.headers,
                Accept: constants.APPLICATION_JSON,
                'Content-Type': constants.APPLICATION_JSON,
                'Accept-Language': this.locale,
            }

            if (this.interceptor) {
                this.setToken(this.interceptor())
            }

            if (this.baseOptions.auth.headerName) {
                config.headers[this.baseOptions.auth.headerName] =
                    this.baseOptions.auth.token
            }

            return config
        })

        return instance
    }

    /**
     * Set as token the return value of the callback function.
     *
     * @param {function} callback   Function that returns the JWT token
     */
    setTokenInterceptor(callback) {
        this.setToken(callback())
    }

    /**
     * Set token using the parameter.
     *
     * @param {string} token
     */
    setToken(token) {
        if (token) {
            this.baseOptions.auth = {
                type: constants.BEARER_LABEL,
                token: token,
                headerName: constants.AUTHORIZATION,
            }
            this.token = token

            if (this.baseOptions.with_default_params) {
                const payload = this.getPayload()

                if (payload.enterprise_id) {
                    this.baseOptions.default_params = {
                        ...this.baseOptions.default_params,
                        enterprise_id: payload.enterprise_id
                    }
                }
            }
        }

        return this.token
    }

    /**
     * Set default parameter.
     *
     * @param {string} key
     * @param {object} value
     */
    setDefaultParam(key, value) {
        _set(this.baseOptions.default_params, key, value)
    }

    /**
     * Return the token set into the object.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @returns boolean | string
     *
     * @see   Why we don't throw an exception instead returning a boolean.
     *        Even we can return undefined or an empty string.
     *        It would be mode consistenent with the function itself.
     */
    getToken() {
        if (!this.token) {
            return false
        }
        return this.token
    }

    /**
     * Returns the token or an exception if it is not set.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @returns {string}  the decoded token.
     * @throws  {Error}   if the token is not set.
     */
    getPayload() {
        if (!this.token) {
            throw new Errors.InvalidTokenException()
        }

        return jwt_decode(this.token)
    }

    /**
     * Authenticate the user using username and password
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} username
     * @param {string} password
     *
     * @returns {object}
     */
    authenticate(username, password, corporateAccount = null) {
        var data = {}

        if (username && password) {
            data = {
                username: username,
                password: password,
            }

            if (corporateAccount) {
                data = {
                    ...data,
                    corporate_account: corporateAccount
                }
            }
        } else if (this.baseOptions.auth.username) {
            data = {
                username: this.baseOptions.auth.username,
                password: this.baseOptions.auth.password,
            }
        } else if (this.baseOptions.auth.api_key) {
            data = {
                api_key: this.baseOptions.auth.api_key,
                api_secret: this.baseOptions.auth.api_secret,
            }
        }

        const request = this.prepareRequest(constants.TOKEN_LABEL, {
            method: constants.HTTP_POST,
            data: data,
            ignore_default_params: true
        })

        return this.makeRequest(request).then((res) => {
            if (res.success) {
                this.setToken(res.data.token)
                return this.user.getProfile().then((user) => {
                    return {
                        token: this.token,
                        user: user
                    }
                })
            }
        })
    }

    logout() {
        this.initialize(this.options)
    }

    /**
     * Authenticate the user using booking locator and customer email
     *
     * @param {string} bookingLocator
     * @param {string} customerEmail
     *
     * @returns {object}
     */
    authenticateCustomer(bookingLocator, customerEmail) {
        var data = {
            type: constants.SITE_LABEL,
            api_key: this.baseOptions.auth.token,
            booking_locator: bookingLocator,
            customer_email: customerEmail,
        }

        const request = this.prepareRequest(constants.TOKEN_LABEL, {
            method: constants.HTTP_POST,
            data: data,
        })

        return this.makeRequest(request).then((res) => {
            if (res.success) {
                this.setToken(res.data.token)
                return {
                    success: true,
                    token: this.token,
                }
            }
        })
    }

    /**
     * Do a reset password request for an application user and sends mail to user email with the reset password link.
     *
     * @param {string}  email               Email
     * @param {string}  corporateAccount    User corporate account
     *
     * @return {Entity}
     */
    resetPasswordRequest(email, corporateAccount) {
        const data = {
            email: email,
            corporate_account: corporateAccount,
        }

        return this.makeResourceRequest(
            constants.RESET_PASSWORD_REQUEST,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Reset an application user password
     *
     * @param { object } data   This should have the following format.
     *                          {
     *                            "reset_hash": "cb24413df603182af3beda902c73ba53",
     *                            "password": "test",
     *                            "password_repeat": "test"
     *                          }
     * 
     * @return {Entity}
     */
    resetPassword(data) {
        return this.makeResourceRequest(
            constants.RESET_PASSWORD,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Using the API endpoint to renew token.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Oct 2020
     */
    renewToken(enterpriseId = null) {
        const token = this.getToken()

        var data = {}

        if (enterpriseId !== null) {
            data = {
                enterprise_id: enterpriseId
            }
        }

        const request = this.prepareRequest(constants.TOKEN_RENEW, {
            method: constants.HTTP_POST,
            data: data,
            ignore_default_params: true,
            parameters: {
                token: token
            }
        })

        return this.makeRequest(request).then((res) => {
            if (res.success) {
                return this.setToken(res.data.token, true)
            }
        })
    }

    /**
     * Creates an object with all info needed by a request and using the  based on the default template for one
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} uri
     * @param {prepareRequestOptions} [options] - an object containing request parameters
     *
     * @returns {object}
     */
    prepareRequest(path, options = {}) {
        // check parameters
        if (options.parameters) {
            Object.entries(options.parameters).forEach((param) => {
                path = path.replace(':' + param[0], param[1])
            })
        }

        if (this.baseOptions.with_default_params && !options.ignore_default_params) {
            options.query = {
                ...this.baseOptions.default_params,
                ...options.query
            }
        }

        const result = {
            url: options.isFull
                ? path
                : options.isAdmin
                    ? this.makeAdminUri(path)
                    : this.makeUri(path),
            method: options.method || constants.HTTP_GET,
            timeout: options.timeout || this.timeout,
            data: options.data || null,
            params: options.query,
        }

        if (options.currency) {
            result.headers = {
                'x-currency': options.currency || null,
            }
        }

        return result
    }

    /**
     * Create a valid URI with the path given
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} path    String to be added at the end of the URL
     *
     * @returns {string}  a wellformed URI
     */
    makeUri(path) {
        const uri = url.format({
            protocol: this.protocol,
            hostname: this.host,
            port: this.port,
            pathname: `${this.base}/${path}`,
        })

        return decodeURIComponent(uri)
    }

    /**
     * Returns an URI with the admin prefix and with path as sufix
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} path
     *
     * @returns {string}
     */
    makeAdminUri(path) {
        return this.makeUri(constants.ADMIN_URL_PREFIX + `/${path}`)
    }

    /**
     * Do a request and returns the response's data or false if the callback is called.
     *
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {AxiosRequestConfig}  requestOptions
     * @param {function}            errorCallback
     *
     * @throws {Error}  An error with the following data:
     *                    code:     HTTP Error code.
     *                    message:  Human readable message from the response.
     *                    type:     Error type
     *                    data:     Additional data of the error.
     * @returns {object | boolean}
     */
    async makeRequest(requestOptions, errorCallback) {
        try {
            const res = await this.client.request(requestOptions)

            if (res.data && res.data.error) {
                const err = new Error(res.data.message)
                err.type = res.data.type
                err.data = res.data.data
                err.code = res.data.code
                throw err
            } else {
                return res.data
            }
        } catch (err) {
            if (err instanceof Error) {
                throw err
            } else if (errorCallback) {
                errorCallback(err)
            }
        }

        return false
    }

    /**
     * Do a GET request to an 'admin/*' URL and return the data as a collection.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     * @version   Ago 2020
     *
     * @param {string} endpoint
     * @param {object} query
     * @param {object} config
     * @param {*} entityClass
     *
     * @returns {Collection}
     */
    makeCollectionRequest(endpoint, query, config, entityClass = null) {

        entityClass = entityClass || Entity

        const baseConfig = {
            method: constants.HTTP_GET,
            isAdmin: true,
        }

        query = query || {}
        query = {
            ...query,
            page: query.page || constants.DEFAULT_PAGE_NUMBER,
            limit: query.limit || constants.DEFAULT_ELEMENTS_PER_PAGE,
        }

        const request = this.prepareRequest(endpoint, {
            ...baseConfig,
            ...config,
            query: query || null,
        })

        return this.makeRequest(request).then((res) => {
            return new Collection(this, res, entityClass)
        })
    }

    makeCollectionQueryRequest(endpoint, query, config, entityClass = null) {

        entityClass = entityClass || Entity

        const baseConfig = {
            method: constants.HTTP_GET,
        }

        query = query || {}

        if (query.isAdmin === undefined) {
            baseConfig.isAdmin = false
        }

        query = {
            ...query,
            page: query.page || constants.DEFAULT_PAGE_NUMBER,
            limit: query.limit || constants.DEFAULT_ELEMENTS_PER_PAGE,
        }

        const request = this.prepareRequest(endpoint, {
            ...baseConfig,
            ...config,
            query: query || null,
        })

        return this.makeRequest(request).then((res) => {
            return new Collection(this, res, entityClass)
        })
    }

    /**
     * Make a request to return an entity or object after do the request using the verb specified with method param.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     *            Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version   Ago 2020
     *
     * @param {string} method         en HTTP verb
     * @param {number|string} id      Id
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     * @param {string|null} entityClass Class to be used to create the entity.
     *
     * @return {object | Entity}
     */
    makeEntityRequest(endpoint, id, method, data, params, callback, entityClass) {
        // Check params structure.
        params = params || { path: {}, query: {}, isAdmin: false }

        if (params.path === undefined) {
            params.path = {}
        }

        params.path.id = id

        return this.makeResourceRequest(endpoint, method, data, params, callback, entityClass)
    }

    /**
     * Make a request to return a resource after do the request using the verb specified with method param.
     *
     * @param {string} method         en HTTP verb
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     *
     * @return {object | Entity}
     */
    makeResourceRequest(endpoint, method, data, params, callback, entityClass) {
        // Check params structure.
        params = params || { path: {}, query: {}, isAdmin: false }

        if (params.query === undefined) {
            params.query = {}
        }

        if (params.isAdmin === undefined) {
            params.isAdmin = false
        }

        const request = this.prepareRequest(endpoint, {
            method: method,
            isAdmin: params.isAdmin,
            parameters: params.path,
            query: params.query,
            timeout: params.timeout || this.timeout,
        })

        if (data) {
            request.data = data
        }

        return this.makeRequest(request).then((res) => {
            if (callback) {
                return callback(res)
            }

            if (res != null) {
                return this.getEntityInstance(entityClass, res[Object.keys(res)[0]])
            } else {
                return null
            }

        })
    }

    /**
     * Make a request to return an entity or object after do the request using the verb specified with method param.
     *
     * @author    Roberto Muñoz <rmunglez@gmail.com>
     *            Cayetano H. Osma <chernandez@volcanoteide.com>
     * @version   Ago 2020
     *
     * @param {string} method         en HTTP verb
     * @param {number|string} id      Id
     * @param {string} endpoint       endpoint without 'admin' prefix.
     * @param {object} data           request body. We never use PUT
     * @param {object} params         Dictionary with pairs params - values to be replaced on route
     *                                {
     *                                  path: { key: value },     parameters to change in the URL
     *                                  query: { key: value}      query parameters.
     *                                  isAdmin: true | false     specify admin prefix for the URL
     *                                }
     *                              where key is the route parameter name without colon.
     * @param {function} callback   Function to do extra processing before return data.
     *
     * @return {object | Entity}
     */
    makeFileRequest(endpoint, id, isAdmin, params) {

        params = _isObject(params) ? params : {}
        const parameters = _get(params, 'parameters', {})
        delete params.parameters

        const request = {
            ...this.prepareRequest(endpoint, {
                method: constants.HTTP_GET,
                isAdmin: isAdmin,
                parameters: {
                    id: id,
                    ...parameters
                },
                query: {
                    ...params,
                }
            }),
            responseType: 'blob',
        }

        return this.makeRequest(request)
    }

    /**
     * Returns an Entity as result of a GET request to the endpoint given
     *
     * @param {string}            endpoint
     * @param {BigInteger|string} id
     * @param {boolean|null}      isAdmin
     * @param {object|null}       params
     * @param {string|null}       entityClass
     *
     * @returns {Entity}
     */
    getEntityRequest(endpoint, id, isAdmin, params, entityClass) {
        params = _isObject(params) ? params : {}

        const routeParams = _get(params, 'parameters', { id: id })

        delete params['parameters']

        const request = this.prepareRequest(endpoint, {
            method: 'GET',
            isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
            parameters: routeParams,
            query: params
        })

        return this.makeRequest(request).then((res) => this.getEntityInstance(entityClass, res[Object.keys(res)[0]]))
    }

    /**
     * Creates an Entity instance based on the entityClass param.
     * 
     * @param {string|null} entityClass 
     * @param {*} data 
     * @returns 
     */
    getEntityInstance(entityClass, data) {
        if (entityClass && entityHandlers.hasOwnProperty(entityClass)) {
            return new entityHandlers[entityClass](data)
        }

        return new Entity(data)
    }

    /**
     * Returns an Entity as result of a POST request to the endpoint given
     *  
     * @param {string} endpoint 
     * @param {*} data 
     * @param {boolean} isAdmin 
     * @returns {Entity}
     */
    addEntityRequest(endpoint, data, isAdmin, params = {}) {
        return this.makeResourceRequest(
            endpoint,
            constants.HTTP_POST,
            data,
            {
                ...params,
                isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
            }
        )
    }

    /**
     * Returns an Entity as result of a PATCH request to the endpoint given.
     * 
     * @param {string} endpoint 
     * @param {*} id 
     * @param {*} data 
     * @param {boolean} isAdmin 
     * @returns {Entity}
     */
    editEntityRequest(endpoint, id, data, isAdmin, params = {}) {
        return this.makeResourceRequest(
            endpoint,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
                path: {
                    id: id
                },
                ...params
            },
            null
        )
    }

    /**
     * Deletes an entity.
     *  
     * @param {string} endpoint 
     * @param {*} id 
     * @param {boolean} isAdmin 
     * @returns {boolean} true if the entity was deleted
     */
    deleteResourceRequest(endpoint, id, isAdmin, params) {
        return this.makeResourceRequest(
            endpoint,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: typeof isAdmin === 'undefined' ? true : isAdmin,
                path: {
                    id: id,
                    ...(params && params.path ? params.path : {})
                }
            }
        )
    }

    /**
     * Export the data based on options object elements.
     *
     * @param {string} endpoint
     * @param {object} query
     * @param {object} config
     * @param {object[]} fields
     * @param { string } filename
     *
     * @returns {object}
     */
    makeCollectionExportRequest(
        endpoint,
        query,
        limit,
        fields,
        filename,
        progressHandler
    ) {
        limit = limit || 100
        query = {
            ...query,
            page: 1,
            limit: limit,
        }

        // TODO we should create an Request object to manage the request along the code.
        this.makeCollectionRequest(endpoint, query).then((collection) => {
            const pageLimit = query.pageLimit || collection.getPageCount() - 1

            const exportDataQuery = async () => {
                const total = collection.getTotal()

                // Create the export-agent object
                let exportData = new DataExport.DataExport(
                    fields,
                    DataExport.EXCEL_FORMAT,
                    filename
                )

                // Export the first page.
                exportData.addData(collection.getItems())
                progressHandler &&
                    progressHandler(
                        Math.min(100, Math.round(100 * (query.page * query.limit) / total))
                    )

                // Iterate over the paginated results
                while (query.page <= pageLimit) {
                    query.page = query.page + 1
                    const tCollection = await collection.paginate(query.page)
                    exportData.addData(tCollection.getItems())
                    progressHandler &&
                        progressHandler(
                            Math.min(100, Math.round(100 * (query.page * query.limit) / total))
                        )
                }

                return new Promise((resolve, reject) => resolve(exportData))
            }

            exportDataQuery().then((exportData) => {
                exportData.convertTo()
                exportData.download()
            })
        })
    }
}

export default VolcanoApi
