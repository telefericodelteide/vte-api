
/**
 * Return true if 
 * @param {date} d
 */
function isValidDate(d) {
  return d instanceof Date && !isNaN(d);
}

function setCookie(name, value, days) {
  value = value || "";
  days = days || 1;
  var date = new Date();
  date.setTime(date.getTime() + (days*24*60*60*1000));
  const expires = "; expires=" + date.toUTCString();
  document.cookie = name + "=" + value  + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function deleteCookie(name) {   
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/**
 * Return true if object is empty, otherwise return true
 *
 * @author Cayetano H. Osma <chernandez@volcanoteide.com>
 * @version Oct 2020
 *
 * @param {object} object   Object to be tested.
 *
 * @returns {boolean}
 */
function objectIsEmpty(object) {
  return (Object.keys(object).length === 0 && object.constructor === Object);
}

export {
  isValidDate,
  setCookie,
  getCookie,
  deleteCookie,
  objectIsEmpty
}