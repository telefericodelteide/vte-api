import VolcanoApi from "../vte"
import constants from '../constants'

const ENDPOINT_GET_CONTACTS_SCHEMA = 'crm/contacts/schema'
const ENDPOINT_GET_REFUNDS_SCHEMA = 'refunds/schema'
const ENDPOINT_GET_CRM_SCHEMA = 'crm/schema'
const ENDPOINT_GET_NOTIFICATIONS_SCHEMA = 'notification-templates/schema'
const ENDPOINT_GET_PRODUCTS_SCHEMA = 'catalog/products/schema'

export default SchemaClient

/**
 * Container which has the schemas to get form config to create/edit an entity
 *
 * @param {VolcanoApi} volcanoClient
 */
function SchemaClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    this.getContactsSchema = () => {
        return this.getSchema(ENDPOINT_GET_CONTACTS_SCHEMA, true)
    }

    this.getRefundsSchema = () => {
        return this.getSchema(ENDPOINT_GET_REFUNDS_SCHEMA, true)
    }

    this.getCrmIntermediariesSchema = () => {
        return this.getSchema(ENDPOINT_GET_CRM_SCHEMA, true)
    }

    this.getNotificationsSchema = () => {
        return this.getSchema(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, false)
    }

    this.getEntitySchema = (entity) => {
        switch (entity ) {
            case 'contact_schema' :
                return this.getSchema(ENDPOINT_GET_CONTACTS_SCHEMA, true)
            case 'crm_intermediaries_schema' :
                return this.getSchema(ENDPOINT_GET_CRM_SCHEMA, true)
            case 'notifications_schema':
                return this.getSchema(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, false);
            case 'products_schema':
                return this.getSchema(ENDPOINT_GET_PRODUCTS_SCHEMA, true);
            default:
                return this.getSchema(ENDPOINT_GET_REFUNDS_SCHEMA, true)
        }
    }

    this.getSchema = (endpoint, isAdmin) => {
        const request = this.volcanoClient.prepareRequest(endpoint, {
            method: constants.HTTP_GET,
            isAdmin: isAdmin
        })

        return this.volcanoClient.makeRequest(request)
    }
}