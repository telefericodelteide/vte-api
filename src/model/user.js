import { isEmpty, replace } from "lodash";
import constants from '../constants';
import Entity from './entity/entity';
import User from "./entity/user";
export default UserClient;

const API_ADMIN_USERS = "users";

// TYPES
const USER_TYPE_APPLICATION = 'application'
const USER_TYPE_SITE = 'site'
const USER_TYPE_API = 'api'
//

// Endpoints
const USER_GET_ME = 'auth/me'
const USER_GET_USERS = 'auth/:type-users'
const USER_GET_USER_BY_ID = 'auth/:type-users/:id'
const USER_GET_USER_PERMISSIONS = 'auth/:type-users/:id/permissions'
const USER_ADD = 'auth/:type-users'
const USER_EDIT = 'auth/:type-users/:id/edit'
const USER_SET_ACTIVE = 'auth/:type-users/:id/set-active'
const USER_DELETE = 'auth/:type-users/:id'

export {
    USER_TYPE_APPLICATION,
    USER_TYPE_SITE,
    USER_TYPE_API,
    USER_GET_ME,
    USER_GET_USERS,
    USER_GET_USER_BY_ID,
    USER_GET_USER_PERMISSIONS,
    USER_ADD,
    USER_EDIT,
    USER_SET_ACTIVE,
    USER_DELETE
}

const prepareAppUserDataToSave = (data) => {
    data = {
        ...data,
        name: data.first_name,
        surname: data.last_name
    }

    if (data.active !== undefined) {
        data = {
            ...data,
            active: JSON.parse(data.active)
        }
    }

    delete data.first_name
    delete data.last_name

    return data
}

/**
 * Uses to access Volcano API user endpoints.
 *
 * @constructor UserClient
 * @param {VolcanoClient} volcanoClient
 */
function UserClient(volcanoClient) {
    this.volcanoClient = volcanoClient;

    this.getProfile = (userId) => {
        const params = userId ? {} : { enterprise_id: null }

        if (!userId) {
            const payload = this.volcanoClient.getPayload();
            userId = payload.sub;
        }

        // load the user and their permissions
        return Promise.all([
            this.getApplicationUser(userId, params),
            this.getUserPermissions(userId, params),
        ]).then(function ([user, permissions]) {
            user.permissions = permissions;

            return user;
        });
    };

    this.getUserPermissions = (userId, params) => {
        const request = this.volcanoClient.prepareRequest(USER_GET_USER_PERMISSIONS, {
            method: "GET",
            isAdmin: true,
            parameters: {
                id: userId,
                type: USER_TYPE_APPLICATION
            },
            query: params
        });

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]];
        });
    };

    this.getApplicationUsers = (params) => {
        return this.volcanoClient.makeCollectionRequest(USER_GET_USERS, params, {
            parameters: {
                type: USER_TYPE_APPLICATION
            }
        });
    };

    this.getUser = (endpoint, userId, params) => {
        return this.volcanoClient.getEntityRequest(endpoint, userId, true, params).then((res) => new User(res));
    };

    this.me = () => {
        const endpoint = USER_GET_ME;

        return this.getUser(endpoint, null);
    }

    this.getApplicationUser = (userId, params) => {
        const endpoint = replace(USER_GET_USER_BY_ID, ":type", USER_TYPE_APPLICATION);

        return this.getUser(endpoint, userId, params);
    };

    this.getApiUser = (userId) => {
        const endpoint = replace(USER_GET_USER_BY_ID, ':type', USER_TYPE_API);

        return this.getUser(endpoint, userId);
    };

    this.getSiteUser = (userId) => {

        if (!userId) {
            // load all sites and get first
            const endpoint = replace(USER_GET_USERS, ':type', USER_TYPE_SITE);
            return this.get
        }

        const endpoint = replace(USER_GET_USER_BY_ID, ':type', USER_TYPE_SITE);
        return this.getUser(endpoint, userId);
    };

    this.exportApplicationUsers = (fields, params, progressHandler) => {
        const endpoint = replace(USER_GET_USERS, ':type', USER_TYPE_APPLICATION);

        const user_application_type = "" + params['is_admin'];
        params = {
            ...params,
            application_type: user_application_type === '' ? 'all' : (user_application_type === "0" ? 'external' : 'internal')
        }

        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            null,
            fields,
            "users",
            progressHandler
        );
    };

    /**
       * Create user
       *
       * @param { object } data   This should have the following format.
       *                          {
       *                             "username": "username",
       *                             "password": "test",
       *                             "password_repeat": "test",
       *                             "name": "Name",
       *                             "surname": "",
       *                             "email": "test@domain.com",
       *                             "phone": "922333333",
       *                             "active": 1,
       *                             "collaborator_id": "393637",
       *                             "salesman": {
       *                                 "office_id": "",
       *                                 "id_card": "00000000T"
       *                             },
       *                             "roles": [
       *                                 {"id": 1}
       *                             ],
       *                             "collaborator_register": false
       *                         }
       * @param {string}  userType  User type (application, site or api)
       *
       * @return {Entity}
       */
    this.createUser = (data, userType) => {
        userType = (userType == undefined) ? USER_TYPE_APPLICATION : userType;

        if (userType == USER_TYPE_APPLICATION) {
            data = prepareAppUserDataToSave(data)
        }

        return this.processUserRequest(() => this.volcanoClient.makeResourceRequest(
            USER_ADD,
            constants.HTTP_POST,
            data,
            {
                path: {
                    type: userType
                },
                isAdmin: true
            },
            null,
            'user'
        ))
    }

    /**
       * Edit user
       *
       * @param {string}  userId  User identifier
       * @param { object } data   This should have the following format.
       *                          {
       *                            "username": "Mamen",
       *                            "name": "Carmen",
       *                            "surname": "test",
       *                            "email": "reservas@gaea-travel.com",
       *                            "phone": "",
       *                            "active": 1,
       *                            "corporate_account": {
       *                                "id": "b2fa82d7-28e0-11eb-80ba-0cc47ac3a6b8"
       *                            },
       *                            "collaborator_id": "393637",
       *                            "salesman": {
       *                                "office_id": "",
       *                                "id_card": "00000000T"
       *                            },
       *                            "roles": [
       *                                {"id": 1}
       *                            ],
       *                            "languages": [
       *                                {"id": "3"},
       *                                {"id": "4"}
       *                            ]
       *                        }
       * @param {string}  userType  User type (application, site or api)
       *
       * @return {Entity}
       */
    this.editUser = (userId, data, userType) => {
        if (isEmpty(userId)) {
            return null
        }

        if (userType == USER_TYPE_APPLICATION) {
            data = prepareAppUserDataToSave(data)
        }

        userType = (userType == undefined) ? USER_TYPE_APPLICATION : userType;

        return this.processUserRequest(() => this.volcanoClient.makeEntityRequest(
            USER_EDIT,
            userId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: userId,
                    type: userType,
                },
                isAdmin: true,
            },
            null,
            'user'
        ))
    }

    /**
       * Set user active / unactive
       *
       * @param {string}  userId  User identifier
       * @param {boolean} active  True to activate user, false otherwise
       * @param {string}  userType  User type (application, site or api)
       *
       * @return {Entity}
       */
    this.setUserActive = (userId, active, userType) => {
        if (isEmpty(userId)) {
            return null
        }

        userType = (userType == undefined) ? USER_TYPE_APPLICATION : userType;

        const data = {
            active: active ? 1 : 0
        }

        return this.volcanoClient.makeEntityRequest(
            USER_SET_ACTIVE,
            userId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: userId,
                    type: userType,
                },
                isAdmin: true,
            },
            null,
            'user'
        )
    }

    /**
       * Delete user
       *
       * @param {string}  userId  User identifier
       * @param {string}  userType  User type (application, site or api)
       *
       * @return {Entity}
       */
    this.deleteUser = (userId, userType) => {
        if (isEmpty(userId)) {
            return null
        }

        userType = (userType == undefined) ? USER_TYPE_APPLICATION : userType;

        return this.volcanoClient.makeEntityRequest(
            USER_DELETE,
            userId,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: userId,
                    type: userType,
                },
                isAdmin: true,
            },
            null,
            'user'
        )
    }

    /**
       * Change application user password
       *
       * @param {string}  userId  User identifier
       * @param { object } data   This should have the following format.
       *                          {
       *                            "password": "test",
       *                            "password_repeat": "test"
       *                          }
       *
       * @return {Entity}
       */
    this.appUserChangePassword = (userId, data) => {
        if (isEmpty(userId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            USER_EDIT,
            userId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: userId,
                    type: USER_TYPE_APPLICATION
                },
                isAdmin: true,
            },
            null,
            'user'
        )
    }

    this.processUserRequest = (request) => {
        return request()
            .then((res) => res)
            .catch((error) => {
                if (error.data?.application_user) {
                    const errorData = error.data.application_user;
                    if (errorData?.username?.unique) {
                        error.message = "Username already in use"
                    } else if (errorData?.email?.unique) {
                        error.message = "Email already in use"
                    }
                }

                throw error
            })
    }
}
