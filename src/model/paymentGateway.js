export default PaymentGatewayClient;

/**
 * Uses to access Volcano API payment gateways endpoints.
 * 
 * @constructor PaymentGatewayClient
 * @param {VolcanoClient} volcanoClient 
 */
function PaymentGatewayClient(volcanoClient) {
  this.volcanoClient = volcanoClient;
  
  this.getPaymentGateways = (params) => {
    const endpoint = 'payment-gateways';

    return this.volcanoClient.makeCollectionRequest(endpoint, params);
  }

  this.getPaymentGateway = (paymentGatewayId) => {
    const endpoint = 'payment-gateways/:id';
    return this.volcanoClient.getEntityRequest(endpoint, paymentGatewayId);
  }

}