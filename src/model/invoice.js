import constants from "../constants"
import { isObject } from 'lodash'
import Entity from './entity/entity'
import { format, isDate } from "date-fns"

// Endpoints
const ENDPOINT_GET_INVOICES = 'invoices'
const ENDPOINT_GET_INVOICE = ENDPOINT_GET_INVOICES + '/:id'
const ENDPOINT_GET_INVOICE_PDF = ENDPOINT_GET_INVOICE + '/pdf'
const ENDPOINT_CREATE_INVOICE = ENDPOINT_GET_INVOICES
const ENDPOINT_AMEND_INVOICE = ENDPOINT_GET_INVOICE + '/amend'
const ENDPOINT_GET_ACCOUNTANCY_CONFIGS = "accountancy-configs"

export default InvoiceClient

/**
 * Uses to access Volcano API invoice endpoints.
 *
 * @constructor InvoiceClient
 * @param {VolcanoClient} volcanoClient
 */
function InvoiceClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of invoices that matches the query parameters passed.
     *
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection}
     */
    this.getInvoices = (params) => {
        const endpoint = ENDPOINT_GET_INVOICES
        if (isDate(params.date_from)) {
            params.date_from = format(params.date_from, "yyyy-MM-dd")
        }
        return this.volcanoClient.makeCollectionRequest(endpoint, params)
    }

    this.getInvoice = (invoiceId) => {
        const endpoint = ENDPOINT_GET_INVOICE

        return this.volcanoClient.getEntityRequest(endpoint, invoiceId)
    }

    this.exportInvoices = (fields, params, progressHandler) => {
        const endpoint = ENDPOINT_GET_INVOICES
        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            null,
            fields,
            "invoices",
            progressHandler
        )
    }

    /**
     * Return an application/PDF octet
     *
     * @author Roberto Muñoz <rmunglez@gmail.com>
     * @version Nov 2020
     *
     * @param invoicegId
     *
     * @returns {null|Object|Entity}
     */
    this.getInvoicePdf = (invoiceId, params) => {
        const id = parseInt(invoiceId)
        if (isNaN(invoiceId)) {
            return null
        }

        params = isObject(params) ? params : {}

        return this.volcanoClient.makeFileRequest(ENDPOINT_GET_INVOICE_PDF, id, true, params)
    }

    /**
     * Make a request to create a customer or collaborator invoice
     *
     * @param {numeric | string}  orderId     Id of the booking which want to be cancelled.
     *
     * @return {Entity}
     */
    this.createInvoice = (orderId, data) => {
        const id = parseInt(orderId)
        if (isNaN(orderId)) {
            return null
        }

        data = {
            ...data,
            allow_repeated: true
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_CREATE_INVOICE,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
                query: {
                    order_id: id
                }
            },
            null
        )
    }

    /**
    * Make a request to create a customer or collaborator invoice
    *
    * @param {array}  bookings Bookings to generate invoice
    *
    * @return {Entity}
    */
    this.createBookingsInvoice = (bookings, data) => {
        data = {
            ...data,
            bookings: bookings
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_CREATE_INVOICE,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Make a request to update an invoice
     *
     * @param {numeric}  invoiceId id of the invoice to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editInvoice = (invoiceId, data) => {
        const id = parseInt(invoiceId)
        if (isNaN(invoiceId)) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_INVOICE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Make a request to update an invoice
     *
     * @param {numeric}  invoiceId     id of the invoice to be updated
     * @param {string}   amendmentType amendment type (full, partial)
     *
     * @return {Entity}
     */
    this.amendInvoice = (invoiceId, amendmentType) => {
        const id = parseInt(invoiceId)
        if (isNaN(invoiceId)) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_AMEND_INVOICE,
            constants.HTTP_POST,
            {
                amendment_type: amendmentType
            },
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Return a collection with all accountancy configs with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getAccountancyConfigs = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ACCOUNTANCY_CONFIGS,
            params
        );
    };
}
