import constants from "../constants";
import VolcanoApi from "../vte";

const ENDPOINT_DOWNLOADS = "dl"
const ENDPOINT_DOWNLOADS_CREATE = ENDPOINT_DOWNLOADS + "/create"


/**
 * Download resources client.
 *
 * @param {VolcanoApi} volcanoClient
 */
function DownloadClient(volcanoClient) {

    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    this.createLink = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_DOWNLOADS_CREATE,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }
}

export default DownloadClient;
