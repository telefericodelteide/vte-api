import VolcanoApi from "../vte";
import constants from "../constants";

export default EnterpriseClient;

// Endpoints
const ENDPOINT_GET_ENTERPRISES = "enterprises"
const ENDPOINT_GET_ENTERPRISE = ENDPOINT_GET_ENTERPRISES + "/:id"
const ENDPOINT_GET_ENTERPRISE_CURRENCIES = ENDPOINT_GET_ENTERPRISE + "/currencies"
const ENDPOINT_GET_ENTERPRISE_CURRENCY = ENDPOINT_GET_ENTERPRISE_CURRENCIES + "/:currencyId"
const ENDPOINT_GET_ENTERPRISE_TAXES = ENDPOINT_GET_ENTERPRISE + "/taxes"

/**
 * Container which has the enterprises methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function EnterpriseClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
     * Return a collection with all Enterprises with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getEnterprises = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ENTERPRISES,
            params,
            {
                ignore_default_params: true
            }
        );
    };

    /**
     * Return an Entity containing a tag which match by Id
     *
     * @param {string} [id]  enterprise ID.
     *
     * @returns {Entity}
     */
    this.getEnterprise = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_ENTERPRISE, id, true);
    };

    /**
     * Edit an enterprise which match by Id 
     *
     * @param {numeric}  id        id of the enterprise to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity} 
     */
    this.editEnterprise = (id, data) => {
        if (isNaN(parseInt(id))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ENTERPRISE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Return a collection with all currencies of an enterprise
     * 
     * @param {numeric} enterpriseId enterprise ID
     * @param {boolean} available    if true, return only available currencies not assigned to the enterprise
     * @return {Collection}
     */
    this.getCurrencies = (enterpriseId, available) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ENTERPRISE_CURRENCIES,
            { available: available },
            {
                parameters: {
                    id: enterpriseId,
                }
            }
        )
    }

    /**
         * Return an Entity containing a currency which match by Id `currencyId`.
         *
         * @param {numeric} [enterpriseId]  enterprise ID.
         * @param {string} [currencyId]  currency ID.
         *
         * @returns {Entity}
         */
    this.getCurrency = (enterpriseId, currencyId, params) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ENTERPRISE_CURRENCY,
            constants.HTTP_GET,
            [],
            {
                isAdmin: true,
                path: {
                    id: enterpriseId,
                    currencyId: currencyId.toLowerCase()
                }
            },
            null
        )
    }

    /**
     * Set currency as default for an enterprise
     *
     * @param {numeric}  id        id of the enterprise to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    /*this.setMainCurrency = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_ENTERPRISE_SET_MAIN_CURRENCY,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            }
        )
    }*/

    /**
     * Add a list of currencies to an enterprise
     *
     * @param {numeric} id id of the enterprise to be updated
     * @param {array} data data to be updated
     *
     * @return {Entity}
     */
    this.addCurrency = function (id, data) {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ENTERPRISE_CURRENCIES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            }
        )
    }

    /**
 * Edit the ocnfiguration of a currency in an enterprise
 *
 * @param {numeric} id id of the enterprise to be updated
 * @param {string} currencyId currency id to be updated
 * @param {array} data data to be updated
 *
 * @return {Entity}
 */
    this.editCurrency = function (id, currencyId, data) {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ENTERPRISE_CURRENCY,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id,
                    currencyId: currencyId.toLowerCase()
                }
            }
        )
    }

    /**
     * Remove a currency from an enterprise
     *
     * @param {numeric} id id of the enterprise to be updated
     * @param {string} currencyId currency id to be removed
     *
     * @return {bool}
     */
    this.deleteCurrency = (id, currencyId) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ENTERPRISE_CURRENCY,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id,
                    currencyId: currencyId.toLowerCase()
                }
            }
        )
    }

    /**
     * Return an Entity containing a tag which match by enterpriseId
     *
     * @param {string} [enterpriseId]  enterprise ID.
     *
     * @returns {Entity}
     */
    this.getTaxes = (enterpriseId) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_ENTERPRISE_TAXES,enterpriseId)
    }

}