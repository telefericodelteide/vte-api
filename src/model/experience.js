import { addMonths, format } from 'date-fns'
import isObject from 'lodash/isObject'

/**
 * Volcano API experience client
 *
 * The ExperienceClient class wraps the experience content resource and booking process endpoints of the Volcano REST API.
 *
 * @file   This files defines the ExperienceClient class.
 * @author Roberto Muñoz <rmunglez@gmail.com>
 * @author Cayetano H. Osma <chernandez@volcanoteide.com>
 * @version Ago 2020
 *
 * @since  0.1
 */
export default ExperienceClient

// Constants for the endpoints
const GET_EXPERIENCES = 'experiences'
const GET_PRODUCTS = 'experiences/:id/products'
const GET_PRODUCT_AVAILABILITY = 'products/:id/availability/:date'
const GET_PRODUCT_RATES = 'products/:id/rates/:date'

/**
 * Uses to access Volcano API experience content endpoints.
 *
 * @constructor ExperienceClient
 * @param {VolcanoClient} volcanoClient
 */
function ExperienceClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Retrieve all available experiences from API
     *
     * @param {Object} params
     * @returns {Collection}
     */
    this.getExperiences = (params) => {
        params = params || {}
        return this.volcanoClient.makeCollectionRequest(GET_EXPERIENCES, params)
    }

    /**
     * Retrieve all available products of an experiences from API
     * 
     * @param {number} experienceId
     *
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProducts = (experienceId) => {
        const request = this.volcanoClient.prepareRequest(GET_PRODUCTS, {
            method: 'GET',
            isAdmin: false,
            parameters: {
                id: experienceId,
            },
        })

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Return the experience for ID given.
     *
     * @param {number} experienceId
     *
     * @returns {PromiseLike<Entity> | Promise<Entity>}
     */
    this.getExperience = (experienceId) => {
        return this.volcanoClient.content.getExperience(experienceId)
    }

    /**
     * Returns the availability for a product.
     *
     * @param {number} productId
     * @param {Date} date
     * @param {boolean} fullMonth true if the availability for the entire month of the date should be retrieved
     * @param {params} params
     *
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductAvailability = (productId, date, fullMonth, params) => {
        if (!date) {
            date = new Date()
        }

        params = isObject(params) ? params : {}

        const request = this.volcanoClient.prepareRequest(
            GET_PRODUCT_AVAILABILITY,
            {
                method: 'GET',
                isAdmin: false,
                parameters: {
                    id: productId,
                    date: fullMonth
                        ? format(date, 'yyyy-MM')
                        : format(date, 'yyyy-MM-dd'),
                },
                query: {
                    ...params,
                },
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Returns the availability for a product.
     *
     * @param {number} productId
     * @param {Date} date
     * @param {number} months number of months to query
     * @param {params} params
     **
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductAvailabilityRange = (productId, date, months, params) => {
        if (!date) {
            date = new Date()
        }

        if (!months) {
            months = 1
        }

        params = isObject(params) ? params : {}

        var requests = new Array(months)
        for (var i = 0; i < months; i++) {
            const request = this.volcanoClient.prepareRequest(
                GET_PRODUCT_AVAILABILITY,
                {
                    method: 'GET',
                    isAdmin: false,
                    parameters: {
                        id: productId,
                        date: format(date, 'yyyy-MM'),
                    },
                    query: {
                        ...params,
                    },
                }
            )

            date = addMonths(date, 1)

            requests[i] = this.volcanoClient
                .makeRequest(request)
                .then((res) => {
                    return res[Object.keys(res)[0]]
                })
        }

        return Promise.all(requests).then((values) => {
            return values.flat()
        })
    }

    /**
     * Returns the rates for a product in a specific date
     * 
     * @param {number} productId
     * @param {Date} date
     *
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductRates = (productId, date, collaboratorId = null) => {
        const params = (collaboratorId == null) ? {} : { collaborator_id: collaboratorId }

        if (!date) {
            date = new Date()
        }

        const request = this.volcanoClient.prepareRequest(GET_PRODUCT_RATES, {
            method: 'GET',
            isAdmin: false,
            parameters: {
                id: productId,
                date: format(date, 'yyyy-MM-dd'),
            },
            query: params
        })

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }
}
