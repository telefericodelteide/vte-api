import constants from "../constants"
import Entity from "./entity/entity"

// Endpoints
const ENDPOINT_GET_BILLING_INFORMATIONS = "billing-informations"
const ENDPOINT_GET_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id"
const ENDPOINT_CREATE_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS
const ENDPOINT_CREATE_BILLING_INFORMATION_INVOICE = ENDPOINT_GET_BILLING_INFORMATION + '/invoices'
const ENDPOINT_EDIT_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id"
const ENDPOINT_DELETE_BILLING_INFORMATION = ENDPOINT_GET_BILLING_INFORMATIONS + "/:id"

export default BillingInformationClient

/**
 * Uses to access Volcano API billing information endpoints.
 *
 * @constructor BillingInformationClient
 * @param {VolcanoClient} volcanoClient
 */
function BillingInformationClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of billing information hat matches the query parameters passed.
     *
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection}
     */
    this.getBillingInformations = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_BILLING_INFORMATIONS, params)
    }

    this.getBillingInformation = (billingInformationId) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_BILLING_INFORMATION, billingInformationId)
    }

    this.exportBillingInformations = (fields, params, progressHandler) => {
        return this.volcanoClient.makeCollectionExportRequest(
            ENDPOINT_GET_BILLING_INFORMATIONS,
            params,
            null,
            fields,
            "billing-informations",
            progressHandler
        )
    }

    /**
     * Make a request to create a customer or collaborator billing information
     *
     * @param { object } data   This should have the following format.
     *
     * @return {Entity}
     */
    this.createBillingInformation = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_CREATE_BILLING_INFORMATION,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Make a request to update an billing information
     *
     * @param {string}  billingInformationId id of the billing information to be updated
     * @param { object } data   This should have the following format.
     *
     * @return {Entity}
     */
    this.editBillingInformation = (billingInformationId, data) => {
        if (!billingInformationId) {
            return null;
        }
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_EDIT_BILLING_INFORMATION,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: billingInformationId
                }
            },
            null
        )
    }

    /**
   * Delete billing information
   *
   * @param {string}  billingInformationId  Billing information identifier
   * 
   * @return {Entity}
   */
    this.deleteBillingInformation = (billingInformationId) => {
        if (!billingInformationId) {
            return null;
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_DELETE_BILLING_INFORMATION,
            billingInformationId,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
            },
            null
        )
    };
}
