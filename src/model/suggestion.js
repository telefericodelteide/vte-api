import isEmpty from 'lodash/isEmpty';
import constants from "../constants";
import VolcanoApi from "../vte";

// Endpoints
const ENDPOINT_GET_SUGGESTIONS = "suggestions";
const ENDPOINT_GET_SUGGESTION = ENDPOINT_GET_SUGGESTIONS + "/:id";
const ENDPOINT_SUGGESTION_SET_MANAGED = ENDPOINT_GET_SUGGESTION + "/set-managed";

export default SuggestionClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SuggestionClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Suggestion methods */

    this.addSuggestion = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_SUGGESTIONS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: false,
            }
        );
    };

    /**
     * Return a collection with all suggestions with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getSuggestions = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_SUGGESTIONS,
            params
        );
    };

    /**
     * Return an Entity containing a suggestion which match by Id `suggestionId`.
     *
     * @param {string} [id]  suggestion ID.
     *
     * @returns {Entity}
     */
    this.getSuggestion = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_SUGGESTION, id, true, {
            parameters: { id: id },
        });
    };

    /**
    * Set as 'managed' a suggestion that matches the id passed.
    * 
    * @param {string} id
    * @param {boolean} managed 
    * @returns 
    */
    this.setSuggestionManaged = (id, managed) => {
        if (isEmpty(id)) {
            return null
        };
        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_SUGGESTION_SET_MANAGED,
            id,
            constants.HTTP_POST,
            { managed: managed },
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            }
        )
    }
}
