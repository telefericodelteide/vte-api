import isEmpty from 'lodash/isEmpty'
import Entity from './entity/entity'
import constants from '../constants'
import { isObject } from 'lodash'
import Product from './entity/catalog/product'

const ENDPOINT_SIGNUP_INTERMEDIARY = 'intermediaries/signup'
const ENDPOINT_GET_INTERMEDIARIES = 'crm/intermediaries'
const ENDPOINT_GET_INTERMEDIARY = ENDPOINT_GET_INTERMEDIARIES + '/:id'
const ENDPOINT_GET_INTERMEDIARY_PDF = ENDPOINT_GET_INTERMEDIARY + '/pdf'
const ENDPOINT_GET_INTERMEDIARY_PRODUCTS = ENDPOINT_GET_INTERMEDIARY + '/products'
const ENDPOINT_GET_INTERMEDIARY_PRODUCT = ENDPOINT_GET_INTERMEDIARY_PRODUCTS + '/:product_id'

const ENDPOINT_GET_OFFICES = 'crm/offices'
const ENDPOINT_GET_OFFICE = ENDPOINT_GET_OFFICES + '/:id'
const ENDPOINT_GET_INTERMEDIARY_OFFICES = ENDPOINT_GET_INTERMEDIARY + '/offices'

const ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK = ENDPOINT_GET_INTERMEDIARY + '/payments/one-click'

const ENDPOINT_GET_ENTITY_ACTION_LOGS = ENDPOINT_GET_INTERMEDIARY + '/entity-action-logs'

export default IntermediaryClient

/**
 * Container which has the intermediary and offices methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function IntermediaryClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of intermediaries that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getIntermediaries = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_INTERMEDIARIES, params)
    }

    /**
     * Returns the intermediary that matches the id passed.
     * 
     * @param {string} intermediaryId 
     * @returns {Entity}
     */
    this.getIntermediary = (intermediaryId) => {
        const endpoint = 'crm/intermediaries/:id'

        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_INTERMEDIARY, intermediaryId)
    }

    this.signup = (data) => {
        const request = this.volcanoClient.prepareRequest(ENDPOINT_SIGNUP_INTERMEDIARY, {
            method: "POST",
            isAdmin: false,
            data: data
        })

        return this.volcanoClient.makeRequest(request)
            .then(res => new Entity(res))
    }

    /**
     * Changes the state of the intermediary that matches the id passed.
     * 
     * @param {string} intermediaryId 
     * @param {string} state 
     * @returns 
     */
    this.setIntermediaryState = (intermediaryId, state) => {

        if (isEmpty(intermediaryId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_INTERMEDIARY + '/change-state',
            intermediaryId,
            constants.HTTP_POST,
            {
                state: state
            },
            {
                path: {
                    id: intermediaryId,
                },
                isAdmin: true,
            }
        )
    }

    /**
     * Creates a new intermediary.
     * 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.createIntermediary = (data) => {

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_INTERMEDIARIES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            },
            null,
            'intermediary'
        )
    }

    /**
     * Updates the intermediary that matches the id passed.
     * 
     * @param {string} intermediaryId 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.editIntermediary = (intermediaryId, data) => {
        if (isEmpty(intermediaryId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_INTERMEDIARY,
            intermediaryId,
            constants.HTTP_PATCH,
            data,
            {
                path: {
                    id: intermediaryId,
                },
                isAdmin: true,
            }
        )
    }


    /** Intermediary offices management */


    /**
     * Returns the collection of offices that matches the query parameters passed.
     * 
     * @param {string|null} intermediaryId 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getIntermediaryOffices = (intermediaryId, params) => {
        if (!intermediaryId) {
            return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_OFFICES, params)
        } else {
            return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_INTERMEDIARY_OFFICES, params, {
                parameters: {
                    id: intermediaryId
                }
            })
        }
    }

    /**
     * Returns the office that matches the id passed.
     * 
     * @param {string} officeId 
     * @returns {Entity}
     */
    this.getIntermediaryOffice = (officeId) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_OFFICE, officeId)
    }

    /**
     * Creates a new intermediary office.
     * 
     * @param {string} intermediaryId 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.createIntermediaryOffice = (intermediaryId, data) => {
        if (isEmpty(intermediaryId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_INTERMEDIARY_OFFICES,
            intermediaryId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: intermediaryId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Updates the office that matches the id passed.
     * 
     * @param {string} officeId 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.editIntermediaryOffice = (officeId, data) => {
        if (isEmpty(officeId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_OFFICE,
            officeId,
            constants.HTTP_PATCH,
            data,
            {
                path: {
                    id: officeId,
                },
                isAdmin: true,
            }
        )
    }

    /**
     * Enables or disables the office that matches the id passed.
     * 
     * @param {string} officeId 
     * @param {boolean} isActive 
     * @returns 
     */
    this.setIntermediaryOfficeState = (officeId, isActive) => {
        if (isEmpty(officeId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_OFFICE + '/set-active',
            officeId,
            constants.HTTP_POST,
            { active: isActive },
            {
                path: {
                    id: officeId,
                },
                isAdmin: true,
            }
        )
    }

    /**
     * Requests a payment token for the intermediary that matches the id passed.
     *  
     * @param {string} intermediaryId 
     * @returns {object} payment data
     */
    this.requestPaymentToken = (intermediaryId) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK,
            constants.HTTP_POST,
            null,
            {
                isAdmin: true,
                path: {
                    id: intermediaryId
                }
            },
            null
        )
    }

    /**
     * Deletes the payment token for the intermediary that matches the id passed. 
     * @param {string} intermediaryId 
     * @returns 
     */
    this.deletePaymentToken = (intermediaryId) => {
        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_INTERMEDIARY_PAYMENT_ONECLICK,
            intermediaryId,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: intermediaryId,
                },
                isAdmin: true,
            }
        )
    }

    /** 
     * Returns a collection with all products of an intermediary with pagination.
     * 
     * @param {string} id intermediary identifier
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     * 
     * @returns {Collection} 
     */
    this.getIntermediaryProducts = (id, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_INTERMEDIARY_PRODUCTS,
            params,
            {
                parameters: { id: id },
            },
            Product
        )
    }

    /** 
     * Add and/or delete a collection of products from an intermediary.
     * 
     * @param {string} id intermediary identifier
     * @param {array} data 
     * 
     * @returns {Collection} 
     */
    this.editIntermediaryProducts = (id, data) => {
        return this.volcanoClient.addEntityRequest(
            ENDPOINT_GET_INTERMEDIARY_PRODUCTS,
            data,
            true,
            {
                path: {
                    id: id
                }
            }
        )
    }

    /** 
     * Edit the configuration of a product in an intermediary.
     * 
     * @param {string} id intermediary identifier
     * @param {int} productId product identifier
     * @param {array} data 
     * 
     * @returns {Entity} 
     */
    this.editIntermediaryProduct = (id, productId, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_GET_INTERMEDIARY_PRODUCT,
            id,
            data,
            true,
            {
                path: {
                    id: id,
                    product_id: productId
                }
            }
        )
    }

    /**
     * Deletes a product from an intermediary.
     * @param {*} id intermediary identifier
     * @param {*} productId product identifier
     * @returns 
     */
    this.deleteIntermediaryProduct = (id, productId) => {
        return this.volcanoClient.deleteResourceRequest(
            ENDPOINT_GET_INTERMEDIARY_PRODUCT,
            id,
            true,
            {
                path: {
                    product_id: productId,
                }
            }
        )
    }

    this.exportIntermediaries = (fields, params, progressHandler) => {
        return this.volcanoClient.makeCollectionExportRequest(
            ENDPOINT_GET_INTERMEDIARIES,
            params,
            null,
            fields,
            'intermediaries',
            progressHandler
        )
    }

    /**
     * Returns the collection of entity action logs for this intermediary
     * 
     * @param {string|null} intermediaryId 
     * @returns {Collection} 
     */
    this.getEntityActionLogs = (intermediaryId, params) => {

        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ENTITY_ACTION_LOGS,
            params,
            {
                parameters: {
                    id: intermediaryId
                }
            }
        )
    }
}