export default HubspotClient

const ENDPOINT_GET_POSTS = 'hubspot/posts'
const ENDPOINT_GET_DEALS = 'hubspot/deals'

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function HubspotClient(volcanoClient) {
    // This is under admin namespace.
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of posts that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getPosts = (params) => {
        const request = this.volcanoClient.prepareRequest(
            ENDPOINT_GET_POSTS,
            {
                method: 'GET',
                isAdmin: false,
                query: params,
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => res.data)
    }

    /**
     * Returns the collection of posts that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getDeals = (params) => {
        const request = this.volcanoClient.prepareRequest(
            ENDPOINT_GET_DEALS,
            {
                method: 'GET',
                isAdmin: false,
                query: params,
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => res.data)
    }
}
