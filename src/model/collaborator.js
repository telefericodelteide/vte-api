import Entity from './entity/entity'

const ENDPOINT_GET_COLLABORATORS = 'colaborators'
const ENDPOINT_GET_COLLABORATOR = 'colaborators/:id'

export default CollaboratorClient

/**
 * Container which has the collaborator methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CollaboratorClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of collaborators that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getCollaborators = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_COLLABORATORS, params, { isAdmin: false })
    }

    /**
     * Returns the collaborator that matches the id passed.
     * 
     * @param {string} collaboratorId 
     * @returns {Entity}
     */
    this.getCollaborator = (collaboratorId) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_COLLABORATOR, collaboratorId, false)
    }
}