export default PermissionClient;

const PERMISSIONS_GET_PERMISSIONS = 'auth/permissions'

/**
 * Uses to access Volcano API permissions endpoints.
 * 
 * @constructor PermissionClient
 * @param {VolcanoClient} volcanoClient 
 */
function PermissionClient(volcanoClient) {
    this.volcanoClient = volcanoClient;

    this.getPermissions = (params) => {
        params = {
            ...params,
            limit: 10000
          } 
        return this.volcanoClient.makeCollectionRequest(PERMISSIONS_GET_PERMISSIONS, params);
    }
}