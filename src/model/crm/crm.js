import SalesmanClient from "./salesman"

export default CrmClient

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CrmClient(volcanoClient) {

    this.salesman = new SalesmanClient(volcanoClient)
}
