import isEmpty from 'lodash/isEmpty'
import constants from '../../constants'

const ENDPOINT_GET_SALESMEN = 'crm/salesmen'
const ENDPOINT_GET_SALESMAN = ENDPOINT_GET_SALESMEN + '/:id'

export default SalesmanClient

/**
 * Container which has the salesmen methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SalesmanClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of salesmen that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getSalesmen = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_SALESMEN, params)
    }

    /**
     * Returns the salesman that matches the id passed.
     * 
     * @param {string} salesmanId 
     * @returns {Entity}
     */
    this.getSalesman = (salesmanId) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_SALESMAN, salesmanId)
    }

    /**
     * Creates a new salesman.
     * 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.createSalesman = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_SALESMEN,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            },
            null,
            'salesmen'
        )
    }

    /**
     * Updates the salesman that matches the id passed.
     * 
     * @param {string} salesmanId 
     * @param {Object} data 
     * @returns {Entity}
     */
    this.editSalesman = (salesmanId, data) => {
        if (isEmpty(salesmanId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_GET_SALESMAN,
            salesmanId,
            constants.HTTP_PATCH,
            data,
            {
                path: {
                    id: salesmanId,
                },
                isAdmin: true,
            }
        )
    }
}