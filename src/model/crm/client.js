// This file contains the client model for the CRM module
const ENDPOINT_GET_BILLING_CLIENT = 'crm/clients'

export default BillingClient

/**
 * Container which has the salesmen methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function BillingClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Returns the collection of salesmen that matches the query parameters passed.
     * 
     * @param {Object} params query params for the related API endpoint
     * @returns {Collection} 
     */
    this.getSalesmen = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_BILLING_CLIENT, params, {})
    }

}
