import VolcanoApi from "../vte";
import constants from '../constants'
import Entity from './entity/entity';

export default OrderClient

const ENDPOINT_GET_ORDERS = 'orders'
const ENDPOINT_GET_ORDER = ENDPOINT_GET_ORDERS + '/:id'
const ENDPOINT_ORDER_CHANGE_CUSTOMER = ENDPOINT_GET_ORDER + '/customers/:customer_id'
const ENDPOINT_ORDER_CHANGE_INTERMEDIARY = ENDPOINT_GET_ORDER + '/collaborator-change'

export {
    ENDPOINT_ORDER_CHANGE_CUSTOMER as ORDER_CHANGE_CUSTOMER
}
/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function OrderClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;


    /**
     * Let to change the customer in the booking record.
     *
     * @param {string | number} orderId   order ID
     * @param {string | number} customerId   customer ID
     * @param {object} data Customer Data to be updated.
     *                      The data should be in the following format
     *
     *                      {
     *                          "comments": "",
     *                          "email": "trash@volcanoteide.com",
     *                          "language_code3": "fra",
     *                          "name": "Juan",
     *                          "phone": "666666666"
     *                          "surename": "Perez"
     *                      }
     *
     *                      And it should contains at leastone of the below items.
     *
     *                      comments:           Customer Comment
     *                      email:              Customer email address
     *                      language_code3":    Customer language
     *                      name:               Customer Name
     *                      phone:              Customer phone number
     *                      surname:            Customer surname
     *
     * @returns {Entity}
     */
    this.changeOrderCustomer = (orderId, customerId, data) => {
        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_ORDER_CHANGE_CUSTOMER,
            orderId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: orderId,
                    customer_id: customerId,
                },
                isAdmin: true,
            },
            null)
    }

    /**
     * Updates the intermediary of the order.
     * 
     * @param {number} orderId order id
     * @param {object} data update data
     * @returns 
     */
    this.setIntermediary = (orderId, data) => {
        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_ORDER_CHANGE_INTERMEDIARY,
            orderId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: orderId,
                },
                isAdmin: true,
            },
            null)
    }
}

