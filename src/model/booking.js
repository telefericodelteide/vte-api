import constants from '../constants'
import Entity from './entity/entity'
import { get, isEmpty, set } from 'lodash';
import { format } from 'date-fns-tz'

/** Endpoints */

// Bookings
const ENDPOINT_GET_BOOKINGS = 'bookings'
const ENDPOINT_GET_PRODUCTS_FOR_CHANGE = ENDPOINT_GET_BOOKINGS + '/available-products-change'

// Booking
const ENDPOINT_GET_BOOKING = ENDPOINT_GET_BOOKINGS + '/:id'
const BOOKING_GET_RELATED_BOOKINGS = ENDPOINT_GET_BOOKING + '/bookings'
const BOOKING_GET_HISTORY = ENDPOINT_GET_BOOKING + '/booking-history'
const BOOKING_ADD_COMMENT = ENDPOINT_GET_BOOKING + '/add-comment'
const BOOKING_SET_VOUCHER = ENDPOINT_GET_BOOKING + '/set-voucher'
const BOOKING_SET_NOTES = ENDPOINT_GET_BOOKING + '/set-notes'
const BOOKING_SET_GIFT = ENDPOINT_GET_BOOKING + '/set-gift'
const BOOKING_ASSIGN_PAX_MANAGER_COLLABORATOR = ENDPOINT_GET_BOOKING + '/assign-pax-manager-collaborator'
const BOOKING_CHANGE_BOOKING_DATE = ENDPOINT_GET_BOOKING + '/change-booking-date'
const BOOKING_CHANGE_PRODUCT_REQUEST = ENDPOINT_GET_BOOKING + '/product-change-request'
const BOOKING_CHANGE_RATES_REQUEST = ENDPOINT_GET_BOOKING + '/rates-change-request'
const BOOKING_CHANGE_PARTICIPANTS = ENDPOINT_GET_BOOKING + '/participants'
const BOOKING_CHANGE_PICKUP_POINT = ENDPOINT_GET_BOOKING + '/change-pickup-point'
const BOOKING_CANCEL_REQUEST = ENDPOINT_GET_BOOKING + '/cancellation-request'
const BOOKING_SET_NO_SHOW = ENDPOINT_GET_BOOKING + '/no-show'
const BOOKING_REVERT_NO_SHOW = ENDPOINT_GET_BOOKING + '/revert-no-show'
const BOOKING_SET_VALIDATION_DATE = ENDPOINT_GET_BOOKING + '/exchange-date'
const BOOKING_SEND_EMAIL = ENDPOINT_GET_BOOKING + '/send-email'
const BOOKING_PDF = ENDPOINT_GET_BOOKING + '/pdf'
const BOOKING_QR_CODES = ENDPOINT_GET_BOOKING + '/qr-codes'
const BOOKING_GET_BOOKING_CHECK_ACTION = ENDPOINT_GET_BOOKING + '/check-action/:action'
const BOOKING_CHANGE_CONFIRM = ENDPOINT_GET_BOOKING + '/confirm'
const BOOKING_CONFIRM_PAYMENT = ENDPOINT_GET_BOOKING + '/confirm-payment'
const BOOKING_LOCK = ENDPOINT_GET_BOOKING + '/lock'
const BOOKING_UNLOCK = ENDPOINT_GET_BOOKING + '/unlock'
const BOOKING_UNLOCK_ALL = ENDPOINT_GET_BOOKINGS + '/unlock-all'
const BOOKING_CHANGE_MANAGED_STATE = ENDPOINT_GET_BOOKING + '/managed'

// Booking validations
const ENDPOINT_ADD_VALIDATION_BY_LOCATOR = ENDPOINT_GET_BOOKINGS + '/validations'
const ENDPOINT_BOOKING_GET_VALIDATIONS = ENDPOINT_GET_BOOKING + '/validations'
const ENDPOINT_BOOKING_GET_VALIDATION = ENDPOINT_BOOKING_GET_VALIDATIONS + '/:validation_id'

// Payment transaction
const BOOKING_GET_TRANSACTION_BOOKINGS = 'payment-transactions/:id/client-result'

// Booking constants
const BOOKING_DATE_NULL = "1980-01-01 00:00:00"

export default BookingClient

/**
 * Container which has the booking methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function BookingClient(volcanoClient) {
    // This is under admin namespace.
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Return a collection with all Bookings with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getBookings = (params) => {
        this.extendEntity(this)

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_BOOKINGS,
            params
        )
    }

    /**
     * Return a collection with all Bookings with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getRelatedBookings = (bookingId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            BOOKING_GET_RELATED_BOOKINGS,
            params, {
            isAdmin: true,
            parameters: {
                id: bookingId,
            },
        })
    }


    this.exportBookings = (fields, params, progressHandler) => {
        return this.volcanoClient.makeCollectionExportRequest(
            ENDPOINT_GET_BOOKINGS,
            params,
            null,
            fields,
            'bookings',
            progressHandler
        )
    }

    /**
     * Return a booking from transaction
     *
     * @param transactionId
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getBookingsFromTransaction = (transactionId) => {
        const request = this.volcanoClient.prepareRequest(
            BOOKING_GET_TRANSACTION_BOOKINGS,
            {
                method: 'GET',
                isAdmin: true,
                parameters: {
                    id: transactionId,
                },
                query: {
                    sid: this.volcanoClient.cart.getSessionId(),
                },
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res.bookings
        })
    }

    /**
     * Return an Entity containing a booking which match by Id `bookingId`.
     *
     * @param {BigInteger} [bookingId]  booking ID.
     *
     * @returns {Entity}
     */
    this.getBooking = (bookingId, params) => {
        this.extendEntity()
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_BOOKING,
            bookingId,
            true,
            params
        )
    }

    /**
     * Return a booking from transaction
     *
     * @param bookingId
     * @param action
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.checkBookingAction = (bookingId, action) => {
        if (!(bookingId && action)) {
            return null
        }

        const request = this.volcanoClient.prepareRequest(
            BOOKING_GET_BOOKING_CHECK_ACTION,
            {
                method: 'GET',
                isAdmin: true,
                parameters: {
                    id: bookingId,
                    action: action,
                }
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return { allowed: res.allowed }
        })
    }

    /**
     * Return a history list from booking
     *
     * @param {number} bookingId  booking ID.
     * @param {object} params Query parameters
     * 
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getBookingHistory = (bookingId, params) => {
        params = params || {}

        return this.volcanoClient.makeCollectionRequest(
            BOOKING_GET_HISTORY,
            params,
            {
                isAdmin: true,
                parameters: {
                    id: bookingId
                }
            }
        )
    }

    /**
     * Return a the validations of a booking
     *
     * @param {number} bookingId booking ID.
     * @param {object} params Query parameters
     * 
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getValidations = (bookingId, params) => {
        params = params || {}

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_BOOKING_GET_VALIDATIONS,
            params,
            {
                isAdmin: true,
                parameters: {
                    id: bookingId
                }
            }
        )
    }

    /**
     * Delete a validation or all validations from a booking. 
     * 
     * @param {number} bookingId 
     * @param {number|null} validationId 
     * 
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.deleteValidation = (bookingId, validationId) => {
        return this.volcanoClient.makeResourceRequest(
            validationId ? ENDPOINT_BOOKING_GET_VALIDATION : ENDPOINT_BOOKING_GET_VALIDATIONS,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: bookingId,
                    validation_id: validationId,
                },
                isAdmin: true
            }
        )
    }

    /**
     * Requests a validation for a booking or booking ticket by locator 
     * @param {*} params validation request data
     *  {
     *     locator: string,                 // booking or ticket locator
     *     date: string,                    // validation date
     *     access_control_point_id: string, // access control point id
     *     activity_id: string,             // activity id
     *     coordinates: {                   // coordinates of the validation
     *        lat: float,
     *        lon: float,
     *        status: string                // status of the coordinates (ignored, available, unavailable, denied)
     *     }
     *  }
     * 
     * @returns 
     */
    this.addValidationFromLocator = (params) => {
        params = params || {}

        return this.volcanoClient.makeEntityRequest(
            ENDPOINT_ADD_VALIDATION_BY_LOCATOR,
            null,
            constants.HTTP_POST,
            params,
            {
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Returns the collection of products available for change for a collection of bookings.
     * 
     * @param {number} experienceId
     * @param {array} bookings bookings data required for the endpoint
     * 
     * @returns {Collection} 
     */
    this.getProductsForChange = (experienceId, bookings) => {
        const request = this.volcanoClient.prepareRequest(
            ENDPOINT_GET_PRODUCTS_FOR_CHANGE,
            {
                method: 'POST',
                isAdmin: true,
                query: {
                    experience_id: experienceId,
                    limit: 1000
                },
                data: {
                    bookings: (bookings ?? []).map(booking => ({
                        created: get(booking, "order.created"),
                        enterprise_id: get(booking, "order.enterprise.id"),
                        site_id: get(booking, "order.site.id"),
                        collaborator_id: get(booking, "order.collaborator.id"),
                        crm_intermediary_id: get(booking, "order.collaborator.crm_intermediary_id"),
                        product_id: get(booking, "product.id"),
                        booking_date: get(booking, "booking_date"),
                        billing_type: get(booking, "order.billing_type"),
                        currency_code: get(booking, "order.currency"),
                        booking_rates: booking.product_rates.map(bookingRate => ({
                            customer_type_ids: bookingRate.customer_type_ids,
                            qty: bookingRate.qty,
                            total_amount: bookingRate.total_amount
                        }))
                    }))
                }
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res.products
        })
    }

    /**
     * Add comment to booking
     *
     * @param {string | number} bookingId Booking ID
     * @param {string} comment Comment text
     *
     * @returns {Entity}
     */
    this.addComment = (bookingId, comment) => {
        return this.volcanoClient.makeEntityRequest(
            BOOKING_ADD_COMMENT,
            bookingId,
            constants.HTTP_POST,
            {
                comment: comment
            },
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Set voucher for a given booking
     *
     * @param {string | number} bookingId Booking ID
     * @param {string} voucher voucher id
     *
     * @returns {Entity}
     */
    this.setVoucher = (bookingId, voucher) => {
        return this.volcanoClient.makeEntityRequest(
            BOOKING_SET_VOUCHER,
            bookingId,
            constants.HTTP_POST,
            {
                voucher_id: voucher
            },
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Set note for a given booking
     *
     * @param {string | number} bookingId Booking ID
     * @param {string} notes Note
     *
     * @returns {Entity}
     */
    this.setNotes = (bookingId, notes) => {
        return this.volcanoClient.makeEntityRequest(
            BOOKING_SET_NOTES,
            bookingId,
            constants.HTTP_POST,
            {
                notes: notes
            },
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Set gift info for a given booking
     *
     * @param {string | number} bookingId Booking ID
     * @param {string} gift Gift info
     *
     * @returns {Entity}
     */
    this.setGift = (bookingId, gift) => {
        return this.volcanoClient.makeEntityRequest(
            BOOKING_SET_GIFT,
            bookingId,
            constants.HTTP_POST,
            { gift },
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Change the date of a booking given.
     *
     * @param { number | string } bookingId  id of the booking.
     * @param {date} date  The data containing the new date to be set for that booking.
     *                      {
     *                        "reservation_date" : "2020-10-11 14:00:00"
     *                      }
     *
     * @returns {Entity}
     */
    this.changeBookingDate = (bookingId, date) => {
        const bookingDate = date ? format(date, 'yyyy-MM-dd HH:mm:ss', { timeZone: 'Atlantic/Canary' }) : BOOKING_DATE_NULL

        const dateJsonObject = {
            booking_date: bookingDate,
        }

        const result = this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_BOOKING_DATE,
            bookingId,
            constants.HTTP_POST,
            dateJsonObject,
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )

        return result
    }

    /**
     * Delete booking date from a given booking.
     *
     * @param {string | number} bookingId Booking ID
     *
     * @returns {Entity}
     */
    this.deleteBookingDate = (bookingId) => {
        return this.changeBookingDate(bookingId)
    }

    /**
     * Do a product change and return an Entity with changes.
     *
     * @param { number | string } bookingId
     * @param { object } data This object must be in the following form:
     *                        {
     *                          "product_id": Product Id to be changed,
     *                          "booking_date": New booking date (optional)
     *                        }
     * @return { Entity }
     */
    this.changeProduct = (bookingId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_PRODUCT_REQUEST,
            bookingId,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
                path: {
                    id: bookingId,
                },
            },
            null
        )
    }

    /**
     * Do a change rate and return the Entity
     *
     * @param {number | string } bookingId
     * @param { object } data   This should have the following format.
     *                          {
     *                            "product_id": 1785,
     *                            "product_rates": [
     *                              {
     *                                "id": 2273918, (if rate exists in booking)
     *                                "rate_id": 15810, (if it's a new rate in booking)
     *                                "qty": 4,
     *                                "unit_price": 28.5, (optional)
     *                                "participants": [
     *                                  {
     *                                      "id":89880, (if participant exists in booking rate)
     *                                      "first_name":"00000000A",
     *                                      "last_name":"11111111A",
     *                                      "id_card":"00000000A"
     *                                  },
     *                                  {
     *                                      "id":89881,
     *                                      "deleted":true
     *                                  },
     *                                  ...
     *                                ]
     *                              },
     *                              ...
     *                            ]
     *                          }
     *
     * @retuen { Entity }
     */
    this.changeRates = (bookingId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_RATES_REQUEST,
            bookingId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Do a participants change and return the Entity
     *
     * @param {number | string } bookingId
     * @param { object } data   This should have the following format.
     *                          {
     *                            "participants": [
     *                              {
     *                                  "id":89880,
     *                                  "first_name":"00000000A",
     *                                   "last_name":"11111111A",
     *                                   "id_card":"00000000A"
     *                               },
     *                              ...
     *                            ]
     *                          }
     *
     * @retuen { Entity }
     */
    this.changeParticipants = (bookingId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_PARTICIPANTS,
            bookingId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: bookingId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Let to change the pickup in the booking record.
     *
     * @param {string | number} bookingId   booking ID
     * @param {object} data Pickup data to be updated.
     *                      The data should be in the following format
     *
     *                      {
     *                          "pickup_point_id": "eea4f932-8c3b-41a4-ba27-f89f2499ed1d",
                                "lodgin_id": "f0e22021-66fa-4f27-b06c-348a976c7c7b"
     *                      }
     *
     * @returns {Entity}
     */
    this.changePickupPoint = (bookingId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_PICKUP_POINT,
            bookingId,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Assign some booking rates pax to some collaborator.
     *
     * @param {string | number} bookingId   booking ID
     * @param {object} data Data to perform action.
     *                      The data should be in the following format
     *
     *                      {
     *                          "manager_collaborator_id": 35733,
     *                          "booking_rates": [
     *                              {
     *                                  "id": 2087453,
     *                                  "qty": 1
     *                              }
     *                          ]
     *                      }
     *
     * @returns {Entity}
     */
    this.assignPaxManagerCollaborator = (bookingId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_ASSIGN_PAX_MANAGER_COLLABORATOR,
            bookingId,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Do a booking Cancellation or make a request to cancel
     * depends on if a refund is needed.
     *
     * @param {numeric} bookingId Id of the booking which want to be cancelled.
     * @param {object}  data      Data to perform action.
     *
     * @return {Entity}
     */
    this.cancelBooking = (bookingId, data) => {
        return this.executeAction(bookingId, data, BOOKING_CANCEL_REQUEST)
    }

    /**
     * Sets booking no show.
     *
     * @param {number} bookingId id of the booking
     * @param {object}  data      data to executes the action
     *
     * @return {Entity}
     */
    this.setNoShow = (bookingId, data) => {
        return this.executeAction(bookingId, data, BOOKING_SET_NO_SHOW)
    }

    /**
     * Revert booking no show.
     *
     * @param {number} bookingId id of the booking
     *
     * @return {Entity}
     */
    this.revertNoShow = (bookingId) => {
        return this.executeAction(bookingId, null, BOOKING_REVERT_NO_SHOW)
    }

    /**
     * Sets booking validation date.
     *
     * @param {number} bookingId id of the booking
     * @param {object}  data      data to executes the action
     *
     * @return {Entity}
     */
    this.setValidationDate = (bookingId, data) => {
        return this.executeAction(bookingId, data, BOOKING_SET_VALIDATION_DATE)
    }

    /**
     * Update booking requires_confirmed to 2 (state confirmed).
     *
     * @param bookingId
     *
     * @returns {null|Object|Entity}
     */
    this.confirm = (bookingId) => {
        return this.executeAction(bookingId, null, BOOKING_CHANGE_CONFIRM)
    }

    /**
     * Confirms payment.
     *
     * @param {number} bookingId id of the booking
     *
     * @return {Entity}
     */
    this.confirmPayment = (bookingId) => {
        return this.executeAction(bookingId, null, BOOKING_CONFIRM_PAYMENT)
    }

    /**
     * 
     * @param {number} bookingId id of the booking
     * @param {object}  data      data to executes the action
     * @param {string}  endpoint  action endpoint
     * 
     * @returns {Entity}
     */
    this.executeAction = (bookingId, data, endpoint) => {
        const id = parseInt(bookingId)
        if (isNaN(bookingId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            endpoint,
            id,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Send the emails regarding with the given booking.
     *
     * @param bookingId
     * @param emails
     *
     * @returns {null|Object|Entity}
     */
    this.sendBookingEmail = (bookingId, emails) => {
        const emailAddresses = emails || {}

        const id = parseInt(bookingId)
        if (isNaN(bookingId)) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            BOOKING_SEND_EMAIL,
            id,
            constants.HTTP_POST,
            emailAddresses,
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Return an application/PDF octet
     *
     * @param bookingId
     *
     * @returns {null|Object|Entity}
     */
    this.getBookingPdf = (bookingId, sid, viewMode) => {

        const params = {
            ...(sid && { sid }),
            ...(viewMode && { view_mode: viewMode })
        };

        const id = parseInt(bookingId)
        if (isNaN(id)) {
            return null
        }

        return this.volcanoClient.makeFileRequest(BOOKING_PDF, id, true, params)
    }

    /**
     * Return the booking QR codes
     *
     * @param bookingId
     * @param sid
     *
     * @returns {null|Object|Entity}
     */
    this.getBookingQrCodes = (bookingId, sid) => {
        const id = parseInt(bookingId)
        if (isNaN(id)) {
            return null
        }

        const request = this.volcanoClient.prepareRequest(
            BOOKING_QR_CODES,
            {
                method: 'GET',
                isAdmin: false,
                parameters: {
                    id: id,
                },
                query: {
                    sid: sid,
                },
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res.qr_codes
        })
    }

    this.exportBookings = (fields, params, progressHandler) => {
        const endpoint = 'bookings'
        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            100,
            fields,
            'bookings',
            progressHandler
        )
    }

    this.extendEntity = () => {
        Entity.prototype.getTotalQuantity = function () {
            return get(this, 'product_rates').reduce(
                (acc, rate) => (acc += rate.qty),
                0
            )
        }

        Entity.prototype.getRelatedEntities = async function () {
            const promises = [
                volcanoClient.refund
                    .getRefunds({
                        booking_id: get(this, 'id'),
                        order_id: get(this, 'order.id'),
                        sort_by_created: 'desc',
                    }),
                volcanoClient.invoice
                    .getInvoices({
                        order_id: get(this, 'order.id')
                    })
            ]

            return Promise.all(promises)
                .then(([refunds, invoices]) => {
                    set(this, "refunds", refunds.getCount() > 0 ? refunds.getItems() : [])
                    set(this, "invoices", invoices.getCount() > 0 ? invoices.getItems() : [])

                    return this
                })
                .catch(function (error) {
                    set(this, "refunds", null)
                    set(this, "invoices", null)

                    return this
                })
        }

        Entity.prototype.getPendingRefund = async function () {
            let pendingRefund = null

            if (get(this, 'state') === 'refund_requested') {
                pendingRefund = isEmpty(get(this, 'refunds')) ? null : get(this, 'refunds')[0]
            }

            return pendingRefund
        }

        Entity.prototype.getAvailableActions = async function () {
            const actions = this.getActions()

            const promises = [
                this.getPendingRefund()
            ]

            return Promise.all(promises)
                .then(([pendingRefund]) => {
                    return {
                        change_date: !isEmpty(actions['booking_date_change']),
                        cancel: !isEmpty(actions['cancellation_request']),
                        cancel_refund: !isEmpty(pendingRefund) && !isEmpty(pendingRefund.getActions()['cancel_refund']),
                        request_invoice: !isEmpty(actions['create_customer_invoice']),
                    }
                })
                .catch(function (error) {
                    return {
                        change_date: !isEmpty(actions['booking_date_change']),
                        cancel: !isEmpty(actions['cancellation_request']),
                        cancel_refund: false,
                        request_invoice: !isEmpty(actions['create_customer_invoice']),
                    }
                })
        }
    }

    /**
     * Lock booking for managed in routes
     *
     * @param bookingId
     *
     * @returns {Object|Entity|null}
     */
    this.lockBooking = (bookingId) => {
        if (isNaN(bookingId)) {
            return null
        }

        const id = parseInt(bookingId)


        return this.volcanoClient.makeEntityRequest(
            BOOKING_LOCK,
            id,
            constants.HTTP_POST,
            null,
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Unlock booking for managed in routes
     *
     */
    this.unlockBooking = (bookingId) => {
        if (isNaN(bookingId)) {
            return null
        }

        const id = parseInt(bookingId)

        return this.volcanoClient.makeEntityRequest(
            BOOKING_UNLOCK,
            id,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Unlock all bookings
     *
     * @param params
     * @returns {Object|Entity|null}
     */
    this.unlockAllBookings = (params) => {
        params = params || {}

        return this.volcanoClient.makeEntityRequest(
            BOOKING_UNLOCK_ALL,
            null,
            constants.HTTP_DELETE,
            params,
            {
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Change booking managed state
     *
     * @param bookingId
     * @param data
     * @returns {Object|Entity|null}
     */
    this.changeManagedState = (bookingId, data) => {
        if (isNaN(bookingId) || !data) {
            return null
        }

        const id = parseInt(bookingId)

        return this.volcanoClient.makeEntityRequest(
            BOOKING_CHANGE_MANAGED_STATE,
            id,
            constants.HTTP_PATCH,
            data,
            {
                path: {
                    id: id,
                },
                isAdmin: true,
            },
            null
        )
    }


}

