import _isObject from 'lodash/isObject'
import constants from "../../constants";

// Endpoints
const ENDPOINT_GET_ACTIVITY_CONFIGS = 'activity-manager/activity-configs'
const ENDPOINT_GET_ACTIVITY_CONFIG = ENDPOINT_GET_ACTIVITY_CONFIGS + '/:id'
const ENDPOINT_UPDATE_ACTIVITY_CONFIG = ENDPOINT_GET_ACTIVITY_CONFIG
const ENDPOINT_UPDATE_ACTIVITY_CONFIG_PRODUCTS = ENDPOINT_UPDATE_ACTIVITY_CONFIG + '/update-products'
const ENDPOINT_UPDATE_ACTIVITY_CONFIG_CONFIGURATION = ENDPOINT_UPDATE_ACTIVITY_CONFIG + '/update-configurations'

export default ActivityConfigClient

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityConfigClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Returns a collection with activities.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activity config.
     * 
     * @returns {Collection}
     */
    this.getActivityConfigs = (params, viewMode) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ACTIVITY_CONFIGS,
            params,
            {})
    }

    /**
     * Return an Entity containing an activity which match by Id `activityId`.
     *
     * @param {string} [activityConfigId]  activity ID.
     * @param {array} [params]  Optional object which contains the parameters to build the
     *
     * @returns {Entity}
     */
    this.getActivityConfig = (activityConfigId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_ACTIVITY_CONFIG,
            activityConfigId,
            true,
            params,
            null
        )
    }

    /**
     * Add a new activity config.
     * @param data
     * @returns {Object|Entity}
     */
    this.addActivityConfig = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ACTIVITY_CONFIGS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Edit an activity config.
     * @param {string} id
     * @param {Object} data
     */
    this.editActivityConfig = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_ACTIVITY_CONFIG,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Update the configuration of an activity config.
     * @param id
     * @param data
     * @returns {Object|Entity}
     */
    this.updateConfiguration = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_ACTIVITY_CONFIG_CONFIGURATION,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Update the products of an activity config.
     * @param id
     * @param data
     * @returns {Object|Entity}
     */
    this.updateProducts = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_ACTIVITY_CONFIG_PRODUCTS,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

    /**
     * delete an activity config.
     * @param {string} id
     */
    this.deleteActivityConfig = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_ACTIVITY_CONFIG,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }
}

