import _isObject from 'lodash/isObject'
import constants from "../../constants";

const ENDPOINT_GET_ROUTES = 'activity-manager/routes'
const ENDPOINT_GET_ROUTE = ENDPOINT_GET_ROUTES + '/:id'
const ENDPOINT_UPDATE_CONFIGURATION = ENDPOINT_GET_ROUTES + '/:id/update-configuration'
const ENDPOINT_UPDATE_PRODUCTS = ENDPOINT_GET_ROUTES + '/:id/update-update-products'
const ENDPOINT_UPDATE_PICKUP_CONFIGS = ENDPOINT_GET_ROUTES + '/:id/update-pickup-configs'


export default RouteClient

function RouteClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of routes
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getRoutes = (params, viewMode  ) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ROUTES,
            params
        )
    }

    /**
     * Get a route
     *
     * @param id uuid id of the route
     * @param params
     * @returns {Entity}
     */
    this.getRoute = (id, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_ROUTE,
            id,
            true,
            params
        )
    }

    /**
     * Add a new route
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addRoute = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ROUTES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        )
    }

    /**
     * Make a request to update a route
     *
     * @param {string}  id        uuid id of the route to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editRoute = (id, data) => {

        // validate id
        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ROUTE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Update configuration of a route
     * @param {string} id
     * @param {Object} data
     *
     * @returns {Entity}
     */
    this.updateConfiguration = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_CONFIGURATION,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Update products of a route
     * @param {string} id
     * @param {Object} data
     *
     * @returns {Entity}
     */
    this.updateProducts = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_PRODUCTS,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Update pickup configs of a route
     * @param {string} id
     * @param {Object} data
     *
     * @returns {Entity}
     */
    this.updatePickupConfigs = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_PICKUP_CONFIGS,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * (Soft) delete a route
     *
     * @param {string}  id   route Id
     *
     * @return {Entity}
     */
    this.deleteRoute = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ROUTE,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

}
