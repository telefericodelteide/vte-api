import _isObject from 'lodash/isObject'
import constants from "../../constants";

const ENDPOINT_GET_AREAS = 'activity-manager/areas'

export default AreaClient

function AreaClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of zones
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getAreas = (params, viewMode) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_AREAS,
            params
        )
    }


}
