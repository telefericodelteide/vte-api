import _isObject from 'lodash/isObject'
import constants from "../../constants";

const ENDPOINT_GET_PICKUP_POINTS = 'activity-manager/pickup-points'
const ENDPOINT_GET_PICKUP_POINT = ENDPOINT_GET_PICKUP_POINTS + '/:id'
const ENDPOINT_GET_PICKUP_POINT_LODGINGS = ENDPOINT_GET_PICKUP_POINT + '/lodgings'

export default PickupPointClient

function PickupPointClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of pickup points
     *
     * @param {object} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getPickupPoints = (params, viewMode) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_PICKUP_POINTS,
            params
        )
    }

    /**
     * Get a pickup point
     *
     * @param pickupPointId
     * @param params
     * @returns {Entity}
     */
    this.getPickupPoint = (pickupPointId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_PICKUP_POINT,
            pickupPointId,
            true,
            params
        )

    }

    /**
     * Add a new pickup point
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addPickupPoint = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_POINTS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        )
    }

    /**
     * Make a request to update a pickup point
     *
     * @param {string}  id        uuid id of the pickup point to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editPickupPoint = (id, data) => {

        // validate id
        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_POINT,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Get a collection of lodgings for a pickup point
     *
     * @param id
     * @param params
     * @returns {Collection}
     */
    this.getLodgings = (id, params) => {
        const endpoint = ENDPOINT_GET_PICKUP_POINT_LODGINGS.replace(':id', id)
        return this.volcanoClient.makeCollectionRequest(
            endpoint,
            params,
            {}
        )
    }

    /**
     * Delete a pickup point
     *
     * @param {string}  id   pickup point Id
     * @return {Entity}
     */
    this.deletePickupPoint = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_POINT,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

}
