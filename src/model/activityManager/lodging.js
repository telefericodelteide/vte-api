import _isObject from 'lodash/isObject'
import constants from "../../constants";

const ENDPOINT_GET_LODGINGS = 'activity-manager/lodgings'
const ENDPOINT_GET_LODGING = ENDPOINT_GET_LODGINGS + '/:id'
const ENDPOINT_UPDATE_PICKUP_POINTS = ENDPOINT_GET_LODGINGS + '/:id/pickup-points-update'
const ENDPOINT_GET_PICKUP_POINTS = ENDPOINT_GET_LODGINGS + '/:id/pickup-points'

export default LodgingClient

function LodgingClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of lodgings
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getLodgings = (params, viewMode  ) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_LODGINGS,
            params
        )
    }

    /**
     * Get a lodging
     *
     * @param id uuid id of the lodging
     * @param params
     * @returns {Entity}
     */
    this.getLodging = (id, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_LODGING,
            id,
            true,
            params
        )
    }

    /**
     * Add a new lodging
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addLodging = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_LODGINGS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        )
    }

    /**
     * Make a request to update a lodging
     *
     * @param {string}  id        uuid id of the lodging to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editLodging = (id, data) => {

        // validate id
        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_LODGING,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Get a collection of pickup points
     *
     * @param id
     * @param params
     * @returns {any}
     */
    this.getPickupPoints = (id, params) => {
        const endpoint = ENDPOINT_GET_PICKUP_POINTS.replace(':id', id);

        return this.volcanoClient.makeCollectionRequest(
            endpoint,
            params,
            {}
        )
    }

    /**
     * Make a request to update a pickup points in lodging
     *
     * @param {string}  id        uuid id of the lodging to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editPickupPoints = (id, data) => {

        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_PICKUP_POINTS,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * (Soft) delete a lodging
     *
     * @param {string}  id   lodging Id
     *
     * @return {Entity}
     */
    this.deleteLodging = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_LODGING,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

}
