import VolcanoApi from "../../vte"
import Entity from '../entity/entity'
import _isObject from 'lodash/isObject'
import constants from "../../constants";

// Endpoints
const ENDPOINT_GET_ACTIVITIES = 'activity-manager/activities'
const ENDPOINT_GET_ACTIVITY = ENDPOINT_GET_ACTIVITIES + '/:id'
const ENDPOINT_UPDATE_ACTIVITY = ENDPOINT_GET_ACTIVITY
const ENDPOINT_GET_ACTIVITIES_STATES = ENDPOINT_GET_ACTIVITIES + '/states'

export default ActivityClient

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Returns a collection with activities.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     * 
     * @returns {Collection}
     */
    this.getActivities = (params, viewMode = 'compact') => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ACTIVITIES,
            params)
    }

    /**
     * Return an Entity containing an activity which match by Id `activityId`.
     *
     * @param {string} [activityId]  activity ID.
     *
     * @returns {Entity}
     */
    this.getActivity = (activityId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_ACTIVITY,
            activityId,
            true,
            params
        )
    }

    /**
     * Add a new activity
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addActivity = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ACTIVITIES,
            constants.HTTP_POST,
            data,
            { isAdmin: true }
        )
    }

    /**
     * Get Activity states filter options
     */
    this.getActivityStates = (isAdmin) => {
        const request = this.volcanoClient.prepareRequest(ENDPOINT_GET_ACTIVITIES_STATES, {
            method: constants.HTTP_GET,
            isAdmin: isAdmin
        })

        return this.volcanoClient.makeRequest(request)
    }

    /**
     * delete an activity
     */
    this.deleteActivity = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_ACTIVITY,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }
}
