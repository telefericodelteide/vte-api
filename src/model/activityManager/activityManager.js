import ActivityClient from "./activity"
import ActivityConfigClient from "./activityConfig"
import ZoneClient from "./zone";
import AreaClient from "./area";
import PickupPoint from "./pickupPoint";
import LodgingClient from "./lodging";
import PickupConfigClient from "./pickupConfig";
import RouteClient from "./route";

export default ActivityManagerClient

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ActivityManagerClient(volcanoClient) {

    this.activity = new ActivityClient(volcanoClient)
    this.activityConfig = new ActivityConfigClient(volcanoClient)
    this.zone = new ZoneClient(volcanoClient)
    this.area = new AreaClient(volcanoClient)
    this.pickupPoint = new PickupPoint(volcanoClient)
    this.lodging = new LodgingClient(volcanoClient)
    this.pickupConfig = new PickupConfigClient(volcanoClient)
    this.route = new RouteClient(volcanoClient)
}
