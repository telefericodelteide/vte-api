import _isObject from 'lodash/isObject'
import constants from "../../constants";


const ENDPOINT_GET_PICKUP_CONFIGS = 'activity-manager/pickup-configs'
const ENDPOINT_GET_PICKUP_CONFIG = ENDPOINT_GET_PICKUP_CONFIGS + '/:id'
const ENDPOINT_GET_PICKUP_CONFIG_PICKUP_POINTS = ENDPOINT_GET_PICKUP_CONFIG + '/pickup-points'
const ENDPOINT_UPDATE_PICKUP_POINTS = ENDPOINT_GET_PICKUP_CONFIGS + '/:id/pickup-points-update'

export default PickupConfigClient

function PickupConfigClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of pickup configs
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getPickupConfigs = (params, viewMode) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_PICKUP_CONFIGS,
            params
        )
    }

    /**
     * Get a pickup config
     *
     * @param pickupPointId
     * @param params
     * @returns {Entity}
     */
    this.getPickupConfig = (pickupPointId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_PICKUP_CONFIG,
            pickupPointId,
            true,
            params
        )

    }

    /**
     * Add a new pickup config
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addPickupConfig = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_CONFIGS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        )
    }

    /**
     * Make a request to update a pickup config
     *
     * @param {string}  id        uuid id of the pickup config to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editPickupConfig = (id, data) => {

        // validate id
        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_CONFIG,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     *
     * @param id
     * @param params
     * @returns {any}
     */
    this.getPickupConfigPickupPoints = (id, params) => {
        const endpoint = ENDPOINT_GET_PICKUP_CONFIG_PICKUP_POINTS.replace(':id', id)
        return this.volcanoClient.makeCollectionRequest(
            endpoint,
            params,
            {}
        )
    }


    /**
     * Update a pickup points for a pickup config
     */
    this.editPickupConfigPickupPoints = (id, data) => {

        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_UPDATE_PICKUP_POINTS,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Delete a pickup config
     *
     * @param {string}  id   pickup config Id
     * @return {Entity}
     */
    this.deletePickupConfig = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_PICKUP_CONFIG,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

}
