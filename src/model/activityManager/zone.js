import _isObject from 'lodash/isObject'
import constants from "../../constants";

const ENDPOINT_GET_ZONES = 'activity-manager/zones'
const ENDPOINT_GET_ZONE = ENDPOINT_GET_ZONES + '/:id'

export default ZoneClient

function ZoneClient(volcanoClient) {

    this.volcanoClient = volcanoClient

    /**
     * Get a collection of zones
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     * request, for instance, if you want to move to specific page,
     * you must add 'page: 3' to the params object.
     * @param {string} [viewMode]  Optional string with the view mode of the activities.
     *
     * @returns {Collection}
     *
     */
    this.getZones = (params, viewMode) => {
        params = _isObject(params) ? params : {}
        params.view_mode = viewMode

        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_ZONES,
            params
        )
    }

    /**
     * Get a zone
     *
     * @param zoneId
     * @param params
     * @returns {Entity}
     */
    this.getZone = (zoneId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_ZONE,
            zoneId,
            true,
            params
        )

    }

    /**
     * Add a new zone
     *
     * @param {Object}   data      data to be created
     *
     * @returns {Entity}
     */
    this.addZone = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ZONES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        )
    }

    /**
     * Make a request to update a zone
     *
     * @param {string}  id        uuid id of the zone to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editZone = (id, data) => {

        // validate id
        if (!id) {
            return Promise.reject(new Error('id is required'))
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ZONE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * (Soft) delete a zone
     *
     * @param {string}  id   zone Id
     *
     * @return {Entity}
     */
    this.deleteZone = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_ZONE,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

}
