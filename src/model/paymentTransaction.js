import VolcanoApi from "../vte";
import { InvalidSessionIdException, InvalidTransactionIdException } from "./errors";
import _get from "lodash/get";
import Entity from './entity/entity';

export default PaymentTransactionClient;

// Endpoints
const TRANSACTION_RESULT = "payment-transactions/:id/client-result";
const TRANSACTION_GET_TRANSACTIONS = 'payment-transactions'
const TRANSACTION_GET_TRANSACTION_BY_ID = TRANSACTION_GET_TRANSACTIONS + '/:id'

/**
 * Container which has the payment transactions methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function PaymentTransactionClient(volcanoClient) {

  /**
   * @var {VolcanoApi}
   */
  this.volcanoClient = volcanoClient;

  /**
   * Returns the collection of payment transactions that matches the query parameters.
   *
   * @param {Object} params query params for the related API endpoint
   * @returns {Collection}
   */
  this.getTransactions = (params) => {

    params = {
      ...params,
      isAdmin: true
    }

    return this.volcanoClient.makeCollectionRequest(TRANSACTION_GET_TRANSACTIONS, params);
  };

  /**
     * Return an Entity containing a transaction which match by Id `transactionId`.
     *
     * @param {integer} [transactionId]  transaction ID.
     *
     * @returns {Entity}
     */
  this.getTransaction = (transactionId, params) => {
    return this.volcanoClient.getEntityRequest(
      TRANSACTION_GET_TRANSACTION_BY_ID,
      transactionId,
      true,
      params
    )
  }

  this.processTransactionResult = (params, sessionId) => {
    //todo: check payment gateway

    sessionId = sessionId || this.volcanoClient.cart.getSessionId();

    if (!sessionId) {
      throw new InvalidSessionIdException();
    }

    const transactionId = _get(params, "vte_transaction_id", null);

    if (!transactionId) {
      throw new InvalidTransactionIdException();
    }

    delete params.vte_transaction_id;

    return this.getBookingsFromTransaction(transactionId, sessionId, params)
      .then(bookings => {
        this.volcanoClient.cart.resetLocalCart();
        return bookings;
      });
  }

  this.getBookingsFromTransaction = (transactionId, sessionId, params) => {
    const request = this.volcanoClient.prepareRequest(TRANSACTION_RESULT, {
      method: "GET",
      isAdmin: false,
      parameters: {
        id: transactionId
      },
      query: {
        sid: sessionId || this.volcanoClient.cart.getSessionId(),
        ...params
      }
    });

    return this.volcanoClient.makeRequest(request)
      .then(res => {
        return res.bookings;
      });
  }
}
