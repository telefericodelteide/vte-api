import constants from "../constants"

// Endpoints
const ENDPOINT_GET_CONFIG = 'vbms/config'
const ENDPOINT_GET_USER_SETTINGS = 'vbms/user-settings/:id'

export default VbmsClient

/**
 * Uses to access Volcano API VBMS endpoints.
 * 
 * @constructor VbmsClient
 * @param {VolcanoClient} volcanoClient 
 */
function VbmsClient(volcanoClient) {
  this.volcanoClient = volcanoClient

  this.getConfig = (params) => {
    const request = this.volcanoClient.prepareRequest(
      ENDPOINT_GET_CONFIG,
      {
        method: constants.HTTP_GET,
        isAdmin: false
      }
    )

    return this.volcanoClient.makeRequest(request)
  }

  this.getUserSettings = (id) => {
    return this.volcanoClient.getEntityRequest(ENDPOINT_GET_USER_SETTINGS, id, false)
  }
}