import VolcanoApi from "../vte";

export default CurrencyClient;

// Endpoints
const ENDPOINT_GET_CURRENCIES = "currencies"

export {
    ENDPOINT_GET_CURRENCIES,
};

/**
 * Container which has the currencies methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CurrencyClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
     * Return a collection with all Currencies with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getCurrencies = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_CURRENCIES, params);
    };
}