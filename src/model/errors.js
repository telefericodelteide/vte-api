
const AuthenticationException = () => {
  return new Error("User could not be authenticated");
}

const InvalidTokenException = () => {
  return new Error("Invalid token");
}

const InvalidSessionIdException = () => {
  return new Error("Invalid session identifier")
}

const InvalidTransactionIdException = () => {
  return new Error("Invalid payment transaction identifier");
}

const ObjectIsEmpty = (objectName) => {
  return new Error("The object " + objectName + " is empty");
}

const ParameterMissing = (parameter) => {
  return new Error('Missing parameter ' + parameter);
}

const MissingProperty = (property) => {
  return new Error("The " + property + " is missing into the object");
}

AuthenticationException.prototype = Object.create(Error.prototype);
InvalidTokenException.prototype = Object.create(Error.prototype);
InvalidSessionIdException.prototype = Object.create(Error.prototype);
InvalidTransactionIdException.prototype = Object.create(Error.prototype);

module.exports = {
  AuthenticationException,
  InvalidTokenException,
  InvalidSessionIdException,
  InvalidTransactionIdException,
  ObjectIsEmpty,
  MissingProperty,
  ParameterMissing
}

