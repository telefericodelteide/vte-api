import { format } from "date-fns";

/**
 * Volcano API route client
 *
 * The RouteClient class has all the required functionalities to query and manage routes with the Volcano REST API.
 *
 * @file   This files defines the RouteClient class.
 * @author Roberto Muñoz (rmunglez@gmail.com)
 * @since  0.1
 */
export default RouteClient;

/**
 * Uses to access Volcano API Route endpoints.
 * 
 * @constructor RouteClient
 * @param {VolcanoClient} volcanoClient 
 */
function RouteClient(volcanoClient) {
  this.volcanoClient = volcanoClient;
  this.baseEndpoint = 'routes'
  
  this.getPickupData = (productId, date) => {
    const endpoint = 'routes/product/:product_id/:date';

    const request = this.volcanoClient.prepareRequest(
      endpoint,
      {
        method: "GET",
        isAdmin: false,
        parameters: {
          product_id: productId,
          date: format(date, "yyyy-MM-dd"),
        },
      }
    );

    return this.volcanoClient.makeRequest(request)
      .then((res) => {
        return res.route;
      })
      .catch(err => {
        if (err instanceof Error && err.type == "NOT FOUND") {
          return null;
        }

        throw err;
      });
  }
}