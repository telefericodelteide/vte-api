import { v4 as uuidv4 } from 'uuid'
import { setCookie, getCookie, deleteCookie } from "../functions"
import Entity from './entity/entity'

/**
 * Volcano API cart client
 *
 * The CartClient class has all the required functionalities to manage a shopping cart with the Volcano REST API.
 *
 * @file   This files defines the CartClient class.
 * @author Roberto Muñoz (rmunglez@gmail.com)
 * @since  0.1
 */
export default CartClient

/**
 * Uses to access Volcano API Cart endpoints.
 * 
 * @constructor CartClient
 * @param {VolcanoClient} volcanoClient 
 */
function CartClient(volcanoClient) {
  this.volcanoClient = volcanoClient
  this.baseEndpoint = 'carts'

  this.makeParams = (extra) => {
    extra = extra || {}

    const cartId = this.getCartId()

    const params = {
      path: {
        ...extra
      },
      query: {
        sid: this.getSessionId()
      }
    }

    if (cartId) {
      params.path.id = cartId
    }

    return params
  }

  /**
   * Sets the cart uuid in local storage
   * @param {*} cartId uuid of the cart
   */
  this.setCartId = (cartId) => {
    setCookie("vle_cart_id", cartId, 30)

  }

  /**
   * Returns the cart uuid from local storage
   * @returns {*} cart identifier
   */
  this.getCartId = () => {
    return getCookie("vle_cart_id")
  }

  /**
   * Sets the client session uuid in local storate.
   * @param {*} sessionId  uuid of the local session
   */
  this.setSessionId = (sessionId) => {
    setCookie("vle_cart_session_id", sessionId, 30)
  }

  /**
   * Returns the local session uuid from local storage.
   * @returns {*} session identifier 
   */
  this.getSessionId = () => {
    return getCookie("vle_cart_session_id")
  }

  this.storeLocalCart = (config) => {
    this.setCartId(config.cart_id)
    this.setSessionId(config.session_id)
  }

  this.removeLocalCart = () => {
    deleteCookie("vle_cart_id")
    deleteCookie("vle_cart_session_id")
  }

  this.resetLocalCart = () => {
    this.removeLocalCart()

    // create a local session identifier
    const sessionId = uuidv4()
    this.setSessionId(sessionId)
  }

  /**
   * Creates a shopping cart with the information of a booking.
   * @param {*} data 
   */
  this.createCart = (data) => {
    const endpoint = this.baseEndpoint

    this.resetLocalCart()
    return this.makeCartRequest(endpoint, "POST", this.makeParams(), data).then(cart => {
      this.setCartId(cart.id)
      return cart
    })
  }

  this.getCart = () => {
    const endpoint = this.baseEndpoint + '/:id'
    return this.makeCartRequest(endpoint, "GET", this.makeParams())
      .catch(err => {
        if (err instanceof Error && err.type == "NOT_FOUND") {
          this.removeLocalCart()
        }

        throw err
      })
  }

  this.getSummary = () => {
    const endpoint = this.baseEndpoint + '/:id/summary'
    return this.makeCartRequest(endpoint, "GET", this.makeParams())
      .catch(err => {
        if (err instanceof Error && err.type == "NOT_FOUND") {
          this.removeLocalCart()
        }

        throw err
      })
  }

  this.addBooking = (data) => {
    if (this.getCartId()) {
      // if cart exists then add the booking to the current cart
      return this.addLineItem(data)
    } else {
      // if the cart does not exist create a new one
      return this.createCart(data)
    }
  }

  this.addDiscount = (discountId) => {
    return this.addLineItem({
      type: "discount",
      discount_id: discountId
    })
  }

  this.addLineItem = (data) => {
    const endpoint = this.baseEndpoint + '/:id/line-items'
    return this.makeCartRequest(endpoint, "POST", this.makeParams(), data)
  }

  this.removeLineItem = (lineItemId) => {
    const endpoint = this.baseEndpoint + '/:id/line-items/:line_item_id'
    return this.makeCartRequest(endpoint, "DELETE", this.makeParams({ line_item_id: lineItemId }))
  }

  this.confirmCart = (data) => {
    const endpoint = this.baseEndpoint + '/:id/confirm'
    return this.makeCartRequest(endpoint, "POST", this.makeParams(), data)
  }

  this.getPaymentGateways = () => {
    const endpoint = this.baseEndpoint + '/:id/payment-gateways'
    return this.makeCartRequest(endpoint, "GET", this.makeParams(), null, function (res) {
      return res.payment_gateways
    })
  }

  this.makeCartRequest = (endpoint, method, params, data, callback) => {
    const request = this.volcanoClient.prepareRequest(endpoint, {
      method: method,
      isAdmin: false,
      parameters: params.path,
      query: params.query
    })

    if (data) {
      request.data = data
    }

    return this.volcanoClient.makeRequest(request)
      .then(res => {
        if (callback) {
          return callback(res)
        } else {
          return new Entity(res[Object.keys(res)[0]])
        }
      })
  }
}