import VolcanoApi from '../../vte';
import Collection from '../entity/collection';
import constants from '../../constants';

// Endpoints
const GET_PRODUCTS_ROOT = "content/products"
const GET_PRODUCT = GET_PRODUCTS_ROOT + "/:id"

export default ProductClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ProductClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
    * Retrieve a collection of Products
    *
    * @param params
    *
    * @returns {Collection}
    */
    this.getProducts = (params) => {
        return this.volcanoClient.makeCollectionRequest(GET_PRODUCTS_ROOT, params, {
            isAdmin: false
        })
    }

    /**
        * Return the product for ID given.
        *
        * @param productId
        * @param params
        *
        * @returns {Entity}
        */
    this.getProduct = (productId, params) => {
        return this.volcanoClient.makeEntityRequest(
            GET_PRODUCT,
            productId,
            constants.HTTP_GET,
            null,
            {
                path: {
                    id: productId,
                },
                isAdmin: false,
                query: params
            },
            null
        )
    }
}