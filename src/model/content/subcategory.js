import VolcanoApi from '../../vte';
import Collection from '../entity/collection';

// Endpoints
const GET_SUBCATEGORIES = "content/sites/:id/subcategories"
const GET_SUBCATEGORIES_ROOT = "content/subcategories"
const ENDPOINT_GET_SUBCATEGORY = GET_SUBCATEGORIES + "/:id"

export default SubcategoryClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SubcategoryClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
     * Return the subcategories for site ID given.
     * @param {number} siteId
     * @param {string} viewMode
     * @param params
     * @returns {Collection}
     */
    this.getSubcategories = (siteId, viewMode, params) => {
        if (!params) {
            params = {}
        }

        const query = {
            ...params,
            view_mode: viewMode || "summary",
        }

        if (!siteId) {
            return this.volcanoClient.makeCollectionRequest(GET_SUBCATEGORIES_ROOT, query, {
                isAdmin: false,
            })
        }

        return this.volcanoClient.makeCollectionRequest(GET_SUBCATEGORIES, query, {
            isAdmin: false,
            parameters: {
                id: siteId,
            },
        })
    }

    /**
     * Return an Entity containing a subcategory which match by `subcategoryId`.
     *
     * @param {BigInteger} [subcategoryId] subcategory ID.
     *
     * @returns {Entity}
     */
    this.getSubcategory = (subcategoryId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_SUBCATEGORY,
            subcategoryId,
            true,
            params
        )
    }
}
