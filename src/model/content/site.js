import constants from "../../constants";
import VolcanoApi from "../../vte";
import Collection from '../entity/collection';

// Endpoints
const GET_SITES_ROOT = "content/sites"
const GET_SITE = GET_SITES_ROOT + "/:id"
const SITE_CACHE_INVALIDATIONS = "content/sites/:id/cache-invalidations"
const SITE_PUBLISH = "content/sites/:id/publications"
const GET_FEATURED_PRODUCTS = "content/sites/:id/featured-products"
const SET_FEATURED_PRODUCTS = "content/sites/:id/config/featured-products"
const SET_FEATURED_EXPERIENCES = "content/sites/:id/config/featured-experiences"
const GET_MENU = "content/sites/:id/menus/:menu_id"
const ENDPOINT_GET_ENTITY_ACTION_LOGS = 'entity-logs/entity-action-logs'

export default SiteClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SiteClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Site methods */

    /**
    * Return a collection with all sites with pagination.
    *
    * @param {array} [params]  Optional object which contains the parameters to build the
    *                          request, for instance, if you want to move to specific page,
    *                          you must add 'page: 3' to the params object.
    *
    * @returns {Collection}
    */
    this.getSites = (params) => {
        return this.volcanoClient.makeCollectionRequest(GET_SITES_ROOT, params, {
            isAdmin: false,
        })
    };

    /**
    * Return an Entity containing a site which match by Id
    *
    * @param {string} [id]  site ID.
    *
    * @returns {Entity}
    */
    this.getSite = (id) => {
        return this.volcanoClient.getEntityRequest(GET_SITE, id, false, {
            parameters: { id: id },
        });
    };

    /**
    * Add a new site
    *
    * @param {Object}   data      data to be updated
    *
    * @returns {Entity}
    */
    this.addSite = (data) => {
        return this.volcanoClient.makeResourceRequest(
            GET_SITE,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        );
    };

    /**
    * Make a request to update a site
    *
    * @param {numeric}  id        id of the site to be updated
    * @param {Object}   data      data to be updated
    *
    * @return {Entity}
    */
    this.editSite = (id, data) => {
        if (isNaN(parseInt(id))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            GET_SITE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
    * (Soft) delete a site
    *
    * @param {string}  id   site Id
    *
    * @return {Entity}
    */
    this.deleteSite = (id) => {
        return this.volcanoClient.makeResourceRequest(
            GET_SITE,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

    /**
    * Clean site cache
    *
    * @param {string}  id   site Id
    *
    * @return {Entity}
    */
    this.invalidateCache = (id) => {
        return this.volcanoClient.makeResourceRequest(
            SITE_CACHE_INVALIDATIONS,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

    /**
    * Publish site
    *
    * @param {string}  id   site Id
    *
    * @return {Entity}
    */
    this.publish = (id) => {
        return this.volcanoClient.makeResourceRequest(
            SITE_PUBLISH,
            constants.HTTP_POST,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }

    /**
    * Return the featured products for site ID given.
    * 
    * @param {number} siteId 
    * @param {string} viewMode 
    * @returns {Collection}
    */
    this.getFeaturedProducts = (siteId, viewMode = 'compact') => {
        return this.volcanoClient.makeCollectionRequest(
            GET_FEATURED_PRODUCTS,
            { view_mode: viewMode },
            {
                isAdmin: false,
                parameters: {
                    id: siteId
                },
            }
        )
    }

    /**
    * Set featured products from site
    *
    * @param {string} id site Id
    * @param {array} products
    *
    * @return {Entity}
    */
    this.setFeaturedProducts = (id, products) => {
        return this.volcanoClient.makeEntityRequest(
            SET_FEATURED_PRODUCTS,
            id,
            constants.HTTP_POST,
            {
                featured_products: products
            },
            {
                isAdmin: true
            },
            null
        )
    }

    /**
    * Set featured experiences from site
    *
    * @param {string} id site Id
    * @param {array} experiences
    *
    * @return {Entity}
    */
    this.setFeaturedExperiences = (id, experiences) => {
        return this.volcanoClient.makeEntityRequest(
            SET_FEATURED_EXPERIENCES,
            id,
            constants.HTTP_POST,
            {
                featured_experiences: experiences
            },
            {
                isAdmin: true
            },
            null
        )
    }

    /**
     * Returns the collection of entity action logs for this site
     * 
     * @param {string|null} intermediaryId 
     * @returns {Collection} 
     */
    this.getEntityActionLogs = (siteId, params) => {

        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_ENTITY_ACTION_LOGS,
            {
                ...params,
                entity_id: siteId,
                entity_model: 'Sites'

            },
            {
                isAdmin: true,
            }
        )
    }
}
