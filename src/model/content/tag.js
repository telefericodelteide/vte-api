import constants from "../../constants";
import VolcanoApi from "../../vte";
import Collection from '../entity/collection';

// Endpoints
const ENDPOINT_GET_TAGS = "content/tags";
const ENDPOINT_GET_TAG = ENDPOINT_GET_TAGS + "/:id";
const ENDPOINT_GET_TAG_GROUPS = ENDPOINT_GET_TAGS + "/groups";

export default TagClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function TagClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Tag methods */

    /**
    * Return a collection with all tags with pagination.
    *
    * @param {array} [params]  Optional object which contains the parameters to build the
    *                          request, for instance, if you want to move to specific page,
    *                          you must add 'page: 3' to the params object.
    *
    * @returns {Collection}
    */
    this.getTags = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_TAGS, params, {
            isAdmin: false
        });
    };

    /**
    * Return an Entity containing a tag which match by Id
    *
    * @param {string} [id]  tag ID.
    *
    * @returns {Entity}
    */
    this.getTag = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_TAG, id, false, {
            parameters: { id: id },
        });
    };


    /**
    * Return a collection with all tag groups with pagination.
    *
    * @param {array} [params] 
    * 
    * @return {Collection}
    */
    this.getTagGroups = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_TAG_GROUPS, params, {
            isAdmin: false
        })
    }

    /**
    * Add a new tag
    *
    * @param {Object}   data      data to be updated
    *
    * @returns {Entity}
    */
    this.addTag = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TAGS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        );
    };

    /**
    * Make a request to update a tag
    *
    * @param {numeric}  id        id of the tag to be updated
    * @param {Object}   data      data to be updated
    *
    * @return {Entity}
    */
    this.editTag = (id, data) => {
        if (isNaN(parseInt(id))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TAG,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
    * (Soft) delete a tag
    *
    * @param {string}  id   tag Id
    *
    * @return {Entity}
    */
    this.deleteTag = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TAG,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }
}
