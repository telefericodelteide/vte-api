import VolcanoApi from '../../vte';
import constants from '../../constants';

// Endpoints
const GET_SECTION = "content/sites/:siteId/sections/:id"

export default SectionClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SectionClient(volcanoClient) {
    /**
         * @var {VolcanoApi}
         */
    this.volcanoClient = volcanoClient;

    /**
        * Return the section for ID given.
        *
        * @param sectionId
        *
        * @returns {Entity}
        */
    this.getSection = (siteId, sectionId) => {
        return this.volcanoClient.makeEntityRequest(
            GET_SECTION,
            sectionId,
            constants.HTTP_GET,
            null,
            {
                path: {
                    id: sectionId,
                    siteId: siteId,
                },
                isAdmin: false,
            },
            null
        )
    }
}