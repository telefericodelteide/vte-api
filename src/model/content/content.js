import SiteClient from "./site"
import ExperienceClient from "./experience";
import ProductClient from "./product";
import SubcategoryClient from "./subcategory";
import SectionClient from "./section";
import ArticleClient from "./article";
import TagClient from "./tag";

export default ContentClient

/**
 * Container which has the content clients and methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ContentClient(volcanoClient) {

    this.site = new SiteClient(volcanoClient)
    this.subcategory = new SubcategoryClient(volcanoClient)
    this.section = new SectionClient(volcanoClient)
    this.article = new ArticleClient(volcanoClient)
    this.experience = new ExperienceClient(volcanoClient)
    this.product = new ProductClient(volcanoClient)
    this.tag = new TagClient(volcanoClient)

    // Deprecate methods
    this.getSites = (params) => {
        return this.site.getSites(params)
    }

    this.getSite = (siteId) => {
        return this.site.getSite(siteId)
    }

    this.getExperiences = (params) => {
        return this.experience.getExperiences(params)
    }

    this.getExperience = (experienceId) => {
        return this.experience.getExperience(experienceId)
    }

    this.getProducts = (params) => {
        return this.product.getProducts(params)
    }

    this.getProduct = (productId) => {
        return this.product.getProduct(productId)
    }

    this.getArticle = (siteId, sectionId, articleId) => {
        return this.article.getArticle(siteId, sectionId, articleId)
    }

    this.getSubcategories = (siteId, viewMode) => {
        return this.subcategory.getSubcategories(siteId, viewMode)
    }

    this.getSubcategory = (siteId, viewMode) => {
        return this.subcategory.getSubcategory(siteId, viewMode)
    }

    this.getFeaturedProducts = (siteId) => {
        return this.site.getFeaturedProducts(siteId)
    }
    //
}
