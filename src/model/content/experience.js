import VolcanoApi from '../../vte';
import Collection from '../entity/collection';

// Endpoints
const GET_EXPERIENCES_ROOT = "content/experiences"
const GET_EXPERIENCE = GET_EXPERIENCES_ROOT + "/:id"

export default ExperienceClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ExperienceClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
    * Retrieve a collection of Experiences
    *
    * @param params
    *
    * @returns {Collection}
    */
    this.getExperiences = (params) => {
        return this.volcanoClient.makeCollectionRequest(GET_EXPERIENCES_ROOT, params, {
            isAdmin: false
        })
    }

    /**
    * Return the experience for ID given.
    *
    * @param experienceId
    *
    * @returns {Entity}
    */
    this.getExperience = (experienceId) => {
        return this.volcanoClient.getEntityRequest(GET_EXPERIENCE, experienceId, false)
    }
}
