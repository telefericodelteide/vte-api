import VolcanoApi from '../../vte';
import constants from '../../constants';

// Endpoints
const GET_ARTICLE = "content/sites/:siteId/sections/:sectionId/articles/:id"

export default ArticleClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ArticleClient(volcanoClient) {
    /**
         * @var {VolcanoApi}
         */
    this.volcanoClient = volcanoClient;

    /**
        * Return the article for ID given.
        *
        * @param articleId
        *
        * @returns {Entity}
        */
    this.getArticle = (siteId, sectionId, articleId) => {
        return this.volcanoClient.makeEntityRequest(
            GET_ARTICLE,
            articleId,
            constants.HTTP_GET,
            null,
            {
                path: {
                    id: articleId,
                    sectionId: sectionId,
                    siteId: siteId,
                },
                isAdmin: false,
            },
            null
        )
    }
}