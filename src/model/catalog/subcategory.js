import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_SUBCATEGORIES = "catalog/subcategories";
const ENDPOINT_GET_SUBCATEGORY = ENDPOINT_GET_SUBCATEGORIES + "/:id";

export default SubcategoryClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SubcategoryClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Subcategories methods */

    /**
     * Return a collection with all Subcategories with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getSubcategories = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_SUBCATEGORIES,
            params
        );
    };

    /**
     * Return an Entity containing a Subcategory which match by Id.
     *
     * @param {number} [id]  CATEGORY ID.
     *
     * @returns {Entity}
     */
    this.getSubcategory = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_SUBCATEGORY, id, true, {
            parameters: { id: id },
        });
    };

    /**
     * Return a XLS file containing a collection of Subcategories.
     */
    this.exportSubcategories = (fields, params, progressHandler) => {
        const endpoint = ENDPOINT_GET_SUBCATEGORIES
        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            null,
            fields,
            "subcategories",
            progressHandler
        )
    }
}
