import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_CATEGORIES = "catalog/categories";
const ENDPOINT_GET_CATEGORY = ENDPOINT_GET_CATEGORIES + "/:id";

export default CategoryClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CategoryClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Categories methods */

    /**
     * Return a collection with all Categories with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getCategories = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_CATEGORIES,
            params
        );
    };

    /**
     * Return an Entity containing a Category which match by Id .
     *
     * @param {number} [id]  CATEGORY ID.
     *
     * @returns {Entity}
     */
    this.getCategory = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_CATEGORY, id, true, {
            parameters: { id: id },
        });
    };
}
