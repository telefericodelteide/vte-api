import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_LANGUAGES = "catalog/languages";

export default LanguageClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function LanguageClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Languages methods */

    /**
     * Return a collection with all languages with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getLanguages = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_LANGUAGES,
            params
        );
    };
}
