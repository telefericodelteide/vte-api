import SubcategoryClient from "./subcategory"
import ExperienceClient from "./experience"
import ProductClient from "./product"
import SupplierClient from "./supplier"
import DiscountCodeClient from "./discountCode"
import LanguageClient from "./language"
import TransportClient from "./transport"
import RemoteProductProvider from "./remoteProductProvider"
import RateGroupClient from "./rateGroup"
import ProductRatesPublicationClient from "./productRatesPublication"
import CustomerTypeGroupClient from "./customerTypeGroup"

export default CatalogClient

/**
 * Container which has the catalog clients and methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CatalogClient(volcanoClient) {
    this.customerTypeGroup = new CustomerTypeGroupClient(volcanoClient)
    this.subcategory = new SubcategoryClient(volcanoClient)
    this.experience = new ExperienceClient(volcanoClient)
    this.product = new ProductClient(volcanoClient)
    this.supplier = new SupplierClient(volcanoClient)
    this.discountCode = new DiscountCodeClient(volcanoClient)
    this.language = new LanguageClient(volcanoClient)
    this.transport = new TransportClient(volcanoClient)
    this.remoteProductProvider = new RemoteProductProvider(volcanoClient)
    this.rateGroup = new RateGroupClient(volcanoClient)
    this.productRatesPublication = new ProductRatesPublicationClient(volcanoClient)
}
