import VolcanoApi from '../../vte'
import Entity from '../entity/entity'
import constants from "../../constants";
export default ExperienceClient

// Constants for the endpoints
const ENDPOINT_GET_EXPERIENCES = 'catalog/experiences'
const ENDPOINT_GET_EXPERIENCE = ENDPOINT_GET_EXPERIENCES + '/:id'
const SET_EXPERIENCE_PRODUCTS = ENDPOINT_GET_EXPERIENCE + "/config/experience-products"

/**
 * Container which has the product methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ExperienceClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Return a collection with all Experiences with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getExperiences = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_EXPERIENCES,
            params
        )
    }

    /**
     * Return an Entity containing an experience which match by Id `experienceId`.
     *
     * @param {BigInteger} [experienceId] experience ID.
     *
     * @returns {Entity}
     */
    this.getExperience = (experienceId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_EXPERIENCE,
            experienceId,
            true,
            params
        )
    }

    /**
     * Return a XLS file containing a collection of Experiences.
     */
    this.exportExperiences = (fields, params, progressHandler) => {
        const endpoint = ENDPOINT_GET_EXPERIENCES
        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            null,
            fields,
            "experiences",
            progressHandler
        )
    }

    /**
     * Set products from experience
     *
     * @param {string} id experience Id
     * @param {array} products
     *
     * @return {Entity}
     */
    this.setExperienceProducts = (id, products) => {
        return this.volcanoClient.makeEntityRequest(
            SET_EXPERIENCE_PRODUCTS ,
            id,
            constants.HTTP_POST,
            {
                experience_products: products
            },
            {
                isAdmin: true
            },
            null
        )
    }
}
