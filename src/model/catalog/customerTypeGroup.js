import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_CUSTOMER_TYPE_GROUPS = "catalog/customer-type-groups";

export default CustomerTypeGroupClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function CustomerTypeGroupClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /** Categories methods */

    /**
     * Return a collection with all Customer type groups with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getCustomerTypeGroups = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_CUSTOMER_TYPE_GROUPS,
            params
        )
    }
}
