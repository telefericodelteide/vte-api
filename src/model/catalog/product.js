import { addMonths, format } from 'date-fns'
import isObject from 'lodash/isObject'
import VolcanoApi from '../../vte'
import Entity from '../entity/entity'
import constants from '../../constants'
import Product from '../entity/catalog/product'

export default ProductClient

// Constants for the endpoints
const ENDPOINT_GET_CORE_PRODUCTS = "products"
const ENDPOINT_GET_CORE_PRODUCT = ENDPOINT_GET_CORE_PRODUCTS + "/:id"
const ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY = ENDPOINT_GET_CORE_PRODUCT + "/availability/:date"
const ENDPOINT_GET_CORE_PRODUCT_RATES = ENDPOINT_GET_CORE_PRODUCT + "/rates/:date"

const ENDPOINT_GET_PRODUCTS = "catalog/products"
const ENDPOINT_GET_PRODUCT = ENDPOINT_GET_PRODUCTS + "/:id"
const ENDPOINT_GET_PRODUCT_CONTAINERS = ENDPOINT_GET_PRODUCT + "/containers"
const ENDPOINT_GET_PRODUCT_CONTAINER = ENDPOINT_GET_PRODUCT_CONTAINERS + "/:container_id"
const ENDPOINT_GET_COMPOUND_PRODUCTS = ENDPOINT_GET_PRODUCT + "/compound-products"
const ENDPOINT_GET_PRODUCT_EXPERIENCES = ENDPOINT_GET_PRODUCT + "/experiences"
const ENDPOINT_GET_IACPOS_PRODUCT_RATES = "products/:id/iacpos_rates"

/**
 * Container which has the product methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ProductClient(volcanoClient) {
    this.volcanoClient = volcanoClient

    /**
     * Return a collection with all Products with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getProducts = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_PRODUCTS,
            params,
            {},
            Product
        )
    }

    /**
     * Return a collection with all featured products.
     * 
     * @param {array} [params] Optional object which contains the parameters to build the request.
     */
    this.getFeaturedProducts = (params) => {
        return this.getProducts({
            ...params,
            featured: true,
        })
    }

    /**
     * Return an Entity containing a product which match by Id `productId`.
     *
     * @param {BigInteger} [id]  booking ID.
     * @param {boolean} ratesPublished true to get the rates published of the product
     * @param {array} [params] Optional object which contains the parameters to build the request.
     *
     * @returns {Entity}
     */
    this.getProduct = (id, ratesPublished, params) => {
        params = isObject(params) ? params : {}
        params.rates_published = ratesPublished ? 1 : 0
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_PRODUCT,
            id,
            true,
            params,
            "product"
        )
    }

    /**
     * Add a product
     *
     * @param {Object}   data     
     *
     * @returns {Entity}
     */
    this.addProduct = (data) => {
        return this.volcanoClient.addEntityRequest(ENDPOINT_GET_PRODUCTS, data);
    };

    /**
     * Edit a product which match by Id 
     *
     * @param {numeric}  id        id of the product to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editProduct = (id, data, ratesPublished) => {
        if (isNaN(parseInt(id))) {
            return null
        }
        const params = { query: { rates_published: ratesPublished ? 1 : 0 } }
        return this.volcanoClient.editEntityRequest(ENDPOINT_GET_PRODUCT, id, data, true, params)
    }

    /** 
     * Edit the configuration of a product at enterprise level.
     * 
     * @param {string} id product id
     * @param {array} data 
     * 
     * @returns {Entity} 
     */
    this.editContainerProduct = (id, containerId, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_GET_PRODUCT_CONTAINER,
            id,
            data,
            true,
            {
                path: {
                    id: id,
                    container_id: containerId,
                }
            }
        )
    }

    /**
     * Returns the availability for a product.
     *
     * @param {number} productId
     * @param {Date} date
     * @param {boolean} fullMonth true if the availability for the entire month of the date should be retrieved
     * @param {params} params
     *
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductAvailability = (productId, date, fullMonth, params) => {
        if (!date) {
            date = new Date()
        }

        params = isObject(params) ? params : {}

        const request = this.volcanoClient.prepareRequest(
            ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY,
            {
                method: "GET",
                isAdmin: false,
                parameters: {
                    id: productId,
                    date: fullMonth
                        ? format(date, "yyyy-MM")
                        : format(date, "yyyy-MM-dd"),
                },
                query: {
                    ...params,
                },
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Returns the availability for a product.
     *
     * @param {number} productId
     * @param {Date} date
     * @param {number} months number of months to query
     * @param {params} params
     **
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductAvailabilityRange = (productId, date, months, params) => {
        if (!date) {
            date = new Date()
        }

        if (!months) {
            months = 1
        }

        params = isObject(params) ? params : {}

        var requests = new Array(months)
        for (var i = 0; i < months; i++) {
            const request = this.volcanoClient.prepareRequest(
                ENDPOINT_GET_CORE_PRODUCT_AVAILABILITY,
                {
                    method: "GET",
                    isAdmin: false,
                    parameters: {
                        id: productId,
                        date: format(date, "yyyy-MM"),
                    },
                    query: {
                        ...params,
                    },
                }
            )

            date = addMonths(date, 1)

            requests[i] = this.volcanoClient
                .makeRequest(request)
                .then((res) => {
                    return res[Object.keys(res)[0]]
                })
        }

        return Promise.all(requests).then((values) => {
            return values.flat()
        })
    }

    /**
     * Return a collection with all rates of a given product id with pagination.
     *
     * @param {number} productId
     * 
     * @returns {Collection}
     */
    this.getProductRates = (productId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_PRODUCT_RATES,
            params,
            {
                is_admin: true,
                parameters: {
                    id: productId,
                },
            }
        );
    }

    /**
     * Return a collection with all compound products of a given product id with pagination.
     *
     * @param {number} productId
     * 
     * @returns {Collection}
     */
    this.getCompoundProducts = (productId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_COMPOUND_PRODUCTS,
            params,
            {
                is_admin: true,
                parameters: {
                    id: productId,
                },
            }
        );
    }

    /**
     * Edit the base products of a given compound product id
     *
     * @param {numeric}  productId  id of compound products base product
     * @param {Object}   data       data to be updated
     *
     * @return {Entity}
     */
    this.editCompoundProducts = (productId, data) => {
        if (isNaN(parseInt(productId))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_COMPOUND_PRODUCTS,
            constants.HTTP_PATCH,
            data.result,
            {
                isAdmin: true,
                path: {
                    id: productId
                }
            },
            null
        )
    }

    /**
     * Return a collection with all iacpos product rates of a given product id with pagination.
     *
     * @param {number} productId
     * 
     * @returns {Collection}
     */
    this.getProductIacposRates = (productId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_IACPOS_PRODUCT_RATES,
            params,
            {
                is_admin: true,
                parameters: {
                    id: productId,
                },
            }
        );
    }

    /**
     * Returns the rates for a product in a specific date
     * 
     * @param {number} productId
     * @param {Date} date
     *
     * @returns {PromiseLike<*> | Promise<*>}
     */
    this.getProductRatesForDate = (productId, queryDate, dateFrom, crmIntermediaryId = null, currency = null) => {

        if (!queryDate) {
            queryDate = new Date()
        }

        if (!dateFrom) {
            dateFrom = new Date()
        }

        const params = {
            query_date: format(queryDate, "yyyy-MM-dd"),
            crm_intermediary_id: (crmIntermediaryId == null) ? null : crmIntermediaryId
        }


        const request = this.volcanoClient.prepareRequest(ENDPOINT_GET_CORE_PRODUCT_RATES, {
            method: "GET",
            isAdmin: false,
            currency: currency,
            parameters: {
                id: productId,
                date: format(dateFrom, "yyyy-MM-dd"),
            },
            query: params
        })

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Return a collection with all Shared Experiences of a given product id with pagination.
     *
     * @param {number} productId
     * 
     * @returns {Collection}
     */
    this.getProductSharedExperiences = (productId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_PRODUCT_EXPERIENCES,
            params,
            {
                is_admin: true,
                parameters: {
                    id: productId,
                },
            }
        );
    }

    /**
     * Return a XLS file containing a collection of Products.
     */
    this.exportProducts = (fields, params, progressHandler) => {
        const endpoint = ENDPOINT_GET_PRODUCTS
        return this.volcanoClient.makeCollectionExportRequest(
            endpoint,
            params,
            null,
            fields,
            "products",
            progressHandler
        )
    }
}
