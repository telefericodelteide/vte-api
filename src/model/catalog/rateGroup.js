import { isObject } from "lodash"
import VolcanoApi from "../../vte"
import Entity from "../entity/entity"
import Product from "../entity/catalog/product"

// Endpoints
const ENDPOINT_RATE_GROUPS = "catalog/rate-groups"
const ENDPOINT_RATE_GROUP = ENDPOINT_RATE_GROUPS + "/:id"
const ENDPOINT_RATE_GROUP_PDF = ENDPOINT_RATE_GROUP + '/pdf'
const ENDPOINT_RATE_GROUP_PRODUCTS = ENDPOINT_RATE_GROUP + "/products"
const ENDPOINT_RATE_GROUP_PRODUCT = ENDPOINT_RATE_GROUP_PRODUCTS + "/:product_id"
const ENDPOINT_RATE_GROUP_INTERMEDIARIES = ENDPOINT_RATE_GROUP + "/intermediaries"
const ENDPOINT_RATE_GROUP_EXPERIENCES = ENDPOINT_RATE_GROUP + "/experiences"
const ENDPOINT_RATE_GROUP_COPY = ENDPOINT_RATE_GROUP + "/copy"

export default RateGroupClient;

/**
 * Container which has the rate groups methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function RateGroupClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Returns a collection with all rate groups with pagination.
     *
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getRateGroups = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_RATE_GROUPS,
            params
        )
    }

    /**
     * Returns an entity containing a rate group which match by Id 
     *
     * @param {string} [id] rate group identifier.
     *
     * @returns {Entity}
     */
    this.getRateGroup = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_RATE_GROUP, id, true, {
            parameters: { id: id },
        })
    }

    /**
     * Creates a rate group
     *
     * @param {Object} data data to be updated
     *
     * @returns {Entity}
     */
    this.addRateGroup = (data) => {
        return this.volcanoClient.addEntityRequest(ENDPOINT_RATE_GROUPS, this.processRateGroupRequestData(data), true)
    }

    /**
     * Updates a rate group
     *
     * @param {string} id id of the rate group to be updated
     * @param {Object} data data to be updated
     *
     * @return {Entity}
     */
    this.editRateGroup = (id, data) => {
        return this.volcanoClient.editEntityRequest(ENDPOINT_RATE_GROUP, id, this.processRateGroupRequestData(data), true)
    }

    /**
     * Copies a rate group
     *
     * @param {string} id id of the rate group to be copied
     * @param {Object} data data to be updated
     *
     * @return {Entity}
     */
    this.copyRateGroup = (id, data) => {
        return this.volcanoClient.addEntityRequest(
            ENDPOINT_RATE_GROUP_COPY, 
            this.processRateGroupRequestData(data), 
            true,
            {
                path: {
                    id: id
                }
            }
        )
    }

    /**
     * Deletes a rate group
     * 
     * @param {string} id 
     * 
     * @returns {boolean} true if the rate group was deleted
     */
    this.deleteRateGroup = (id) => {
        return this.volcanoClient.deleteResourceRequest(ENDPOINT_RATE_GROUP, id, true)
    }

    /**
     * Return an application/PDF octet
     *
     * @param id
     *
     * @returns {Object|Entity}
     */
    this.getRateGroupPdf = (id, params) => {
        params = isObject(params) ? params : {}
        return this.volcanoClient.makeFileRequest(ENDPOINT_RATE_GROUP_PDF, id, true, params)
    }

    /** 
     * Returns a collection with all products of a rate group with pagination.
     * 
     * @param {string} id rate group identifier
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     * 
     * @returns {Collection} 
     */
    this.getRateGroupProducts = (id, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_RATE_GROUP_PRODUCTS,
            params,
            {
                parameters: { id: id },
            },
            Product
        )
    }

    /** 
     * Add and/or delete a collection of products from a rate group.
     * 
     * @param {string} id rate group identifier
     * @param {array} data 
     * 
     * @returns {Collection} 
     */
    this.editRateGroupProducts = (id, data) => {
        return this.volcanoClient.addEntityRequest(
            ENDPOINT_RATE_GROUP_PRODUCTS,
            data,
            true,
            {
                path: {
                    id: id
                }
            }
        )
    }

    /** 
     * Edit the configurarion of a product in a rate group.
     * 
     * @param {string} id rate group identifier
     * @param {int} productId product identifier
     * @param {array} data 
     * 
     * @returns {Entity} 
     */
    this.editRateGroupProduct = (id, productId, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_RATE_GROUP_PRODUCT,
            id,
            data,
            true,
            {
                path: {
                    id: id,
                    product_id: productId
                }
            }
        )
    }

    /**
     * Deletes a product from a rate group 
     * @param {*} id rate group identifier
     * @param {*} productId product identifier
     * @returns 
     */
    this.deleteRateGroupProduct = (id, productId) => {
        return this.volcanoClient.deleteResourceRequest(
            ENDPOINT_RATE_GROUP_PRODUCT,
            id,
            true,
            {
                path: {
                    product_id: productId,
                }
            }
        )
    }

    /**
     * Returns the list of experiences of a rate group
     *  
     * @param {string} id rate group identifier  
     * @returns {Array}  
     */
    this.getRateGroupExperiences = (id) => {
        const request = this.volcanoClient.prepareRequest(ENDPOINT_RATE_GROUP_EXPERIENCES, {
            method: 'GET',
            isAdmin: true,
            parameters: {
                id: id,
            },
        })

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Updates the order of the experiences of a rate group.
     *  
     * @param {*} id rate group identifier
     * @param {*} data sorted experiences
     * @returns {Array}
     */
    this.editRateGroupExperiences = (id, data) => {
        return this.volcanoClient.editEntityRequest(            
            ENDPOINT_RATE_GROUP_EXPERIENCES,
            id,
            data
        )
    }

    /** 
     * Returns a collection with all intermediaries of a rate group with pagination.
     * 
     * @param {string} id rate group identifier
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     * 
     * @returns {Collection} 
     */
    this.getRateGroupIntermediaries = (id, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_RATE_GROUP_INTERMEDIARIES,
            params,
            {
                parameters: { id: id },
            }
        )
    }

    this.processRateGroupRequestData = (data) => {
        if (data.config) {
            if (typeof data.config.protected === 'string') {
                data.config.protected = data.config.protected === 'true' || data.config.protected === '1'
            }

            data.config.protected = !!data.config.protected

            if (data.config.default_commission) {
                data.config.default_commission = parseFloat(data.config.default_commission)
            }
        }

        return data
    }
}
