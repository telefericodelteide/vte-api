import VolcanoApi from "../../vte"
import Entity from "../entity/entity"
import { format, isDate } from "date-fns"

// Endpoints
const ENDPOINT_PRODUCT_RATES_PUBLICATIONS = "catalog/product-rates-publications"
const ENDPOINT_PRODUCT_RATES_PUBLICATION = ENDPOINT_PRODUCT_RATES_PUBLICATIONS + "/:id"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINERS = ENDPOINT_PRODUCT_RATES_PUBLICATION + "/containers"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER = ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINERS + "/:container_id"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PDF = ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER + "/pdf"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_EXPERIENCES = ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER + "/experiences"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PRODUCTS = ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER + "/products"
const ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PRODUCT = ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PRODUCTS + "/:product_id?container_type=:container_type"

export default ProductRatesPublicationClient;

/**
 * Container which has the product rates publications methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function ProductRatesPublicationClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /**
     * Returns a collection with all product rates publications with pagination.
     *
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getProductRatesPublications = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATIONS,
            params
        )
    }

    /**
     * Returns an entity containing a product rates publication which match by Id 
     *
     * @param {string} [id] product rates publication identifier.
     *
     * @returns {Entity}
     */
    this.getProductRatesPublication = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_PRODUCT_RATES_PUBLICATION, id, true, {
            parameters: { id: id },
        })
    }

    /**
     * Return an application/PDF octet
     *
     * @param id
     *
     * @returns {Object|Entity}
     */
    this.getProductRatesPublicationPdf = (id, containerId, containerType) => {
        return this.volcanoClient.makeFileRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PDF,
            id,
            true,
            {
                parameters: { container_id: containerId },
                container_type: containerType
            }
        )
    }

    /** 
     * Returns a collection with all the containers of a product rates publication with pagination.
     * 
     * @param {string} id product rates publication identifier
     * @param {array} [params] Optional object which contains the parameters to build the request, for instance,
     * if you want to move to specific page, you must add 'page: 3' to the params object.
     * 
     * @returns {Collection} 
     */
    this.getProductRatesPublicationContainers = (id, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINERS,
            params,
            {
                parameters: { id: id },
            }
        )
    }

    /**
     * Creates a product rates publication
     *
     * @param {Object} data data to be updated
     *
     * @returns {Entity}
     */
    this.addProductRatePublication = (data) => {
        return this.volcanoClient.addEntityRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATIONS,
            this.processProductRatesPublicationRequestData(data),
            true,
            {
                timeout: 60000, // this process can take a while
            }
        )
    }

    /**
     * Edit a product rates publication
     *
     * @param {string} id product rates publication identifier     * 
     * @param {Object} data data to be updated
     *
     * @returns {Entity}
     */
    this.editProductRatePublication = (id, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION,
            id,
            this.processProductRatesPublicationRequestData(data),
            true,
            {
                timeout: 60000, // this process can take a while
            }
        )
    }

    /** 
     * Add and/or delete a collection of products from a product rates publication.
     * 
     * @param {string} id product rates publication identifier
     * @param {array} data 
     * 
     * @returns {Collection} 
     */
    this.editProductRatePublicationProducts = (id, containerId, containerType, data) => {
        return this.volcanoClient.addEntityRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PRODUCTS + "?container_type=:container_type",
            data,
            true,
            {
                path: {
                    id: id,
                    container_id: containerId,
                    container_type: containerType
                }
            }
        )
    }

    /** 
     * Edit the configuration of a product in a container of a product rates publication.
     * 
     * @param {string} id product rates publication identifier
     * @param {string} containerId container identifier
     * @param {int} productId product identifier
     * @param {array} data 
     * 
     * @returns {Entity} 
     */
    this.editContainerProduct = (id, containerId, containerType, productId, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_PRODUCT,
            id,
            data,
            true,
            {
                path: {
                    id: id,
                    container_id: containerId,
                    product_id: productId,
                    container_type: containerType
                }
            }
        )
    }

    /**
     * Returns the list of experiences of a container of a product rates publication.
     *  
     * @param {string} id product rates publication identifier
     * @param {string} containerId container identifier
     * @param {string} containerType container type
     * @returns {Array}  
     */
    this.getContainerExperiences = (id, containerId, containerType) => {
        const request = this.volcanoClient.prepareRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_EXPERIENCES + "?container_type=:container_type",
            {
                method: 'GET',
                isAdmin: true,
                parameters: {
                    id: id,
                    container_id: containerId,
                    container_type: containerType
                },
            }
        )

        return this.volcanoClient.makeRequest(request).then((res) => {
            return res[Object.keys(res)[0]]
        })
    }

    /**
     * Updates the order of the experiences of a container in a product rates publication.
     *  
     * @param {*} id product rates publication identifier
     * @param {*} containerId container identifier
     * @param {*} data sorted experiences
     * @returns {Array}
     */
    this.editContainerExperiences = (id, containerId, data) => {
        return this.volcanoClient.editEntityRequest(
            ENDPOINT_PRODUCT_RATES_PUBLICATION_CONTAINER_EXPERIENCES,
            id,
            data,
            true,
            {
                path: {
                    id: id,
                    container_id: containerId
                }
            }
        )
    }

    this.processProductRatesPublicationRequestData = (data) => {
        // process dates (format dates as YYYY-mm-dd)
        const dateFields = ['valid_from', 'date_from', 'date_to']
        dateFields.forEach((field) => {
            if (data[field] && isDate(data[field])) {
                data[field] = format(data[field], "yyyy-MM-dd")
            }
        })

        return data
    }
}
