import VolcanoApi from "../../vte";
import constants from "../../constants";

// Endpoints
const ENDPOINT_GET_DISCOUNT_CODES = "catalog/discount-codes";
const ENDPOINT_GET_DISCOUNT_CODE = ENDPOINT_GET_DISCOUNT_CODES + "/:id";
const ENDPOINT_DISCOUNT_CODE_PRODUCTS = ENDPOINT_GET_DISCOUNT_CODE + "/products"
const ENDPOINT_DISCOUNT_CODE_DELETE_PRODUCT = ENDPOINT_DISCOUNT_CODE_PRODUCTS + "/:product_id"

export default DiscountCodeClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function DiscountCodeClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Discount Code methods */

    /**
     * Return a collection with all discount codes with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getDiscountCodes = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_DISCOUNT_CODES,
            params
        );
    };

    /**
     * Return an Entity containing a discount code which match by Id 
     *
     * @param {string} [id]  discount code ID.
     *
     * @returns {Entity}
     */
    this.getDiscountCode = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_DISCOUNT_CODE, id, true, {
            parameters: { id: id },
        });
    };

    /**
     * Add a discount code
     *
     * @param {Object}   data      data to be updated
     *
     * @returns {Entity}
     */
    this.addDiscountCode = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_DISCOUNT_CODES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        );
    };

    /**
     * Edit a discount code which match by Id 
     *
     * @param {numeric}  id        id of the discount code to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.editDiscountCode = (id, data) => {
        if (isNaN(parseInt(id))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_DISCOUNT_CODE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Delete a discount code which match by Id 
     * 
     * @param {number} id 
     * 
     * @returns {Entity}
     */
    this.deleteDiscountCode = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_DISCOUNT_CODE,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: id
                },
                isAdmin: true
            }
        )
    }

    /**
     * Add a product to a discount code id
     *
     * @param {numeric}  id        id of the discount code to be updated
     * @param {Object}   data      data to be updated
     *
     * @return {Entity}
     */
    this.addProduct = (id, data) => {
        if (isNaN(parseInt(id))) {
            return null
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_DISCOUNT_CODE_PRODUCTS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
     * Delete a product from a discount code id
     * 
     * @param {number} id
     * @param {number} product_id
     * 
     * @returns {Entity}
     */
    this.deleteProduct = (id, product_id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_DISCOUNT_CODE_DELETE_PRODUCT,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    id: id,
                    product_id: product_id
                },
                isAdmin: true
            }
        )
    }
}
