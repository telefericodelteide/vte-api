import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_TRANSPORTS = "catalog/transports";

export default TransportClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function TransportClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Transports methods */

    /**
     * Return a collection with all transports with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getTransports = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_TRANSPORTS,
            params
        );
    };
}
