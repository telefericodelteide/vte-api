import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_SUPPLIERS = "catalog/suppliers";
const ENDPOINT_GET_SUPPLIER = ENDPOINT_GET_SUPPLIERS + "/:id";

export default SupplierClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function SupplierClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Suppliers methods */

    /**
     * Return a collection with all suppliers with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getSuppliers = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_SUPPLIERS,
            params
        );
    };

    /**
     * Return an Entity containing a supplier which match by Id .
     *
     * @param {string} [id]  supplier ID.
     *
     * @returns {Entity}
     */
    this.getSupplier = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_SUPPLIER, id, true, {
            parameters: { id: id },
        });
    };
}
