import VolcanoApi from "../../vte";

// Endpoints
const ENDPOINT_GET_REMOTE_PRODUCT_PROVIDERS = "catalog/remote-product-providers";

export default RemoteProductProviderClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function RemoteProductProviderClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** Remote product providers methods */

    /**
     * Return a collection with all remote product providers with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getRemoteProductProviders = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_REMOTE_PRODUCT_PROVIDERS,
            params
        );
    };
}
