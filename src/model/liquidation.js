import constants from "../constants"
import VolcanoApi from "../vte"
import Entity from './entity/entity'
import _isObject from 'lodash/isObject'

// Endpoints
const ENDPOINT_GET_LIQUIDATIONS = 'liquidations'
const ENDPOINT_GENERATE_LIQUIDATIONS = ENDPOINT_GET_LIQUIDATIONS + '/generate'
const ENDPOINT_GET_LIQUIDATION = ENDPOINT_GET_LIQUIDATIONS + '/:id'
const ENDPOINT_GET_LIQUIDATION_PDF = ENDPOINT_GET_LIQUIDATION + '/pdf'

export default LiquidationClient

/**
 * Container which has the order methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function LiquidationClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    this.generate = () => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GENERATE_LIQUIDATIONS,
            constants.HTTP_POST,
            null,
            {
                isAdmin: true
            }
        )
    }

    /**
     * Return a collection with all liquidations with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getLiquidations = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_LIQUIDATIONS,
            params)
    }

    /**
     * Return an Entity containing a liquidation which match by Id `liquidationId`.
     *
     * @param {BigInteger} [liquidationId]  liquidation ID.
     *
     * @returns {Entity}
     */
    this.getLiquidation = (liquidationId, params) => {
        return this.volcanoClient.getEntityRequest(
            ENDPOINT_GET_LIQUIDATION,
            liquidationId,
            true,
            params
        )
    }

    /**
     * Return an application/PDF octet
     *
     * @param liquidationId
     *
     * @returns {null|Object|Entity}
     */
    this.getLiquidationPdf = (liquidationId, params) => {
        const id = parseInt(liquidationId)
        if (isNaN(liquidationId)) {
            return null
        }

        params = _isObject(params) ? params : {}

        return this.volcanoClient.makeFileRequest(ENDPOINT_GET_LIQUIDATION_PDF, id, true, params)
    }

}



