import constants from "../constants"
import VolcanoApi from "../vte"

// Endpoints
const ENDPOINT_BASE_ACCESS_CONTROL = 'access-control'
const ENDPOINT_GET_VENUES = ENDPOINT_BASE_ACCESS_CONTROL + '/venues'
const ENDPOINT_GET_VENUE = ENDPOINT_GET_VENUES + '/:venue_id'
const ENDPOINT_GET_POINTS = ENDPOINT_GET_VENUE + '/points'
const ENDPOINT_GET_POINT = ENDPOINT_GET_POINTS + '/:point_id'

export default AccessControlClient

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function AccessControlClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    /** Access control venues methods */

    this.addVenue = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            }
        )
    }

    /**
     * Return a collection with all venues with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getVenues = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_VENUES,
            params
        )
    }

    /**
     * Return an Entity containing a venue which match by Id `venueId`.
     *
     * @param {string} [id]  venue ID.
     *
     * @returns {Entity}
     */
    this.getVenue = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, { parameters: { venue_id: id } })
    }


    this.deleteVenue = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUE,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    venue_id: venueId
                },
                isAdmin: true
            }
        )
    }

    /** Access control points methods */

    this.addPoint = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_POINTS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            }
        )
    }

    /**
     * Return a collection with all points for a venue with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getPoints = (venueId, params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_VENUES,
            params,
            {
                isAdmin: true,
                parameters: {
                    venue_id: venueId,
                },
            }
        )
    }

    /**
     * Return an Entity containing a point within a venue which match by Id `pointId`.
     *
     * @param {string} [venueId]  venue ID.
     * @param {string} [id]  point ID.
     *
     * @returns {Entity}
     */
    this.getPoint = (venueId, id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, { parameters: { venue_id: venueId, point_id: id } })
    }


    this.deletePoint = (venueId, id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUE,
            constants.HTTP_DELETE,
            null,
            {
                path: {
                    venue_id: venueId,
                    point_id: id,
                },
                isAdmin: true
            }
        )
    }
}
