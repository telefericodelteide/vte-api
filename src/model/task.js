import constants from "../constants"
import VolcanoApi from "../vte"

// Endpoints
const ENDPOINT_GET_TASKS = 'tasks'
const ENDPOINT_GET_TASK = ENDPOINT_GET_TASKS + '/:id'

export default TaskClient

/**
 * Container which has the task methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function TaskClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient

    this.add = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TASKS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            }
        )
    }

    /**
     * Create a task to perform an operation on a set of entities.
     * 
     * @param {*} operation operation to perform
     * @param {*} entityType type of the entities to perform the operation
     * @param {*} entities array of entities ids
     * @param {*} payload operation payload (optional) 
     * @returns 
     */
    this.createEntitiesTask = (operation, entityType, entities, payload = null) => {
        const data = {
            operation: operation,
            entity_type: entityType,
            entities: entities,
            payload: payload
        }

        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TASKS,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true
            }
        )
    }

    /**
     * Return a collection with all tasks with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getTasks = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            ENDPOINT_GET_TASKS,
            params)
    }

    /**
     * Return an Entity containing a task which match by Id `taskId`.
     *
     * @param {BigInteger} [id]  task ID.
     *
     * @returns {Entity}
     */
    this.getTask = (id, params) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_TASK, id)
    }


    this.cancelTask = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_TASK + '/cancel',
            constants.HTTP_POST,
            null,
            {
                path: {
                    id: id,
                },
                isAdmin: true
            }
        )
    }
}



