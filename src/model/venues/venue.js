import constants from "../../../dist/constants";
import VolcanoApi from "../../../dist/vte";
import Collection from '../entity/collection';
import FacilityBookLineClient from "./facilityBookLine";

// Endpoints
const ENDPOINT_GET_VENUES = "venues";
const ENDPOINT_GET_VENUE = ENDPOINT_GET_VENUES + "/:id";

export default VenueClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function VenueClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;
    this.facilityBookLine = new FacilityBookLineClient(volcanoClient)    

    /** Venue methods */

    /**
    * Return a collection with all venues with pagination.
    *
    * @param {array} [params]  Optional object which contains the parameters to build the
    *                          request, for instance, if you want to move to specific page,
    *                          you must add 'page: 3' to the params object.
    *
    * @returns {Collection}
    */
    this.getVenues = (params) => {
        return this.volcanoClient.makeCollectionRequest(ENDPOINT_GET_VENUES, params, {
            isAdmin: true
        });
    };

    /**
    * Return an Entity containing a venue which match by Id
    *
    * @param {string} [id]  venue ID
    *
    * @returns {Entity}
    */
    this.getVenue = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_VENUE, id, true, {
            parameters: { id: id },
        });
    };

    /**
    * Add a new venue
    *
    * @param {Object}   data      data to be updated
    *
    * @returns {Entity}
    */
    this.addVenue = (data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUES,
            constants.HTTP_POST,
            data,
            {
                isAdmin: true,
            }
        );
    };

    /**
    * Make a request to update a venue
    *
    * @param {string}}  id        id of the venue to be updated
    * @param {Object}   data      data to be updated
    *
    * @return {Entity}
    */
    this.editVenue = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    /**
    * (Soft) delete a venue
    *
    * @param {string}  id   venue Id
    *
    * @return {Entity}
    */
    this.deleteVenue = (id) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_VENUE,
            constants.HTTP_DELETE,
            null,
            {
                isAdmin: true,
                path: {
                    id: id
                },
            }
        )
    }
}

