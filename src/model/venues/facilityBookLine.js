import constants from "../../../dist/constants";
import VolcanoApi from "../../../dist/vte";
import Collection from '../entity/collection';

// Endpoints
const ENDPOINT_GET_FACILITYBOOKLINES_SCHEMA = 'facility-book-lines/schema'
const ENDPOINT_GET_VENUE_FACILITYBOOKLINES = "venues/:venue_id/facility-book-lines";
const ENDPOINT_GET_FACILITYBOOKLINES = "facility-book-lines";
const ENDPOINT_GET_FACILITYBOOKLINE = "facility-book-lines/:id";

export default FacilityBookLineClient;

/**
 * Container which has the access control methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function FacilityBookLineClient(volcanoClient) {
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /** FacilityBookLine methods */

    /**
    * Return a collection with all venues with pagination.
    *
    * @param {int} [venueId]
    * @param {array} [params]       Optional object which contains the parameters to build the
    *                               request, for instance, if you want to move to specific page,
    *                               you must add 'page: 3' to the params object.
    *
    * @returns {Collection}
    */
    this.getFacilityBookLines = (venueId, params) => {
        let endpoint = ENDPOINT_GET_FACILITYBOOKLINES
        let queryParams = {}

        if (venueId!=null) {
            endpoint= ENDPOINT_GET_VENUE_FACILITYBOOKLINES
            queryParams = {
                venue_id: venueId,
            }
        }

        return this.volcanoClient.makeCollectionRequest(endpoint, params, {
            isAdmin: true,
            parameters: queryParams,
        });
    };

    /**
    * Return an Entity containing a venue which match by Id
    *
    * @param {int} [venueId]
    * @param {string} [id]  facilityBookLine Id
    *
    * @returns {Entity}
    */
    this.getFacilityBookLine = (id) => {
        return this.volcanoClient.getEntityRequest(ENDPOINT_GET_FACILITYBOOKLINE, id, true, {
            parameters: {                 
                id: id 
            },
        });
    };


    /**
    * Make a request to update a venue
    *
    * @param {int} venueId
    * @param {string}}  id        id of the facilityBookLine to be updated
    * @param {Object}   data      data to be updated
    *
    * @return {Entity}
    */
    this.editFacilityBookLines = (id, data) => {
        return this.volcanoClient.makeResourceRequest(
            ENDPOINT_GET_FACILITYBOOKLINE,
            constants.HTTP_PATCH,
            data,
            {
                isAdmin: true,
                path: {
                    id: id
                }
            },
            null
        )
    }

    this.getFacilityStates = () => {

        const request = this.volcanoClient.prepareRequest(ENDPOINT_GET_FACILITYBOOKLINES_SCHEMA, {
          method: constants.HTTP_GET,
          isAdmin: true
        })
    
        return this.volcanoClient.makeRequest(request).then((res) => {
            return Object.entries(res['facility_state_id']['options'])
        })    
      }
}
