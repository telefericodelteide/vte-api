import Entity from "./entity"

class User extends Entity {
    /**
     * Initializes the User object with the entity passed.
     * 
     * @param {*} props 
     */
    constructor(props) {
        super(props)

        if (!this.hasOwnProperty('permissions') || this.permissions === null) {
            this.permissions = {}
        }
    }

    /**
     * Get corporate account from user
     *
     * @returns {object} Corporate account
     */
    getCorporateAccount() {
        return this.corporate_account
    }

    /**
     * Checks if the user has the permission passed.
     *
     * @param {string} permission
     * @returns {boolean} true if the user has the permission, false otherwise.
     */
    hasPermission(permission) {
        return this.permissions.hasOwnProperty(permission)
    }

    /**
     * Checks if the user has any of the permissions passed.
     * 
     * @param {array} permissions 
     * @returns {boolean} true if the user has any of the permissions, false otherwise.
     */
    hasAnyPermission(permissions) {
        return permissions.some(permission => this.hasPermission(permission))
    }

    /**
     * Checks if the user has the role passed.
     * @param {string} role 
     * @returns {boolean} true if the user has the role, false otherwise.
     */
    hasRole(role) {
        return this.roles.some(userRole => userRole.slug === role)
    }

    /**
     * Check if the user is an intermediary
     * @returns {boolean} true if the user is an intermediary, false otherwise.
     */
    isIntermediary() {
        return this.hasOwnProperty("crm_intermediary")
    }
}

export default User