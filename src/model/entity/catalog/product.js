import { de } from "date-fns/locale"
import Entity from "../entity"

class Product extends Entity {
    /**
     * Initializes the Product object with the entity passed.
     * 
     * @param {*} props 
     */
    constructor(props) {
        super(props)
    }

    /**
     * Returns the qty restrictions of the product.
     *
     * @param {object} defaultValues Default values to return if the product has no qty restrictions
     * @returns {object} Valid qty restrictions of the product
     */
    getQtyRestrictions(defaultValues) {

        let result = {
            min: this?.qty_restrictions?.min || defaultValues?.min || -1,
            max: this?.qty_restrictions?.max || defaultValues?.max || -1,
        }

        if (this?.container_configuration?.qty_restrictions !== undefined) {
            result = {
                ...result,
                min: this?.container_configuration?.qty_restrictions?.min || result.min,
                max: this?.container_configuration?.qty_restrictions?.max || result.max,
            }
        }

        // process customer types restrictions
        if (this?.container_configuration?.customer_types_restrictions !== undefined && this.rates) {
            result.customer_types_restrictions = this?.container_configuration?.customer_types_restrictions
                .map(customerTypeId => {
                    const rate = this.rates
                        .find(rate => 
                            rate.customer_types.customer_types.some(customerType => customerType.id === customerTypeId)
                        )
    
                    return {
                        id: customerTypeId,
                        name: rate.customer_types.customer_types.find(customerType => customerType.id === customerTypeId).name,
                    }
                })
        }

        return result
    }
}

export default Product