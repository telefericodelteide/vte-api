
class Entity {
    /**
     * 
     * @param {*} props 
     */
    constructor(props) {
        Object.assign(this, props);

        this.actions = []
        if (this._links) {
            Object.entries(this._links).forEach(link => {
                const action = link[0].indexOf(':') < 0 ? link[0] : link[0].split(':')[1]

                if (link[1].data) {
                    this.actions[action] = {
                        href: link[1].href,
                        data: link[1].data,
                    }
                } else {
                    this.actions[action] = link[1].href
                }
            })
        }
    }

    /**
     * Returns the actions of the entity.
     * 
     * @returns {array}
     */
    getActions() {
        return this.actions
    }

    /**
     * Checks if the entity has the action passed.
     * 
     * @param {string} action 
     * @param {string} operation Action's operation (optional)
     * 
     * @returns {boolean} true if the entity has the action, false otherwise.
     */
    hasAction(action, allowedOperation = null) {
        if (this.actions[action] === undefined) {
            return false
        }

        if (allowedOperation === null) {
            return true
        }

        if (this.actions[action].data && this.actions[action].data.allowed_operations) {
            return this.actions[action].data.allowed_operations.includes(allowedOperation)
        }

        return false
    }
}

export default Entity

