import Entity from "./entity"

const PAGINATOR_LINKS = ['first', 'last', 'next', 'prev', 'self']

export default Collection

function Collection(volcanoClient, data, entityClass) {
    entityClass = entityClass || Entity
    this.volcanoClient = volcanoClient
    this.total = data.total
    this.pageCount = data.page_count
    this.count = data.count
    this.currentPage = data.page
    this.items = Array.isArray(Object.values(data._embedded)[0])
        ? Object.values(data._embedded)[0].map(entity => new entityClass(entity))
        : [new entityClass(Object.values(data._embedded)[0])];

    this.aggregateFields = Object.values(data._aggregate)[0]
    this.resoureType = Object.keys(data._embedded)[0]

    this.paginator = []

    this.actions = []

    if (data._links) {
        Object.entries(data._links).forEach(link => {
            if (PAGINATOR_LINKS.includes(link[0])) {
                // paginator link
                this.paginator[link[0]] = link[1].href
            } else if (link[1].hasOwnProperty(this.resoureType)) {
                if (link[0] === "add") {
                    // add link
                    link[1][this.resoureType].scope = "add"
                }

                if (link[1][this.resoureType]?.scope !== undefined) {
                    // add action
                    this.actions[link[0]] = {
                        scope: link[1][this.resoureType].scope,
                        href: link[1][this.resoureType].href,
                        data: link[1][this.resoureType]?.data,
                    }
                }
            }
        })
    }

    /**
     * @return {number} total results in the collection
     */
    this.getTotal = () => {
        return this.total
    }

    /**
     * @return {number} total number of pages in the collection
     */
    this.getPageCount = () => {
        return this.pageCount
    }

    /**
     * @return {number} total number of results in the current page
     */
    this.getCount = () => {
        return this.count
    }

    /**
     * @return {number} current page index, starting in 1
     */
    this.getCurrentPage = () => {
        return this.currentPage
    }

    /**
     * @return {Array} the entities in the current page
     */
    this.getItems = () => {
        return this.items
    }

    /**
     * @return {object} the paginator for the current page
     */
    this.getPaginator = () => {
        return this.paginator
    }

    /**
     * @return {object} the aggregate fields for the current page
     */
    this.getAggregateFields = () => {
        return this.aggregateFields
    }

    /**
     * @return {string} the type of the entities of the collection
     */
    this.getResourceType = () => {
        return this.resoureType
    }

    this.paginate = (page) => {
        const endpoint = this.paginator.self.replace(/page=\d+/, 'page=' + page)

        const request = this.volcanoClient.prepareRequest(endpoint, {
            method: 'GET',
            isFull: true
        })

        return this.volcanoClient.makeRequest(request)
            .then(res => {
                return new Collection(this.volcanoClient, res)
            })
    }

    /**
     * Returns the actions of the collection.
     * 
     * @returns {array}
     */
    this.getActions = () => {
        return this.actions
    }

    /**
     * Checks if the collection has the action passed.
     * 
     * @param {string} action 
     * @param {string} operation Action's operation (optional)
     * 
     * @returns {boolean} true if the entity has the action, false otherwise.
     */
    this.hasAction = (action, allowedOperation = null) => {
        if (this.actions[action] === undefined) {
            return false
        }

        if (allowedOperation === null) {
            return true
        }

        if (this.actions[action].data && this.actions[action].data.allowed_operations) {
            return this.actions[action].data.allowed_operations.includes(allowedOperation)
        }

        return false
    }
}