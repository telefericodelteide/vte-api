import { isEmpty, replace } from "lodash";
import constants from '../constants';
import Entity from './entity/entity';
//import Rol from "./entity/rol";
export default RoleClient;


const ROL_GET_ROLES = 'auth/roles'
const ROL_GET_ROL_BY_ID = 'auth/roles/:id'
const ROL_GET_ROL_PERMISSIONS = 'auth/roles/:id/permissions'
const ROL_ADD = 'auth/roles/add'
const ROL_EDIT = 'auth/roles/:id/edit'

const prepareRoleData = (data) => {

  console.log("6")
  console.log (data)
  console.log("7")
  console.log(Boolean(data.is_system))
  console.log("8vv")

  if (data.is_system !== undefined) {
    data = {
        ...data,
        is_system: Boolean(data.is_system)
    }
  }

  if (data.corporate_account_available !== undefined) {
    data = {
        ...data,
        corporate_account_available: Boolean(data.corporate_account_available)
    }
  }
  
  console.log (data)
  console.log("9")

  return data

}



/**
 * Uses to access Volcano API role endpoints.
 * 
 * @constructor RoleClient
 * @param {VolcanoClient} volcanoClient 
 */
function RoleClient(volcanoClient) {
  this.volcanoClient = volcanoClient;
  
  this.getPermissions = (roleId) => {
    return this.volcanoClient.makeCollectionRequest(ROL_GET_ROL_PERMISSIONS, roleId);
  }

  this.getRoles = (params) => {
    return this.volcanoClient.makeCollectionRequest(ROL_GET_ROLES, params);
  }

  this.getRole = (roleId) => {
    return this.volcanoClient.getEntityRequest(ROL_GET_ROL_BY_ID, roleId);
  }

  /**
       * Create rol
       *
       * @param { object } data   This should have the following format.
       *                          {
       *                             "name": "rol_name",
       *                             "slug": "slug",
       *                             "is_system": true,
       *                             "corporate_account_available": false,
       *                             "permissions": [
       *                                 {"id": 1}
       *                             ],
       *                         }
       *
       * @return {Entity}
       */
  this.createRol = (data) => {

    data = prepareRoleData(data)

    return this.volcanoClient.makeResourceRequest(
        ROL_ADD,
        constants.HTTP_POST,
        data,
        {
          isAdmin: true
        },
        null,
        'role'
    )
  }

      /**
       * Edit rol
       *
       * @param {string}  rolId  Role identifier
       * @param { object } data   This should have the following format.
       *                          {
       *                             "name": "rol_name",
       *                             "slug": "slug",
       *                             "is_system": true,
       *                             "corporate_account_available": true,
       *                             "permissions": [
       *                                 {"id": 1}
       *                             ],
       *                        }
       * @return {Entity}
       */
      this.editRol = (rolId, data) => {
        if (isEmpty(rolId)) {
            return null
        }

        data = prepareRoleData(data)

        console.log("a!")
        console.log(data)
        console.log("b")
    
    
        return this.volcanoClient.makeEntityRequest(
            ROL_EDIT,
            rolId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: rolId,
                },
                isAdmin: true
              },
            null,
            'role'
        )
    }


}