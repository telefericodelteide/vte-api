import VolcanoApi from "../vte";
import Entity from "./entity/entity";
import constants from '../constants';
import { isEmpty, isObject } from 'lodash';

export default RefundClient;

// Endpoints
const REFUND_GET_REFUNDS = "refunds"
const REFUND_GET_REFUND = REFUND_GET_REFUNDS + '/:id'
const REFUND_ADD_COMMENT = REFUND_GET_REFUND + '/add-comment'
const REFUND_ASSIGN = REFUND_GET_REFUND + '/assign'
const REFUND_ACCEPT = REFUND_GET_REFUND + '/accept'
const REFUND_REJECT = REFUND_GET_REFUND + '/reject'
const REFUND_CANCEL_REFUND = REFUND_GET_REFUND + "/cancel"
const REFUND_COST_REFUND = REFUND_GET_REFUND + "/refund-cost"

export {
    REFUND_GET_REFUNDS,
    REFUND_GET_REFUND
};

/**
 * Container which has the booking refunds methods to interact with the API endpoints
 *
 * @param {VolcanoApi} volcanoClient
 */
function RefundClient(volcanoClient) {
    // This is under admin namespace.
    /**
     * @var {VolcanoApi}
     */
    this.volcanoClient = volcanoClient;

    /**
     * Return a collection with all Refunds with pagination.
     *
     * @param {array} [params]  Optional object which contains the parameters to build the
     *                          request, for instance, if you want to move to specific page,
     *                          you must add 'page: 3' to the params object.
     *
     * @returns {Collection}
     */
    this.getRefunds = (params) => {
        return this.volcanoClient.makeCollectionRequest(
            REFUND_GET_REFUNDS,
            params
        );
    };

    /**
     * Return an Entity containing a refund which match by Id `refundId`.
     *
     * @param {string} [refundId]  refund ID.
     *
     * @returns {Entity}
     */
    this.getRefund = (refundId, params) => {
        return this.volcanoClient.getEntityRequest(
            REFUND_GET_REFUND,
            refundId,
            true,
            params
        )
    }

    this.exportRefunds = (fields, params, progressHandler) => {
        return this.volcanoClient.makeCollectionExportRequest(
            REFUND_GET_REFUNDS,
            params,
            null,
            fields,
            'refunds',
            progressHandler
        )
    }

    /**
     * Add comment to refund
     *
     * @param {string} refundId Refund ID
     * @param {string} comment Comment text
     *
     * @returns {Entity}
     */
    this.addComment = (refundId, comment) => {
        return this.volcanoClient.makeEntityRequest(
            REFUND_ADD_COMMENT,
            refundId,
            constants.HTTP_POST,
            {
                comment: comment
            },
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Assign refund to user and return the Entity
     *
     * @param { string } refundId
     * @param { string } userId User to assign the refund (optional)
     *
     * @return { Entity }
     */
    this.assign = (refundId, userId = null) => {
        const data = (userId === null) ? {} : { user_id: userId }

        return this.volcanoClient.makeEntityRequest(
            REFUND_ASSIGN,
            refundId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Accept refund and return the Entity
     *
     * @param { string } refundId
     * @param { object } data   This should have the following format.
     *                          {
     *                            "cost": 5.05,
     *                            "manual_refund": true,
     *                            "comment": "Comment text..."
     *                          }
     *
     *
     * @return { Entity }
     */
    this.accept = (refundId, data) => {
        if (data === null) {
            return null
        }

        return this.volcanoClient.makeEntityRequest(
            REFUND_ACCEPT,
            refundId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Reject refund and return the Entity
     *
     * @param { string } refundId
     * @param { boolean } notifyClient True to notify client, false otherwise
     *
     * @return { Entity }
     */
    this.reject = (refundId, notifyClient = false) => {
        return this.volcanoClient.makeEntityRequest(
            REFUND_REJECT,
            refundId,
            constants.HTTP_POST,
            { notify_client: notifyClient },
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
            },
            null
        )
    }

    /**
     * Do a refund refund cancellation.
     *
     * @param {string}  refundId     Id of the refund which want to be cancelled.
     *
     * @return {Entity}
     */
    this.cancelRefund = (refundId, params) => {
        if (isEmpty(refundId)) {
            return null;
        }

        params = isObject(params) ? params : {}

        return this.volcanoClient.makeEntityRequest(
            REFUND_CANCEL_REFUND,
            refundId,
            constants.HTTP_POST,
            null,
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
                query: params
            },
            null
        );
    };

    /**
     * Creates new refund for the refund cost of the entity given. Returns the new Entity
     *
     * @param { string } refundId
     *
     * @return { Entity }
     */
    this.refundCost = (refundId) => {
        if (isEmpty(refundId)) {
            return null;
        }

        const data = {}

        return this.volcanoClient.makeEntityRequest(
            REFUND_COST_REFUND,
            refundId,
            constants.HTTP_POST,
            data,
            {
                path: {
                    id: refundId,
                },
                isAdmin: true,
            },
            null
        )
    }
}