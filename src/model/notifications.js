import { isEmpty, isNull } from "lodash";
import constants from '../constants';
import Entity from './entity/entity';
export default NotificationClient;

const ENDPOINT_GET_NOTIFICATIONS_SCHEMA = 'notification-templates/schema'

const NT_GET_TEMPLATES = 'notification-templates'
const NT_GET_TEMPLATE_BY_ID = 'notification-templates/:id'
const NT_EDIT_TEMPLATE = 'notification-templates/:id/edit'
const NT_COPY_TEMPLATE = 'notification-templates/:id/copy'
const NT_GET_PRODUCTS_TEMPLATE_BY_ID = 'notification-templates/:id/products'
const NT_DELETE_PRODUCT_TEMPLATE = 'notification-templates/:id/delete-product'
const NT_ADD_PRODUCTS_TEMPLATE = 'notification-templates/:id/add-products'
const NT_EDIT_RECIPIENT_TEMPLATE = 'notification-templates/:id/edit-recipient'

const N_GET_NOTIFICATIONS = 'notifications'
const N_GET_NOTIFICATION_BY_ID = N_GET_NOTIFICATIONS + '/:id'
const N_EDIT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/edit'
const N_ADD_PRODUCTS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/add-products'
const N_DELETE_PRODUCT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/delete-product'
const N_EDIT_RECIPIENT_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/edit-recipient'
const N_UPDATE_PRODUCTS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/update-products'
const N_PREPARE_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/prepare'
const N_SET_SEND_RECIPIENTS_NOTIFICATION = 'notifications/:id/set-send-recipients'
const N_SET_EDITION_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/set-edition'
const N_SET_DATE_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/set-date'
const N_SEND_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/send'
const N_CANCEL_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/cancel'
const N_PREVIEW_NOTIFICATION = N_GET_NOTIFICATIONS + '/:id/preview/:nr'
const N_DETAILS_NOTIFICATION = N_GET_NOTIFICATION_BY_ID + '/detail'

const N_GET_RECIPIENTS = N_GET_NOTIFICATION_BY_ID + '/notification-recipients'


/**
 * Uses to access Volcano API notification templates endpoints.
 * 
 * @constructor NotificationTemplatesClient
 * @param {VolcanoClient} volcanoClient 
 */
function NotificationClient(volcanoClient) {
  this.volcanoClient = volcanoClient;

  this.getNotificationTemplates = (params) => {
    return this.volcanoClient.makeCollectionQueryRequest(NT_GET_TEMPLATES, params);
  }

  this.getNotificationTemplate = (templateId) => {
    return this.volcanoClient.getEntityRequest(NT_GET_TEMPLATE_BY_ID, templateId, false);
  }

  /**
       * Create notification template
       *
       * @param { object } data   This should have the following format.
       *                          {
       *                             "name": "template",
       *                             "system_template": true,
       *                             "enterpriseId": 1,
       *                         }
       *
       * @return {Entity}
       */
  this.createNotificationTemplate = (data) => {

    return this.volcanoClient.makeResourceRequest(
      NT_GET_TEMPLATES,
      constants.HTTP_POST,
      data,
      {
        isAdmin: false
      },
      null,
      'template'
    )
  }

  /**
   * Edit template
   *
   * @param {string}  templateId  Template identifier
   * @param { object } data   This should have the following format.
   *                          {
   *                             "name": "template",
   *                             "system_template": true,
   *                             "enterpriseId": 1,
   *                         }
   * @return {Entity}
   */
  this.editNotificationTemplate = (templateId, data) => {
    if (isEmpty(templateId)) {
      return null
    }

    return this.volcanoClient.makeEntityRequest(
      NT_EDIT_TEMPLATE,
      templateId,
      constants.HTTP_POST,
      data,
      {
        path: {
          id: templateId,
        },
        isAdmin: false
      },
      null,
      'template'
    )
  }

  this.deleteNotificationTemplate = (templateId) => {
    if (isEmpty(templateId)) {
      return null
    }

    return this.volcanoClient.makeResourceRequest(
      NT_GET_TEMPLATE_BY_ID,
      constants.HTTP_DELETE,
      null,
      {
        path: {
          id: templateId
        },
        isAdmin: false
      }
    )
  }

  /**
   * Copy template
   *
   * @param {string}  templateId  Template identifier
   * @return {Entity}
   */
  this.copyNotificationTemplate = (templateId) => {
    if (isEmpty(templateId)) {
      return null
    }

    return this.volcanoClient.makeEntityRequest(
      NT_COPY_TEMPLATE,
      templateId,
      constants.HTTP_POST,
      [],
      {
        path: {
          id: templateId,
        },
        isAdmin: false
      },
      null,
      'template'
    )
  }

  /**
   * Returns the products realted with the notification template
   * 
   * @param {int} templateId 
   * @param {array} params 
   * @returns 
   */
  this.getNotificationTemplateProducts = (templateId, params) => {
    if (isEmpty(templateId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      NT_GET_PRODUCTS_TEMPLATE_BY_ID,
      {
        method: 'GET',
        isAdmin: false,
        query: {
          ...params
        },
        parameters: {
          id: templateId
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res.notification_template_products
    })
  }

  /**
   * Deletes the relation between the notification template and the product
   * 
   * @param {int} templateId 
   * @param {int} productId 
   * @returns 
   */
  this.deleteProductFromNotificationTemplate = (templateId, productId) => {

    if (isEmpty(templateId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      NT_DELETE_PRODUCT_TEMPLATE,
      {
        method: constants.HTTP_DELETE,
        isAdmin: false,
        parameters: {
          id: templateId,
        },
        data: {
          productId: productId
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  /**
   * Add the relation among the notification template and the products in data
   * 
   * @param {int} templateId 
   * @param {array} data 
   * @returns 
   */
  this.addProductToNotificationTemplate = (templateId, data) => {
    if (isEmpty(templateId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      NT_ADD_PRODUCTS_TEMPLATE,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: templateId,
        },
        data: {
          products: data
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.modifyRecipientToNotificationTemplate = (templateId, recipientType, data) => {
    if (isEmpty(templateId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      NT_EDIT_RECIPIENT_TEMPLATE,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: templateId,
        },
        data: {
          recipient_type: recipientType,
          template: data
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.getLegend = () => {

    const request = this.volcanoClient.prepareRequest(ENDPOINT_GET_NOTIFICATIONS_SCHEMA, {
      method: constants.HTTP_GET,
      isAdmin: false
    })

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res['legend_send_method']['options']
    })

  }

  /**
   * Returns the notifications
   * 
   * @param {array} params 
   * @returns 
   */
  this.getNotifications = (params) => {
    return this.volcanoClient.makeCollectionQueryRequest(N_GET_NOTIFICATIONS, params);
  }

  /**
   * Get the notification
   * 
   * @param {int} notificationId 
   * @returns 
   */
  this.getNotification = (notificationId) => {
    return this.volcanoClient.getEntityRequest(N_GET_NOTIFICATION_BY_ID, notificationId, false);
  }

  /**
 * Edit notification
 *
 * @param {string}  notificationId  Template identifier
 * @param { object } data   This should have the following format.
 *                          {
 *                             "name": "notification",
 *                             "enterpriseId": 1,
 *                         }
 * @return {Entity}
 */
  this.editNotification = (notificationId, data) => {
    if (isEmpty(notificationId)) {
      return null
    }

    return this.volcanoClient.makeEntityRequest(
      N_EDIT_NOTIFICATION,
      notificationId,
      constants.HTTP_POST,
      data,
      {
        path: {
          id: notificationId,
        },
        isAdmin: false
      },
      null,
      'notification'
    )
  }

  /**
   * 
   * @param { string } notificationId 
   * @returns 
   */
  this.deleteNotification = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    return this.volcanoClient.makeResourceRequest(
      N_GET_NOTIFICATION_BY_ID,
      constants.HTTP_DELETE,
      null,
      {
        path: {
          id: notificationId
        },
        isAdmin: false
      }
    )
  }


  /**
   * Deletes the relation between the notification and the product
   * 
   * @param {int} notificationId 
   * @param {int} productId 
   * @returns 
   */
  this.deleteProductFromNotification = (notificationId, productId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_DELETE_PRODUCT_NOTIFICATION,
      {
        method: constants.HTTP_DELETE,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data: {
          productId: productId
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  /**
   * Add the relation among the notification and the products in data
   * 
   * @param {int} notificationId 
   * @param {array} data 
   * @returns 
   */
  this.addProductToNotification = (notificationId, data) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_ADD_PRODUCTS_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data: {
          products: data
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.updateProductsNotification = (notificationId, data) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_UPDATE_PRODUCTS_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data: {
          products: data.products
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res.notification
    })


  }

  this.createNotification = (data) => {

    return this.volcanoClient.makeResourceRequest(
      N_GET_NOTIFICATIONS,
      constants.HTTP_POST,
      data,
      {
        isAdmin: false
      },
      null,
      'notification'
    )
  }

  this.createNotificationFromTemplate = (templateId) => {

    const data = {
      template_id: templateId
    }

    const request = this.volcanoClient.prepareRequest(
      N_GET_NOTIFICATIONS,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        data: data
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res.notification
    })

  }

  this.modifyRecipientToNotification = (notificationId, recipientType, data) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_EDIT_RECIPIENT_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data: {
          recipient_type: recipientType,
          template: data
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.prepareNotification = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_PREPARE_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.markSendRecipients = (notificationId, elements, type = null) => {
    if (isEmpty(notificationId) || elements == null) {
      return null
    }

    const recipients = elements.map((element) => element.id)

    const request = this.volcanoClient.prepareRequest(
      N_SET_SEND_RECIPIENTS_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data: {
          type: type,
          recipients: recipients
        }
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res.notification
    })

  }

  this.setEditNotification = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_SET_EDITION_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.setDateNotification = (notificationId, data) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_SET_DATE_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
        data
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.sendNotification = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_SEND_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.cancelNotification = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_CANCEL_NOTIFICATION,
      {
        method: constants.HTTP_POST,
        isAdmin: false,
        parameters: {
          id: notificationId,
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.getPreview = (notificationId, notificationRecipientId) => {
    if (isEmpty(notificationId) || isEmpty(notificationRecipientId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_PREVIEW_NOTIFICATION,
      {
        method: constants.HTTP_GET,
        isAdmin: false,
        parameters: {
          id: notificationId,
          nr: notificationRecipientId
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      return res
    })

  }

  this.getDetails = (notificationId) => {
    if (isEmpty(notificationId)) {
      return null
    }

    const request = this.volcanoClient.prepareRequest(
      N_DETAILS_NOTIFICATION,
      {
        method: constants.HTTP_GET,
        isAdmin: false,
        parameters: {
          id: notificationId
        },
      }
    )

    return this.volcanoClient.makeRequest(request).then((res) => {
      if (res.notification && res.notification.details){
        return res.notification.details
      } else {
        return 
      }
    })

  }

  this.getRecipients = (notificationId, params) => {
    if (params == null) {
      return null
    }

    const result = this.volcanoClient.makeCollectionQueryRequest(
      N_GET_RECIPIENTS,
      params,
      {
        parameters: {
          id: notificationId
        }
      }
    )
    
    return result

  }

}

