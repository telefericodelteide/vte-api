import { USER_APP_RESET_PASSWORD } from './model/user';

const DEFAULT_PAGE_NUMBER = 1;
const DEFAULT_ELEMENTS_PER_PAGE = 20;

const HTTP_GET = 'GET';
const HTTP_POST = 'POST';
const HTTP_PUT = 'PUT';
const HTTP_PATCH = 'PATCH';
const HTTP_DELETE = 'DELETE';
const HTTP_OPTIONS = 'OPTIONS';

const HTTP_PROTOCOL = "http";
const HTTPS_PROTOCOL = "https";

const STRICT_SSL = "strictSSL";

const API_KEY_LABEL = "api-key";
const X_API_KEY_LABEL = "x-api-key";

const SITE_LABEL = "site";

const APPLICATION_JSON = "application/json";

const BEARER_LABEL = "Bearer";
const AUTHORIZATION = "Authorization";

const ADMIN_URL_PREFIX = 'admin';
const TOKEN_LABEL = "token";
const TOKEN_RENEW = "refresh/:token";
const RESET_PASSWORD_REQUEST = 'auth/application-users/reset-password-request';
const RESET_PASSWORD = 'auth/application-users/reset-password'

export default {
  DEFAULT_PAGE_NUMBER,
  DEFAULT_ELEMENTS_PER_PAGE,
  HTTP_GET,
  HTTP_POST,
  HTTP_PUT,
  HTTP_PATCH,
  HTTP_DELETE,
  HTTP_OPTIONS,
  HTTP_PROTOCOL,
  HTTPS_PROTOCOL,
  STRICT_SSL,
  API_KEY_LABEL,
  X_API_KEY_LABEL,
  SITE_LABEL,
  APPLICATION_JSON,
  BEARER_LABEL,
  AUTHORIZATION,
  ADMIN_URL_PREFIX,
  TOKEN_LABEL,
  TOKEN_RENEW,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD
};