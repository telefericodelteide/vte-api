import Errors from "../model/errors";
import xlsx from "xlsx";
import _get from "lodash/get";
import _isFunction from "lodash/isFunction";

export const EXCEL_FORMAT = "excel";

/**
 * Class to manage the data exportation to several formats and from
 * several sources.
 *
 * @author Cayetano H. Osma <chernandez@volcanoteide.com>
 * @version Oct 2020
 *
 * @param {array} header
 * @param {string} type
 * @param {string} filename
 *
 */
class DataExport {
  /**
   * constructor
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @param {object[]} header     Fields definition.
   * @param {string} type         Output format.
   * @param {string} filename     Filename to be exported.
   */
  constructor(header, type, filename) {
    /**
     * array containing the allowed types.
     *
     * @type {string[]}
     */
    this.allowedTypes = [EXCEL_FORMAT];

    if (!header.length) {
      throw new Errors.ObjectIsEmpty("header");
    }

    /**
     * Array of object with fields definition.
     *
     * key:       Mandatory field name into data from database.
     * name:      Mandatory column name into the new results.
     * callback:  Optionally a callback to return the complex data or data in a deep level (> 1).
     *
     * @type {object[]}
     */
    this.fields = header;

    if (!type.length && !this.allowedTypes.includes(type)) {
      throw new Errors.ParameterMissing("type");
    }

    /**
     * Which kind of exportation we should do.
     *
     * @type {string}
     * @see Constants at the begin of the file
     */
    this.type = type;

    if (!filename.length) {
      throw new Errors.ParameterMissing("filename");
    }

    /**
     * filename where the data should be persisted.
     *
     * @type {string}
     */
    this.filename = filename;

    /**
     * Resulting object where the data will be stored.
     *
     * @type {object[]}
     */
    this.result = [];

    this.convertedData = [];

    this.workBook = undefined;
  }

  /**
   * Return the filename
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   * 
   * @returns {string}
   */
  getFilename() {
    return this.filename;
  }

  /**
   * Return the type
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @returns {string}
   */
  getType() {
    return this.type;
  }

  /**
   * Return the result
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @returns {object}
   */
  getResult() {
    return this.result;
  }

  /**
   * Returns an array of objects with the items ready to export.
   *
   * @author Cayetano H. Osma <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @param {object[]} data   Data to be added
   */
  addData(data) {
    data.forEach((item) =>
      this.result.push(
        this.fields.reduce((rowData, field) => {
          return {
            ...rowData,
            [field.name]: _isFunction(field.callback)
              ? field.callback(item)
              : _get(item, field.key, ""),
          };
        }, {})
      )
    );
  }

  /**
   * Format the data as parameter type indicate.
   *
   * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   * @params {string} format  Format to be converted.
   */
  convertTo(format) {
    this.format = format || this.getType();

    switch (this.type) {
      case EXCEL_FORMAT:
        this.doExcelExport();
        break;
      default:
        this.doExcelExport();
    }
  }

  /**
   * Do the export to excel format (CSV)
   *
   * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
   * @version Oct 2020
   */
  doExcelExport() {
    this.convertToExcel();
    /* create a new blank workbook */
    this.workBook = xlsx.utils.book_new();
    let workSheet = xlsx.utils.aoa_to_sheet(this.convertedData, {
      dateNF: "dd/MM/yyyy h:mm"
    });
    xlsx.utils.sheet_to_csv(workSheet);
    xlsx.utils.book_append_sheet(this.workBook, workSheet, "Export Data");
  }

  /**
   * Transform the result array to the specific format to be converted.
   * The result will be stored in convertedData array
   *
   * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
   * @version Oct 2020
   *
   */
  convertToExcel() {
    this.convertedData.push(
      this.fields
        .filter((field) => {
          return !field.hasOwnProperty("found") || field.found !== false;
        })
        .map((field) => {
          return field.name;
        })
    );

    // this.convertedData.push(this.fields.map((field) => {
    //   if (!field.hasOwnProperty('found') || field.found !== false) {
    //     return field.name;
    //   }
    // }));
    this.result.forEach((data) => {
      this.convertedData.push(Object.values(data));
    });
  }

  /**
   * Download the file to the browser.
   *
   * @author Cayetano H. Osma. <chernandez@volcanoteide.com>
   * @version Oct 2020
   */
  download() {
    let destFile = this.filename + "_" + Date.now();

    switch (this.getType()) {
      case EXCEL_FORMAT:
        destFile = destFile + ".xlsx";
        break;
      default:
        destFile = destFile + ".csv";
    }

    return xlsx.writeFile(this.workBook, destFile);
  }
}

export default {
  EXCEL_FORMAT,
  DataExport,
};
