import VolcanoApi from "../src/vte";
import devOptions from "./dev_options";
import url from "url";

/**
 * Return the options to construct the VolcanoAPI
 *
 * @param {object|undefined} options
 */
function getVolcanoOptions(options) {
  const defaultOptions = options
    ? { ...devOptions.actualOptions, ...options }
    : { ...devOptions.actualOptions };

  return {
    ...defaultOptions,
    ...{
      site_key: defaultOptions.site_key || "test",
    },
  };
}

function getClientToRequest(options) {
  //const client = new VolcanoApi(getVolcanoOptions(options));
  const client = new VolcanoApi({
    protocol: 'https',
    host: 'api.staging.volcanoteide.com',
    timeout: 10000,
    locale: 'es'
  });

  return client.authenticate(devOptions.USERNAME, devOptions.PASSWORD)
  .then(data => {
    client.setToken(data.token);

    return client;
  });
}

function getSiteClientToRequest(options) {
  options = {
    protocol: 'https',
    host: 'api.volcanoteide.com',
    port: 443,
    strictSSL: true,
    site_key: '--',
  }
  const client = new VolcanoApi(getVolcanoOptions(options));
  
  return client;
}

/**
 * Return a valid Volcano URL with param as end of it.
 * 
 * @param {string} path 
 */
function getVolcanoUri(path) {
  if (path === undefined || !path.length) {
    path = '';
  }

  const volcanoUrl = url.format({
      protocol: devOptions.DEFAULT_API_PROTOCOL,
      hostname: devOptions.DEFAULT_API_HOST,
      port: devOptions.DEFAULT_API_PORT,
      pathname: path
    });

  return encodeURI(volcanoUrl);
}

/**
 * Return the modked token.
 */
function getMockedToken() {
  return devOptions.TOKEN
}

// Default exports
export default {
  getVolcanoOptions,
  getClientToRequest,
  getSiteClientToRequest,
  getMockedToken,
  getVolcanoUri
};