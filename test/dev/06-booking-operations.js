import {expect} from "chai";
import testUtils from "../test_utils";
import devOptions from "../dev_options";

// const BOOKING_ID = 1338506;
const BOOKING_ID = 1338505;
const BOOKING_DATE_TO_BE_CHANGED = '2020-11-05 09:00:00'

describe("Booking Operations", () => {

    it("Should change the reservation date to booking " + BOOKING_ID, () => {
        return testUtils
            .getClientToRequest({
                username: devOptions.USERNAME,
                password: devOptions.PASSWORD,
            })
            .then((client) => {
                return client.booking.changeBookingDate(
                    BOOKING_ID,
                    new Date(BOOKING_DATE_TO_BE_CHANGED)
                ).then((result) => {
                    expect(result.booking_date).equal(BOOKING_DATE_TO_BE_CHANGED);
                });
            });
    });

    it("Should request the cancellation of a booking " + BOOKING_ID, () => {
        return testUtils
            .getClientToRequest({
                username: devOptions.USERNAME,
                password: devOptions.PASSWORD,
            })
            .then((client) => {
                return client.booking.cancelBooking(
                    BOOKING_ID
                ).then((result) => {
                    expect(result.id).equal(BOOKING_ID);
                });
            });
    });

    it("Should send the emails regarding with the booking " + BOOKING_ID, () => {
        return testUtils
            .getClientToRequest({
                username: devOptions.USERNAME,
                password: devOptions.PASSWORD,
            })
            .then((client) => {
                return client.booking.sendBookingEmail(
                    BOOKING_ID
                ).then((result) => {
                    expect(result.id).equal(BOOKING_ID);
                });
            });
    });

    it("Should request the link for PDF document for booking " + BOOKING_ID, () => {
        return testUtils
          .getClientToRequest({
              username: devOptions.USERNAME,
              password: devOptions.PASSWORD,
          })
          .then((client) => {
              return client.booking.getLinkPDFBooking(
                BOOKING_ID
              ).then((result) => {
                  expect(result).to.have.any.keys('pdf-link');
              });
          });
    });

    it("Should request the PDF document itself (application/PDF) for booking " + BOOKING_ID, () => {
        return testUtils
          .getClientToRequest({
              username: devOptions.USERNAME,
              password: devOptions.PASSWORD,
          })
          .then((client) => {
              return client.booking.getPDFBooking(
                BOOKING_ID
              ).then((result) => {
                  console.log(result);
                  // expect(result).to.have.any.keys('pdf-link');
              });
          });
    });

}); // Booking tests
