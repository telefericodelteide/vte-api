import { expect } from "chai";
import { parseISO } from "date-fns";
import testUtils from "../test_utils";

const client = testUtils.getSiteClientToRequest();

describe("Route tests", () => {
  
  describe("Pickup info", () => {
    const date = parseISO('2021-06-23');

    it("should return a route for a product and date", () => {
      return client.route.getPickupData(1869, date).then((route) => {
        expect(route.id).to.be.not.empty;
      });
    });

    it("should return null", () => {
      return client.route.getPickupData(1, date).then((route) => {
        expect(route).to.be.null;
      });
    });
  });
});
