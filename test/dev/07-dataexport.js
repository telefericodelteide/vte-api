import { expect } from "chai";
import testUtils from "../test_utils";
import devOptions from "../dev_options";
import { BOOKING_GET_BOOKINGS } from "../../src/model/booking";

// const BOOKING_ID = 1338506;
const BOOKING_ID = 1338505;
const BOOKING_DATE_TO_BE_CHANGED = "2020-11-05 09:00:00";

describe("Export processes", () => {
  it("Testing the export process... ", () => {
    return testUtils
      .getClientToRequest({
        username: devOptions.USERNAME,
        password: devOptions.PASSWORD,
      })
      .then((client) => {
        client.booking.exportBookings(
          [
            { key: "id", name: "Identificador" },
            { key: "locator", name: "Localizador" },
            { key: "total", name: "Importe total" },
            {
              key: "product",
              name: "Nombre de producto",
              callback: (data) => {
                return data.product.id;
              },
            },
            { key: "state", name: "State" },
          ],
          { pageLimit: 3 }
        );
      });
  });
}); // Booking tests
