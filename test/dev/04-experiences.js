import { expect } from "chai";
import testUtils from "../test_utils";
import devOptions from "../dev_options";

const TEST_EXPERIENCE_ID = 48;
const TEST_PRODUCT_ID = 1927;
const TEST_DATE = new Date('2021-04-01');

describe("Experience tests", () => {
  
  describe("Experiences content", () => {

    /*it("should return an experience collection", () => {
      const client = testUtils.getSiteClientToRequest();
      
      return client.experience.geExperiences()
      .then((collection) => {
        expect(collection.getCount()).to.be.above(0);
      });
    });*/

    it("should return an experience", () => {
      const client = testUtils.getSiteClientToRequest();
      
      return client.experience.getExperience(TEST_EXPERIENCE_ID)
      .then((experience) => {
        expect(experience.id).to.eql(TEST_EXPERIENCE_ID);
      });
    });

    it("should return a 404", () => {
      const client = testUtils.getSiteClientToRequest();

      return client.experience.getExperience(-1).catch((error) => {
        expect(error.message).is.equal("Not found");
      });
    });
  });

  describe("Product", () => {

    it("should return the product availability", () => {
      const client = testUtils.getSiteClientToRequest();
      
      return client.experience.getProductAvailability(TEST_PRODUCT_ID, TEST_DATE, true)
      .then((availability) => {
        expect(availability).to.be.an('array');
      });
    });

    it("should return the product rates", () => {
      const client = testUtils.getSiteClientToRequest();
      
      return client.experience.getProductRates(TEST_PRODUCT_ID, TEST_DATE)
      .then((rates) => {
        expect(rates).to.be.an('array');
      });
    });

    it("should return the product availability range", () => {
      const client = testUtils.getSiteClientToRequest();
      
      return client.experience.getProductAvailabilityRange(TEST_PRODUCT_ID, TEST_DATE, 2)
      .then((availability) => {
        console.log(availability);
        expect(availability).to.be.an('array');
      });
    });
  });
});
