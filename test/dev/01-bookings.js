import { expect } from "chai";
import testUtils from "../test_utils";
import devOptions from "../dev_options";

describe("Booking tests", () => {
  describe("Retrieving bookings", () => {
    it("Should return a bookings collection", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking.getBookings().then((collection) => {
            expect(collection.getCount()).to.be.above(0);
          });
        });
    });

    it("Should return a 3 bookings collection", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .getBookings({ page: 3, limit: 3 })
            .then((collection) => {
              expect(collection.getCount()).to.be.equals(3);
              expect(collection.getCurrentPage()).to.be.equals(3);
            });
        });
    });

    it("Should return page number 3 with a 4 bookings collection", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .getBookings({ page: 3, limit: 4 })
            .then((collection) => {
              expect(collection.getCurrentPage()).to.be.equals(3);
              expect(collection.getCount()).to.be.equals(4);
            });
        });
    });

    it("Should return only a booking with specific ID", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking.getBooking(1338313).then((booking) => {
            expect(booking.id).to.eql(1338313);
          });
        });
    });

    it("Should return a 404 due to the booking is not found", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .catch((client) => {
          return client.booking
            .getBooking(devOptions.NOT_FOUND_RECORD_ID)
            .catch((error) => {
              expect(error.code).is.equal(404);
            });
        });
    });
  }); // Retrieving booking tests

  describe("Updating booking", () => {
    it("Should update customer name", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer(1288706, {
              name: "Bob Parr",
            })
            .then((booking) => {
              expect(booking.order.customer.name).is.equal("Bob Parr");
            });
        });
    });

    it("Should update customer name and email", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer(1288706, {
              name: "Bob Parr",
              email: "info@bobparr.com",
            })
            .then((booking) => {
              expect(booking.order.customer.name).is.equal("Bob Parr");
              expect(booking.order.customer.email).is.equal("info@bobparr.com");
            });
        });
    });

    it("Must return an error due the bookingID is not a number", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer("1288706", {
              name: "Bob Parr",
              email: "info@bobparr.com",
            })
            .catch((error) => {
              expect(error.code).is.equal(404);
            });
        });
    });

    it("Must return an error due the bookingID is undefined", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer(undefined, {
              name: "Bob Parr",
              email: "info@bobparr.com",
            })
            .catch((error) => {
              expect(error.code).is.equal(404);
            });
        });
    });

    it("Must return an error due the data to be changed is undefined", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer(1288706, undefined)
            .catch((error) => {
              expect(error.code).is.equal(500);
            });
        });
    });

    it("Must return an error due the data not have the appropriate fields", () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingCustomer(1288706, {
              cayetano: "cayetano",
            })
            .catch((error) => {
              expect(error.code).is.equal(404);
            });
        });
    });

    it("Should change the booking date of given booking.\n\tNOTE: Remember to make this test to work correctly, You must update the booking 1338322 and change manually the reservation_date to 2020-10-11 14:00:00", () => {
      const BOOKING_ID = 1338322;
      const NEW_BOOKING_DATE = "2020-10-11 14:00:00";

      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingDate(1338322, NEW_BOOKING_DATE)
            .then((booking) => {
              expect(booking.booking_date).is.equal(NEW_BOOKING_DATE);
            });
        });
    });

    it("Should not able to change the booking date of the given booking due to it has no availability on that date.", () => {
      const BOOKING_ID = 1338322;
      const NEW_BOOKING_DATE = "2020-10-12 14:00:00";

      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.booking
            .changeBookingDate(1338322, NEW_BOOKING_DATE)
            .catch((error) => {
              expect(error.message).is.equals(
                "There is no availability for the product (903) for date (2020-10-11 14:00:00)"
              );
              expect(error.code).is.equals(500);
            });
        });
    });

  }); // Updating booking tests

}); // Booking tests
