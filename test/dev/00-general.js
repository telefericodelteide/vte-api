import VolcanoApi from "../../src/vte";
import { expect, assert } from "chai";
import devOptions from "../dev_options";
import testUtils from "../test_utils";

describe("General test", () => {
  describe("Base methods", () => {
    it("Constructor functions properly", () => {
      const client = new VolcanoApi(testUtils.getVolcanoOptions());

      expect(client.protocol).to.eql(devOptions.DEFAULT_API_PROTOCOL);
      expect(client.host).to.eql(devOptions.DEFAULT_API_HOST);
      expect(client.port).to.eql(devOptions.DEFAULT_API_PORT);
      expect(client.baseOptions.auth.type).to.eql(
        devOptions.DEFAULT_API_AUTH_TYPE
      );
      expect(client.baseOptions.auth.headerName).to.eql(
        devOptions.DEFAULT_API_HEADER_NAME
      );
      expect(client.baseOptions.auth.token).to.eql(
        devOptions.DEFAULT_API_TOKEN
      );
    });
  });

  describe("Request format tests", () => {
    it("URI built properly", () => {
      const client = new VolcanoApi(testUtils.getVolcanoOptions());
      const uri = client.makeUri("test");

      expect(uri).to.eql(testUtils.getVolcanoUri("test"));
    });

    it("Request options built properly", () => {
      const client = new VolcanoApi(testUtils.getVolcanoOptions());
      const request = client.prepareRequest("test", {
        method: "POST",
        data: {
          test: "test",
        },
      });

      expect(request.url).to.eql(testUtils.getVolcanoUri("test"));

      expect(request.method).to.eql("POST");
      expect(request.timeout).to.eql(devOptions.DEFAULT_API_TIMEOUT);
      expect(request.data.test).to.eql("test");
    });
  }); // End Request format tests

  describe("Security tests", () => {
    describe("Authentication tests", () => {
      it("should return a valid token for a valid username and password", () => {
        const options = testUtils.getVolcanoOptions({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        });

        const client = new VolcanoApi(options);
        const data = client.authenticate()
        .then(() => {
           const token = client.getToken();
           expect(token).to.be.not.empty;
        });

      });

      it("should return an error for an invalid username and password", () => {
        const client = new VolcanoApi(
          testUtils.getVolcanoOptions({
            username: devOptions.USERNAME_NOT_VALID,
            password: devOptions.PASSWORD_NOT_VALID
          })
        );
        return client.authenticate()
          .catch((error) => {
            expect(error.type).to.be.eql.toString("UNDEFINED");
          });
      });
    }); // Authentication tests
  }); // End Security tests
});
