import { expect } from "chai";
import testUtils from "../test_utils";
import devOptions from "../dev_options";

const TESTING_ROLE_ID = 6;
const TESTING_PERMISSION_ID = 12;

describe("Roles tests", () => {
  
  describe("Listing roles", () => {
    it("should return a roles collection", () => {
      const client = testUtils.getClientToRequest();

      return client.role.getRoles().then((collection) => {
        expect(collection.getCount()).to.be.above(0);
      });
    });

    it("should return a role", () => {
      const client = testUtils.getClientToRequest();

      return client.role.getRole(TESTING_ROLE_ID).then((role) => {
        expect(role.id).to.eql(TESTING_ROLE_ID);
      });
    });

    it("should return a 404", () => {
      const client = testUtils.getClientToRequest();

      return client.role.getRole(devOptions.NOT_FOUND_RECORD_ID).catch((error) => {
        expect(error.message).is.equal("Not found");
      });
    });
  });

  describe("Listing Permissions", () => {
    it("should return a permissions collection", () => {
      const client = testUtils.getClientToRequest();

      return client.role.getPermissions().then((collection) => {
        expect(collection.getCount()).to.be.above(0);
      });
    });

    it("should return a 404", () => {
      const client = testUtils.getClientToRequest();

      return client.role.getPermissions(devOptions.NOT_FOUND_RECORD_ID).catch((error) => {
        expect(error.message).is.equal("Not found");
      });
    });
  });

});
