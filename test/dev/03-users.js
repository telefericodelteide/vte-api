import { expect } from 'chai';
import testUtils from '../test_utils';
import devOptions from '../dev_options';

const TESTING_USER_ID = '02ce85db-aac9-11ec-a04c-0cc47ac3a6b8';
const TESTING_USER_USERNAME = 'MARGAVIAJES';


describe('Users tests', () => {

  describe('Listing users', () => {

    it('should return a user collection', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.user.getApplicationUsers()
            .then((collection) => {
              expect(collection.getCount()).to.be.above(0);
            });
        });
    });

    it('should return a user profile', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          const localUser = client.user.getProfile(TESTING_USER_ID);

          return localUser
            .then((user) => {
              expect(user.id).to.eql(TESTING_USER_ID);
            })
        });
    });

    // it('should return an user', () => {
    //   const client = testUtils.getClientToRequest();

    //   return client.role.getRole(TESTING_ROLE_ID).then((role) => {
    //     expect(role.id).to.eql(TESTING_ROLE_ID);
    //   });
    // });

    // it('should return a 404', () => {
    //   const client = testUtils.getClientToRequest();

    //   return client.role.getRole(devOptions.NOT_FOUND_RECORD_ID).catch((error) => {
    //     expect(error.message).is.equal('Not found');
    //   });
    // });
  });

  // describe('Listing Permissions', () => {
  //   it('should return a permissions collection', () => {
  //     const client = testUtils.getClientToRequest();

  //     return client.role.getPermissions().then((collection) => {
  //       expect(collection.getCount()).to.be.above(0);
  //     });
  //   });

  //   it('should return a 404', () => {
  //     const client = testUtils.getClientToRequest();

  //     return client.role.getPermissions(devOptions.NOT_FOUND_RECORD_ID).catch((error) => {
  //       expect(error.message).is.equal('Not found');
  //     });
  //   });
  // });

  describe('Managing users', () => {
    // it('should add an application user', () => {
    //   return testUtils
    //     .getClientToRequest({
    //       username: devOptions.USERNAME,
    //       password: devOptions.PASSWORD,
    //     })
    //     .then((client) => {
    //       return client.user.createAppUser({
    //         'username': 'vteapi.test@prueba.com',
    //         'password': 'test',
    //         'password_repeat': 'test',
    //         'name': 'Vendedor de prueba',
    //         'surname': '',
    //         'email': 'vteapi.test@prueba.com',
    //         'phone': '922333333',
    //         'active': 1,
    //         'collaborator_id': '393767',
    //         'salesman': {
    //           'office_id': '',
    //           'id_card': '00000000T'
    //         },
    //         'roles': [
    //           {
    //             'id': 8
    //           }
    //         ]
    //       }).then((user) => {
    //         expect(user.id).to.exist;
    //         expect(user.salesman.id).to.exist;
    //       })
    //     });
    // });

    it('should change an application user name', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.user.editAppUser(TESTING_USER_ID, {
            'name': 'Nuevo nombre',
          }).then((user) => {
            expect(user.id).to.exist;
            expect(user.first_name).to.eq('Nuevo nombre');
          })
        });
    });

    it('should set an application user as active', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.user.setAppUserActive(TESTING_USER_ID, true)
            .then((user) => {
              expect(user.active).to.eq(true);
            })
        });
    });

    it('should change user password', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.user.appUserChangePassword(TESTING_USER_ID, {
            password: "test",
            password_repeat: "test"
          }).then((user) => {
            expect(user.id).to.exist;
          })
        });
    });

    it('should make a reset password request', () => {
      return testUtils
        .getClientToRequest({
          username: devOptions.USERNAME,
          password: devOptions.PASSWORD,
        })
        .then((client) => {
          return client.user.appUserResetPasswordRequest(TESTING_USER_ID, TESTING_USER_USERNAME)
            .then((result) => {
              expect(result).to.eq(null);
            })
        });
    });

    // it('should delete an user', () => {
    //   return testUtils
    //     .getClientToRequest({
    //       username: devOptions.USERNAME,
    //       password: devOptions.PASSWORD,
    //     })
    //     .then((client) => {
    //       return client.user.deleteAppUser(TESTING_USER_ID)
    //         .then((result) => {
    //           expect(result).to.eq(null);
    //         })
    //     });
    // });
  });

});
