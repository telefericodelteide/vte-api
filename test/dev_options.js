const NOT_FOUND_RECORD_ID = 0;

const DEFAULT_API_PROTOCOL = 'https';
const DEFAULT_API_SEQURED_PROTOCOL = 'https';
const DEFAULT_API_HOST = 'api.staging.volcanoteide.com';
const DEFAULT_API_PORT = '80';
const DEFAULT_API_TIMEOUT = 10000;
const DEFAULT_API_LOCALE = 'es';
const DEFAULT_API_SSL = true;
const DEFAULT_API_AUTH_TYPE = 'api-key';
const DEFAULT_API_HEADER_NAME = 'x-api-key';

const TOKEN = '***';
const DEFAULT_API_TOKEN = '***';

const USERNAME = 'test_masca_1';
const PASSWORD = 'test1';

const USERNAME_NOT_VALID = 'unknown';
const PASSWORD_NOT_VALID = 'unknown';

const API_KEY = '********************************';
const API_SECRET = '********************************';

const SITE_KEY = '***';

const actualOptions = {
  protocol: DEFAULT_API_PROTOCOL,
  host: DEFAULT_API_HOST,
  port: DEFAULT_API_PORT,
  timeout: DEFAULT_API_TIMEOUT,
  locale: DEFAULT_API_LOCALE,
  //strictSSL: DEFAULT_API_SSL // ,
};

export default {
  actualOptions, 
  NOT_FOUND_RECORD_ID,
  DEFAULT_API_PROTOCOL, 
  DEFAULT_API_HOST, 
  DEFAULT_API_PORT, 
  DEFAULT_API_TIMEOUT, 
  DEFAULT_API_LOCALE, 
  DEFAULT_API_SSL, 
  DEFAULT_API_AUTH_TYPE, 
  DEFAULT_API_HEADER_NAME, 
  DEFAULT_API_TOKEN, 
  USERNAME,
  PASSWORD,
  USERNAME_NOT_VALID,
  PASSWORD_NOT_VALID,
  TOKEN,
  SITE_KEY
};